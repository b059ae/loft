<?php

namespace app\widgets;

use app\models\EmailForm;
use yii\base\Widget;


class Subscribe extends Widget
{
    public function run(){
        $emailForm = new EmailForm();
        return $this->render('_subscribe',[
            'emailForm' => $emailForm
        ]);
    }
}