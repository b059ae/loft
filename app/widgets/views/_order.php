<?php
/** @var $model \app\models\CallbackForm */
/** @var $id integer */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="hidden">
    <div class="window" id="popup-order">
        <div class="window__main">
            <button class="window__close js-window-close button-close"></button>
            <div class="window__content">
                <p class="window__title">Оставить заявку</p>
                <?php $form = ActiveForm::begin(['action'=>'/order']);?>
                <div class="window-form">
                    <?= $form->field($model, 'id')
                        ->hiddenInput([
                            'value' => $id
                        ])->label(false); ?>
                    <?= $form->field($model, 'name')
                        ->textInput([
                            'placeholder' => 'Ваше имя'
                        ])->label(false); ?>
                    <?= $form->field($model, 'phone')
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7 (999) 999 99 99',
                        ])->textInput([
                            'placeholder' => 'Ваш номер телефона'
                        ])->label(false); ?>
                    <?= Html::submitButton('Отправить', ['class' => 'window-form__button button-1','onclick' => "metrikaReachGoal('order-send')"]) ?>
                </div>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>