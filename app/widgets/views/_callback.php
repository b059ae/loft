<?php
/** @var $model \app\models\CallbackForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="hidden">
    <div class="window" id="popup-callback">
        <div class="window__main">
            <button class="window__close js-window-close button-close"></button>
            <div class="window__content">
                <p class="window__title">Заказать звонок</p>
                <?php $form = ActiveForm::begin(['action'=>'/callback']);?>
                <div class="window-form">
                    <?= $form->field($model, 'name')
                        ->textInput([
                            'placeholder' => 'Ваше имя',
                             'class' => 'window-form__input input-1',
                        ])->label(false); ?>
                    <?= $form->field($model, 'phone')
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7 (999) 999 99 99',
                        ])->textInput([
                            'placeholder' => 'Ваш номер телефона',
                            'class' => 'window-form__input input-1',
                        ])->label(false); ?>
                    <?= Html::submitButton('Отправить', ['class' => 'window-form__button button-1','onclick' => "metrikaReachGoal('callback-send')"]) ?>
                </div>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>