<?php
/** @var $emailForm \app\models\EmailForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="subscribe">
    <?php $form = ActiveForm::begin(['action'=>'/subscribe']);?>
    <p class="subscribe__text">Подпишитесь на рассылку новых предложений</p>
    <div class="subscribe__form subscribe__form_top">
        <?= $form->field($emailForm, 'email')
            ->textInput([
                'placeholder' => 'Укажите ваш e-mail',
                'class' => 'subscribe__input input-2'
            ])->label(false); ?>
        <?= Html::submitButton('<span class="button-1__icon">Отправить', ['class' => 'subscribe__button button-3 send_email_result','onclick' => "metrikaReachGoal('subscribe')"]) ?>
    </div>
    <?php ActiveForm::end();?>
</div>