<?php

namespace app\widgets;

use app\models\CallbackForm;
use yii\base\Widget;


class Callback extends Widget
{
    public function run(){
        $callbackForm = new CallbackForm();
        return $this->render('_callback',[
            'model' => $callbackForm
        ]);
    }
}