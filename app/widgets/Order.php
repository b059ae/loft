<?php

namespace app\widgets;

use app\models\OrderForm;
use yii\base\Widget;


class Order extends Widget
{
    public $id;
    public function run(){
        $orderForm = new OrderForm();
        return $this->render('_order',[
            'model' => $orderForm,
            'id' => $this->id
        ]);
    }
}