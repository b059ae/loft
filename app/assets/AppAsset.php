<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
//        'css/nouislider.css',
//        'css/nouislider.min.css',
        'css/slick.css',
        'css/styles.css?20180308',
//        'css/slick-theme.css',
    ];
    public $js = [
        'js/nouislider.js',
        'js/nouislider.min.js',
        'js/scripts.js',
        'js/custom.js',
        'js/formstyler.js',
        'js/html5.js',
        'js/jquery.ellipsis.min.js',
        'js/jquery.fancybox.min.js',
        'js/jquery.fancybox-thumbs.js',
        'js/jquery-ui-1.10.3.custom.min.js',
        'js/jquery.maskedinput.js',
        'js/mcustomscrollbar.js',
        'js/mousewheel.js',
        'js/rating.js',
        'js/slick.js',
        'js/smoothscroll.js',
        'js/share.js',
        'js/callback.js',
        'js/jquery-migrate-v1.4.1.js',
        'js/jquery.mousewheel-3.1.13.js',
        'js/yii.activeForm.js',
//        'js/yii.validation.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
