<?php

$params = require(__DIR__ . '/params.php');

$basePath =  dirname(__DIR__);
$webroot = dirname($basePath);

Yii::setAlias('@uploads', $webroot .DIRECTORY_SEPARATOR.'web'. DIRECTORY_SEPARATOR . 'uploads');
//Yii::setAlias('@uploads', 'uploads');

$config = [
    'id' => 'app',
    'basePath' => $basePath,
    'bootstrap' => ['debug'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['1.2.3.4', '127.0.0.1', '::1']
        ]
    ],

    'language' => 'ru',
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'salpfm3kfmk34mf3m4fklm',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true
        ],
        'urlManager' => [
            'rules' => [
                'services/<slug:[\w-]+>' => 'services/view',

                '<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/cat/<slug:[\w-]+>' => '<controller>/cat',
                'sale' => 'site/sale',
                'rent' => 'site/rent',
                'long-term-rent' => 'site/long-term-rent',
                'sale-view' => 'site/sale-view',
                'rent-view' => 'site/rent-view',
                'long-term-rent-view' => 'site/long-term-rent-view',
                'services' => 'services/index',
                'lot' => 'site/lot',
                'news' => 'site/news',
                'news-view' => 'site/news-view',
                'lists' => 'site/lists',
                'about' => 'site/about',
                'contacts' => 'site/contacts',
                'callback' => 'site/callback',
                'callback-services' => 'site/callback-services',
                'send' => 'site/send',
                'subscribe' => 'site/subscribe',
                'order' => 'site/order',
                'send-contact' => 'site/send-contact',
            ],
        ],
         /*'assetManager' => [
            'bundles' => require(__DIR__ . '/assets-local.php'),                
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    
    $config['components']['db']['enableSchemaCache'] = false;
}

return array_merge_recursive($config, require(dirname(__FILE__) . '/../../vendor/noumo/easyii/config/easyii.php'));
