<?php
namespace app\models;
use yii\base\Model;

class EmailForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            [['email'], 'string'],
            [['email'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

}