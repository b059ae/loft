<?php
namespace app\models;
use yii\base\Model;

class ContactForm extends Model
{
    public $name;
    public $phone;
    public $message;

    public function rules()
    {
        return [
            [['name','phone','message'], 'string'],
            [['name','phone','message'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше ФИО',
            'phone' => 'Номер телефона',
            'message' => 'Сообщение',
        ];
    }

}