<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\easyii\modules\catalog\api\Catalog;

class SaleFilterForm extends Model
{
    public $city;
    public $type;
    public $min_area;
    public $max_area;
    public $range_price_min;
    public $range_price_max;
    public $room;
    public $bathroom;
    public $balkon;
    public $pool;
    public $parking;
    public $sea;
    public $sort = 'price ASC';
    public $pageSize = 16;
    public $pageCount;
    public $switch = 'switch2';


    public function rules()
    {
        return [
            [['city','type','sort','range_price_min','range_price_max', 'switch'], 'string'],
            [['min_area','max_area',
                'room','bathroom','sea','pageSize','pageCount'], 'integer'],
            [['balkon','pool','parking'], 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'city' => 'Город',
            'type' => 'Тип недвижимости',
            'min_area' => 'Площадь от',
            'max_area' => 'Площадь до',
            'range_price_min' => 'Цена от',
            'range_price_max' => 'Цена до',
            'room' => 'Количество комнат',
            'bathroom' => 'Количество ванных комнат',
            'sea' => 'Расстояние до моря',
            'balkon' => 'Балкон',
            'pool' => 'Бассейн',
            'parking' => 'Парковка',
            'sort' => 'Сортировка',
            'pageSize' => 'Размер страницы',
            'pageCount' => 'Номер страницы',
        ];
    }

    public function parseFilters()
    {
        $filters = [];
        if ($this->city) {
            $filters['city'] = $this->city;
        }
        if ($this->type) {
            $filters['type'] = $this->type;
        }
        if ($this->balkon ) {
            $filters['balkon'] = $this->balkon;
        }
        if ($this->pool ) {
            $filters['pool'] = $this->pool;
        }
        if ($this->parking ) {
            $filters['parking'] = $this->parking;
        }
        if ($this->min_area && $this->max_area) {
            $filters['square'] = ['between',(int)$this->min_area,(int)$this->max_area];
        }elseif ($this->max_area) {
            $filters['square'] = ['<=',(int)$this->max_area];
        }elseif ($this->min_area) {
            $filters['square'] = ['>=',(int)$this->min_area];
        }
        if($this->room) {
            if($this->room == 4)
                $filters['room'] = ['>=', 4];
            else
                $filters['room'] = $this->room;
        }
        if ($this->bathroom) {
            if($this->bathroom == 3)
                $filters['bathroom'] = ['>=', 3];
            else
                $filters['bathroom'] = $this->bathroom;
        }
        if ($this->sea) {
            if($this->sea == 100)
                $filters['sea'] = ['<=', 100];
            if($this->sea == 101)
                $filters['sea'] = ['>', 100];
        }
        return $filters;
    }

    /**
     * Устанавливает размер и номер страницы и возвращает факт скрытия кнопки "показать ещё"
     * @param $filters array Фильтры
     * @return bool
     */
    public function getPage($filters,$price){
        if (isset(Yii::$app->request->post('SaleFilterForm')['pageCount'])) {
            $this->pageCount = Yii::$app->request->post('SaleFilterForm')['pageCount'];
            $this->pageSize = 16 * $this->pageCount;
        }else{
            $this->pageCount = 1;
        }
        $pages = count(Catalog::cat('sale')->getItems([
            'pagination' => ['pageSize' => ''],
            'orderBy' => $this->sort,
            'filters' => $filters,
            'where' =>
                ['between', 'price', (int)$price['min'], (int)$price['max']]
            ,
        ]));
        if($this->pageSize < $pages){
            $hide_button = true;
        }else{
            $hide_button = false;
        }
        return $hide_button;
    }

    /**
     * Возвращает диапазон цены на ползунках
     * @return mixed
     */
    public function getPrice(){
        if ($this->range_price_min > 0){
            $price['min'] = preg_replace('{ }','', $this->range_price_min);

        }else{
            $price['min'] = 0;
        }
        if ($this->range_price_max > 0){
            $price['max'] = preg_replace('{ }','', $this->range_price_max);

        }else{
            $price['max'] = 1000000;
        }
        return $price;
    }
}