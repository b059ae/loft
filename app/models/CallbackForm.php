<?php
namespace app\models;
use yii\base\Model;

class CallbackForm extends Model
{
    public $name;
    public $phone;

    public function rules()
    {
        return [
            [['name','phone'], 'string'],
            [['name','phone'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Номер телефона',
        ];
    }

}