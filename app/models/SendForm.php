<?php
namespace app\models;
use yii\base\Model;

class SendForm extends Model
{
    public $object_view;
    public $name;
    public $phone;
    public $date;

    public function rules()
    {
        return [
            [['name','phone','object_view'], 'string'],
            ['date', 'date'],
            [['name','phone','date'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'object_view' => 'Адрес страницы объекта',
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'date' => 'Дата',
        ];
    }

}