<?php
namespace app\models;
use yii\base\Model;

class OrderForm extends Model
{
    public $id;
    public $name;
    public $phone;

    public function rules()
    {
        return [
            ['id', 'integer'],
            [['name','phone'], 'string'],
            [['name','phone'], 'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'id' => 'Id объекта',
        ];
    }

}