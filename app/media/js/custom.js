// viewport size
function viewport() {
    var a = window, b = "inner";
    return "innerWidth" in window || (b = "client", a = document.documentElement || document.body), {
        width: a[b + "Width"],
        height: a[b + "Height"]
    }
}
var winW = viewport().width;
// viewport size

// map var
var maps = $(".rent-map__code").html();
// map var

/*----------begin doc ready----------*/
$(document).ready(function () {

// ios fix
    /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) && $("body").addClass("ios");
// ios fix

// ie fix
    var ms_ie = false;
    var ua = window.navigator.userAgent;
    var old_ie = ua.indexOf('MSIE ');
    var new_ie = ua.indexOf('Trident/');
    if ((old_ie > -1) || (new_ie > -1)) {
        ms_ie = true;
    }
    if (ms_ie) {
        $("body").addClass("ie");
        !function (a) {
            "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof module && module.exports ? require("jquery") : jQuery)
        }(function (a) {
            function b(b) {
                var c = {}, d = /^jQuery\d+$/;
                return a.each(b.attributes, function (a, b) {
                    b.specified && !d.test(b.name) && (c[b.name] = b.value)
                }), c
            }

            function c(b, c) {
                var d = this, f = a(this);
                if (d.value === f.attr(h ? "placeholder-x" : "placeholder") && f.hasClass(n.customClass))if (d.value = "", f.removeClass(n.customClass), f.data("placeholder-password")) {
                    if (f = f.hide().nextAll('input[type="password"]:first').show().attr("id", f.removeAttr("id").data("placeholder-id")), b === !0)return f[0].value = c, c;
                    f.focus()
                } else d == e() && d.select()
            }

            function d(d) {
                var e, f = this, g = a(this), i = f.id;
                if (!d || "blur" !== d.type || !g.hasClass(n.customClass))if ("" === f.value) {
                    if ("password" === f.type) {
                        if (!g.data("placeholder-textinput")) {
                            try {
                                e = g.clone().prop({type: "text"})
                            } catch (j) {
                                e = a("<input>").attr(a.extend(b(this), {type: "text"}))
                            }
                            e.removeAttr("name").data({
                                "placeholder-enabled": !0,
                                "placeholder-password": g,
                                "placeholder-id": i
                            }).bind("focus.placeholder", c), g.data({
                                "placeholder-textinput": e,
                                "placeholder-id": i
                            }).before(e)
                        }
                        f.value = "", g = g.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id", g.data("placeholder-id")).show()
                    } else {
                        var k = g.data("placeholder-password");
                        k && (k[0].value = "", g.attr("id", g.data("placeholder-id")).show().nextAll('input[type="password"]:last').hide().removeAttr("id"))
                    }
                    g.addClass(n.customClass), g[0].value = g.attr(h ? "placeholder-x" : "placeholder")
                } else g.removeClass(n.customClass)
            }

            function e() {
                try {
                    return document.activeElement
                } catch (a) {
                }
            }

            var f, g, h = !1, i = "[object OperaMini]" === Object.prototype.toString.call(window.operamini),
                j = "placeholder" in document.createElement("input") && !i && !h,
                k = "placeholder" in document.createElement("textarea") && !i && !h, l = a.valHooks, m = a.propHooks,
                n = {};
            j && k ? (g = a.fn.placeholder = function () {
                return this
            }, g.input = !0, g.textarea = !0) : (g = a.fn.placeholder = function (b) {
                var e = {customClass: "placeholder"};
                return n = a.extend({}, e, b), this.filter((j ? "textarea" : ":input") + "[" + (h ? "placeholder-x" : "placeholder") + "]").not("." + n.customClass).not(":radio, :checkbox, [type=hidden]").bind({
                    "focus.placeholder": c,
                    "blur.placeholder": d
                }).data("placeholder-enabled", !0).trigger("blur.placeholder")
            }, g.input = j, g.textarea = k, f = {
                get: function (b) {
                    var c = a(b), d = c.data("placeholder-password");
                    return d ? d[0].value : c.data("placeholder-enabled") && c.hasClass(n.customClass) ? "" : b.value
                }, set: function (b, f) {
                    var g, h, i = a(b);
                    return "" !== f && (g = i.data("placeholder-textinput"), h = i.data("placeholder-password"), g ? (c.call(g[0], !0, f) || (b.value = f), g[0].value = f) : h && (c.call(b, !0, f) || (h[0].value = f), b.value = f)), i.data("placeholder-enabled") ? ("" === f ? (b.value = f, b != e() && d.call(b)) : (i.hasClass(n.customClass) && c.call(b), b.value = f), i) : (b.value = f, i)
                }
            }, j || (l.input = f, m.value = f), k || (l.textarea = f, m.value = f), a(function () {
                a(document).delegate("form", "submit.placeholder", function () {
                    var b = a("." + n.customClass, this).each(function () {
                        c.call(this, !0, "")
                    });
                    setTimeout(function () {
                        b.each(d)
                    }, 10)
                })
            }), a(window).bind("beforeunload.placeholder", function () {
                var b = !0;
                try {
                    "javascript:void(0)" === document.activeElement.toString() && (b = !1)
                } catch (c) {
                }
                b && a("." + n.customClass).each(function () {
                    this.value = ""
                })
            }))
        });
        $('input, textarea').placeholder({customClass: 'input-placeholder'});
    }
// ie fix

// placeholder
    $("input, textarea").each(function () {
        var a = $(this).attr("placeholder");
        $(this).focus(function () {
            $(this).attr("placeholder", "")
        }), $(this).focusout(function () {
            $(this).attr("placeholder", a)
        })
    });
// placeholder

// fancybox
    var popLen1 = $(".js-popup-1").length;
    if (popLen1 > 0) {
        $(".js-popup-1").fancybox({
            autoSize: false,
            fitToView: false,
            maxWidth: '561px',
            width: '100%',
            height: 'auto',
            margin: 15,
            padding: 0,
            closeSpeed: 0
        })
    }

    var popLen2 = $(".js-popup-2").length;
    if (popLen2 > 0) {
        $(".js-popup-2").fancybox({
            autoSize: false,
            fitToView: false,
            margin: 0,
            padding: 0,
            maxWidth: '1191px',
            width: '100%',
            height: 'auto',
            afterClose: function () {
                $('.js-slider-4').slick('unslick');
            }
        })
    }

    $(".js-open").live("click", function () {
        var curSLide = $(this).closest(".js-open-item").data("item") - 1;
        $('.js-slider-4').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 800,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 200,
            initialSlide: curSLide
        });
    })

    $(".js-open").live("mouseover", function () {
        $(this).closest(".slider-3").find(".js-loop").stop().fadeIn(200);
        $(this).closest(".gal-1").find(".js-loop").stop().fadeIn(200);
        // $(this).closest(".slider-5").find(".js-loop").stop().fadeIn(200);
    })

    $(".js-loop").live("click", function () {
        $(this).closest(".slider-3").find(".slider-3__item.slick-active .js-open").click();
        $(this).closest(".gal-1").find(".gal-1__item.slick-active .js-open").click();
        $(this).closest(".slider-5").find(".slider-5__item.slick-active .js-open").click();
    }).live("mouseover", function () {
        $(this).stop().fadeIn(200);
    })

    $(".slider-3__item").live("mouseleave", function () {
        $(this).closest(".slider-3").find(".slider-3__loop").stop().fadeOut(200);
    })
    $(".gal-1__item").live("mouseleave", function () {
        $(this).closest(".gal-1").find(".gal-1__loop").stop().fadeOut(200);
    })
    $(".slider-5__item").live("mouseleave", function () {
        $(this).closest(".slider-5").find(".slider-5__loop").stop().fadeOut(200);
    })

    var popLen3 = $(".js-popup-3").length;
    if (popLen3 > 0) {
        $(".js-popup-3").fancybox({
            autoSize: false,
            fitToView: false,
            maxWidth: '420px',
            width: '100%',
            height: 'auto',
            margin: 15,
            padding: 0,
            closeSpeed: 0
        })
    }

    $(".js-window-close").live("click", function () {
        $(".fancybox-close").click();
        return false;
    })
// fancybox

// gallery
    var galleryLen = $(".gal").length;

    if (galleryLen > 0) {
        $('.js-slider-5').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 800,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 200
        });
        $('.js-slider-6').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 500,
            slidesToShow: 7,
            slidesToScroll: 1,
            touchThreshold: 200,
            responsive: [
                {breakpoint: 993, settings: {slidesToShow: 6}},
                {breakpoint: 768, settings: {slidesToShow: 5}},
                {breakpoint: 600, settings: {slidesToShow: 4}},
                {breakpoint: 500, settings: {slidesToShow: 3}},
                {breakpoint: 400, settings: {slidesToShow: 2}}
            ]
        });
        $('.js-slider-5').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            var curSlide = nextSlide + 1;
            $(".gal-2__item").removeClass("active");
            $(".gal-2__item[data-item=" + curSlide + "]").addClass("active");
        });
        $('.js-slider-6').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            $(".gal-2__mask").fadeOut(0);
            var curData = $(".gal-2__item.active").data("item");
        });
        $(".gal-2__image").live("click", function () {
            var curItem = $(this).closest(".gal-2__item").data("item");
            $(".gal-1 .slick-dots li:nth-child(" + curItem + ")").click();
        })
    }
// gallery

// tabs
    $(".js-tabs-link").click(function (e) {
        e.preventDefault();
        var curHref = $(this).attr("href");
        $(this).closest(".tabs-nav").find(".tabs-nav__item").removeClass("active");
        $(this).closest(".tabs-nav__item").addClass("active");
        $(this).closest(".tabs").find(".tabs-content__item").removeClass("active");
        $(curHref).addClass("active");
        // full height
        $(".js-full-height").css("min-height", "0");
        var headH = $(".header").height();
        var footH = $(".footer").height();
        var docH = $(document).height();
        var fixH = docH - (headH + footH);
        $(".js-full-height").css("min-height", fixH + "px");
        // full height
    })
// tabs

// ellip
    var ellipLen1 = $('.js-ellip-2').length;
    var ellipLen2 = $('.js-ellip-3').length;
    var ellipLen3 = $('.js-ellip-4').length;
    if (ellipLen1 > 0 || ellipLen2 > 0 || ellipLen3 > 0) {
        setTimeout(function () {
            $('.js-ellip-2').ellipsis({lines: 2, ellipClass: 'ellip', responsive: false});
        }, 300);
        setTimeout(function () {
            $('.js-ellip-3').ellipsis({lines: 3, ellipClass: 'ellip', responsive: false});
        }, 300);
        setTimeout(function () {
            $('.js-ellip-4').ellipsis({lines: 4, ellipClass: 'ellip', responsive: false});
        }, 300);
    }
// ellip

// header nav
    $(".js-nav-link").mouseenter(function () {
        $(this).next(".js-nav-hide").stop().fadeIn(0);
        $(this).closest(".js-nav-wrapper").find(".js-nav-fix").stop().fadeIn(0);
        $(this).removeClass("fixed");
    })
    $(".js-nav-wrapper").mouseleave(function () {
        $(this).find(".js-nav-hide").stop().fadeOut(0);
        $(this).find(".js-nav-fix").stop().fadeOut(0);
    })
    $(".js-nav-link").live("click", function () {
        if ($(this).hasClass("fixed")) {
            $(this).removeClass("fixed");
            $(this).next(".js-nav-hide").stop().fadeIn(0);
            $(this).closest(".js-nav-wrapper").find(".js-nav-fix").stop().fadeIn(0);
        }
        else {
            $(this).addClass("fixed");
            $(this).next(".js-nav-hide").stop().fadeOut(0);
            $(this).find(".js-nav-fix").stop().fadeOut(0);
        }
    })

    $(".js-slide-link").live("click", function () {
        $(this).closest(".js-slide-wrapper").find(".js-slide-hide").stop().slideToggle(300);
        $(this).closest(".js-slide-wrapper").toggleClass("active");
        $(this).toggleClass("active");
    })
    $(".ios .js-nav-link, .ios .js-slide-link").live("click", function (e) {
        e.preventDefault();
    })
// header nav

// mobile nav
    $(".header-mobile__hide").hide();
    $(".js-mobile-open").click(function () {
        $(".header-mobile").addClass("active");
        $(".header-mobile__hide").fadeIn(0);
    });
    $(".js-mobile-close").click(function () {
        $(".header-mobile").removeClass("active");
        $(".header-mobile__hide").fadeOut(0);
    });
// mobile nav

// fade
    $(".js-fade-link").live("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).closest(".js-fade-wrapper").find(".js-fade-hide").stop().fadeOut(0);
        }
        else {
            $(this).addClass("active");
            $(this).closest(".js-fade-wrapper").find(".js-fade-hide").stop().fadeIn(0);
        }
    })

    $(".ios .js-fade-link").click(function (e) {
        e.preventDefault();
    })
// fade

// datepicker
    var dateLen = $(".js-datepicker").length;
    if (dateLen > 0) {
        $.datepicker.regional['ru'] = {
            prevText: '',
            nextText: '',
            navigationAsDateFormat: true,
            currentText: 'Ð¡ÐµÐ³Ð¾Ð´Ð½Ñ',
            monthNames: ['Ð¯Ð½Ð²Ð°Ñ€ÑŒ', 'Ð¤ÐµÐ²Ñ€Ð°Ð»ÑŒ', 'ÐœÐ°Ñ€Ñ‚', 'ÐÐ¿Ñ€ÐµÐ»ÑŒ', 'ÐœÐ°Ð¹', 'Ð˜ÑŽÐ½ÑŒ',
                'Ð˜ÑŽÐ»ÑŒ', 'ÐÐ²Ð³ÑƒÑÑ‚', 'Ð¡ÐµÐ½Ñ‚ÑÐ±Ñ€ÑŒ', 'ÐžÐºÑ‚ÑÐ±Ñ€ÑŒ', 'ÐÐ¾ÑÐ±Ñ€ÑŒ', 'Ð”ÐµÐºÐ°Ð±Ñ€ÑŒ'],
            monthNamesShort: ['Ð¯Ð½Ð²', 'Ð¤ÐµÐ²', 'ÐœÐ°Ñ€', 'ÐÐ¿Ñ€', 'ÐœÐ°Ð¹', 'Ð˜ÑŽÐ½',
                'Ð˜ÑŽÐ»', 'ÐÐ²Ð³', 'Ð¡ÐµÐ½', 'ÐžÐºÑ‚', 'ÐÐ¾Ñ', 'Ð”ÐµÐº'],
            dayNames: ['Ð²Ð¾ÑÐºÑ€ÐµÑÐµÐ½ÑŒÐµ', 'Ð¿Ð¾Ð½ÐµÐ´ÐµÐ»ÑŒÐ½Ð¸Ðº', 'Ð²Ñ‚Ð¾Ñ€Ð½Ð¸Ðº', 'ÑÑ€ÐµÐ´Ð°', 'Ñ‡ÐµÑ‚Ð²ÐµÑ€Ð³', 'Ð¿ÑÑ‚Ð½Ð¸Ñ†Ð°', 'ÑÑƒÐ±Ð±Ð¾Ñ‚Ð°'],
            dayNamesShort: ['Ð²ÑÐº', 'Ð¿Ð½Ð´', 'Ð²Ñ‚Ñ€', 'ÑÑ€Ð´', 'Ñ‡Ñ‚Ð²', 'Ð¿Ñ‚Ð½', 'ÑÐ±Ñ‚'],
            dayNamesMin: ['Ð’Ñ', 'ÐŸÐ½', 'Ð’Ñ‚', 'Ð¡Ñ€', 'Ð§Ñ‚', 'ÐŸÑ‚', 'Ð¡Ð±'],
            weekHeader: 'ÐÐµÐ´',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            showOtherMonths: true,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        $(".js-datepicker").datepicker({
            inline: false,
            showAnim: '',
            onSelect: function (target) {
                $(".datepicker .change_date").change();
                $(".datepicker").removeClass("active");
            },
            onClose: function () {
                $(".datepicker").removeClass("active");
            }
        });
    }

    $(".datepicker__icon-1, .datepicker__icon-2").click(function () {
        $(this).closest(".datepicker").find(".js-datepicker").focus();
    })
    $(".datepicker__input").focus(function () {
        $(".datepicker").removeClass("active");
        $(this).closest(".datepicker").addClass("active");
    })
// datepicker

// scroll to id
    $(".js-scroll").click(function (event) {
        event.preventDefault();
    })

    function scrollId(curSel, curSpeed) {
        curSpeed = parseInt(curSpeed, 10) === curSpeed ? curSpeed : 300;
        $(curSel).on('click', function (event) {
            event.preventDefault();
            var url = $(this).attr('href');
            $("html, body").animate({
                scrollTop: parseInt($(url).offset().top)
            }, curSpeed);
        });
    }

    scrollId('.js-scroll', 500);
// scroll to id

// video
    $(".js-video").live("click", function () {
        var videoSrc = $(this).closest(".video").data("frame");
        $(this).closest(".video").append('<iframe class="video__frame" frameborder="0" allowfullscreen></iframe>');
        $(this).closest(".video").find(".video__frame").attr("src", videoSrc).fadeIn(0);
        $(this).fadeOut(100);
    })
// video

// remove checkbox
    $(".js-check-remove").live("click", function () {
        $(this).closest(".checkbox__item").fadeOut(300, function () {
            var checkLen = $(this).closest(".checkbox").find(".checkbox__item").length;
            if (checkLen == 1) {
                $(this).closest(".checkbox").fadeOut(0, function () {
                    $(this).remove();
                })
            }
            $(this).remove();
        })
    })
// remove checkbox

// open map
    $(".js-open-map").click(function () {
        $(this).closest(".map").find(".map__hide").toggleClass("fixed");
    })
// open map

// formstyler
    var formLen3 = $("select").length;
    if (formLen3 > 0) {
        $('select').styler({});
        $(".jq-selectbox__dropdown").each(function () {
            var liLen = $(this).find("li").length;
            if (liLen > 17) {
                $(this).find(".jq-scroll").mCustomScrollbar({
                    horizontalScroll: false,
                    scrollButtons: {enable: false},
                    advanced: {updateOnContentResize: true},
                    advanced: {updateOnBrowserResize: true},
                    scrollInertia: 0,
                    mouseWheelPixels: 30
                });
            }
        });
    }
// formstyler

// switch
//     $(".js-switch").click(function () {
//         if ($(this).closest(".switch__item").hasClass("active")) {
//             return
//         }
//         $(".switch__item").removeClass("active");
//         $(this).closest(".switch__item").addClass("active");
//         if ($(this).hasClass("switch__button_icon-1")) {
//             $(".rent-page__main").removeClass("fixed-1").addClass("fixed-2");
//             var winH = $(window).height();
//             var filterH = $(".filter-2").height();
//             var newH = winH - filterH;
//             $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", newH + "px");
//             $(".rent-map__code").html(maps);
//             if ($("body").hasClass("ios")) {
//                 var winFix = newH + 25;
//                 $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", newH + "px");
//                 $(".rent-map__code").html(maps);
//             }
//         }
//         if ($(this).hasClass("switch__button_icon-2")) {
//             $(".rent-page__main").removeClass("fixed-1").removeClass("fixed-2");
//         }
//         if ($(this).hasClass("switch__button_icon-3")) {
//             $(".rent-page__main").removeClass("fixed-2").addClass("fixed-1");
//             $(".rent-map__code").html(maps);
//         }
//     })
// switch

// steps
    $(".js-to-step-1").click(function () {
        $(".event-ad, .steps__item").removeClass("active");
        $(".js-step-1").addClass("active");
        $(".steps__item_1").addClass("active");
    })
    $(".js-to-step-2").click(function () {
        $(".event-ad, .steps__item").removeClass("active");
        $(".js-step-2").addClass("active");
        $(".steps__item_1, .steps__item_2").addClass("active");
    })
    $(".js-to-step-3").click(function () {
        $(".event-ad, .steps__item").removeClass("active");
        $(".js-step-3").addClass("active");
        $(".steps__item_1, .steps__item_2, .steps__item_3").addClass("active");
    })
    $(".js-to-step-4").click(function () {
        $(".event-ad, .steps__item").removeClass("active");
        $(".js-step-4").addClass("active");
        $(".steps__item_1, .steps__item_2, .steps__item_3, .steps__item_4").addClass("active");
    })
    $(".js-to-step-5").click(function () {
        $(".event-ad").removeClass("active");
        $(".js-step-5").addClass("active");
        $(".steps__item").addClass("active");
    })
// steps

// scrollbar
    var chatLen = $(".js-scrollbar").length;
    if (chatLen > 0) {
        $(".js-scrollbar").mCustomScrollbar({
            horizontalScroll: false,
            scrollButtons: {enable: false},
            advanced: {updateOnContentResize: true},
            advanced: {updateOnBrowserResize: true},
            scrollInertia: 0,
            mouseWheelPixels: 30,
            scrollbarPosition: "outside",
            autoDraggerLength: false
        });
    }
// scrollbar

// change event form
    $(".js-event-form-1").live("click", function () {
        $(".event-ads__hide").removeClass("active");
        $(".js-event-hide-1").addClass("active");
    })
    $(".js-event-form-2").live("click", function () {
        $(".event-ads__hide").removeClass("active");
        $(".js-event-hide-2").addClass("active");
    })
// change event form

// remove photo
    $(".js-remove-photo").live("click", function () {
        $(this).closest(".photos__item").fadeOut(300, function () {
            $(this).remove();
        })
    })
// remove photo

// file
    $(".js-file").live("click", function () {
        $(this).next(".file__input").click();
    })
// file
})
/*----------doc ready eof----------*/

/*----------begin window load----------*/
$(window).load(function () {
    // body fix
    $('body').removeClass('loaded');
    // body fix
})
/*----------window load eof----------*/

/*----------begin touch----------*/
$(document).on('touchstart', function () {
    documentClick = true;
});
$(document).on('touchmove', function () {
    documentClick = false;
});
$(document).on('click touchend', function (event) {
    if (event.type == "click") documentClick = true;
    if (documentClick) {
        var target = $(event.target);
        if (target.is('.js-fade-wrapper *')) {
            return
        }
        if (target.is('.js-nav-wrapper *')) {
            return
        }
        if (target.is('.js-slide-wrapper *')) {
            return
        }
        if (target.is('.header-mobile *')) {
            return
        }
        if (target.is('#mask')) {
            $(".js-fade-hide").stop().fadeOut(0);
            $(".js-nav-wrapper").removeClass("active");
            $(".js-nav-hide").stop().fadeOut(200);
            $(".header-mobile").removeClass("active");
            $("#mask").stop().fadeOut(200);
        }
        else {
            $(".js-fade-hide").stop().fadeOut(0);
            $(".js-fade-link").removeClass("active");
            $(".js-nav-wrapper").removeClass("active");
            $(".js-nav-hide").stop().fadeOut(200);
            $(".header-mobile").removeClass("active");
            $("#mask").stop().fadeOut(200);
        }
    }
});
/*----------touch eof----------*/

/*----------begin bind load & resize & orientation eof----------*/
var handler1 = function () {
// footer fix
    setTimeout(function () {
        var footH = $(".footer").height();
        $(".main-wrapper").css("padding-bottom", footH + "px");
    }, 1);
// footer fix

// filter fix //
    var winW = viewport().width;
    if (winW > 767) {
        $("body").addClass("body-1").removeClass("body-2");
        $(".filter-2").removeClass("js-slide-wrapper").addClass("js-fade-wrapper");
        $(".filter-2__more").removeClass("js-slide-link").addClass("js-fade-link");
        $(".filter-2__hide").removeClass("js-slide-hide").addClass("js-fade-hide");
    }
    if (winW <= 767) {
        $("body").addClass("body-2").removeClass("body-1");
        $(".filter-2").addClass("js-slide-wrapper").removeClass("js-fade-wrapper");
        $(".filter-2__more").addClass("js-slide-link").removeClass("js-fade-link");
        $(".filter-2__hide").addClass("js-slide-hide").removeClass("js-fade-hide");
    }
// filter fix //

// ellip
    var ellipLen1 = $('.js-ellip-2').length;
    var ellipLen2 = $('.js-ellip-3').length;
    var ellipLen3 = $('.js-ellip-4').length;
    if (ellipLen1 > 0 || ellipLen2 > 0 || ellipLen3 > 0) {
        $('.js-ellip-2').ellipsis({lines: 2, ellipClass: 'ellip', responsive: false});
        $('.js-ellip-3').ellipsis({lines: 3, ellipClass: 'ellip', responsive: false});
        $('.js-ellip-4').ellipsis({lines: 4, ellipClass: 'ellip', responsive: false});
    }
// ellip

// full height
    setTimeout(function () {
        $(".js-full-height").css("min-height", "0");
        var headH = $(".header").height();
        var footH = $(".footer").height();
        var docH = $(document).height();
        var fixH = docH - (headH + footH);
        $(".js-full-height").css("min-height", fixH + "px");
    }, 1);
// full height

// equal offers
    var compLen = (".offers").length;
    if (compLen > 0) {
        $(".offers__loc").css("height", "auto");
        $(".offers__list").each(function () {
            for (var i = 0; i < 999; i += 4) {
                if (i >= 4) {
                    item1 = i - 3;
                    item2 = i - 2;
                    item3 = i - 1;
                    item4 = i;
                    $(this).find(".offers__item-wrapper:nth-child(" + item1 + ")").addClass("col-4-" + i);
                    $(this).find(".offers__item-wrapper:nth-child(" + item2 + ")").addClass("col-4-" + i);
                    $(this).find(".offers__item-wrapper:nth-child(" + item3 + ")").addClass("col-4-" + i);
                    $(this).find(".offers__item-wrapper:nth-child(" + item4 + ")").addClass("col-4-" + i);
                }
            }
        });
        $(".offers__list").each(function () {
            for (var i = 0; i < 999; i += 2) {
                if (i >= 2) {
                    item1 = i - 1;
                    item2 = i;
                    $(this).find(".offers__item-wrapper:nth-child(" + item1 + ")").addClass("col-2-" + i);
                    $(this).find(".offers__item-wrapper:nth-child(" + item2 + ")").addClass("col-2-" + i);
                }
            }
        });

        var winW = viewport().width;
        if (winW > 992) {
            $(".offers__list").each(function () {
                for (var i = 0; i < 999; i += 4) {
                    if (i >= 4) {
                        var height1 = 0;
                        $(this).find('.offers__item-wrapper.col-4-' + i + ' .offers__loc').each(function () {
                            height1 = height1 > $(this).height() ? height1 : $(this).height();
                        });
                        $(this).find('.offers__item-wrapper.col-4-' + i + ' .offers__loc').each(function () {
                            $(this).css("height", height1 + "px")
                        });
                        var height2 = 0;
                        $(this).find('.offers__item-wrapper.col-4-' + i + ' .offers__cat').each(function () {
                            height2 = height2 > $(this).height() ? height2 : $(this).height();
                        });
                        $(this).find('.offers__item-wrapper.col-4-' + i + ' .offers__cat').each(function () {
                            $(this).css("height", height2 + "px")
                        });
                    }
                }
            });
        }
        if (winW <= 992) {
            $(".offers__list").each(function () {
                for (var i = 0; i < 999; i += 2) {
                    if (i >= 2) {
                        var height1 = 0;
                        $(this).find('.offers__item-wrapper.col-2-' + i + ' .offers__loc').each(function () {
                            height1 = height1 > $(this).height() ? height1 : $(this).height();
                        });
                        $(this).find('.offers__item-wrapper.col-2-' + i + ' .offers__loc').each(function () {
                            $(this).css("height", height1 + "px")
                        });
                        var height2 = 0;
                        $(this).find('.offers__item-wrapper.col-2-' + i + ' .offers__cat').each(function () {
                            height2 = height2 > $(this).height() ? height2 : $(this).height();
                        });
                        $(this).find('.offers__item-wrapper.col-2-' + i + ' .offers__cat').each(function () {
                            $(this).css("height", height2 + "px")
                        });
                    }
                }
            });
        }
    }
// equal offers

// service cols
    var eqLen = (".serv__list").length;
    if (eqLen > 0) {
        $(".serv__item").css("min-height", "0");
        $(".serv__list").each(function () {
            for (var i = 0; i < 100; i += 2) {
                if (i >= 2) {
                    item1 = i - 1;
                    item2 = i;
                    $(this).find(".serv__item-wrapper:nth-child(" + item1 + ")").addClass("eq-2-" + i);
                    $(this).find(".serv__item-wrapper:nth-child(" + item2 + ")").addClass("eq-2-" + i);
                }
            }
        });

        $(".serv__list").each(function () {
            for (var i = 0; i < 100; i += 2) {
                if (i >= 2) {
                    var height1 = 0;
                    $(this).find('.eq-2-' + i + ' .serv__item').each(function () {
                        height1 = height1 > $(this).height() ? height1 : $(this).height();
                    });
                    $(this).find('.eq-2-' + i + ' .serv__item').each(function () {
                        $(this).css("min-height", height1 + "px")
                    });
                    var height2 = 0;
                    $(this).find('.eq-2-' + i + ' .serv__item').each(function () {
                        height2 = height2 > $(this).height() ? height2 : $(this).height();
                    });
                    $(this).find('.eq-2-' + i + ' .serv__item').each(function () {
                        $(this).css("min-height", height2 + "px")
                    });
                }
            }
        });

    }
// service cols
}
$(window).bind('orientationchange', handler1);
$(window).bind('resize', handler1);
$(window).bind('load', handler1);
/*----------bind load & resize & orientation eof----------*/

/*----------begin bind load & resize----------*/
var winHeight1 = viewport().height;
var handler2 = function () {
    var winHeight2 = viewport().height;

// slick resize fix
    setTimeout(function () {
        var sliderLen4 = $('.fancybox-wrap .js-slider-4').length;
        if (sliderLen4 > 0) {
            $(".js-slider-4").slick('setPosition');
        }
        var sliderLen5 = $('.js-slider-5').length;
        if (sliderLen5 > 0) {
            $(".js-slider-5").slick('setPosition');
        }
        var sliderLen6 = $('.js-slider-6').length;
        if (sliderLen6 > 0) {
            $(".js-slider-6").slick('setPosition');
        }
    }, 500);
// slick resize fix

// map size
    if ($("body").hasClass("ios") && winHeight1 != winHeight2) {
        return;
    }
    else {
        var winH = $(window).height();
        var filterH = $(".filter-2").height();
        var newH = winH - filterH;
        $(".body-1 .rent-map").css("height", newH + "px");
        if ($("body").hasClass("ios")) {
            var winFix = newH + 25;
            $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", winFix + "px");
            $(".rent-map__code").html(maps);
        }
        $(".rent-map__code").html(maps);
    }
// map size

// float filters on resize & load
    var floatLen = $(".filter-2").length;
    if (floatLen > 0) {
        var filterH = $(".filter-2").height();
        var headH = $(".header").height();
        var scrollH = $(window).scrollTop();
        if (scrollH > headH) {
            $(".filter-2").addClass("fixed");
            $(".header").css("margin-bottom", filterH + "px");
        }
        else {
            $(".filter-2").removeClass("fixed");
            $(".header").css("margin-bottom", "0");
        }
    }
// float filters on resize & load

// float map on resize & load
    var floatLen = $(".filter-2").length;
    if (floatLen > 0) {
        var headH = $(".header").height();
        var filterH = $(".filter-2").height();
        var headNew = headH;
        var mapH = $(".js-float-map").height();
        var blockH = $(".rent-page__main").height();
        var end = (headNew + blockH) - mapH;
        var scrollH = $(window).scrollTop();
        if (scrollH > headH) {
            $(".js-float-map").css("position", "fixed").css("top", filterH + "px").css("margin-top", "0");
            if (scrollH > end) {
                $(".js-float-map").css("margin-top", end - headNew + "px").css("position", "absolute").css("top", "0");
            }
        }
        else {
            $(".js-float-map").css("margin-top", "0").css("position", "absolute").css("top", "0");
        }
    }
// float map on resize & load
}
$(window).bind('resize', handler2);
$(window).bind('load', handler2);
/*----------bind load & resize eof----------*/

/*----------begin win load----------*/
var handler3 = function () {
// formstyler
    var formLen1 = $("input[type=radio]").length;
    var formLen2 = $("input[type=checkbox]").length;
    if (formLen1 > 0 || formLen2 > 0) {
        $('input[type=checkbox]').styler({});
        $('.radio input[type=radio]').styler({wrapper: '.radio'});
    }
// formstyler
}
$(window).bind('load', handler3);
/*----------win load eof----------*/

/*----------begin bind load & click----------*/
var handler4 = function () {
// formstyler fix
    $(".rent-form__radio-1 label").each(function () {
        var a = $(this).find(".jq-radio.checked").length;
        a >= 1 ? $(this).addClass("active") : $(this).removeClass("active")
    }),
        $(".rent-form__radio-1 label").each(function () {
            var a = $(this).find(".jq-radio.disabled").length;
            a >= 1 ? $(this).addClass("disabled") : $(this).removeClass("disabled")
        });
    // $(".radio__item").each(function () {
    //     var a = $(this).find(".jq-radio.checked").length;
    //     a >= 1 ? $(this).addClass("active") : $(this).removeClass("active")
    // }),
    //     $(".radio__item").each(function () {
    //         var a = $(this).find(".jq-radio.disabled").length;
    //         a >= 1 ? $(this).addClass("disabled") : $(this).removeClass("disabled")
    //     }),
    //     $(".checkbox__item").each(function () {
    //         var a = $(this).find(".jq-checkbox.checked").length;
    //         a >= 1 ? $(this).addClass("active") : $(this).removeClass("active")
    //     }),
    //     $(".checkbox__item").each(function () {
    //         var a = $(this).find(".jq-checkbox.disabled").length;
    //         a >= 1 ? $(this).addClass("disabled") : $(this).removeClass("disabled")
    //     });
// formstyler fix
}
$(window).bind('click', handler4);
$(window).bind('load', handler4);
/*----------bind load & click eof----------*/

/*----------begin bind orientation----------*/
var handler5 = function () {
    var sliderLen1 = $('.js-slider-1').length;
    if (sliderLen1 > 0) {
        $(".js-slider-1").slick('setPosition');
    }
    var sliderLen2 = $('.js-slider-2').length;
    if (sliderLen2 > 0) {
        $(".js-slider-2").slick('setPosition');
    }
    var sliderLen3 = $('.js-slider-3').length;
    if (sliderLen3 > 0) {
        $(".js-slider-3").slick('setPosition');
    }
    var sliderLen4 = $('.js-slider-4').length;
    if (sliderLen4 > 0) {
        $(".js-slider-4").slick('setPosition');
    }
    var sliderLen5 = $('.js-slider-5').length;
    if (sliderLen5 > 0) {
        $(".js-slider-5").slick('setPosition');
    }
    var sliderLen6 = $('.js-slider-6').length;
    if (sliderLen6 > 0) {
        $(".js-slider-6").slick('setPosition');
    }
    var sliderLen7 = $('.js-slider-7').length;
    if (sliderLen7 > 0) {
        $(".js-slider-7").slick('setPosition');
    }
    var sliderLen8 = $('.js-slider-8').length;
    if (sliderLen8 > 0) {
        $(".js-slider-8").slick('setPosition');
    }

// map size
    var winH = $(window).height();
    var filterH = $(".filter-2").height();
    var newH = winH - filterH;
    $(".body-1 .rent-map").css("height", newH + "px");
    if ($("body").hasClass("ios")) {
        var winFix = newH + 25;
        $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", winFix + "px");
        $(".rent-map__code").html(maps);
    }
    $(".rent-map__code").html(maps);
// map size
}
$(window).bind('orientationchange', handler5);
/*----------bind orientation eof----------*/

// float block
$(document).scroll(function () {
    var floatLen = $(".js-float-wrapper").length;
    if (floatLen > 0) {
        var position1 = $(".js-float-wrapper").position();
        var position2 = position1.top;
        var headH = $(".header").height() + position2;
        var menuH = $(".js-float-bar").height();
        var blockH = $(".js-float-wrapper").height();
        var end = (headH + blockH) - menuH;
        var scrollH = $(window).scrollTop();
        if (scrollH > headH) {
            $(".js-float-bar").css("margin-top", scrollH - headH + "px");
            if (scrollH > end) {
                $(".js-float-bar").css("margin-top", end - headH + "px");
            }
        }
        else {
            $(".js-float-bar").css("margin-top", "0");
        }
    }
});
// float block

// float filters
$(document).scroll(function () {
    var floatLen = $(".filter-2").length;
    if (floatLen > 0) {
        var filterH = $(".filter-2").height();
        var headH = $(".header").height();
        var scrollH = $(window).scrollTop();
        if (scrollH > headH) {
            $(".filter-2").addClass("fixed");
            $(".header").css("margin-bottom", filterH + "px");
        }
        else {
            $(".filter-2").removeClass("fixed");
            $(".header").css("margin-bottom", "0");
        }
    }
});
// float filters

// float map
$(document).scroll(function () {
    var floatLen = $(".filter-2").length;
    if (floatLen > 0) {
        var headH = $(".header").height();
        var filterH = $(".filter-2").height();
        var headNew = headH;
        var mapH = $(".js-float-map").height();
        var blockH = $(".rent-page__main").height();
        var end = (headNew + blockH) - mapH;
        var scrollH = $(window).scrollTop();
        if (scrollH > headH) {
            $(".js-float-map").css("position", "fixed").css("top", filterH + "px").css("margin-top", "0");
            if (scrollH > end) {
                $(".js-float-map").css("margin-top", end - headNew + "px").css("position", "absolute").css("top", "0");
            }
        }
        else {
            $(".js-float-map").css("margin-top", "0").css("position", "absolute").css("top", "0");
        }
    }
});
// float map


// sliders
$(document).ready(function() {
var sliderLen6 = $(".js-slider-6").length;
if (sliderLen6 > 0) {
        $('.js-slider-6').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1
    });
    }
    var sliderLen1 = $(".js-slider-1").length;
    if (sliderLen1 > 0) {
        $('.js-slider-1').slick({
            dots: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 10000,
            speed: 800,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: false,
            fade: true,
            pauseOnHover: false
        });
    }

    var sliderLen2 = $(".js-slider-2").length;
    if (sliderLen2 > 0) {
        $('.js-slider-2').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 200
        })
        $('.offers__top').mousewheel(function (e) {
            e.preventDefault();
            if ($("body").hasClass("ios")) {
                return
            }
            else {
                if (e.deltaY < 0) {
                    $(this).find(".slick-next").click();
                }
                else {
                    $(this).find(".slick-prev").click();
                }
            }
        });
    }

    var sliderLen3 = $(".js-slider-3").length;
    if (sliderLen3 > 0) {
        $('.js-slider-3').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 800,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 200
        });
    }

    var sliderLen7 = $(".js-slider-7").length;
    if (sliderLen7 > 0) {
        $('.js-slider-7').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 800,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 200
        });
    }

    var sliderLen7 = $(".js-slider-8").length;
    if (sliderLen7 > 0) {
        $('.js-slider-8').slick({
            dots: true,
            infinite: true,
            autoplay: false,
            speed: 800,
            slidesToShow: 1,
            slidesToScroll: 1,
            touchThreshold: 200,
            adaptiveHeight: true
        });
    }

// fancybox
    $("a.service_image").fancybox();
    $("a#inline").fancybox({
        'hideOnContentClick': true
    });
    $("a.group").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'overlayShow'	:	false
    });
// fancybox

});
// sliders

    // range
var range1 = $(".js-range-1").length;
if(range1>0){
	noUiSlider.create($(".js-range-1"),{connect: true, range:{'min': 1000000,'max': 444000000},step: 1000000,start: [1000000, 444000000]});
	$(".js-range-1").Link('lower').to($(".js-range-1-min"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-1").Link('lower').to($("#range_price_min"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-1").Link('upper').to($(".js-range-1-max"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-1").Link('upper').to($("#range_price_max"), null, wNumb({decimals:0, thousand: ' '}));
}
var range2 = $(".js-range-2").length;
if(range2>0){
	$(".js-range-2").noUiSlider({connect: true, range:{'min': 90000,'max': 1500000},step: 10000,start: [90000, 1500000]});
	$(".js-range-2").Link('lower').to($(".js-range-2-min"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-2").Link('lower').to($("#range_price_min_m2"), null, wNumb({decimals:0, thousand: ' '}));

	$(".js-range-2").Link('upper').to($(".js-range-2-max"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-2").Link('upper').to($("#range_price_max_m2"), null, wNumb({decimals:0, thousand: ' '}));
}
var range3 = $(".js-range-3").length;
if(range3>0){
	$(".js-range-3").noUiSlider({connect: true, range:{'min': 1000000,'max': 444000000},step: 1000000,start: [1000000, 444000000]});
	$(".js-range-3").Link('lower').to($(".js-range-3-min"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-3").Link('upper').to($(".js-range-3-max"), null, wNumb({decimals:0, thousand: ' '}));
}
var range4 = $(".js-range-4").length;
if(range4>0){
	$(".js-range-4").noUiSlider({connect: true, range:{'min': 90000,'max': 1500000},step: 10000,start: [90000, 1500000]});
	$(".js-range-4").Link('lower').to($(".js-range-4-min"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-4").Link('upper').to($(".js-range-4-max"), null, wNumb({decimals:0, thousand: ' '}));
}
var range5 = $(".js-range-5").length;
if(range5>0){
	$(".js-range-5").noUiSlider({connect: true, range:{'min': 1,'max': 87},step: 1,start: [1, 87]});
	$(".js-range-5").Link('lower').to($(".js-range-5 .noUi-handle-lower"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-5").Link('lower').to($("#min_floor"), null, wNumb({decimals:0, thousand: ' '}));

	$(".js-range-5").Link('upper').to($(".js-range-5 .noUi-handle-upper"), null, wNumb({decimals:0, thousand: ' '}));
	$(".js-range-5").Link('upper').to($("#max_floor"), null, wNumb({decimals:0, thousand: ' '}));
}
    $(".js-filter-switch").click(function (e) {
        e.preventDefault();
        var curHref = $(this).attr("href");
        $(this).closest(".filter-nav").find(".filter-nav__item").removeClass("active");
        $(this).closest(".filter-nav__item").addClass("active");
        $(this).closest(".filter-switch").find(".filter-switch__item").removeClass("active");
        $(curHref).addClass("active");
    });

    $(".filter-2__right").addClass("fixed");
    $(".filter-2__hide").fadeOut(0);
// range
