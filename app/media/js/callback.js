jQuery(document).ready(function () {
    // main bottom accordeon
    var full_accordeon_height = $('.accordeon_block_inner').height() + 10;
    if (full_accordeon_height > 300) {
        $('.accordeon_block_inner').height(full_accordeon_height / 3.1);
    } else {
        $('section.accordeon_block .button-2').hide();
    }
    $('section.accordeon_block .button-2').on('click', function () {
        if ($('.accordeon_block_inner').height() == full_accordeon_height) {
            $('.accordeon_block_inner').animate({'height': full_accordeon_height / 3.1}, 600)
        } else {
            $('.accordeon_block_inner').animate({'height': full_accordeon_height}, 600)
        }
        return false;
    });


//     $('.tabs-nav__link').on('click', function () {
//         $('.tabs-nav__item').removeClass('active');
//         $(this).parent().addClass('active');
//         $('[name="cat_p"]').val($(this).data('url'));
//         if ($(this).data('url') == 'rent' || $(this).data('url') == 'long-term-rent') {
//             $('.filter-item__label .month').show();
//
//             $(".js-range-1")[0].destroy();
//             $(".js-range-1").noUiSlider({
//                 connect: true,
//                 start: [2700.00, 19546490.00],
//                 range: {
//                     'min': 2700.00,
//                     'max': 19546490.00
//                 },
//                 step: 1000000
//             });
//
//             $(".js-range-2")[0].destroy();
//             $(".js-range-2").noUiSlider({
//                 connect: true,
//                 start: [637.29, 102367.00],
//                 range: {
//                     'min': 637.29,
//                     'max': 102367.00
//                 },
//                 step: 10000
//             });
//         } else {
//             $('.filter-item__label .month').hide();
//
//             $(".js-range-1")[0].destroy();
//             $(".js-range-1").noUiSlider({
//                 connect: true,
//                 start: [0.00, 500000.00],
//                 range: {
//                     'min': 0.00,
//                     'max': 500000.00
//                 },
//                 step: 10000
//             });
//
//             $(".js-range-2")[0].destroy();
//             $(".js-range-2").noUiSlider({
//                 connect: true,
//                 start: [22000.00, 4326671.00],
//                 range: {
//                     'min': 22000.00,
//                     'max': 4326671.00
//                 },
//                 step: 10000
//             });
//         }
//         return false;
//     });
//
//     var range1 = $(".js-range-1").length;
//     if (range1 > 0) {
//         $(".js-range-1").noUiSlider({
//             connect: true,
//             range: {'min': 0.00, 'max': 500000.00},
//             step: 10000,
//             start: [0.00, 500000.00]
//         });
//         $(".js-range-1").Link('lower').to($(".js-range-1-min"), null, wNumb({decimals: 0, thousand: ' '}));
//         $(".js-range-1").Link('lower').to($("#range_price_min"), null, wNumb({decimals: 0, thousand: ' '}));
//
//         $(".js-range-1").Link('upper').to($(".js-range-1-max"), null, wNumb({decimals: 0, thousand: ' '}));
//         $(".js-range-1").Link('upper').to($("#range_price_max"), null, wNumb({decimals: 0, thousand: ' '}));
//     }
//
//     var range2 = $(".js-range-2").length;
//     if (range2 > 0) {
//         $(".js-range-2").noUiSlider({
//             connect: true,
//             range: {'min': 22000.00, 'max': 4326671.00},
//             step: 10000,
//             start: [22000.00, 4326671.00]
//         });
//         $(".js-range-2").Link('lower').to($(".js-range-2-min"), null, wNumb({decimals: 0, thousand: ' '}));
//         $(".js-range-2").Link('lower').to($("#range_price_min_m2"), null, wNumb({decimals: 0, thousand: ' '}));
//
//         $(".js-range-2").Link('upper').to($(".js-range-2-max"), null, wNumb({decimals: 0, thousand: ' '}));
//         $(".js-range-2").Link('upper').to($("#range_price_max_m2"), null, wNumb({decimals: 0, thousand: ' '}));
//     }
//
     mainSearch = function () {
         if (location.search) {
             location.reload()
         } else {
             $('#dinamic_items').attr('action', $('[name="cat_p"]').val());
             $('#dinamic_items').submit();
         }
         metrikaReachGoal('search-main');
     }
//     jQuery('#callback_form').yiiActiveForm([{
//         "id": "objectform-fio",
//         "name": "fio",
//         "container": ".field-objectform-fio",
//         "input": "#objectform-fio",
//         "enableAjaxValidation": true
//     }, {
//         "id": "objectform-phone",
//         "name": "phone",
//         "container": ".field-objectform-phone",
//         "input": "#objectform-phone",
//         "enableAjaxValidation": true,
//         "validate": function (attribute, value, messages, deferred, $form) {
//             yii.validation.required(value, messages, {"message": "Необходимо заполнить «Телефон»."});
//         }
//     }], {"validationUrl": "\/objects\/callback-validate"});
});