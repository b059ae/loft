$(document).ready(function () {

    Share = {
        vkontakte: function (purl, ptitle, pimg, text) {
            url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);
        },
        odnoklassniki: function (purl, text) {
            url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(text);
            url += '&st._surl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        facebook: function (purl, ptitle, pimg, text) {
            url = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]=' + encodeURIComponent(ptitle);
            url += '&p[summary]=' + encodeURIComponent(text);
            url += '&p[url]=' + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);
        },
        twitter: function (purl, ptitle) {
            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        mailru: function (purl, ptitle, pimg, text) {
            url = 'http://connect.mail.ru/share?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl=' + encodeURIComponent(pimg);
            Share.popup(url);
        },

        gp: function (purl) {
            url = 'https://plus.google.com/share?';
            url += 'url=' + encodeURIComponent(purl);
            Share.popup(url);
        },

        popup: function (url) {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }
    };

    bannerBeforeShow();
    bannerShow();

    function bannerBeforeShow() {
        $.ajax({
            url: '/bnn/before-show',
            type: 'post',
            data: {},
            success: function (data) {
            }
        });
    }

    function bannerShow() {
        var dataList = $(".banner").map(function () {
            return $(this).data("id");
        }).get();

        if (dataList.length > 0) {
            dataList = dataList.join(",");

            $.ajax({
                url: '/bnn/show',
                type: 'post',
                data: {
                    ids: dataList
                },
                success: function (data) {
                }
            });
        }
    }

    $('.banner').on('click', function () {
        $.ajax({
            url: '/bnn/click',
            type: 'post',
            data: {
                id: $(this).data("id")
            },
            success: function (data) {
            }

        });
    });

    var tagsArray = [];
    $('#dinamic_items').on('change', function () {
        form = $(this);
        var cat = form.find('[name="cat_p"]').val();
        $('.filter-3__button-wrapper').fadeOut();

        var commerceType = $('[name="commerceType"]').val();
        if (commerceType == 11 || commerceType == 12) {
            $('[name="rooms[]"], [name="bathrooms[]"]').attr('checked', false);
            $('.params_fade_if_commerce').fadeOut();
            $('.params_fade_if_commerce .checkbox__item').removeClass("active");
            $('.params_fade_if_commerce .checkbox__item .jq-checkbox').removeClass("checked");
        } else {
            $('.params_fade_if_commerce').fadeIn();
        }

        $.ajax({
            url: '/' + cat,
            type: 'post',
            dataType: 'json',
            data: {
                form: form.serialize()
            },
            success: function (data) {
                $('.page-nav__more').attr('data-count', 2);
                var url = '';
                if (data.taglist) {
                    data.taglist.forEach(function (entry) {
                        url += '/' + entry.sef;
                    });
                    url += (data.get_p) ? '?' + data.get_p : '';
                    history.pushState('', '', '/' + cat + url);
                    // history.pushState('', '', '/' + form.data('category') + url);

                    $('.offers__list.ld').html(data.data); // sometimes off

                    if (data.map) {
                        $(".rent-map__code").html(data.map);
                        if (typeof myMap != 'undefined') {
                            myMap.container.fitToViewport();
                        }
                    }

                    $('.ajax_list_count, .tt').text(data.count);
                    $('.up_count .c').text(data.page_count);
                    $('.filter-3__button-wrapper').fadeIn();
                    if (data.count == '0') {
                        $('.page-nav__more').hide();
                    } else {
                        $('.page-nav__more').show();
                    }
                }
            }
        });
    });


    // switch city
    $('#dinamic_items [name="city"]').on('change', function () {
        host = location.hostname;
        alias = $(this).val();
        url_no_host = window.location.pathname + window.location.search;

        if (alias != 'msk') {
            host = alias + '.' + host;
        } else {
            host = host.replace(/^.[^.]+./, '');
        }

        reloadUrl = host + url_no_host;
        location.href = 'http://' + reloadUrl;
    });

    // send email lots
    $('.send_email_result').on('click', function () {
        $(this).parent().find('[name="email"]').removeClass('error');
        email = $(this).parent().find('[name="email"]').val();
        if (email.length <= 3) {
            $(this).parent().find('[name="email"]').addClass('error');
        }
        if ($(this).parent().find('.error').length > 0) {
            return false;
        } else {
            $.ajax({
                url: '/category/send-lots-by-params-to-email',
                type: 'post',
                dataType: 'json',
                data: {
                    email: $(this).parent().find('[name="email"]').val(),
                    url: window.location.href
                },
                success: function (data) {
                    location.reload();
                }
            });
        }

        return false;
    });

    $("[name='ObjectForm[phone]'], [name='LotForm[phone]']").mask("+7 (999) 999-99-99");
});
