<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 07.07.17
 * Time: 15:57
 */

use app\helpers\Phone;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $article \yii\easyii\modules\article\api\ArticleObject */
/** @var $callbackForm \app\models\CallbackForm */
$asset = \app\assets\AppAsset::register($this);
?>

<main class="main">
    <div class="inner-page" style="background-image:url('<?= $asset->baseUrl ?>/img/services/fon.jpg');background-size:cover;">
        <section class="service wrapper">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item">
                    <a class="breadcrumbs__link" href="/services">Услуги</a>
                </li>
            </ul>
            <h1 class="service__title"><?= $article->seo('h1', $article->title) ?></h1>

            <!-- BEGIN ADVANTAGES -->
<!--            <section class="advantages">
                <h2 class="advantages__title"><p>Шедевр начинается с наброска. Так и хороший проект начинается с дизайна.</p>
                </h2>
                <ul class="advantages__list">
                    <li class="advantages__item">
                        <img src="<?php //** echo $asset->baseUrl */*/?>/img/services/dizayn/8ccda0c100-6_77x77.png" alt="">
                        <h3 class="advantages__name">Внимание к мелочам</h3>
                    </li>
                    <li class="advantages__item">
                        <img src="<?php //** echo $asset->baseUrl */*/?>/img/services/dizayn/d77260ddba-7_77x77.png" alt="">
                        <h3 class="advantages__name">Мастера своего дела</h3>
                    </li>
                    <li class="advantages__item">
                        <img src="<?php //** echo $asset->baseUrl */*/?>/img/services/dizayn/26259c420a-8_77x77.png" alt="">
                        <h3 class="advantages__name">Уникальность</h3>
                    </li>
                </ul>
            </section>-->
            <!-- ADVANTAGES EOF -->

            <!-- BEGIN HOW IT WORKS -->
<!--            <section class="how">
                <h3 class="how__title">Как все устроено?</h3>
                <ol class="how__list justify">
                    <li class="how__item">
                        <h4 class="how__name"><span>Вы оставляете заявку на дизайн</span></h4>
                    </li>
                    <li class="how__item">
                        <h4 class="how__name"><span>Мы с вами связываемся и уточняем ваши пожелания </span></h4>
                    </li>
                    <li class="how__item">
                        <h4 class="how__name"><span>Через оговоренное время мы передаем вам готовый продукт </span></h4>
                    </li>
                    <li class="how__item">
                        <h4 class="how__name"><span>Наша ремонтная бригада исполняет его в жизнь в соответствии с услугой "Ремонт"</span></h4>
                    </li>
                </ol>
            </section>-->
            <!-- HOW IT WORKS EOF -->

            <!-- BEGIN SLIDER -->

            <section class="slider-6 service-slide">
                <h3 class="slider-6__title">Особенности сервиса</h3>
                <div class="js-slider-6">
                    <?php echo $article->getText() ?>
                </div>
            </section>
            <!-- SLIDER EOF -->
            <!-- BEGIN FORM -->
            <aside class="form-3">
                <h3 class="form-3__title">Получить консультацию</h3>

                <?php $form = ActiveForm::begin(['action'=>'/callback']);?>

                <div class="form-3__sides clearfix">
                    <div class="form-3__inputs-wrapper">
                        <div class="form-3__inputs">
                            <div class="form-3__item">
                    <?= $form->field($callbackForm, 'name')
                        ->textInput([
                            'placeholder' => 'Ваше имя',
                            'class' => 'form-3__input input-2'
                        ])->label(false); ?>
                            </div>
                            <div class="form-3__item">
                    <?= $form->field($callbackForm, 'phone')
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7 (999) 999 99 99',
                        ])->textInput([
                            'placeholder' => 'Ваш номер телефона',
                            'class' => 'form-3__input input-2'
                        ])->label(false); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-3__button-wrapper">
                    <?= Html::submitButton('<span class="button-1__icon">Отправить', ['class' => 'form-3__button button-1']) ?>
                    </div>
                </div>
                <?php ActiveForm::end();?>
                <p class="form-3__text">Позвоните по телефону&nbsp;&nbsp;<a class="header-phone__number" href="tel:<?= Phone::link(Text::get('phone')) ?>"><?= Text::get('phone') ?></a></p>
            </aside>
            <!-- FORM EOF -->
        </section>
    </div>
</main>
