<?php
/** @var $services \yii\easyii\modules\article\models\Item[] */
$asset = \app\assets\AppAsset::register($this);

$this->title = 'Услуги. ADA';
?>
<main class="main">
    <div class="inner-page">
        <section class="serv js-full-height" style="min-height: 1866px;">
            <div class="serv__main wrapper">
                <h1 class="serv__title">Услуги</h1>
                <div class="serv__list">
                    <?php foreach ($services as $item): ?>
                    <article class="serv__item-wrapper eq-2-2">
                        <div class="serv__item" style="min-height: 369px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $item->thumb(77, 77) ?>')"></div>
                            <div class="serv__content">
                                <h2><span><?= $item->title ?></span></h2>
                                <p><?= $item->short ?></p>
                                <a class="button-link" href="/services/<?= $item->slug ?>">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <?php endforeach; ?>
                    <?php /*
                    <article class="serv__item-wrapper eq-2-2">
                        <div class="serv__item" style="min-height: 369px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/sobitiya/a4c88a7599-3_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Размещение объектов для проведения событий                                                                                                                                                                                                                     </span></h2>
                                <p>Alicante discanso предоставляет замечательную возможность владельцам  апартаментов заполнить свободные
                                    даты клиентами!</p>
                                <a class="button-link" href="/services/razmeshchenie-obektov-dlya-provedeniya-sobytiy">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <article class="serv__item-wrapper eq-2-2">
                        <div class="serv__item" style="min-height: 369px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/arenda-prodaja/0c445d8f46-6_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Размещение объектов для продажи и сдачи в аренду</span></h2>
                                <p>У нас вы можете быстро сдать в аренду или продать ваши аппартаменты!
                                    С консьерж агенством этот процесс будет максимально быстрым и выгодным.</p>
                                <a class="button-link" href="/services/razmeshchenie-obektov-dlya-prodaji-i-sdachi-v-arendu">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <article class="serv__item-wrapper eq-2-4">
                        <div class="serv__item" style="min-height: 357px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/poisk/95aa3e33f0-3_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Поиск недвижимости</span></h2>
                                <p>Мы бесплатно подберем для вас отличные варианты, отвечающие всем вашим требованиям!
                                    Просто оставьте заявку в форму ниже и наш менеджер с вами свяжется!</p>
                                <a class="button-link" href="/services/poisk-nedvijimosti">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <article class="serv__item-wrapper eq-2-4">
                        <div class="serv__item" style="min-height: 357px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/konserzh/5156d613b6-3_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Консьерж агентство</span></h2>
                                <p>Специалисты Alicante discanso имеют большой опыт в управлении недвижимостью.<br>
                                    Вы - отдыхаете. Мы - работаем. Оставьте заявку и наш менеджер с вами свяжется!</p>
                                <a class="button-link" href="/services/konserzh-usluga">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <article class="serv__item-wrapper eq-2-6">
                        <div class="serv__item" style="min-height: 403px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/dizayn/965621dd06-3_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Дизайн-проект</span></h2>
                                <p>Alicante Descanso работает с лучшими дизайн студиями Аликанте!
                                    Мы с удовольствием поможем вам с выбором дизайнерского решения для помещения в кратчайшие сроки и с наилучшим качеством!
                                    Оставьте заявку и мыс вами свяжемся!</p>
                                <a class="button-link" href="/services/dizayn">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <article class="serv__item-wrapper eq-2-6">
                        <div class="serv__item" style="min-height: 403px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/remont/365a8ebf43-3_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Ремонт</span></h2>
                                <p>Команда Alicante descanso не только разработает уникальный дизайн для вашего помещения, но и поможет воплотить его в
                                    жизнь мастерами своего дела и в материалах высочайшего качества! Просто оставьте заявку и наш менеджер с вами свяжется!</p>
                                <a class="button-link" href="/services/remont">Подробнее</a>
                            </div>
                        </div>
                    </article>
                    <article class="serv__item-wrapper eq-2-8">
                        <div class="serv__item" style="min-height: 355px;">
                            <div class="serv__icon_n" style="background-image: url('<?= $asset->baseUrl ?>/img/services/v-kredit/18d3e86812-3_77x77.png')"></div>
                            <div class="serv__content">
                                <h2><span>Недвижимость в кредит</span></h2>
                                <p>Наши банки-партнеры поборются за вас и предложат наилучшие условия кредитования!&nbsp;Оставьте заявку и наш менеджер с вами свяжется!<br>
                                    &nbsp;</p>
                                <a class="button-link" href="/services/nedvijimost-v-kredit">Подробнее</a>
                            </div>
                        </div>
                    </article>
 */ ?>
                </div>
            </div>
        </section>
    </div>
</main>