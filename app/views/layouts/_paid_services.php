<?php
/** @var $asset \yii\web\AssetBundle */
$services = [
    'Вас встретит наш сотрудник',
    'Поможем с размещением',
    'Окажем юридическое сопровождение',
    'Предоставим переводчика на всех встречах',
    'Оформим местную сим-карту',
    'Оформим ипотеку'
];
$i = 1;
?>
<section class="paid_services offers">
    <a name="paid_services"></a>
    <div class="paid_services wrapper">
        <div class="row">
            <?php foreach ($services as $s): ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-block" style="background-image:url('<?= $asset->baseUrl ?>/img/services/serv<?=$i?>.jpg');background-size:cover;">
                        <div class="service-table">
                            <div class="service-cell">
                                <div class="serv-wrap">
                                    <div class="service-text">
                                            <?= $s ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>
