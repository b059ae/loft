<?php
/** @var $asset \yii\web\AssetBundle */
/** @var $sale \yii\easyii\modules\catalog\api\ItemObject[] */
?>
<div class="main-block">
    <section id="offers">
        <div class="offers__main wrapper">
            <a name="offers"><h3 class="offers__title main-title">Лучшие предложения для покупки</h3></a>
            <ul class="offers__list">
                <?php foreach ($sale as $item): ?>
                    <a class="cat" href="/sale-view?id=<?= $item->id?>">
                        <li class="offers__item-wrapper col-4-4 col-2-2">
                            <div class="offers__item">
                                <div class="offers__top">
                                    <div class="img-cat" style="background: url(<?= $item->getImage()?>);background-size: cover;">
                                    </div>
                                    <div class="offers__text" href="">
                                        <h4 class="offers__name"><?= $item->type?></h4>
                                        <div class="offers__info">
                                            <p class="offers__price"><?= $item->getPrice() ?> €</p>
                                            <p class="offers__metre"><?= $item->square ?> м²</p>
                                        </div>
                                    </div>
                                </div>
                                <a class="offers__bottom">
                                    <p class="offers__loc" style="height: 32px;">
                                        <?= $item->title ?>
                                    </p>

                                    <p class="offers__cat" style="height: 15px;">
                                        <?= $item->city ?>
                                    </p>
                                </a>
                            </div>
                        </li>
                    </a>
                <?php endforeach; ?>
            </ul>
            <a class="main-button-1 button-2" href="sale">Смотреть все</a>
        </div>
    </section>
</div>