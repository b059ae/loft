<div class="main-block">
    <section class="services wrapper">
        <a name="services"><h3 class="services__title main-title">Наши услуги</h3></a>
        <ul class="services__list">
            <div class="js-slider-6 slider-6">
                <div>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/dizayn.jpg');"
                           href="services/dizajn-proekt">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name">Дизайн-проект</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/excursion.jpg');"
                           href="services/ekskursia">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name">Экскурсия</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </div>
                <div>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/urist.jpg');"
                           href="services/urist">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name">Юрист</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/procedura-pokupki-i-oformlenia-nedvizimosti-v-ispanii.jpg');"
                           href="services/poisk-nedvizimosti-i-procedura-pokupki">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name" style="line-height: 31px;">Поиск недвижимости и процедура покупки</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </div>
                <div>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/remont.jpg');"
                           href="services/remont">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name">Ремонт</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/konserz-usluga.jpg');"
                           href="services/konserz-usluga">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name">Консьерж-услуга</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </div>
                <div>
                    <li class="services__item">
                        <a class="services__link"
                           style="background-image:url('<?= $asset->baseUrl ?>/img/services/nedvizimost-v-kredit.jpg');"
                           href="services/nedvizimost-v-kredit">
                            <div class="services__fix-1">
                                <div class="services__fix-2">
                                    <div class="blue-wrap">
                                    <h4 class="services__name">Недвижимость в кредит</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </div>
            </div>
        </ul>
        <a class="main-button-1 button-2" href="services">Все услуги</a>
    </section>
</div>