<?php
use yii\bootstrap\ActiveForm;
use yii\easyii\modules\text\api\Text;

/** @var $filterForm \app\models\SaleFilterForm */
/** @var $cities array */
/** @var $types array */
/** @var $maxPrice float */
?>
<div class="main-top" style="background-image: url('<?= $asset->baseUrl; ?>/img/photo2.jpg'); width: auto;height:594px;">
    <div id="pogoda">
        <div id="4366879a6d621c86e8ebee0438cbbf41" class="ww-informers-box-854753" style="-webkit-transform:rotateY(90deg);transform:rotateY(90deg);"><p><a href="https://world-weather.ru/">world-weather.ru/</a><br><a href="https://world-weather.ru/pogoda/russia/barnaul/">https://world-weather.ru/pogoda/russia/barnaul/</a></p></div><script type="text/javascript" charset="utf-8" src="https://world-weather.ru/wwinformer.php?userid=4366879a6d621c86e8ebee0438cbbf41"></script>
    </div>
    <!-- BEGIN TITLE -->
    <div class="big-title">
        <h1 class="big-title__text wrapper">
            <span><?= Text::get('main-offer') ?></span>
        </h1>
        <br/>
        <a class="btn btn-primary js-popup-3" href="#popup-callback" onclick="metrikaReachGoal('callback-main')">Получить <span class="hidden-xs">бесплатную</span> консультацию</a>
    </div>
     <!-- TITLE EOF -->
    <form id="dinamic_items" action="/sale" method="post" enctype="multipart/form-data" data-category="sale">
        <input type="hidden" name="_csrf-frontend" value="OGwzNUh1eGh.OF98ISMCEQpaVmAwRUwADjMFQhc8VR5OAnkGMB1KUQ==">
        <input type="hidden" name="cat_p" value="sale">
        <!-- BEGIN MAIN FILTER -->
        <div class="filter-1 wrapper tabs">
            <div class="filter-1__nav tabs-nav">
                <ul class="tabs-nav__list">
                    <li class="tabs-nav__item active">
                        <a class="tabs-nav__link" data-url="sale" href="#">Продажа</a>
                    </li>
<!--                    <li class="tabs-nav__item">
                        <a class="tabs-nav__link" data-url="rent" href="#">Аренда</a>
                    </li>
                    <li class="tabs-nav__item">
                        <a class="tabs-nav__link" data-url="long-term-rent" href="#">Длительная аренда</a>
                    </li>-->
                </ul>
            </div>
            <div class="filter-1__main tabs-content">
                <!-- BEGIN TAB -->
                <div class="filter-1__item tabs-content__item active" id="tab-1">
                    <div class="filter-1__content clearfix">
                        <div class="filter-1__actions">
                            <div class="filter-1__left-wrapper">
                                <div class="filter-1__left">
                                    <div class="filter-item">
                                        <p class="filter-item__label">Город</p>
                                        <?php $fForm = ActiveForm::begin(); ?>
                                        <div class="filter-item__select select">
                                        <?= $fForm->field($filterForm, 'city')
                                            ->dropDownList($cities,['class' => 'none'])->label(false); ?>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <p class="filter-item__label">Тип недвижимости</p>
                                        <div class="filter-item__select select">
                                        <?= $fForm->field($filterForm, 'type')
                                            ->dropDownList($types,['class' => 'none'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-1__right-wrapper">
                                <div class="filter-1__right">
                                    <div class="filter-1__area">
                                        <div class="filter-item clearfix">
                                            <p class="filter-item__label">Площадь</p>
                                            <div class="filter-item__input-wrapper">
                                                <?= $fForm->field($filterForm, 'min_area')
                                                    ->textInput(['class' => 'filter-item__input input-1','placeholder' => 'от'])->label(false); ?>
                                            </div>
                                            <div class="filter-item__input-wrapper">
                                                <?= $fForm->field($filterForm, 'max_area')
                                                    ->textInput(['class' => 'filter-item__input input-1','placeholder' => 'до'])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-1__range">
                                        <div class="filter-item filter-switch">
                                            <div class="filter-item__nav">
                                                <p class="filter-item__label">Стоимость <span
                                                            class="month">в месяц</span></p>
                                            </div>
                                            <div class="filter-switch__content">
                                                <div class="filter-switch__item active" id="range-1">
                                                    <div class="range-1">
                                                        <div class="range-1__top">
                                                            <div id="js-range-1" class="js-range-1">
                                                            </div>
                                                        </div>
                                                        <div class="range-1__text">
                                                            <div class="range-1__left">
                                                                <span class="js-range-1-min"></span>&nbsp;€
                                                                <?= $fForm->field($filterForm, 'range_price_min')
                                                                    ->hiddenInput(['id' => 'range_price_min','class'=>'none'])->label(false); ?>
                                                            </div>
                                                            <div class="range-1__right">
                                                                <span class="js-range-1-max"></span>&nbsp;€
                                                                <?= $fForm->field($filterForm, 'range_price_max')
                                                                    ->hiddenInput(['id' => 'range_price_max','class'=>'none'])->label(false); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter-1__buttons">
                        <div class="filter-1__button-wrapper">
                           <a class="filter-1__submit button-1" onclick="mainSearch(); return false" href="#"
                               type="submit" value="Поиск">Поиск</a>
                        </div>
                        <!--                            <div class="filter-1__button-wrapper">-->
                        <!--                                <a class="filter-1__more button-2" href="-->
                        <!--">Расширенный</a>-->
                        <!--                            </div>-->
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <!-- MAIN FILTER EOF -->
    </form>
</div>

<?php
$script = <<< JS
jQuery(document).ready(function () {
    // main bottom accordeon
    var full_accordeon_height = $('.accordeon_block_inner').height() + 10;
    if (full_accordeon_height > 300) {
        $('.accordeon_block_inner').height(full_accordeon_height / 3.1);
    } else {
        $('section.accordeon_block .button-2').hide();
    }
    $('section.accordeon_block .button-2').on('click', function () {
        if ($('.accordeon_block_inner').height() == full_accordeon_height) {
            $('.accordeon_block_inner').animate({'height': full_accordeon_height / 3.1}, 600)
        } else {
            $('.accordeon_block_inner').animate({'height': full_accordeon_height}, 600)
        }
        return false;
    });


    $('.tabs-nav__link').on('click', function () {
        $('.tabs-nav__item').removeClass('active');
        $(this).parent().addClass('active');
        $('[name="cat_p"]').val($(this).data('url'));
        if ($(this).data('url') == 'rent' || $(this).data('url') == 'long-term-rent') {
            $('.filter-item__label .month').show();

            $(".js-range-1")[0].destroy();
            $(".js-range-1").noUiSlider({
                connect: true,
                start: [2700.00, 19546490.00],
                range: {
                    'min': 2700.00,
                    'max': 19546490.00
                },
                step: 1000000
            });

            $(".js-range-2")[0].destroy();
            $(".js-range-2").noUiSlider({
                connect: true,
                start: [637.29, 102367.00],
                range: {
                    'min': 637.29,
                    'max': 102367.00
                },
                step: 10000
            });
        } else {
            $('.filter-item__label .month').hide();

            $(".js-range-1")[0].destroy();
            $(".js-range-1").noUiSlider({
                connect: true,
                start: [0.00, 1000000],
                range: {
                    'min': 0.00,
                    'max': 1000000
                },
                step: 10000
            });

            $(".js-range-2")[0].destroy();
            $(".js-range-2").noUiSlider({
                connect: true,
                start: [22000.00, 4326671.00],
                range: {
                    'min': 22000.00,
                    'max': 4326671.00
                },
                step: 10000
            });
        }
        return false;
    });

    var range1 = $(".js-range-1").length;
    if (range1 > 0) {
        $(".js-range-1").noUiSlider({
            connect: true,
            range: {'min': 0.00, 'max': 1000000},
            step: 10000,
            start: [0.00, 1000000]
        });
        $(".js-range-1").Link('lower').to($(".js-range-1-min"), null, wNumb({decimals: 0, thousand: ' '}));
        $(".js-range-1").Link('lower').to($("#range_price_min"), null, wNumb({decimals: 0, thousand: ' '}));

        $(".js-range-1").Link('upper').to($(".js-range-1-max"), null, wNumb({decimals: 0, thousand: ' '}));
        $(".js-range-1").Link('upper').to($("#range_price_max"), null, wNumb({decimals: 0, thousand: ' '}));
    }

    var range2 = $(".js-range-2").length;
    if (range2 > 0) {
        $(".js-range-2").noUiSlider({
            connect: true,
            range: {'min': 22000.00, 'max': 4326671.00},
            step: 10000,
            start: [22000.00, 4326671.00]
        });
        $(".js-range-2").Link('lower').to($(".js-range-2-min"), null, wNumb({decimals: 0, thousand: ' '}));
        $(".js-range-2").Link('lower').to($("#range_price_min_m2"), null, wNumb({decimals: 0, thousand: ' '}));

        $(".js-range-2").Link('upper').to($(".js-range-2-max"), null, wNumb({decimals: 0, thousand: ' '}));
        $(".js-range-2").Link('upper').to($("#range_price_max_m2"), null, wNumb({decimals: 0, thousand: ' '}));
    }
});
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
?>