<?php

use app\widgets\Callback;
use yii\helpers\Html;
$callbackForm = new \app\models\CallbackForm();
$asset = \app\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic'
              rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <?php echo Callback::widget() ?>
    <?= $this->render('_header', ['asset' => $asset]); ?>

    <main class="main">
        <div class="inner-page">
    <?= $content ?>
        </div>
    </main>
    <?= $this->render('_footer', ['asset' => $asset]); ?>
    <?php $this->endBody() ?>
    <?= $this->render('/metrika/_metrika'); ?>
    <?= $this->render('/metrika/_goal'); ?>
    <script src="https://callback.onlinepbx.ru/loader.js" charset="UTF-8" data-onpbxcb-id="3a9d66d32ed737fdf025b7113497d2f8"></script>
    </body>
    </html>
<?php $this->endPage() ?>