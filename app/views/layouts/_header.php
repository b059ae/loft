<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */

use app\helpers\Phone;
use app\widgets\Subscribe;
use yii\easyii\modules\text\api\Text;
?>
<header class="header">
    <div class="header-left">
        <a class="header-logo" href="/">
            <img alt="" src="<?= $asset->baseUrl ?>/img/logo2.jpg">
        </a>
    </div>
    <div class="header-content">
        <nav class="header-nav-1">
            <ul class="header-nav-1__list">
                <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                    <li class="header-nav-1__item">
                        <a class="header-nav-1__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
                    </li>
                <?php endforeach; ?>
                <li class="header-nav-1__item js-nav-wrapper">
                    <a class="header-nav-1__link js-nav-link" href="/services">Услуги<div class="header-nav-1__fix js-nav-fix"></div></a>
                    <div class="header-nav-1__hide js-nav-hide">
                        <ul class="header-nav-1__list-in">
                            <?php foreach (yii\easyii\modules\menu\api\Menu::items('uslugi') as $item): ?>
                                <li class="header-nav-1__item-in">
                                    <a class="header-nav-1__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>                                                                                                                                                                                                                   </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <ul class="header-nav-1__list-in">
                            <?php foreach (yii\easyii\modules\menu\api\Menu::items('uslugi-2') as $item): ?>
                                <li class="header-nav-1__item-in">
                                    <a class="header-nav-1__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>                                                                                                                                                                                                                   </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="header-right">
            <div class="header-phone">
                <div class="header-phone__number-wrapper">
                    <a class="header-phone__number" href="tel:<?= Phone::link(Text::get('phone1')) ?>"><?= Text::get('phone1') ?></a>
                </div>
                <a class="header-phone__link js-popup-3" href="#popup-callback" onclick="metrikaReachGoal('callback-header')">Заказать звонок</a>
            </div>
        </div>

        <div class="header-mobile">
            <button class="header-mobile__button js-mobile-open"></button>
            <div class="header-mobile__hide">
                <div class="header-mobile__content">
                    <button class="header-mobile__close js-mobile-close"></button>
                    <nav class="header-nav-2">
                        <ul class="header-nav-2__list">
                            <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                                <li class="header-nav-2__item">
                                    <a class="header-nav-2__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
                                </li>
                            <?php endforeach; ?>
                            <li class="header-nav-2__item header-nav-2__item_hide js-slide-wrapper">
                                <a class="header-nav-2__link js-slide-link" href="/services">
                                    <span class="header-nav-2__icon">Услуги</span>
                                </a>
                                <div class="header-nav-2__hide js-slide-hide">
                                    <ul class="header-nav-2__list-in">
                                        <?php foreach (yii\easyii\modules\menu\api\Menu::items('uslugi') as $item): ?>
                                            <li class="header-nav-2__item-in">
                                                <a class="header-nav-2__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>                                                                                                                                                                                                                   </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <ul class="header-nav-2__list-in">
                                        <?php foreach (yii\easyii\modules\menu\api\Menu::items('uslugi-2') as $item): ?>
                                            <li class="header-nav-2__item-in">
                                                <a class="header-nav-2__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>                                                                                                                                                                                                                   </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                            <li class="header-nav-2__item">
                                <a class="header-nav-2__link" href="/contacts">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                    <ul class="header-login">
                        <li class="header-login__item">
                            <!--                    <a class="header-login__button button-1" href="#">Вход в личный кабинет</a>-->
                        </li>
                    </ul>

                    <!--Подписка на email-рассылку-->
                    <?= Subscribe::widget() ?>

                    <div class="socials-1">
                        <ul class="socials-1__list">
                            <li class="socials-1__item">
                                <a class="socials-1__link socials-1__link_icon-1" href="<?= Text::get('vkontakte') ?>"></a>
                            </li>
                            <li class="socials-1__item">
                                <a class="socials-1__link socials-1__link_icon-2" href="<?= Text::get('facebook') ?>"></a>
                            </li>
                            <li class="socials-1__item">
                                <a class="socials-1__link socials-1__link_icon-3" href="h<?= Text::get('instagram') ?>"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</header>