<?php
$services = [
    'Получение номера иностранца (NIE)',
    'Открытие счёта в банке',
    'Помощь в оформлении контракта на свет (контракт платный)',
    'Помощь в оформлении контракта на воду (контракт платный)',
    'Помощь в оформлении контракта на газ (контракт платный)',
    'Помощь в оформлении ипотеки'
];
$i = 1;
?>
<section class="free_services offers">
    <div class="paid_services wrapper">
        <a name="free_services"><h3 class="offers__title main-title">При покупке жилья мы бесплатно окажем следующие услуги</h3></a>
        <div class="row">
            <?php foreach ($services as $s): ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-block" style="background-image:url('<?= $asset->baseUrl ?>/img/services/free<?=$i?>.jpg');background-size:cover;">
                        <div class="service-table">
                            <div class="service-cell">
                                <div class="serv-wrap">
                                    <div class="service-text">
                                        <?= $s ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>
