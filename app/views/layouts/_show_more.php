<?php

/** @var $url string Ссылка на экшн */

use yii\web\View;

$this->registerJs(<<< JS
    //Показывает больше страниц
    function showMore() {    
        var count = $('#pageCount').val();
        count++;
        $('#pageCount').val(count);
        $("#saleForm").submit();
    }
JS
    , View::POS_HEAD);
?>