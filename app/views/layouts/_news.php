<section class="news">
    <div class="news__main wrapper">
        <div class="news__top">
            <div class="news__left">
                <h3 class="news__title">Последние материалы</h3>
            </div>
            <div class="news__right">
                <a class="button-link" href="lists">Все материалы</a>
            </div>
        </div>
        <ul class="news__list">
            <li class="news__item-wrapper">
                <div class="news__item">
                    <a class="news__image" href="lists/loft---prostranstvo-dlya-jizni-biznesa-i-meropriyatiy" style="background-image:url('<?= $asset->baseUrl ?>/img/articles/0af3830f81-1_367x260.jpg');"></a>
                    <div class="news__content">
                        <h3 class="news__name">
                            <a class="news__link js-ellip-3" href="lists/loft---prostranstvo-dlya-jizni-biznesa-i-meropriyatiy"><span class="ellip"><span style="white-space: nowrap;">Лофт</span> <span style="white-space: nowrap;">-</span> <span style="white-space: nowrap;">пространство</span> <span style="white-space: nowrap;">для</span> <span style="white-space: nowrap;">жизни,</span> <span style="white-space: nowrap;">бизнеса</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">мероприятий</span></span></a>
                        </h3>
                        <div class="news__info">
                            <p class="news__date">17.05.2017</p>
                            <p class="news__cat">
                                <a class="news__color button-link" href="lists/articles">Статьи</a>
                            </p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="news__item-wrapper">
                <div class="news__item">
                    <a class="news__image" href="lists/arenda-ofisa-loft" style="background-image:url('<?= $asset->baseUrl ?>/img/articles/568688720f-1_367x260.jpg');"></a>
                    <div class="news__content">
                        <h3 class="news__name">
                            <a class="news__link js-ellip-3" href="lists/arenda-ofisa-loft"><span class="ellip"><span style="white-space: nowrap;">Аренда</span> <span style="white-space: nowrap;">лофт-офиса:</span> <span style="white-space: nowrap;">выгодно</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">модно</span></span></a>
                        </h3>
                        <div class="news__info">
                            <p class="news__date">12.05.2017</p>
                            <p class="news__cat">
                                <a class="news__color button-link" href="lists/articles">Статьи</a>
                            </p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="news__item-wrapper">
                <div class="news__item">
                    <a class="news__image" href="lists/gde-i-kak-mojno-prodat-loft-v-moskve" style="background-image:url('<?= $asset->baseUrl ?>/img/articles/203fdac4be-1_367x260.jpg');"></a>
                    <div class="news__content">
                        <h3 class="news__name">
                            <a class="news__link js-ellip-3" href="lists/gde-i-kak-mojno-prodat-loft-v-moskve"><span class="ellip"><span style="white-space: nowrap;">Где</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">как</span> <span style="white-space: nowrap;">можно</span> <span style="white-space: nowrap;">продать</span> <span style="white-space: nowrap;">лофт</span> <span style="white-space: nowrap;">в</span> <span style="white-space: nowrap;">Москве?</span></span></a>
                        </h3>
                        <div class="news__info">
                            <p class="news__date">30.04.2017</p>
                            <p class="news__cat">
                                <a class="news__color button-link" href="lists/news">Новости</a>
                            </p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</section>