<?php
use app\helpers\Phone;
use yii\easyii\modules\text\api\Text;
?>
<footer class="footer">
    <div class="footer-main wrapper">
        <div class="footer-left">
            <a class="footer-logo" href="/">
                <img alt="" src="<?= $asset->baseUrl ?>/img/logo.jpg">
            </a>
        </div>
        <div class="footer-right-wrapper">
            <div class="footer-right">
                <nav class="footer-nav">
                    <ul class="footer-nav__list">
                        <?php foreach (yii\easyii\modules\menu\api\Menu::items('footer') as $item): ?>
                            <li class="footer-nav__item">
                                <a class="footer-nav__link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <div class="footer-phone">
                    <a class="footer-phone__number" href="tel:<?= Phone::link(Text::get('phone1')) ?>"><?= Text::get('phone1') ?></a>
                    <a class="footer-phone__button button-2 js-popup-3" href="#popup-callback" onclick="metrikaReachGoal('callback-footer')">Заказать звонок</a>
                </div>
                <?php /*<p class="footer-copyrights">© 2012-2017 Loft Catalog</p> */ ?>
                <div class="socials-1">
                    <ul class="socials-1__list">
                        <li class="socials-1__item">
                            <a class="socials-1__link socials-1__link_icon-1" href="<?= Text::get('vkontakte') ?>"></a>
                        </li>
                        <li class="socials-1__item">
                            <a class="socials-1__link socials-1__link_icon-2" href="<?= Text::get('facebook') ?>"></a>
                        </li>
                        <li class="socials-1__item">
                            <a class="socials-1__link socials-1__link_icon-3" href="<?= Text::get('instagram') ?>"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>