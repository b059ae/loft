<?php
/** @var $sale \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $filterForm \app\models\SaleFilterForm */
/** @var $cities array */
/** @var $types array */

$this->title = 'Купить недвижимость в Испании. ADA';
$asset = \app\assets\AppAsset::register($this);
?>
<section class="main-page">
<?= $this->render('/layouts/_top', ['asset' => $asset,'filterForm'=>$filterForm,'cities'=>$cities,'types'=>$types]); ?>
<?= $this->render('/layouts/_paid_services', ['asset' => $asset, 'sale'=>$sale]); ?>
<?= $this->render('/layouts/_offers', ['asset' => $asset, 'sale'=>$sale]); ?>
<?= $this->render('/layouts/_free_services', ['asset' => $asset, 'sale'=>$sale]); ?>
<?= $this->render('/layouts/_services', ['asset' => $asset]); ?>
</section>