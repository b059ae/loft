<?php
/** @var $article \yii\easyii\modules\article\api\ArticleObject */
?>
<main class="main">
    <div class="inner-page news">
        <section class="wrapper">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item">
                    <a class="breadcrumbs__link" href="/news">Новости</a>
                </li>
            </ul>
            <h1><?= $article->seo('h1', $article->title) ?></h1>
            <section class="slider-6 service-slide">
                <?= $article->text ?>
            </section>
            <!-- SLIDER EOF -->
        </section>
    </div>
</main>