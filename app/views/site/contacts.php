<?php
/** @var $asset \yii\web\AssetBundle */

/** @var $contactForm \app\models\ContactForm */

use app\helpers\Phone;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Контакты. ADA';
?>
<section class="main-page">
    <div class="contacts">
        <div class="contacts__main js-full-height" style="min-height: 871px;">
            <div class="contacts__content-wrapper wrapper">
                <section class="contacts__content">
                    <div class="contacts__sides">
                        <div class="contacts__block">
                            <h1 class="contacts__title">Контактная информация</h1>
                            <div class="contacts-info">
                                <div class="contacts-info__br"><a class="contacts-info__phone"
                                                                  href="tel:<?= Phone::link(Text::get('phone1')) ?>"><?= Text::get('phone1') ?></a>
                                </div>
                                <div class="contacts-info__br"><a class="contacts-info__link"
                                                                  href="mailto:<?= Text::get('mail2') ?>"><?= Text::get('mail2') ?></a>
                                </div>
                                <!--<p class="contacts-info__loc"><?/*= Text::get('address') */?></p>-->
                            </div>
                        </div>
                        <div class="contacts__block">
                            <?php $form = ActiveForm::begin(['action' => '/send-contact']); ?>
                            <h2 class="contacts__title">Напишите нам</h2>
                            <div class="contacts-form">
                                <div class="contacts-form__item" style="width: 47%; float: left">
                                    <p class="contacts-form__label">Ваше ФИО</p>
                                    <div class="form-group field-objectform-fio">
                                        <?= $form->field($contactForm, 'name')
                                            ->textInput([
                                                'class' => 'window-form__input input-1'
                                            ])->label(false); ?>
                                    </div>
                                </div>
                                <div class="contacts-form__item" style="width: 47%; float: right">
                                    <p class="contacts-form__label">Телефон</p>
                                    <div class="form-group field-objectform-phone required">
                                        <?= $form->field($contactForm, 'phone')
                                            ->widget(\yii\widgets\MaskedInput::className(), [
                                                'mask' => '+7 (999) 999 99 99',
                                            ])->textInput([
                                                'class' => 'window-form__input input-1'
                                            ])->label(false); ?>
                                    </div>
                                </div>
                                <div class="contacts-form__item" style="clear:both;">
                                    <p class="contacts-form__label">Сообщение</p>
                                    <div class="form-group field-objectform-text">
                                        <?= $form->field($contactForm, 'message')
                                            ->textarea([
                                                'class' => 'contacts-form__area textarea-1'
                                            ])->label(false); ?>
                                    </div>
                                </div>
                                <?= Html::submitButton('<span class="button-1__icon">Отправить', ['class' => 'contacts-form__button button-1']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div class="contacts__fix" id="scroll-1"></div>
                </section>
            </div>
        </div>
    </div>
</section>