<?php
/** @var $news \yii\easyii\modules\article\models\Item[] */
$this->title = 'Полезно. ADA';
?>
<main class="main">
    <div class="inner-page">
        <section class="news-page js-full-height tabs" style="min-height: 638px;">
            <div class="news-page__top wrapper">
                <div class="news-page__panel-1">
                    <div class="news-page__left">
                        <h1 class="news-page__title">Полезно</h1>
                    </div>
                </div>
            </div>
            <div class="news-page__content wrapper">
                <div class="tabs-content">
                    <!-- BEGIN TAB -->
                    <div class="deals">
                        <?php foreach ($news as $article): ?>
                        <article class="deals__item col-md-3 col-sm-6 col-xs-12">
                            <a class="stock_link" data-id="1" href="/news-view?id=<?= $article->id ?>">
                                <div class="deals__image"
                                     style="background-image:url('<?= $article->getImage() ?>');background-size: cover;"></div>
                                <div class="deals__content">
                                    <h3 class="deals__title js-ellip-2"><span class="ellip"><?=$article->title ?></span>
                                    </h3>
                                    <p class="deals__text js-ellip-3"><?=$article->short ?></p>
                                </div>
                            </a>
                        </article>
                        <?php endforeach; ?>
                    </div>
                    <!-- TAB EOF -->
                </div>
            </div>
        </section>
    </div>
</main>