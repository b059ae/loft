<?php
/** @var $sale \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $filterForm \app\models\SaleFilterForm */
/** @var $cities array */
/** @var $types array */
/** @var $hide_button string показать или скрыть кнопку */
/** @var $count int страница */
/** @var $maxPrice float */
/** @var $price_max int */
/** @var $price_min int */

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Квартиры на продажу. ADA';
?>
<main class="main">
    <div class="inner-page">
        <div class="rent-page">
            <!-- BEGIN FILTERS -->
            <?php $fForm = ActiveForm::begin(['action'=>'/sale','id'=>'saleForm','options' => ['data-pjax' => true]]); ?>
            <aside class="filter-2 js-fade-wrapper">
                <div class="filter-2__main-wrapper">
                    <div class="filter-2__main clearfix">
                        <div class="filter-2__top">
                            <div class="filter-2__left-wrapper">
                                <div class="filter-2__left">
                                    <div class="filter-item">
                                        <p class="filter-item__label">Город</p>
                                        <div class="filter-item__select select">
                                            <?= $fForm->field($filterForm, 'city')
                                                ->dropDownList($cities,['class' => 'none',
                                                    'onchange'=>"this.form.submit()"])->label(false); ?>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <p class="filter-item__label">Тип недвижимости</p>
                                        <div class="filter-item__select select">
                                        <?= $fForm->field($filterForm, 'type')
                                            ->dropDownList($types,['class' => 'none',
                                                'onchange'=>"this.form.submit()"])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-2__right-wrapper">
                                <div class="filter-2__right fixed">
                                    <div class="filter-2__area">
                                        <div class="filter-item clearfix">
                                            <p class="filter-item__label">Площадь</p>
                                            <div class="filter-item__input-wrapper">
                                                <?= $fForm->field($filterForm, 'min_area')
                                                    ->textInput(
                                                            [
                                                            'class' => 'filter-item__input input-1',
                                                            'placeholder' => 'от',
                                                            'onchange'=>"this.form.submit()"
                                                            ])->label(false); ?>
                                            </div>
                                            <div class="filter-item__input-wrapper">
                                                <?= $fForm->field($filterForm, 'max_area')
                                                    ->textInput(
                                                            [
                                                            'class' => 'filter-item__input input-1',
                                                            'placeholder' => 'до',
                                                            'onchange'=>"this.form.submit()"
                                                            ])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-2__range">
                                        <div class="filter-item filter-switch">
                                            <div class="filter-item__nav">
                                                <p class="filter-item__label">Стоимость</p>
                                            </div>
                                            <div class="filter-switch__content">
                                                <div class="filter-switch__item active" id="range-1">
                                                    <div class="range-1">
                                                        <div class="range-1__top">
                                                            <div class="js-range-1">
                                                            </div>
                                                        </div>
                                                        <div class="range-1__text">
                                                            <div class="range-1__left"><span class="js-range-1-min"></span>&nbsp;€
                                                                <?= $fForm->field($filterForm, 'range_price_min')
                                                                        ->hiddenInput(
                                                                                [
                                                                                        'id' => 'range_price_min',
                                                                                        'class'=>'none'
                                                                                ])->label(false); ?>
                                                            </div>

                                                            <div class="range-1__right"><span
                                                                        class="js-range-1-max"></span>&nbsp;€
                                                                <?= $fForm->field($filterForm, 'range_price_max')
                                                                    ->hiddenInput(
                                                                            [
                                                                                    'id' => 'range_price_max',
                                                                                    'class'=>'none'
                                                                            ])->label(false); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="filter-2__open clearfix">
                        <span style="display: block; cursor:pointer;" class="filter-2__more js-fade-link">
                            <span class="filter-2__icon">Расширенный</span>
                            <span class="filter-2__fix"></span>
                        </span>
                            <div class="filter-2__hide js-fade-hide" style="display: none;">
                                <div class="filter-3">
                                    <div class="filter-3__ui">
                                        <div class="filter-3__col params_fade_if_commerce">
                                            <div class="rent-form__left-1">
                                                <p class="rent-form__label">Кол-во комнат</p>
                                                <ul class="rent-form__radio-1 radio">
                                                <?= $fForm->field($filterForm, 'room',[
                                                    'inputOptions'=> [
                                                        'class' => 'radio__item radio__item_1',
                                                    ],
                                                    ])
                                                    ->radioList([
                                                        1 => '1',
                                                        2 => '2',
                                                        3 => '3',
                                                        4 => '4+',
                                                    ],
                                                        [
                                                            'id' => 'room',
                                                        ])->label(false); ?>
                                                    </ul>
                                            </div>
                                            <div class="rent-form__left-1">
                                                <p class="rent-form__label">Кол-во ванных комнат</p>
                                                <ul class="rent-form__radio-1 radio">
                                                    <?= $fForm->field($filterForm, 'bathroom',[
                                                        'inputOptions'=> [
                                                            'class' => 'radio__item radio__item_1',
                                                        ],
                                                    ])
                                                        ->radioList([
                                                            1 => '1',
                                                            2 => '2',
                                                            3 => '3+',
                                                        ],
                                                            [
                                                                'id' => 'bathroom',
                                                            ])->label(false); ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="rent-form__left-1">
                                            <p class="rent-form__label">Балкон</p>
                                            <ul class="rent-form__radio-1 radio">
                                                <?= $fForm->field($filterForm, 'balkon',[
                                                    'inputOptions'=> [
                                                        'class' => 'radio__item radio__item_1',
                                                    ],
                                                ])
                                                    ->radioList([
                                                        true => 'Есть',
                                                        ''=> 'Неважно',
                                                        false => 'Нет',
                                                    ],
                                                        [
                                                            'id' => 'balkon',
                                                        ])->label(false); ?>
                                            </ul>
                                        </div>
                                        <div class="rent-form__left-1">
                                            <p class="rent-form__label">Бассейн</p>
                                            <ul class="rent-form__radio-1 radio">
                                                <?= $fForm->field($filterForm, 'pool',[
                                                    'inputOptions'=> [
                                                        'class' => 'radio__item radio__item_1',
                                                    ],
                                                ])
                                                    ->radioList([
                                                        true => 'Есть',
                                                        ''=> 'Неважно',
                                                        false => 'Нет',
                                                    ],
                                                        [
                                                            'id' => 'pool',
                                                        ])->label(false); ?>
                                            </ul>
                                        </div>
                                        <div class="rent-form__left-1">
                                            <p class="rent-form__label">Парковка</p>
                                            <ul class="rent-form__radio-1 radio">
                                                <?= $fForm->field($filterForm, 'parking',[
                                                    'inputOptions'=> [
                                                        'class' => 'radio__item radio__item_1',
                                                    ],
                                                ])
                                                    ->radioList([
                                                        true => 'Есть',
                                                        ''=> 'Неважно',
                                                        false => 'Нет',
                                                    ],
                                                        [
                                                            'id' => 'parking',
                                                        ])->label(false); ?>
                                            </ul>
                                        </div>
                                        <div class="rent-form__left-1">
                                            <p class="rent-form__label">Расстояние до моря</p>
                                            <ul class="rent-form__radio-1 radio">
                                                <?= $fForm->field($filterForm, 'sea',[
                                                    'inputOptions'=> [
                                                        'class' => 'radio__item radio__item_1',
                                                    ],
                                                ])
                                                    ->radioList([
                                                        100 => '< 100м',
                                                        ''=> 'Неважно',
                                                        101 => '> 100м',
                                                    ],
                                                        [
                                                            'id' => 'sea',
                                                        ])->label(false); ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="filter-3__buttons clearfix">
                                        <div class="filter-3__button-wrapper">
                                            <button class="filter-3__button button-2" type="submit">показать</button>
                                        </div>
                                        <div class="filter-3__button-wrapper">
                                            <a href="/sale" class="filter-3__button button-6"><span>Сбросить все параметры</span></a>
                                        </div>
                                    </div>
<!--                                    <div class="filter-3__subscribe subscribe clearfix">
                                        <p class="subscribe__text">Присылать мне предложения <br>с указанными
                                            параметрами</p>
                                        <div class="subscribe__form">
                                            <input class="subscribe__input input-2" name="email" type="email"
                                                   placeholder="Укажите ваш e-mail">
                                            <button class="subscribe__button button-3 send_email_result"
                                                    type="submit">
                                                Отправить
                                            </button>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- FILTERS EOF -->

            <section class="rent-page__main">
                <h1 class="rent-page__title cat wrapper">
                    <span>Продажа в Испании</span>
                </h1>
                <!-- BEGIN ACTIONS -->
                <div class="actions wrapper">
                    <div class="actions__main">
                        <div class="actions__sides">
                            <div class="actions__left">
                                <?= $fForm->field($filterForm, 'switch')
                                    ->hiddenInput(
                                        [
                                            'id' => 'switch',
                                            'class'=>'none'
                                        ])->label(false); ?>
                                <ul class="switch">
                                    <li id="switch1" class="switch__item">
                                        <span class="switch__button switch__button_icon-1 js-switch"></span>
                                    </li>
                                    <li id="switch2" class="switch__item">
                                        <span class="switch__button switch__button_icon-2 js-switch"></span>
                                    </li>
                                    <li id="switch3" class="switch__item">
                                        <span class="switch__button switch__button_icon-3 js-switch"></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="actions__right">
                                <div class="select">
                                    <div class="jq-selectbox jqselect"
                                         style="display: inline-block; position: relative; z-index:100">
                                        <?= $fForm->field($filterForm, 'sort')
                                            ->dropDownList([
//                                                'popular' => 'Сначала популярные',
                                                'price ASC' => 'Сначала дешёвые',
                                                'price DESC' => 'Сначала дорогие',
                                            ],
                                                [
                                                    'class' => 'none',
                                                    'onchange'=>"this.form.submit()"
                                                ])->label(false); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ACTIONS EOF -->
                <!-- BEGIN MAP -->
                <div class="rent-map js-float-map"
                     style="height: 376px; margin-top: 0px; position: absolute; top: 0px;">
                    <div class="rent-map__code" id="map">
                        <!-- Здесь код карты с метками -->
                        <div id="map"></div>
<!--                        <script>-->
<!--                            var customLabel = {-->
<!--                                restaurant: {-->
<!--                                    label: 'R'-->
<!--                                },-->
<!--                                bar: {-->
<!--                                    label: 'B'-->
<!--                                }-->
<!--                            };-->
<!---->
<!--                            function initMap() {-->
<!--                                var map = new google.maps.Map(document.getElementById('map'), {-->
<!--                                    center: new google.maps.LatLng(-33.863276, 151.207977),-->
<!--                                    zoom: 12-->
<!--                                });-->
<!--                                var infoWindow = new google.maps.InfoWindow;-->
<!---->
<!--                                // Change this depending on the name of your PHP or XML file-->
<!--                                downloadUrl('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function (data) {-->
<!--                                    var xml = data.responseXML;-->
<!--                                    var markers = xml.documentElement.getElementsByTagName('marker');-->
<!--                                    Array.prototype.forEach.call(markers, function (markerElem) {-->
<!--                                        var name = markerElem.getAttribute('name');-->
<!--                                        var address = markerElem.getAttribute('address');-->
<!--                                        var type = markerElem.getAttribute('type');-->
<!--                                        var point = new google.maps.LatLng(-->
<!--                                            parseFloat(markerElem.getAttribute('lat')),-->
<!--                                            parseFloat(markerElem.getAttribute('lng')));-->
<!---->
<!--                                        var infowincontent = document.createElement('div');-->
<!--                                        var strong = document.createElement('strong');-->
<!--                                        strong.textContent = name-->
<!--                                        infowincontent.appendChild(strong);-->
<!--                                        infowincontent.appendChild(document.createElement('br'));-->
<!---->
<!--                                        var text = document.createElement('text');-->
<!--                                        text.textContent = address-->
<!--                                        infowincontent.appendChild(text);-->
<!--                                        var icon = customLabel[type] || {};-->
<!--                                        var marker = new google.maps.Marker({-->
<!--                                            map: map,-->
<!--                                            position: point,-->
<!--                                            label: icon.label-->
<!--                                        });-->
<!--                                        marker.addListener('click', function () {-->
<!--                                            infoWindow.setContent(infowincontent);-->
<!--                                            infoWindow.open(map, marker);-->
<!--                                        });-->
<!--                                    });-->
<!--                                });-->
<!--                            }-->
<!---->
<!---->
<!--                            function downloadUrl(url, callback) {-->
<!--                                var request = window.ActiveXObject ?-->
<!--                                    new ActiveXObject('Microsoft.XMLHTTP') :-->
<!--                                    new XMLHttpRequest;-->
<!---->
<!--                                request.onreadystatechange = function () {-->
<!--                                    if (request.readyState == 4) {-->
<!--                                        request.onreadystatechange = doNothing;-->
<!--                                        callback(request, request.status);-->
<!--                                    }-->
<!--                                };-->
<!---->
<!--                                request.open('GET', url, true);-->
<!--                                request.send(null);-->
<!--                            }-->
<!---->
<!--                            function doNothing() {-->
<!--                            }-->
<!--                        </script>-->
<!--                        <script async defer-->
<!--                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAC0_tDA3IZiRrSw60CQv-GPP7KpXMLYDs&callback=initMap">-->
<!--                        </script>-->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100115.77976433668!2d-0.5425641492390924!3d38.35781996802403!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6235da3b9dab4b%3A0x1d7da872ac0b81e3!2z0JDQu9C40LrQsNC90YLQtSwg0JjRgdC_0LDQvdC40Y8!5e0!3m2!1sru!2sru!4v1503216873722" width="100%;" height="100%;" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <!-- MAP EOF -->

                <?php Pjax::begin(['id' => 'content']); ?>
                <!-- BEGIN OFFERS -->
                <div class="offers">
                    <div class="offers__list-wrapper">
                        <ul class="offers__list">
                            <?php foreach ($sale as $item): ?>
                                <a class="cat" href="/sale-view?id=<?= $item->id ?>">
                                    <li class="offers__item-wrapper status-2 col-4-4 col-2-2">
                                        <div class="offers__item">
                                            <?php if ($item->popular): ?>
                                                <span class="offers__status offers__status_icon-2">Популярный</span>
                                            <?php endif; ?>
                                            <div class="img-cat"
                                                 style="background: url(<?= $item->getImage() ?>);background-size: cover;"
                                            ">
                                        </div>
                                        <div class="offers__top" role="toolbar">
                                            <a class="offers__text" href="/sale-view?id=<?= $item->id ?>">
                                                <h4 class="offers__name"><?= $item->type ?></h4>
                                                <div class="offers__info">
                                                    <p class="offers__price"><?= $item->getPrice() ?> €</p>
                                                    <p class="offers__metre"><?= $item->square ?> м²</p>
                                                </div>
                                            </a>
                                        </div>
                                        <a class="offers__bottom" href="/sale-view?id=<?= $item->id ?>">
                                            <p class="offers__loc" style="height: 32px;">
                                                <?= $item->title ?> </p>

                                            <p class="offers__cat" style="height: 15px;">
                                                <?= $item->city ?> </p>
                                        </a>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php if ($hide_button): ?>
                        <div class="page-nav">
                            <div class="page-nav__main">
                                <?= $fForm->field($filterForm, 'pageCount')
                                    ->hiddenInput(
                                        [
                                            'id' => 'pageCount',
                                            'class'=>'none',
                                            'value' => $count
                                        ])->label(false); ?>
                                <button type="submit" class="page-nav__more button-2" onclick="showMore();">Показать еще</button>
<!--                                <a class="page-nav__more button-2" onclick="showMore();">Показать еще</a>-->
                            </div>
                        </div>
                    <?php endif; ?>
                    <?= $this->render('/layouts/_show_more', ['url' => 'sale']) ?>
                    <?php Pjax::end(); ?>
                    <?php ActiveForm::end(); ?>
                </div>
        </div>
        <!-- OFFERS EOF -->
        </section>

        <!-- BEGIN SEO
        <article class="seo">
            <div class="seo__main">
                <h2 dir="ltr" style="text-align: center;">О покупке недвижимости за границей и не только...</h2>
                Текст статьи
            </div>
        </article>
         SEO EOF -->
    </div>
    </div>
</main>

<?php
$script = <<< JS
jQuery(document).ready(function () {
    var full_accordeon_height = $('.accordeon_block_inner').height() + 10;
    if (full_accordeon_height > 300) {
        $('.accordeon_block_inner').height(full_accordeon_height / 3.1);
    } else {
        $('section.accordeon_block .button-2').hide();
    }
    $('section.accordeon_block .button-2').on('click', function () {
        if ($('.accordeon_block_inner').height() == full_accordeon_height) {
            $('.accordeon_block_inner').animate({'height': full_accordeon_height / 3.1}, 600)
        } else {
            $('.accordeon_block_inner').animate({'height': full_accordeon_height}, 600)
        }
        return false;
    });

    $('.tabs-nav__link').on('click', function () {
        $('.tabs-nav__item').removeClass('active');
        $(this).parent().addClass('active');
        $('[name="cat_p"]').val($(this).data('url'));
        if ($(this).data('url') == 'rent' || $(this).data('url') == 'long-term-rent') {
            $('.filter-item__label .month').show();

            $(".js-range-1")[0].destroy();
            $(".js-range-1").noUiSlider({
                connect: true,
                start: [2700.00, 19546490.00],
                range: {
                    'min': 2700.00,
                    'max': 19546490.00
                },
                step: 1000000
            });

            $(".js-range-2")[0].destroy();
            $(".js-range-2").noUiSlider({
                connect: true,
                start: [637.29, 102367.00],
                range: {
                    'min': 637.29,
                    'max': 102367.00
                },
                step: 10000
            });
        } else {
            $('.filter-item__label .month').hide();

            $(".js-range-1")[0].destroy();
            $(".js-range-1").noUiSlider({
                connect: true,
                start: [0.00, 1000000],
                range: {
                    'min': 0.00,
                    'max': 1000000
                },
                step: 10000
            });

            $(".js-range-2")[0].destroy();
            $(".js-range-2").noUiSlider({
                connect: true,
                start: [22000.00, 4326671.00],
                range: {
                    'min': 22000.00,
                    'max': 4326671.00
                },
                step: 10000
            });
        }
        return false;
    });
    
    var min = Number('$price_min');
    var max = Number('$price_max');
    var range1 = $(".js-range-1").length;
    if (range1 > 0) {
        $(".js-range-1").noUiSlider({
            connect: true,
            range: {'min': 0.00, 'max': 1000000},
            step: 10000,
            start: [min, max]
        });
        $(".js-range-1").Link('lower').to($(".js-range-1-min"), null, wNumb({decimals: 0, thousand: ' '}));
        $(".js-range-1").Link('lower').to($("#range_price_min"), null, wNumb({decimals: 0, thousand: ' '}));

        $(".js-range-1").Link('upper').to($(".js-range-1-max"), null, wNumb({decimals: 0, thousand: ' '}));
        $(".js-range-1").Link('upper').to($("#range_price_max"), null, wNumb({decimals: 0, thousand: ' '}));
    }   
    $(".noUi-handle").mousedown(function() {
        $(window).mouseup(function() {
            $("#saleForm").submit();
        });
    });
    
    // switch
    switch("$filterForm->switch"){
        case 'switch1':
            $("#switch1").addClass("active");
            $(".rent-page__main").removeClass("fixed-1").addClass("fixed-2");
            var winH = $(window).height();
            var filterH = $(".filter-2").height();
            var newH = winH - filterH;
            $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", newH + "px");
            $(".rent-map__code").html(maps);
            if ($("body").hasClass("ios")) {
                var winFix = newH + 25;
                $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", newH + "px");
                $(".rent-map__code").html(maps);
            }
            break;            
        case 'switch2':
            $("#switch2").addClass("active");
            $(".rent-page__main").removeClass("fixed-1").removeClass("fixed-2");
            break;            
        case 'switch3':
            $("#switch3").addClass("active");
            $(".rent-page__main").removeClass("fixed-2").addClass("fixed-1");
            break;
    }
    
    $(".js-switch").click(function () {
        if ($(this).closest(".switch__item").hasClass("active")) {
            return
        }
        $(".switch__item").removeClass("active");
        $(this).closest(".switch__item").addClass("active");
        $("#switch").val($(this).closest(".switch__item").attr('id'));
        if ($(this).hasClass("switch__button_icon-1")) {
            $(".rent-page__main").removeClass("fixed-1").addClass("fixed-2");
            var winH = $(window).height();
            var filterH = $(".filter-2").height();
            var newH = winH - filterH;
            $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", newH + "px");
            $(".rent-map__code").html(maps);
            if ($("body").hasClass("ios")) {
                var winFix = newH + 25;
                $(".body-1 .rent-page__main.fixed-2 .rent-map").css("height", newH + "px");
                $(".rent-map__code").html(maps);
            }
        }
        if ($(this).hasClass("switch__button_icon-2")) {
            $(".rent-page__main").removeClass("fixed-1").removeClass("fixed-2");
        }
        if ($(this).hasClass("switch__button_icon-3")) {
            $(".rent-page__main").removeClass("fixed-2").addClass("fixed-1");
            $(".rent-map__code").html(maps);
        }
    })
// switch
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#show-more").on("pjax:end", function() {
            $.pjax.reload({container:"#show-more"});
        });
    });'
);
?>
