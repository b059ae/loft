<?php
/** @var $asset \yii\web\AssetBundle */
/** @var $sendForm \app\models\SendForm */

use app\helpers\Phone;
use yii\easyii\modules\text\api\Text;
use yii\widgets\ActiveForm;

/** @var $longTermRent \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $object  */
?>
<section class="object-page">
    <div class="object-top">
        <!-- BEGIN OBJECT INFO -->
        <div class="object-info">
            <div class="object-info__main object-info__main_fixed">
                <div class="object-info__top">
                    <ul class="breadcrumbs">
                        <li class="breadcrumbs__item">
                            <a class="breadcrumbs__link" href="/long-term-rent">Длительная аренда</a>
                        </li>
                        <li class="breadcrumbs__item">
                            <a class="breadcrumbs__link" href="/long-term-rent-view?id=<?= $object->id?>"><?= $object->type?></a>
                        </li>
                    </ul>
                    <h2 class="object-info__title"><?= $object->title?></h2>
                </div>
                <ul class="object-about">
                    <li class="object-about__item">
                        <p class="object-about__char"><?= $object->price?> €</p>
                        <p class="object-about__label">стоимость</p>
                    </li>
                    <li class="object-about__item">
                        <p class="object-about__char"><?= $object->square?> м²</p>
                        <p class="object-about__label">площадь</p>
                    </li>
                    <li class="object-about__item">
                        <p class="object-about__char"><?= $object->room?></p>
                        <p class="object-about__label">комнат</p>
                    </li>
                </ul>
                <ul class="object-params">
                    <li class="object-params__item object-params__item_inline">
                        <p class="object-params__icon object-params__icon_6">Ванные: <?= $object->bathroom?></p>
                    </li>
                    <li class="object-params__item object-params__item_inline">
                        <p class="object-params__icon object-params__icon_6">Балконы: <?= $object->balkon?></p>
                    </li>
                    <li class="object-params__item object-params__item_inline">
                        <p class="object-params__icon object-params__icon_7">Бассейны: <?= $object->pool?></p>
                    </li>
                    <li class="object-params__item object-params__item_inline">
                        <p class="object-params__icon object-params__icon_3">Парковка: <?= $object->parking?></p>
                    </li>
                </ul>
                <div class="object-info__buttons">
                    <div class="object-info__button-wrapper">
                        <a class="object-info__button button-1 button-big js-popup-3" href="#popup-3">Оставить
                            заявку</a>
                    </div>
                </div>
            </div>
            <a class="object-info__scroll button-scroll js-scroll" href="#scroll-1"></a>
        </div>
        <!-- OBJECT INFO EOF -->

        <!-- BEGIN OBJECT SLIDER -->
        <div class="object-slider slider-3">
            <ul class="js-slider-3">
                <?php $photos = $object->getPhotos(); ?>
                <?php foreach ($photos as $photo): ?>
                    <div>
                        <a href="/uploads/<?= $photo->model->image_file?>" tabindex="-1">
                            <img src="/uploads/<?= $photo->model->image_file?>" alt=""/>
                        </a>
                    </div>
                <?php endforeach; ?>
            </ul>
        </div>
        <!-- OBJECT SLIDER EOF -->
    </div>
    <!-- BEGIN OBJECT CENTER -->
    <div class="object-center" id="scroll-1">
        <div class="object-center__sides wrapper clearfix">
            <!-- BEGIN OBJECT LEFT -->
            <article class="object-left">
                <div class="object-left__main">
                    <section class="accordeon_block">
                        <div class="accordeon_block_inner">
                            <p><span class="h2">Преимущества</span></p>
                            <?= $object->description ?>
                        </div>
                        <a class="button-2" href="#" tabindex="0">Показать</a>
                    </section>
                </div>
            </article>
        </div>
        <!-- BEGIN OBJECT FORM -->
        <div class="form-2 wrapper">
            <div class="form-2__main">
                <div class="form-2__fix" id="scroll-2"></div>
                <div class="form-2__sides clearfix">
                    <div class="form-2__left">
                        <p class="form-2__text-1">Запишитесь на просмотр</p>
                    </div>
                    <?php $form = ActiveForm::begin(['action'=>'/send']); ?>
                    <div class="form-2__right-wrapper">
                        <div class="form-2__right">
                            <div class="form-2__inputs-wrapper">
                                <div class="form-2__inputs">
                                    <div class="form-2__item">
                                        <?= $form->field($sendForm, 'name')
                                            ->textInput(
                                                [
                                                    'class' => 'form-2__input input-2',
                                                    'placeholder' => "Ваше имя"
                                                ])->label(false); ?>
                                    </div>
                                    <div class="form-2__item">
                                        <?= $form->field($sendForm, 'phone')
                                            ->widget(\yii\widgets\MaskedInput::className(), [
                                                'mask' => '+7(999)-999-9999',
                                            ])
                                            ->textInput(
                                                [
                                                    'class' => 'form-2__input input-2',
                                                    'placeholder' => "Телефон"
                                                ])->label(false); ?>
                                    </div>
                                    <div class="form-2__item datepicker">
                                        <?= $form->field($sendForm, 'date')
                                            ->input('date',['class' => 'form-2__input input-2'])->label(false); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group field-objectform-url">

                                <?= $form->field($sendForm, 'date')
                                    ->hiddenInput([
                                        'value'=> '/sale-view?id='.$object->id,
                                        'class' => 'form-2__input input-2'
                                    ])->label(false); ?>
                            </div>
                            <div class="form-2__button-wrapper">
                                <button class="form-2__button button-1" type="submit">
                                    <span class="button-1__icon">Отправить</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <p class="form-3__text">Или позвоните по телефону <a class="header-phone__number" href="tel:<?= Phone::link(Text::get('phone')) ?>"><?= Text::get('phone') ?></a></p>
            </div>
        </div>
        <!-- OBJECT FORM EOF -->
    </div>
    <!-- OBJECT CENTER EOF -->
    <!-- BEGIN OFFERS -->
    <section class="object-offers offers">
        <div class="offers__main wrapper">
            <h3 class="offers__title main-title">Похожие предложения</h3>
            <ul class="offers__list offers__list_fixed">
                <?php foreach ($longTermRent as $item): ?>
                    <a class="cat" href="/long-term-rent-view?id=<?= $item->id?>">
                        <li class="offers__item-wrapper  col-4-4 col-2-2">
                            <div class="offers__item">
                                <div class="offers__top">
                                    <div class="img-cat" style="background: url(<?= $item->getImage()?>);background-size: cover;">
                                    </div>
                                    <div class="offers__text" href="">
                                        <h4 class="offers__name"><?= $item->type?></h4>
                                        <div class="offers__info">
                                            <p class="offers__price"><?= $item->getPrice() ?> руб.</p>
                                            <p class="offers__metre"><?= $item->square ?> м²</p>
                                        </div>
                                    </div>
                                </div>
                                <a class="offers__bottom">
                                    <p class="offers__loc" style="height: 32px;">
                                        <?= $item->title ?>
                                    </p>

                                    <p class="offers__cat" style="height: 15px;">
                                        <?= $item->city ?>
                                    </p>
                                </a>
                            </div>
                        </li>
                    </a>
                <?php endforeach; ?>
            </ul>
            <a class="main-button-1 button-2" href="/long-term-rent">Искать другие</a>
        </div>
    </section>
    <!-- OFFERS EOF -->
</section>