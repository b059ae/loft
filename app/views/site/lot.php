<main class="main">
    <div class="inner-page">
        <section class="ad-page">
            <div class="ad-page__top wrapper">
                <ul class="breadcrumbs">
                    <li class="breadcrumbs__item">
                        <a class="breadcrumbs__link" href="/lot">Разместить недвижимость</a>
                    </li>
                </ul>
                <h1 class="ad-page__title">Объявление о продаже/аренде</h1>
                <p>Заполните, пожалуйста, данные для размещения вашего объекта на сайте. <br>После этого с вами свяжется менеджер для уточнения всех деталей</p>
            </div>

            <form id="w0" action="/lot" method="post">
                <input type="hidden" name="_csrf-frontend" value="QnhCdGpFeUMzFAg1DDwobjcsMkQzaCYRFQg6BVwkKgAIL3UYPR0RMg==">            <div class="ad-page__content-wrapper wrapper">
                    <div class="ad-page__content">
                        <div class="rent-ads">
                            <ol class="rent-ads__list js-form-1">
                                <li class="rent-ads__item">
                                    <h2 class="rent-ads__title">Контактная информация владельца</h2>
                                    <div class="rent-form">
                                        <div class="rent-form__col clearfix">
                                            <div class="rent-form__item">
                                                <p class="rent-form__label">Ваше ФИО</p>
                                                <input class="rent-form__input input-1" name="LotForm[fio]" value="" type="text" placeholder="Укажите ФИО">
                                            </div>
                                            <div class="rent-form__item">
                                                <p class="rent-form__label">Телефон</p>
                                                <input class="rent-form__input input-1" name="LotForm[phone]" value="" type="tel" placeholder="Укажите номер телефона">
                                            </div>
                                            <div class="rent-form__item">
                                                <p class="rent-form__label">email</p>
                                                <input class="rent-form__input input-1" type="email" name="LotForm[email]" value="" placeholder="Укажите email">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="rent-ads__item">
                                    <h2 class="rent-ads__title">Информация об объекте</h2>
                                    <div class="rent-form">
                                        <div class="rent-form__col clearfix">
                                            <div class="rent-form__left-1">
                                                <div class="rent-form__item">
                                                    <p class="rent-form__label">Тип предложения</p>
                                                    <ul class="rent-form__radio-1 radio">
                                                        <li class="radio__item radio__item_1 active">
                                                            <label>
                                                                <input type="radio" name="LotForm[lot_type]" value="1" checked="" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Продажа
                                                            </label>
                                                        </li>
                                                        <li class="radio__item radio__item_1">
                                                            <label>
                                                                <input type="radio" name="LotForm[lot_type]" value="2" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Сдача в аренду
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="rent-form__right-1">
                                                <div class="rent-form__item">
                                                    <p class="rent-form__label">Тип недвижимости</p>
                                                    <div class="rent-form__select select">
                                                        <div class="jq-selectbox jqselect" style="display: inline-block; position: relative; z-index:100"><select name="LotForm[commerce_type]" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                                                                <option value="1">Апартаменты</option>
                                                                <option value="2">Квартира</option>
                                                                <option value="3">Мансарда</option>
                                                                <option value="4">Пентхаус</option>
                                                                <option value="5">Таунхаус</option>
                                                                <option value="6">Офис</option>
                                                                <option value="7">Коммерческая</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rent-form__col clearfix">
                                            <div class="rent-form__left-2-wrapper">
                                                <div class="rent-form__left-2">
                                                    <div class="rent-form__item">
                                                        <p class="rent-form__label">Адрес объекта</p>
                                                        <input class="rent-form__input input-1" type="text" name="LotForm[address]" value="" placeholder="Укажите адрес объекта">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rent-form__right-2">
                                                <div class="rent-form__item">
                                                    <p class="rent-form__label">Площадь (м²)</p>
                                                    <input class="rent-form__input input-1" name="LotForm[total_area]" value="" type="text">
                                                </div>
                                                <div class="rent-form__item">
                                                    <p class="rent-form__label">Стоимость (€)</p>
                                                    <input class="rent-form__input input-1" name="LotForm[total_price]" value="" type="text" placeholder="Укажите стоимость">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rent-form__col clearfix">
                                            <div class="rent-form__left-2-wrapper">
                                                <div class="rent-form__left-2 clearfix">
                                                    <div class="rent-form__item rent-form__item_50">
                                                        <p class="rent-form__label">Количество спален</p>
                                                        <div class="rent-form__select select">
                                                            <div class="jq-selectbox jqselect" style="display: inline-block; position: relative; z-index:100"><select name="LotForm[bedrooms]" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                                                                    <option value="0">Выберите из списка</option>
                                                                    <option value="1">Студия</option>
                                                                    <option value="2">1</option>
                                                                    <option value="3">2</option>
                                                                    <option value="4">3</option>
                                                                    <option value="5">4+</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="rent-form__item rent-form__item_50">
                                                        <p class="rent-form__label">Количество ванных комнат</p>
                                                        <div class="rent-form__select select">
                                                            <div class="jq-selectbox jqselect" style="display: inline-block; position: relative; z-index:100"><select name="LotForm[bathrooms]" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                                                                    <option value="0">Выберите из списка</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4+</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rent-form__right-2">
                                                <div class="rent-form__item">
                                                    <p class="rent-form__label">Этаж</p>
                                                    <input class="rent-form__input input-1" name="LotForm[floor]" value="" type="text" placeholder="Укажите">
                                                </div>
                                                <div class="rent-form__item">
                                                    <p class="rent-form__label">Количество уровней</p>
                                                    <ul class="rent-form__radio-2 radio">
                                                        <li class="radio__item radio__item_2 active">
                                                            <label>
                                                                <input type="radio" name="LotForm[levels]" value="1" checked="" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                               Один
                                                            </label>
                                                        </li>
                                                        <li class="radio__item radio__item_2">
                                                            <label>
                                                                <input type="radio" name="LotForm[levels]" value="1" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Два
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rent-form__col clearfix">
                                            <div class="rent-form__left-2-wrapper">
                                                <div class="rent-form__left-2 clearfix">
                                                    <div class="rent-form__item rent-form__item_50">
                                                        <p class="rent-form__label">Наличие парковки</p>
                                                        <div class="rent-form__select select">
                                                            <div class="jq-selectbox jqselect" style="display: inline-block; position: relative; z-index:100"><select name="LotForm[parking_type]" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                                                                    <option value="0">Выберите парковку</option>
                                                                    <option value="1">Наземная</option>
                                                                    <option value="2">Подземная</option>
                                                                    <option value="3">Охраняемая</option>
                                                                    <option value="4">Стихийная</option>
                                                                    <option value="5">Нет</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="rent-form__item rent-form__item_50">
                                                        <p class="rent-form__label">Наличие балкона</p>
                                                        <ul class="rent-form__radio-2 rent-form__radio-2_fixed radio">
                                                            <li class="radio__item radio__item_2">
                                                                <label>
                                                                    <input type="radio" name="LotForm[balcony]" value="1" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                    Балкон
                                                                </label>
                                                            </li>
                                                            <li class="radio__item radio__item_2">
                                                                <label>
                                                                    <input type="radio" name="LotForm[balcony]" value="2" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                    Лоджия
                                                                </label>
                                                            </li>
                                                            <li class="radio__item radio__item_2 active">
                                                                <label>
                                                                    <input type="radio" name="LotForm[balcony]" value="3" checked="" style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                    Нет
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rent-form__right-2">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="rent-form__button button-1" type="submit">
                                        <span class="button-1__icon">Отправить заявку</span>
                                    </button>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </form>        </section>
    </div>
</main>