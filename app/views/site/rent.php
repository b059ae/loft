<?php
/** @var $rent \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var $filterForm \app\models\SaleFilterForm */
/** @var $sortForm \app\models\SaleFilterForm */
/** @var $cities array */
/** @var $types array */
/** @var $hide_button string показать или скрыть кнопку */
/** @var $count int страница */

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

?>
<main class="main">
    <div class="inner-page">
        <div class="rent-page">
            <!-- BEGIN FILTERS -->
            <?php $form = ActiveForm::begin(['action'=>'/rent']); ?>
            <aside class="filter-2 js-fade-wrapper">
                <div class="filter-2__main-wrapper">
                    <div class="filter-2__main clearfix">
                        <div class="filter-2__top">
                            <div class="filter-2__left-wrapper">
                                <div class="filter-2__left">
                                    <div class="filter-item">
                                        <p class="filter-item__label">Город</p>
                                        <div class="filter-item__select select">
                                            <?= $form->field($filterForm, 'city')
                                                ->dropDownList($cities,['class' => 'none',
                                                    'onchange'=>"this.form.submit()"])->label(false); ?>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <p class="filter-item__label">Тип недвижимости</p>
                                        <div class="filter-item__select select">
                                        <?= $form->field($filterForm, 'type')
                                            ->dropDownList($types,['class' => 'none',
                                                'onchange'=>"this.form.submit()"])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-2__right-wrapper">
                                <div class="filter-2__right fixed">
                                    <div class="filter-2__area">
                                        <div class="filter-item clearfix">
                                            <p class="filter-item__label">Площадь</p>
                                            <div class="filter-item__input-wrapper">
                                                <?= $form->field($filterForm, 'min_area')
                                                    ->textInput(
                                                        [
                                                            'class' => 'filter-item__input input-1',
                                                            'placeholder' => 'от',
                                                            'onchange'=>"this.form.submit()"
                                                        ])->label(false); ?>
                                            </div>
                                            <div class="filter-item__input-wrapper">
                                                <?= $form->field($filterForm, 'max_area')
                                                    ->textInput(
                                                        [
                                                            'class' => 'filter-item__input input-1',
                                                            'placeholder' => 'до',
                                                            'onchange'=>"this.form.submit()"
                                                        ])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-2__range">
                                        <div class="filter-item filter-switch">
                                            <div class="filter-item__nav">
                                                <p class="filter-item__label">Стоимость</p>
                                            </div>
                                            <div class="filter-switch__content">
                                                <div class="filter-switch__item active" id="range-1">
                                                    <div class="range-1">
                                                        <div class="range-1__top">
                                                            <div class="js-range-1">
                                                            </div>
                                                        </div>
                                                        <div class="range-1__text">
                                                            <div class="range-1__left"><span class="js-range-1-min"></span>&nbsp;€
                                                                <?= $form->field($filterForm, 'range_price_min')
                                                                    ->hiddenInput(
                                                                        [
                                                                            'id' => 'range_price_min',
                                                                            'class'=>'none',
                                                                            'onchange'=>"this.form.submit()"
                                                                        ])->label(false); ?>
                                                            </div>

                                                            <div class="range-1__right"><span
                                                                        class="js-range-1-max"></span>&nbsp;€
                                                                <?= $form->field($filterForm, 'range_price_max')
                                                                    ->hiddenInput(
                                                                        [
                                                                            'id' => 'range_price_max',
                                                                            'class'=>'none',
                                                                            'onchange'=>"this.form.submit()"
                                                                        ])->label(false); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="filter-2__open clearfix">
                        <span style="display: block; cursor:pointer;" class="filter-2__more js-fade-link">
                            <span class="filter-2__icon">Расширенный</span>
                            <span class="filter-2__fix"></span>
                        </span>
                            <div class="filter-2__hide js-fade-hide" style="display: none;">
                                <div class="filter-3">
                                    <div class="filter-3__ui">
                                        <div class="filter-3__col params_fade_if_commerce">
                                            <div class="filter-3__cell">
                                                <div class="filter-3__fix">
                                                    <p class="filter-3__label">Кол-во комнат</p>
                                                    <ul class="filter-3__list filter-3__list_size-3 checkbox">
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="rooms[]" value="13" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                1</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="rooms[]" value="14" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                2</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="rooms[]" value="15" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                3</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="rooms[]" value="16" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                4+</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="filter-3__cell">
                                                <div class="filter-3__fix">
                                                    <p class="filter-3__label">Кол-во ванных комнат</p>
                                                    <ul class="filter-3__list checkbox">
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="bathrooms[]" value="18" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                1</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="bathrooms[]" value="19" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                2</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="bathrooms[]" value="20" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                3</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="bathrooms[]" value="21" type="checkbox"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                4+</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter-3__col">
                                            <div class="filter-3__cell">
                                                <div class="filter-3__fix">
                                                    <p class="filter-3__label">Балкон</p>
                                                    <ul class="filter-3__list filter-3__list_size-1 radio">
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="countLevels[]" value="22"
                                                                       type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Есть</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="countLevels[]" value="23"
                                                                       type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Нет</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="filter-3__cell">
                                                <div class="filter-3__fix">
                                                    <p class="filter-3__label">Бассейн</p>
                                                    <ul class="filter-3__list filter-3__list_size-1 radio">
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="balcony[]" value="24" type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Есть</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="balcony[]" value="25" type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Нет</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="filter-3__cell">
                                                <div class="filter-3__fix">
                                                    <p class="filter-3__label">Парковка</p>
                                                    <ul class="filter-3__list filter-3__list_size-1 radio">
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="parking[]" value="26" type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Есть</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="parking[]" value="27" type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                Нет</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="filter-3__cell">
                                                <div class="filter-3__fix">
                                                    <p class="filter-3__label">Расстояние до моря</p>
                                                    <ul class="filter-3__list filter-3__list_size-1 radio">
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="parking[]" value="26" type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                <100м</label>
                                                        </li>
                                                        <li class="checkbox__item checkbox__item_3">
                                                            <label>
                                                                <input name="parking[]" value="27" type="radio"
                                                                       style="position: absolute; z-index: -1; opacity: 0; margin: 0px; padding: 0px;">
                                                                >100м</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-3__buttons clearfix">
                                        <div class="filter-3__button-wrapper">
                                            <a href="" class="filter-3__button button-2" type="submit">показать
                                                <span
                                                        class="ajax_list_count"></span></a>
                                        </div>
                                        <div class="filter-3__button-wrapper">
                                            <a href="/rent" class="filter-3__button button-6"><span>Сбросить все параметры</span></a>
                                        </div>
                                    </div>
                                    <div class="filter-3__subscribe subscribe clearfix">
                                        <p class="subscribe__text">Присылать мне предложения <br>с указанными
                                            параметрами</p>
                                        <div class="subscribe__form">
                                            <input class="subscribe__input input-2" name="email" type="email"
                                                   placeholder="Укажите ваш e-mail">
                                            <button class="subscribe__button button-3 send_email_result"
                                                    type="submit">
                                                Отправить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <?php ActiveForm::end(); ?>
            <!-- FILTERS EOF -->

            <section class="rent-page__main">
                <h1 class="rent-page__title cat wrapper">
                    <span>Аренда в Испании</span>
                </h1>
                <!-- BEGIN ACTIONS -->
                <div class="actions wrapper">
                    <div class="actions__main">
                        <div class="actions__sides">
                            <div class="actions__left">
                                <ul class="switch">
                                    <li class="switch__item">
                                        <span class="switch__button switch__button_icon-1 js-switch"></span>
                                    </li>
                                    <li class="switch__item active">
                                        <span class="switch__button switch__button_icon-2 js-switch"></span>
                                    </li>
                                    <li class="switch__item">
                                        <span class="switch__button switch__button_icon-3 js-switch"></span>
                                    </li>
                                </ul>
                            </div>
                            <?php $form = ActiveForm::begin(['action'=>'/rent']); ?>
                            <div class="actions__right">
                                <div class="select">
                                    <div class="jq-selectbox jqselect"
                                         style="display: inline-block; position: relative; z-index:100">
                                        <?= $form->field($sortForm, 'sort')
                                            ->dropDownList([
//                                                'popular' => 'Сначала популярные',
                                                'price ASC' => 'Сначала дешёвые',
                                                'price DESC' => 'Сначала дорогие',
                                            ],
                                                [
                                                    'class' => 'none',
                                                    'onchange'=>"this.form.submit()"
                                                ])->label(false); ?>
                                    </div>

                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
                <!-- ACTIONS EOF -->
                <!-- BEGIN MAP -->
                <div class="rent-map js-float-map"
                     style="height: 376px; margin-top: 0px; position: absolute; top: 0px;">
                    <div class="rent-map__code" id="map">
                        <!-- Здесь код карты с метками -->
                        <div id="map"></div>
                        <!--                        <script>-->
                        <!--                            var customLabel = {-->
                        <!--                                restaurant: {-->
                        <!--                                    label: 'R'-->
                        <!--                                },-->
                        <!--                                bar: {-->
                        <!--                                    label: 'B'-->
                        <!--                                }-->
                        <!--                            };-->
                        <!---->
                        <!--                            function initMap() {-->
                        <!--                                var map = new google.maps.Map(document.getElementById('map'), {-->
                        <!--                                    center: new google.maps.LatLng(-33.863276, 151.207977),-->
                        <!--                                    zoom: 12-->
                        <!--                                });-->
                        <!--                                var infoWindow = new google.maps.InfoWindow;-->
                        <!---->
                        <!--                                // Change this depending on the name of your PHP or XML file-->
                        <!--                                downloadUrl('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function (data) {-->
                        <!--                                    var xml = data.responseXML;-->
                        <!--                                    var markers = xml.documentElement.getElementsByTagName('marker');-->
                        <!--                                    Array.prototype.forEach.call(markers, function (markerElem) {-->
                        <!--                                        var name = markerElem.getAttribute('name');-->
                        <!--                                        var address = markerElem.getAttribute('address');-->
                        <!--                                        var type = markerElem.getAttribute('type');-->
                        <!--                                        var point = new google.maps.LatLng(-->
                        <!--                                            parseFloat(markerElem.getAttribute('lat')),-->
                        <!--                                            parseFloat(markerElem.getAttribute('lng')));-->
                        <!---->
                        <!--                                        var infowincontent = document.createElement('div');-->
                        <!--                                        var strong = document.createElement('strong');-->
                        <!--                                        strong.textContent = name-->
                        <!--                                        infowincontent.appendChild(strong);-->
                        <!--                                        infowincontent.appendChild(document.createElement('br'));-->
                        <!---->
                        <!--                                        var text = document.createElement('text');-->
                        <!--                                        text.textContent = address-->
                        <!--                                        infowincontent.appendChild(text);-->
                        <!--                                        var icon = customLabel[type] || {};-->
                        <!--                                        var marker = new google.maps.Marker({-->
                        <!--                                            map: map,-->
                        <!--                                            position: point,-->
                        <!--                                            label: icon.label-->
                        <!--                                        });-->
                        <!--                                        marker.addListener('click', function () {-->
                        <!--                                            infoWindow.setContent(infowincontent);-->
                        <!--                                            infoWindow.open(map, marker);-->
                        <!--                                        });-->
                        <!--                                    });-->
                        <!--                                });-->
                        <!--                            }-->
                        <!---->
                        <!---->
                        <!--                            function downloadUrl(url, callback) {-->
                        <!--                                var request = window.ActiveXObject ?-->
                        <!--                                    new ActiveXObject('Microsoft.XMLHTTP') :-->
                        <!--                                    new XMLHttpRequest;-->
                        <!---->
                        <!--                                request.onreadystatechange = function () {-->
                        <!--                                    if (request.readyState == 4) {-->
                        <!--                                        request.onreadystatechange = doNothing;-->
                        <!--                                        callback(request, request.status);-->
                        <!--                                    }-->
                        <!--                                };-->
                        <!---->
                        <!--                                request.open('GET', url, true);-->
                        <!--                                request.send(null);-->
                        <!--                            }-->
                        <!---->
                        <!--                            function doNothing() {-->
                        <!--                            }-->
                        <!--                        </script>-->
                        <!--                        <script async defer-->
                        <!--                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAC0_tDA3IZiRrSw60CQv-GPP7KpXMLYDs&callback=initMap">-->
                        <!--                        </script>-->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100115.77976433668!2d-0.5425641492390924!3d38.35781996802403!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6235da3b9dab4b%3A0x1d7da872ac0b81e3!2z0JDQu9C40LrQsNC90YLQtSwg0JjRgdC_0LDQvdC40Y8!5e0!3m2!1sru!2sru!4v1503216873722" width="100%;" height="100%;" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <!-- MAP EOF -->

                <!-- BEGIN OFFERS -->
                <div class="offers">
                    <?php Pjax::begin(['id' => 'show-more']); ?>
                    <div class="offers__list-wrapper">
                        <ul class="offers__list">
                            <?php foreach ($rent as $item): ?>
                                <a class="cat" href="/rent-view?id=<?= $item->id ?>">
                                    <li class="offers__item-wrapper status-2 col-4-4 col-2-2">
                                        <div class="offers__item">
                                            <?php if ($item->popular): ?>
                                                <span class="offers__status offers__status_icon-2">Популярный</span>
                                            <?php endif; ?>
                                            <div class="img-cat"
                                                 style="background: url(<?= $item->getImage() ?>);background-size: cover;"
                                            ">
                                        </div>
                                        <div class="offers__top" role="toolbar">
                                            <a class="offers__text" href="/rent-view?id=<?= $item->id ?>">
                                                <h4 class="offers__name"><?= $item->type ?></h4>
                                                <div class="offers__info">
                                                    <p class="offers__price"><?= $item->getPrice() ?> €</p>
                                                    <p class="offers__metre"><?= $item->square ?> м²</p>
                                                </div>
                                            </a>
                                        </div>
                                        <a class="offers__bottom" href="/rent-view?id=<?= $item->id ?>">
                                            <p class="offers__loc" style="height: 32px;">
                                                <?= $item->title ?> </p>

                                            <p class="offers__cat" style="height: 15px;">
                                                <?= $item->city ?> </p>
                                        </a>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php Pjax::end(); ?>
                    <?php if ($hide_button): ?>
                        <div class="page-nav">
                            <div class="page-nav__main">
                                <?php $form = ActiveForm::begin(); ?>
                                <a class="page-nav__more button-2" data-count="2" onclick="showMore();">Показать
                                    еще</a>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
        </div>
        <!-- OFFERS EOF -->
        </section>

        <!-- BEGIN SEO -->
        <article class="seo">
            <div class="seo__main">
                <h2 dir="ltr" style="text-align: center;">О покупке недвижимости за границей и не только...</h2>
                Текст статьи
            </div>
        </article>
        <!-- SEO EOF -->
    </div>
    </div>
</main>
<?= $this->render('/layouts/_show_more', ['url' => 'rent']) ?>

