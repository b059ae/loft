<div class="inner-page">
    <section class="news-page js-full-height tabs" style="min-height: 2529px;">
        <div class="news-page__top wrapper">
            <div class="news-page__panel-2">
                <h1 class="news-page__title">Новости и статьи</h1>
                <div class="news-page__actions">
                    <div class="news-nav tabs-nav">
                        <ul class="tabs-nav__list">
                            <li class="tabs-nav__item active">
                                <h2 class="tabs-nav__title">
                                    <!--                                        <a class="tabs-nav__link js-tabs-link" href="#tab-1">Все материалы</a>-->
                                    <a class="tabs-nav__link" href="/lists">Все материалы</a>
                                </h2>
                            </li>
                            <li class="tabs-nav__item ">
                                <h2 class="tabs-nav__title">
                                    <!--                                        <a class="tabs-nav__link js-tabs-link" href="#tab-2">Новости</a>-->
                                    <a class="tabs-nav__link" href="/lists/news">Новости</a>
                                </h2>
                            </li>
                            <li class="tabs-nav__item ">
                                <h2 class="tabs-nav__title">
                                    <!--                                        <a class="tabs-nav__link js-tabs-link" href="#tab-3">Статьи</a>-->
                                    <a class="tabs-nav__link" href="/lists/articles">Статьи</a>
                                </h2>
                            </li>
                        </ul>
                    </div>
                    <div class="news-page__right">
                        <div class="news-page__datepicker">
                            <p class="news-page__label">Фильтровать</p>
                            <div class="datepicker">
                                <input class="datepicker__input input-1 js-datepicker change_date hasDatepicker" name="news_change_date" type="text" placeholder="Укажите месяц" value="" id="dp1495213062906">
                                <span class="datepicker__icon-1"></span>
                                <span class="datepicker__icon-2"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="news-page__content wrapper">
            <div class="tabs-content">
                <!-- BEGIN TAB -->
                <div class="tabs-content__item active" id="tab-1">
                    <div class="news">
                        <ul class="news__list">
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/loft---prostranstvo-dlya-jizni-biznesa-i-meropriyatiy" style="background-image:url('/uploads/cache/Lists/Lists17/0af3830f81-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="/lists/loft---prostranstvo-dlya-jizni-biznesa-i-meropriyatiy"><span class="ellip"><span style="white-space: nowrap;">Лофт</span> <span style="white-space: nowrap;">-</span> <span style="white-space: nowrap;">пространство</span> <span style="white-space: nowrap;">для</span> <span style="white-space: nowrap;">жизни,</span> <span style="white-space: nowrap;">бизнеса</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">мероприятий</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">17.05.2017</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/articles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/arenda-ofisa-loft" style="background-image:url('/uploads/cache/Lists/Lists16/568688720f-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="/lists/arenda-ofisa-loft"><span class="ellip"><span style="white-space: nowrap;">Аренда</span> <span style="white-space: nowrap;">лофт-офиса:</span> <span style="white-space: nowrap;">выгодно</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">модно</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">12.05.2017</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/articles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/gde-i-kak-mojno-prodat-loft-v-moskve" style="background-image:url('/uploads/cache/Lists/Lists15/203fdac4be-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="/lists/gde-i-kak-mojno-prodat-loft-v-moskve"><span class="ellip"><span style="white-space: nowrap;">Где</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">как</span> <span style="white-space: nowrap;">можно</span> <span style="white-space: nowrap;">продать</span> <span style="white-space: nowrap;">лофт</span> <span style="white-space: nowrap;">в</span> <span style="white-space: nowrap;">Москве?</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">30.04.2017</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/news">Новости</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/arenda-loftov-v-moskve" style="background-image:url('/uploads/cache/Lists/Lists14/79fe005c1c-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="/lists/arenda-loftov-v-moskve"><span class="ellip"><span style="white-space: nowrap;">Где</span> <span style="white-space: nowrap;">и</span> <span style="white-space: nowrap;">как</span> <span style="white-space: nowrap;">можно</span> <span style="white-space: nowrap;">арендовать</span> <span style="white-space: nowrap;">лофт</span> <span style="white-space: nowrap;">в</span> <span style="white-space: nowrap;">Москве?</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">30.04.2017</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/articles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/kroshechnye-apartamenty---novyy-trend-v-moskve" style="background-image:url('/uploads/cache/Lists/Lists12/bed67f9aea-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="/lists/kroshechnye-apartamenty---novyy-trend-v-moskve"><span class="ellip"><span style="white-space: nowrap;">Крошечные</span> <span style="white-space: nowrap;">апартаменты</span> <span style="white-space: nowrap;">-</span> <span style="white-space: nowrap;">новый</span> <span style="white-space: nowrap;">тренд</span> <span style="white-space: nowrap;">в</span> <span style="white-space: nowrap;">Москве</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">21.04.2017</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/articles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/moskovskimi-vlastyami-prinyato-reshenie-o-snijenii-stavok-naloga-na-imushchestvo-organizaciy" style="background-image:url('/uploads/cache/Lists/Lists13/433c501629-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="/lists/moskovskimi-vlastyami-prinyato-reshenie-o-snijenii-stavok-naloga-na-imushchestvo-organizaciy"><span class="ellip">Московскими властями принято решение о снижении ставок налога на имущество <span class="ellip-line">организаций </span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">09.11.2015</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/articles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="news-banner">
                            <a class="banner" target="_blank" data-id="4" href="http://savoy.one/catalog/mebel/">
                                <img alt="" src="/uploads/store/Banners/Banner4/a02380.gif">
                            </a>
                        </div>

                        <ul class="news__list">
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/promzony-v-centre-stolicy-mogut-stat-jilymi-kvartalami" style="background-image:url('/uploads/cache/Lists/Lists11/6490d19806-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="#"><span class="ellip"><span style="white-space: nowrap;">Промзоны</span> <span style="white-space: nowrap;">в</span> <span style="white-space: nowrap;">центре</span> <span style="white-space: nowrap;">столицы</span> <span style="white-space: nowrap;">могут</span> <span style="white-space: nowrap;">стать</span> <span style="white-space: nowrap;">жилыми</span> <span style="white-space: nowrap;">кварталами</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">06.03.2015</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/atricles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/moskovskie-apartamenty-jdut-svoih-pokupateley" style="background-image:url('/uploads/cache/Lists/Lists10/d1e2abb320-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="#"><span class="ellip"><span style="white-space: nowrap;">Московские</span> <span style="white-space: nowrap;">апартаменты</span> <span style="white-space: nowrap;">ждут</span> <span style="white-space: nowrap;">своих</span> <span style="white-space: nowrap;">покупателей</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">05.12.2014</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/atricles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/stanciya-metro-tehnopark-mojet-byt-otkryta-v-2014-godu" style="background-image:url('/uploads/cache/Lists/Lists9/2447b7c32e-2_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="#"><span class="ellip"><span style="white-space: nowrap;">Станция</span> <span style="white-space: nowrap;">метро</span> <span style="white-space: nowrap;">«Технопарк»</span> <span style="white-space: nowrap;">может</span> <span style="white-space: nowrap;">быть</span> <span style="white-space: nowrap;">открыта</span> <span style="white-space: nowrap;">в</span> <span style="white-space: nowrap;">2014</span> <span style="white-space: nowrap;">году</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">03.09.2014</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/atricles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/byvshaya-solodovnya-zavoda-novaya-bavariya-stanet-loftom" style="background-image:url('/uploads/cache/Lists/Lists8/cf84f2c063-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="#"><span class="ellip"><span style="white-space: nowrap;">Бывшая</span> <span style="white-space: nowrap;">солодовня</span> <span style="white-space: nowrap;">завода</span> <span style="white-space: nowrap;">«Новая</span> <span style="white-space: nowrap;">Бавария»</span> <span style="white-space: nowrap;">станет</span> <span style="white-space: nowrap;">лофтом</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">15.05.2014</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/news">Новости</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/v-niderlandah-poyavilsya-bojestvennyy-loft" style="background-image:url('/uploads/cache/Lists/Lists7/3a28ec5845-1_367x260.jpeg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="#"><span class="ellip"><span style="white-space: nowrap;">В</span> <span style="white-space: nowrap;">Нидерландах</span> <span style="white-space: nowrap;">появился</span> <span style="white-space: nowrap;">«Божественный</span> <span style="white-space: nowrap;">лофт»</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">26.03.2014</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/news">Новости</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="news__item-wrapper">
                                <div class="news__item">
                                    <a class="news__image" href="/lists/v-moskve-poyavitsya-eshche-dva-interesnyh-jilyh-kompleksa" style="background-image:url('/uploads/cache/Lists/Lists6/b655ee4fd6-1_367x260.jpg');"></a>
                                    <div class="news__content">
                                        <h3 class="news__name">
                                            <a class="news__link js-ellip-3" href="#"><span class="ellip"><span style="white-space: nowrap;">В</span> <span style="white-space: nowrap;">Москве</span> <span style="white-space: nowrap;">появится</span> <span style="white-space: nowrap;">еще</span> <span style="white-space: nowrap;">два</span> <span style="white-space: nowrap;">интересных</span> <span style="white-space: nowrap;">жилых</span> <span style="white-space: nowrap;">комплекса</span></span></a>
                                        </h3>
                                        <div class="news__info">
                                            <p class="news__date">14.01.2014</p>
                                            <p class="news__cat">
                                                <a class="news__color button-link" href="/lists/atricles">Статьи</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="page-nav">
                            <div class="page-nav__main">
                                <div class="page-nav__left">
                                    <p class="page-nav__text"><b>Показано&nbsp;12</b>&nbsp;из <span class="tt">&nbsp;17</span>&nbsp;предложений</p>
                                </div>
                                <div class="page-nav__right">
                                    <ul class="filter_nav page-nav__list"><li class="prev_s disabled"><span>‹</span></li>
                                        <li class="page-nav__item active"><a href="/lists?per-page=12" data-page="0">1</a></li>
                                        <li class="page-nav__item"><a href="/lists?page=2&amp;per-page=12" data-page="1">2</a></li>
                                        <li class="next_s"><a href="/lists?page=2&amp;per-page=12" data-page="1">›</a></li></ul>                                    </div>
                                <!--                                    <a class="page-nav__more button-2" href="#">Показать еще</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="news-banner">
                <a class="banner" target="_blank" data-id="5" href="http://savoy.one/catalog/mebel/">
                    <img alt="" src="/uploads/store/Banners/Banner5/747426.gif">
                </a>
            </div>
        </div>
    </section>
</div>