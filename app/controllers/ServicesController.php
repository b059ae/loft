<?php

namespace app\controllers;

use app\models\CallbackForm;
use yii\easyii\modules\article\api\Article;
use yii\web\Controller;

class ServicesController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Все услуги
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index',[
            'services' => Article::cat('uslugi')->getItems([
                'pagination' => false,
            ]), // Все услуги
        ]);
    }

    public function actionView($slug)
    {
        $article = Article::get($slug);
        if(!$article){
            throw new \yii\web\NotFoundHttpException('Article not found.');
        }
        $callbackForm = new CallbackForm();
        return $this->render('view', [
            'article' => $article,
            'callbackForm' => $callbackForm
        ]);
    }
}