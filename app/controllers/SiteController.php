<?php

namespace app\controllers;

use app\models\CallbackForm;
use app\models\ContactForm;
use app\models\EmailForm;
use app\models\OrderForm;
use app\models\SaleFilterForm;
use app\models\SendForm;
use Yii;
use yii\db\Exception;
use yii\easyii\modules\article\api\Article;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\text\api\Text;
use yii\web\Controller;
use yii\easyii\modules\subscribe\api\Subscribe;

class SiteController extends Controller
{
    private $cities = [' ' => 'Любой', 'Аликанте' => 'Аликанте','Бенидорм' => 'Бенидорм','Эльче' => 'Эльче','Мадрид' => 'Мадрид','Барселона' => 'Барселона'];
    private $types = [' ' => 'Любой','Аппартаменты' => 'Аппартаменты','Дом' => 'Дом'];

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        $filterForm = new SaleFilterForm();
        return $this->render('index',[
            'sale'=>Catalog::cat('sale')->getItems([
                'pagination' => ['pageSize' => 8],
                'filters' => [
                    'popular' => 1,
                ],
            ]),
            'filterForm' => $filterForm,
            'cities' => $this->cities,
            'types' => $this->types,
            /*            'maxPrice' => $this->getMaxPrice(),*/
        ]);
    }

    /**
     * Покупка
     * @return string
     */
    public function actionSale()
    {
        //Фильтр
        $filters = null;
        $filterForm = new SaleFilterForm();
        if($filterForm->load(Yii::$app->request->post()) && $filterForm->validate()) {
            $filters = $filterForm->parseFilters();
        }
        //Сортировка
        $orderBy = $filterForm->sort;
        //Установка выбранногоо диапазона цены
        $price = $filterForm->getPrice();
        //Пагинация
        $hide_button = $filterForm->getPage($filters,$price);
        return $this->render('sale',[
            'sale'=>Catalog::cat('sale')->getItems([
                'pagination' => ['pageSize' => $filterForm->pageSize],
                'orderBy' => $orderBy,
                'filters' => $filters,
                'where' =>
                    ['between', 'price', (int)$price['min'], (int)$price['max']]
                ,
            ]),
            'filterForm' => $filterForm,
            'hide_button' => $hide_button,
            'count' => $filterForm->pageCount,
            'cities' => $this->cities,
            'types' => $this->types,
            'price_max' => $price['max'],
            'price_min' => $price['min'],
/*            'maxPrice' => $this->getMaxPrice(),*/
        ]);
    }
    /**
     * Аренда
     * @return string
     */
    public function actionRent()
    {
        //Фильтр
        $filterForm = new SaleFilterForm();
        if($filterForm->load(Yii::$app->request->post()) && $filterForm->validate()) {
            $filters = $filterForm->parse();
        }
        //Сортировка
        $sortForm = new SaleSortForm();
        if($sortForm->load(Yii::$app->request->post())) {
            $orderBy = $sortForm->sort;
        }else{
            $orderBy = 'price ASC';
        }
        //Пагинация
        $pages = count(Catalog::cat('rent')->getItems([
            'pagination' => ['pageSize' => ''],
            'orderBy' => $orderBy,
            'filters' => $filters
        ]));
        if($count = Yii::$app->request->get('show')){
            $pageSize = $this->pageSize * $count;
        }else{
            $pageSize = $this->pageSize;
            $count = 1;
        }
        if($pageSize < $pages){
            $hide_button = true;
        }
        return $this->render('rent',[
            'rent'=>Catalog::cat('rent')->getItems([
                'pagination' => ['pageSize' => $pageSize],
                'orderBy' => 'price',
                'filters' => $filters
            ]),
            'filterForm' => $filterForm,
            'sortForm' => $sortForm,
            'hide_button' => $hide_button,
            'cities' => $this->cities,
            'types' => $this->types,
            'count' => $count
        ]);
    }
    /**
     * Длительная аренда
     * @return string
     */
    public function actionLongTermRent()
    {
        //Фильтр
        $filterForm = new SaleFilterForm();
        if($filterForm->load(Yii::$app->request->post()) && $filterForm->validate()) {
            $filters = $filterForm->parse();
        }
        //Сортировка
        $sortForm = new SaleSortForm();
        if($sortForm->load(Yii::$app->request->post())) {
            $orderBy = $sortForm->sort;
        }else{
            $orderBy = 'price ASC';
        }
        //Пагинация
        $pages = count(Catalog::cat('long-term-rent')->getItems([
            'pagination' => ['pageSize' => ''],
            'orderBy' => $orderBy,
            'filters' => $filters
        ]));
        if($count = Yii::$app->request->get('show')){
            $pageSize = $this->pageSize * $count;
        }else{
            $pageSize = $this->pageSize;
            $count = 1;
        }
        if($pageSize < $pages){
            $hide_button = true;
        }
        return $this->render('long-term-rent',[
            'longTermRent'=>Catalog::cat('long-term-rent')->getItems([
                'pagination' => ['pageSize' => $pageSize],
                'orderBy' => 'price',
                'filters' => $filters
            ]),
            'filterForm' => $filterForm,
            'sortForm' => $sortForm,
            'hide_button' => $hide_button,
            'cities' => $this->cities,
            'types' => $this->types,
            'count' => $count
        ]);
    }

    /**
     * Просмотр дома на продажу
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSaleView($id)
    {$object = Catalog::get($id);
        if(!$object){
            throw new \yii\web\NotFoundHttpException('Object not found.');
        }
        $sendForm = new SendForm();
        return $this->render('sale-view',[
            'object' => $object,
            'sale'=>Catalog::cat('sale')->getItems([
                'pagination' => ['pageSize' => 8],
                'filters' => [
                    'popular' => 1,
                ],
            ]),
            'sendForm' => $sendForm,
        ]);
    }

    /**
     * Просмотр дома в аренду
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRentView($id)
    {$object = Catalog::get($id);
        if(!$object){
            throw new \yii\web\NotFoundHttpException('Object not found.');
        }
        $sendForm = new SendForm();
        return $this->render('rent-view',[
            'object' => $object,
            'sale'=>Catalog::cat('rent')->getItems([
                'pagination' => ['pageSize' => 8],
                'filters' => [
                    'popular' => 1,
                ],
            ]),
            'sendForm' => $sendForm,
        ]);
    }

    /**
     * Просмотр дома в длительную аренду
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionLongTermRentView($id)
    {$object = Catalog::get($id);
        if(!$object){
            throw new \yii\web\NotFoundHttpException('Object not found.');
        }
        $sendForm = new SendForm();
        return $this->render('long-term-rent-view',[
            'object' => $object,
            'longTermRent'=>Catalog::cat('sale')->getItems([
                'pagination' => ['pageSize' => 8],
                'filters' => [
                    'popular' => 1,
                ],
            ]),
            'sendForm' => $sendForm,
        ]);
    }
    /**
     * Акции
     * @return string
     */
    public function actionNews()
    {
        return $this->render('news',[
            'news' => Article::cat('news')->getItems([
                'pagination' => false,
            ]),
        ]);
    }

    /**
     * Ghjcvjnh yjdjcnb
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionNewsView($id)
    {
        $article = Article::get($id);
        if(!$article){
            throw new \yii\web\NotFoundHttpException('Article not found.');
        }

        return $this->render('news-view', [
            'article' => $article
        ]);
    }
    /**
     * Размещение
     * @return string
     */
    public function actionLot()
    {
        return $this->render('lot',[
        ]);
    }
    /**
     * Блог
     * @return string
     */
    public function actionLists()
    {
        return $this->render('lists',[
        ]);
    }
    /**
     * О нас
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about',[
        ]);
    }
    /**
     * Контакты
     * @return string
     */
    public function actionContacts()
    {
        $contactForm = new ContactForm();
        return $this->render('contacts',[
            'contactForm' => $contactForm
        ]);
    }
    /**
     * Заказ обратного звонка
     */
    public function actionCallback()
    {
        $model = new CallbackForm();
        if($model->load(Yii::$app->request->post())){
            try{
                //Отправка письма администратору
                Yii::$app->mailer->compose()
                    ->setFrom('callback@spain.com')
                    ->setTo(Text::get('mail'))
                    ->setSubject('Заказ обратного звонка')
                    ->setTextBody('Имя: '.$model->name.'. Номер телефона: '.$model->phone.'.')
                    ->send();
//                $this->telegram('Заказ обратного звонка. Имя: '.$model->name.'. Номер телефона: '.$model->phone.'.');
                $this->callback($model->phone,$model->name);
//                $this->call($model->phone);
                $this->redirect('/');
            }catch(Exception $e){
                $this->redirect('/');
            }
        }
    }
    /**
     * Заказ обратного звонка со страницы услуг
     */
    public function actionCallbackServices()
    {
        $model = new CallbackForm();
        if($model->load(Yii::$app->request->post())){
            try{
                //Отправка письма администратору
                Yii::$app->mailer->compose()
                    ->setFrom('callback@spain.com')
                    ->setTo(Text::get('mail'))
                    ->setSubject('Заказ обратного звонка')
                    ->setTextBody('Имя: '.$model->name.'. Номер телефона: '.$model->phone.'.')
                    ->send();
//                $this->telegram('Заказ обратного звонка со страницы услуг. Имя: '.$model->name.'. Номер телефона: '.$model->phone.'.');
                $this->callback($model->phone,$model->name);
//                $this->call($model->phone);
                $this->redirect('/');

            }catch(Exception $e){
                $this->redirect('/');
            }
        }
    }
    /**
     * Обратная связь через страницу контактов
     */
    public function actionSendContact()
    {
        $model = new ContactForm();
        if($model->load(Yii::$app->request->post())){
            try{
                //Отправка письма администратору
                Yii::$app->mailer->compose()
                    ->setFrom('contacts@spain.com')
                    ->setTo(Text::get('mail'))
                    ->setSubject('Обратная связь')
                    ->setTextBody('ФИО: '.$model->name.'. Номер телефона: '.$model->phone.'. Сообщение: '.$model->message.'.')
                    ->send();
//                $this->telegram('Обратная связь через страницу контактов. ФИО: '.$model->name.'. Номер телефона: '.$model->phone.'. Сообщение: '.$model->message.'.');
                $this->callback($model->phone,$model->name);
//                $this->call($model->phone);
                $this->redirect('/');

            }catch(Exception $e){
                $this->redirect('/');
            }
        }
    }
    /**
     * Заявка
     */
    public function actionOrder()
    {
        $model = new OrderForm();
        if($model->load(Yii::$app->request->post())){
            try{
                //Отправка письма администратору
                Yii::$app->mailer->compose()
                    ->setFrom('order@spain.com')
                    ->setTo(Text::get('mail'))
                    ->setSubject('Заявка на покупку объекта')
                    ->setTextBody('Объект: '.Text::get('main-url').'/sale-view?id='.$model->id.'. Имя: '.$model->name.'. Номер телефона: '.$model->phone.'.')
                    ->send();
//                $this->telegram('Заявка на покупку объекта. Адрес страницы объекта: '.Text::get('main-url').'/sale-view?id='.$model->id.'. Имя: '.$model->name.'. Номер телефона: '.$model->phone.'.');
                $this->callback($model->phone,$model->name);
//                $this->call($model->phone);
                $this->redirect('/');

            }catch(Exception $e){
                $this->redirect('/');
            }
        }
    }
    /**
     * Запись на просмотр
     */
    public function actionSend()
    {
        $model = new SendForm();
        if($model->load(Yii::$app->request->post())){
            try{
                //Отправка письма администратору
                Yii::$app->mailer->compose()
                    ->setFrom('view@spain.com')
                    ->setTo(Text::get('mail'))
                    ->setSubject('Запись на просмотр объекта')
                    ->setTextBody('Имя: '.$model->name.'. Номер: '.$model->phone.'. Дата: '.$model->date.'. Адрес страницы объекта: '.Text::get('main-url').$model->object_view.'.')
                    ->send();
//                $this->telegram('Запись на просмотр объекта. Имя: '.$model->name.'. Номер: '.$model->phone.'. Дата: '.$model->date.'. Адрес страницы объекта: '.Text::get('main-url').$model->object_view.'.');
                $this->callback($model->phone,$model->name);
//                $this->call($model->phone);
                $this->redirect('/');

            }catch(Exception $e){
                $this->redirect('/');
            }
        }
    }
    /**
     * Подписка на рассылку
     */
    public function actionSubscribe()
    {
        $model = new EmailForm();
        if($model->load(Yii::$app->request->post())){
            try{
                $sub = new Subscribe();
                $sub->api_form([
                    'successUrl' => '/',
                    'errorUrl' => '/',
                ]);
                $sub->api_save($model->email);
//                $this->telegram('Подписка на email-рассылку. Email: '.$model->email);
                $this->redirect('/');
            }catch(Exception $e){
                $this->redirect('/');
            }
        }
    }

    /**
     * Отправка сообщения в Телеграм-канал
     */
    public function telegram($text = '', $chatID = '@realty_ada')
    {
        $botToken = '568051053:AAEFZRGCxd1Lt-AEEWOGPJSMLL8OhhIeKg0';
        $url = 'https://api.telegram.org/bot'
            .$botToken.'/sendMessage?chat_id='
            .$chatID
            .'&text='.urlencode($text);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Автоматический звонок клиенту
     */
    public function callback($number, $name)
    {
        //Проверка московского времени
        if($this->timeMoscow()){
            //Удаление из номера лишних символов
            $number = preg_replace('![^0-9]+!','',$number);
            //Замена 7 на 8
            $number = substr_replace($number, '8', 0, 1);
            $post = [
                "phone" => $number,
                "name" => $name,
                "token" => "70cni5MGKUZGp9CJGyUYqYbxICytK3mj"
            ];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'http://195.151.33.114/callback/callback.php');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
            $res = curl_exec($curl);
            curl_close($curl);
        }
        if ($res){return $res;}else{return false;}
    }

    /**
     * Автоматически звонок клиенту
     */
    public function call($number)
    {
        //Проверка московского времени
        if($this->timeMoscow()){
            //Удаление из номера лишних символов
            $number = preg_replace('![^0-9]+!','',$number);
            // Получение секретного ключа
            $domain = 'telecombg.onpbx.ru';
            $apikey = 'ttlqqkxotyLe44reR90Uu5VlcPqykTQDTewvamj8';
            $new = false;
            $data_key_array = $this->onpbx_get_secret_key($domain, $apikey, $new);

            // Совершение запросов
            $secret_key = $data_key_array['data']['key'];
            $key_id = $data_key_array['data']['key_id'];
            $url = 'api.onlinepbx.ru/'.$domain.'/call/now.json';
            $post = ['from' => (string)$number, 'to' => '10'];
            $data_array = $this->onpbx_api_query($secret_key, $key_id, $url, $post);
        }
    }

    /**
     * OnlinePBX Получение секретного ключа
     */
    private function onpbx_get_secret_key($domain, $apikey, $new=false){
        $data = array('auth_key'=>$apikey);
        if ($new){$data['new'] ='true';}

        $ch = curl_init('https://api.onlinepbx.ru/'.$domain.'/auth.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $res = json_decode(curl_exec($ch), true);
        if ($res){return $res;}else{return false;}
    }

    /**
     * OnlinePBX звонок
     */
    private function onpbx_api_query($secret_key, $key_id, $url, $post=array(), $opt=array()){
        $method = 'POST';
        $date = @date('r');

        if (is_array($post)){
            foreach ($post as $key => $val){
                if (is_string($key) && preg_match('/^@(.+)/', $val, $m)){
                    $post[$key] = array('name'=>basename($m[1]), 'data'=>base64_encode(file_get_contents($m[1])));
                }
            }
        }
        $post = http_build_query($post);
        $content_type = 'application/x-www-form-urlencoded';
        $content_md5 = hash('md5', $post);
        $signature = base64_encode(hash_hmac('sha1', $method."\n".$content_md5."\n".$content_type."\n".$date."\n".$url."\n", $secret_key, false));
        $headers = ['Date: '.$date, 'Accept: application/json', 'Content-Type: '.$content_type, 'x-pbx-authentication: '.$key_id.':'.$signature, 'Content-MD5: '.$content_md5];

        $ch = curl_init('https://'.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $res = json_decode(curl_exec($ch), true);
        if ($res){return $res;}else{return false;}
    }

    /**
     * Проверка московского времени
     */
    private function timeMoscow(){
        $moscowTime = (int)gmdate("H", time() + 3600*(3+date("I")));
        if($moscowTime >= 10 && $moscowTime < 22)
            return true;
        else
            return false;
    }
//    /**
//     * Подсчет максимальной стоимости
//     * @return float
//     */
//    public function getMaxPrice()
//    {
//        //Вычисление максимальной цены
//        $cat = Catalog::cat('sale')->getItems();
//        $price = [];
//        foreach ($cat as $c){
//            $price[] = $c->model->price;
//        }
//        return max($price);
//    }
}