-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Ноя 20 2017 г., 18:54
-- Версия сервера: 5.6.33-79.0
-- Версия PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u0193363_span`
--

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_admins`
--

CREATE TABLE `easyii_admins` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `access_token` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_article_categories`
--

CREATE TABLE `easyii_article_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_article_categories`
--

INSERT INTO `easyii_article_categories` (`id`, `title`, `description`, `image_file`, `order_num`, `slug`, `tree`, `lft`, `rgt`, `depth`, `status`) VALUES
(1, 'Услуги', '', NULL, 1, 'uslugi', 1, 1, 2, 0, 1),
(2, 'Новости', '', NULL, 2, 'news', 2, 1, 2, 0, 1),
(4, 'Налоги на недвижимость в Испании для нерезидентов', '<h2>Налогообложение при покупке жилья</h2><h4><br></h4><p>Налог, которым облагается недвижимость данного типа — это НДС (IVA) в размере 10%. Поэтому в случае приобретении жилья на сумму 250 000, оплатить в казну нужно будет 25 000 евро.</p>', 'article/069-ispanija-7aee337e8d.jpg', 3, 'nalogi-na-nedvizimost-v-ispanii-dla-nerezidentov', 4, 1, 2, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_article_items`
--

CREATE TABLE `easyii_article_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `source` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_article_items`
--

INSERT INTO `easyii_article_items` (`id`, `category_id`, `title`, `image_file`, `short`, `text`, `slug`, `time`, `views`, `status`, `source`) VALUES
(1, 1, 'Экскурсия', 'article/fkqngcze62494-4c080e3a4c.jpg', 'Мы предоставляем замечательную возможность нашим клиентам окунуться в мир незабываемого путешествия  .', '<div> \r\n<a class="service_image" href="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed078b3c89a.jpg" tabindex="-1"> \r\n<img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed078b3c89a.jpg" alt=""/> \r\n</a> \r\n<div class="slider-6__content"> \r\n<p class="slider-6__text"><h3>Экскурсии в Аликанте</h3><p>- Обзорная экскурсия по Аликанте (крепость Santa Barbara, парк Las Palmeras)\r\n</div> \r\n</div>\r\n<div> \r\n<a class="service_image" href="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed079733fa8.jpg" tabindex="-1"> \r\n<img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed079733fa8.jpg" alt=""/> \r\n</a> \r\n<div class="slider-6__content"> \r\n<p class="slider-6__text"><h3>Экскурсии с выездом из Аликанте</h3><p>- в музей шоколада “Valor” с дегустацией продукта\r\n</p>\r\n<p>- крепость Guadalest</p>\r\n</div> \r\n</div>', 'ekskursia', 1499428513, 78, 1, NULL),
(3, 1, 'Консьерж-услуга', 'article/concierge2-85dd3ef50f.jpeg', 'Консьерж  услуга - это ваш личный секретарь, переводчик и консультант.Воспользуйтесь нашими услугами – заключите контракт на консьерж-услуги в соответствии с вашими потребностями.', '<div> \r\n<a class="service_image" href="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed096f0cdc1.jpg" tabindex="-1"> \r\n<img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed096f0cdc1.jpg" alt=""/> \r\n</a> \r\n<div class="slider-6__content"> \r\n<p class="slider-6__text">Предоставляемые виды услуг:\r\n<ul>\r\n<li>Мелкие бытовые вопросы (например, замена газового баллона);</li>\r\n<li>Уборка квартиры;</li>\r\n<li>Подключение интернета и спутникового телевидения;</li>\r\n<li>Сдача в аренду Вашей недвижимости;</li>\r\n<li>Общение с соседями и управляющими дома;</li>\r\n<li>Организации закупок;</li>\r\n</ul></p> \r\n</div> \r\n</div>', 'konserz-usluga', 1499430006, 47, 1, NULL),
(4, 1, 'Дизайн-проект', 'article/design-architecture-colors-73e6cdf6d7.jpg', 'Дизайн Проект - это стиль и комфорт Вашей квартиры, дома или офиса.', '<div><img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed0672bdf41.jpg">\r\n</p>\r\n<div class="slider-6__content"> \r\n<p class="slider-6__text">Если Вы решили делать ремонт, то без проекта не обойтись. Наши опытные дизайнеры спланируют и создадут авторский дизайн проект с учётом всех Ваших пожеланий в кротчайшие сроки.\r\n</p>\r\n</div></div>', 'dizajn-proekt', 1499430063, 66, 1, NULL),
(5, 1, 'Ремонт', 'article/images-608af3f94c.jpg', 'Специалисты нашей компании  выполнят за Вас ремонт, потому что этим делом должны заниматься квалифицированные специалисты, кем мы и являемся.', '<ul><li><a class="service_image" href="http://spain.u0193363.plsk.regruhosting.ru/uploads/thumbs/images-608af3f94c-e4442d86208b1e21ddd636ee3922e16e.jpg" tabindex="-1"><img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/thumbs/images-608af3f94c-e4442d86208b1e21ddd636ee3922e16e.jpg" alt=""></a></li>\r\n	<li><em>Мы реализуем все типы ремонтов от Косметического до Капитального:</em></li>\r\n	<li><em>- Косметический от двух недель до одного месяца в зависимости от требования клиента.</em></li>\r\n	<li><em>- Капитальный от полутора месяца до полугода в зависимости от требования клиента.</em></li><li><em><br></em></li><li><em>Косметический ремонт-это, пожалуй, самый простой и не дорогой ремонт, он подходит для того, чтобы придать квартире свежий и обновленный вид с минимальной стоимостью. Косметический ремонт подходит для людей, которые хотят продать квартиру в сжатые сроки или обновить недавно сделанный комплексный ремонт.</em></li>\r\n	<li><em>Капитальный ремонт.Он сложнее косметического и намного дороже. Этот</em><span class="redactor-invisible-space" style="background-color: initial;"><em> ремонт включает в себя отделочные работы с использованием самых новейших материалов. Данный ремонт квартир может менять инженерные коммуникации, а также капитально обновляется отделка квартиры, при которой как стены так и потолки выравниваются. Выполнение ремонта нашими специалистами соответствует всем стандартам, поэтому Вы смело его можете нам доверить.</em></span></li>\r\n</ul>', 'remont', 1499430044, 49, 1, NULL),
(6, 1, 'Недвижимость в кредит', 'article/kreditnuy-dogovor-ne-prigovor-3f9a1591fc.jpg', 'Ипотека в Испании является вполне доступной для иностранных граждан.\r\nОна открывается на основании следующих документов:', '<div><img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed0b678504d.jpg">\r\n<div class="slider-6__content"> \r\n<ol class="slider-6__text">\r\n<li><em>Договор резерва или залога на недвижимость в Испании.</em></li>\r\n<li><em>Справка о доходах форма 2 НДФЛ за предыдущий год.</em></li>\r\n<li><em>Выписка/выписки с личного банковского счета/счетов.</em></li>\r\n<li><em>Справка с места работы.</em></li>\r\n<li><em>Идентификационный номер иностранца(NIE).</em></li>\r\n<li><em>Действующий заграничный паспорт.</em></li>\r\n</ol>\r\n</div></div>', 'nedvizimost-v-kredit', 1499430164, 54, 1, NULL),
(8, 1, 'Процедура покупки и оформления недвижимости в Испании', 'article/8888881-f45770b863.jpg', 'Приобрести дом или квартиру в Испании можно в несколько этапов', '<div><img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed0a0a10ffe.jpg">\r\n<div class="slider-6__content"> \r\n<ul>\r\n<li><strong>1.Открытие счёта в банке:</strong> заграничный паспорт, документ с отметкой о регистрации по месту жительства, справка о доходах.</li>\r\n<li><strong>2. Получение идентификационного номера:</strong> (Número de Identificación de Extranjero, NIE).</li>\r\n<li><strong>4. Подписание предварительного соглашения. </strong>После того как выбран подходящий объект, подписывается предварительное соглашение (contrato privado de compraventa), в котором указывается цена и способ оплаты. Покупатель вносит залог — 10 % от стоимости объекта. Если <nobr rel="background-color: initial;" style="background-color: initial;">по какой-либо </nobr>причине покупатель передумал приобретать данный объект, его аванс пропадает.</li>\r\n<li><strong>5. Подача заявки на ипотечный кредит.</strong></li>\r\n<li><strong>6. Подписание окончательного соглашения. </strong>Это происходит в присутствии нотариуса. Покупателю и продавцу выдаются копии документов, заверенных нотариусом. Покупатель переводит на счёт продавца оставшуюся часть суммы. По истечению срока, не превышающего трёх месяцев, они получают у нотариуса оригиналы договора <nobr rel="background-color: initial;" style="background-color: initial;">купли-продажи</nobr> (escritura pública).</li>\r\n<li><strong>7. Регистрация недвижимости.</strong></li>\r\n</ul></div></div>', 'procedura-pokupki-i-oformlenia-nedvizimosti-v-ispanii', 1507414392, 70, 1, NULL),
(9, 4, 'налог', NULL, 'Налогообложение при покупке жилья', '<p>Налог, которым облагается недвижимость данного типа — это НДС (IVA) в размере 10%. Поэтому в случае приобретении жилья на сумму 250 000, оплатить в казну нужно будет 25 000 евро.</p>', 'nalog', 1507415085, 0, 1, NULL),
(10, 1, 'Юрист', 'article/3301217881261x203yurist-yuridich-88a99beb36.jpg', 'Юридическое оформление', '<div><img src="http://spain.u0193363.plsk.regruhosting.ru/uploads/redactor/59ed054064e9d.jpg">\r\n<div class="slider-6__content"> \r\n<p class="slider-6__text"><strong>Плата за услуги нотариуса и комиссия за регистрацию недвижимости</strong> в земельном кадастре составляет <nobr style="background-color: initial;">0,2–2,0 %</nobr> от стоимости объекта. Чем дороже объект, тем эти расходы меньше.\r\n</p></div></div>', 'urist', 1507415392, 70, 1, NULL),
(12, 2, 'Почему именно мы ?', 'article/2v2425z1382077-b582c92b0b-5f5f915604.jpg', 'Потому, что это очень хорошая возможность совместить приятное с полезным!', '<p>Во-первых, покупая недвижимость у нас, Вы становитесь обладателем жилья на берегу средиземного моря, с последующей возможностью сдачи в аренду, на период вашего отсутствия ,а так же, мы дарим каждому нашему клиенту интересную экскурсию, выбранную на Ваше усмотрение.</p><p><strong></strong></p><p>Во-вторых, Вы сможете посетить живописные пляжи, увидеть средневековую архитектуру и попробовать национальную средиземноморскую кухню!</p>', 'pocemu-imenno-my', 1510685328, 15, 1, 'Источник');

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_carousel`
--

CREATE TABLE `easyii_carousel` (
  `id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_categories`
--

CREATE TABLE `easyii_catalog_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_categories`
--

INSERT INTO `easyii_catalog_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
(1, 'Продажа', '', NULL, '[{"name":"type","title":"\\u0422\\u0438\\u043f \\u043d\\u0435\\u0434\\u0432\\u0438\\u0436\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438","type":"select","options":["\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","\\u0414\\u043e\\u043c"]},{"name":"city","title":"\\u0413\\u043e\\u0440\\u043e\\u0434","type":"select","options":["\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","\\u0411\\u0435\\u043d\\u0438\\u0434\\u043e\\u0440\\u043c","\\u042d\\u043b\\u044c\\u0447\\u0435","\\u041c\\u0430\\u0434\\u0440\\u0438\\u0434","\\u0411\\u0430\\u0440\\u0441\\u0435\\u043b\\u043e\\u043d\\u0430"]},{"name":"address","title":"\\u0410\\u0434\\u0440\\u0435\\u0441","type":"address","options":""},{"name":"room","title":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442","type":"string","options":""},{"name":"square","title":"\\u041f\\u043b\\u043e\\u0449\\u0430\\u0434\\u044c","type":"string","options":""},{"name":"sea","title":"\\u0420\\u0430\\u0441\\u0441\\u0442\\u043e\\u044f\\u043d\\u0438\\u0435 \\u0434\\u043e \\u043c\\u043e\\u0440\\u044f","type":"string","options":""},{"name":"bathroom","title":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0432\\u0430\\u043d\\u043d\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442","type":"string","options":""},{"name":"balkon","title":"\\u0411\\u0430\\u043b\\u043a\\u043e\\u043d","type":"boolean","options":""},{"name":"pool","title":"\\u0411\\u0430\\u0441\\u0441\\u0435\\u0439\\u043d","type":"boolean","options":"\\u0414\\u0430,\\u041d\\u0435\\u0442"},{"name":"parking","title":"\\u041f\\u0430\\u0440\\u043a\\u043e\\u0432\\u043a\\u0430","type":"boolean","options":""},{"name":"popular","title":"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u043e\\u0435","type":"boolean","options":""}]', 'sale', 1, 1, 2, 0, 1, 1),
(2, 'Аренда', '', NULL, '[{"name":"type","title":"\\u0422\\u0438\\u043f \\u043d\\u0435\\u0434\\u0432\\u0438\\u0436\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438","type":"select","options":["\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","\\u0414\\u043e\\u043c"]},{"name":"city","title":"\\u0413\\u043e\\u0440\\u043e\\u0434","type":"select","options":["\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","\\u0411\\u0435\\u043d\\u0438\\u0434\\u043e\\u0440\\u043c","\\u042d\\u043b\\u044c\\u0447\\u0435","\\u041c\\u0430\\u0434\\u0440\\u0438\\u0434","\\u0411\\u0430\\u0440\\u0441\\u0435\\u043b\\u043e\\u043d\\u0430"]},{"name":"address","title":"\\u0410\\u0434\\u0440\\u0435\\u0441","type":"address","options":""},{"name":"room","title":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442","type":"string","options":""},{"name":"square","title":"\\u041f\\u043b\\u043e\\u0449\\u0430\\u0434\\u044c","type":"string","options":""},{"name":"sea","title":"\\u0420\\u0430\\u0441\\u0441\\u0442\\u043e\\u044f\\u043d\\u0438\\u0435 \\u0434\\u043e \\u043c\\u043e\\u0440\\u044f","type":"string","options":""},{"name":"bathroom","title":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0432\\u0430\\u043d\\u043d\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442","type":"string","options":""},{"name":"balkon","title":"\\u0411\\u0430\\u043b\\u043a\\u043e\\u043d","type":"boolean","options":""},{"name":"pool","title":"\\u0411\\u0430\\u0441\\u0441\\u0435\\u0439\\u043d","type":"boolean","options":"\\u0414\\u0430,\\u041d\\u0435\\u0442"},{"name":"parking","title":"\\u041f\\u0430\\u0440\\u043a\\u043e\\u0432\\u043a\\u0430","type":"boolean","options":""},{"name":"popular","title":"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u043e\\u0435","type":"boolean","options":""}]', 'rent', 2, 1, 2, 0, 2, 1),
(3, 'Длительная аренда', '', NULL, '[{"name":"type","title":"\\u0422\\u0438\\u043f \\u043d\\u0435\\u0434\\u0432\\u0438\\u0436\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438","type":"select","options":["\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","\\u0414\\u043e\\u043c"]},{"name":"city","title":"\\u0413\\u043e\\u0440\\u043e\\u0434","type":"select","options":["\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","\\u0411\\u0435\\u043d\\u0438\\u0434\\u043e\\u0440\\u043c","\\u042d\\u043b\\u044c\\u0447\\u0435","\\u041c\\u0430\\u0434\\u0440\\u0438\\u0434","\\u0411\\u0430\\u0440\\u0441\\u0435\\u043b\\u043e\\u043d\\u0430"]},{"name":"address","title":"\\u0410\\u0434\\u0440\\u0435\\u0441","type":"address","options":""},{"name":"room","title":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442","type":"string","options":""},{"name":"square","title":"\\u041f\\u043b\\u043e\\u0449\\u0430\\u0434\\u044c","type":"string","options":""},{"name":"sea","title":"\\u0420\\u0430\\u0441\\u0441\\u0442\\u043e\\u044f\\u043d\\u0438\\u0435 \\u0434\\u043e \\u043c\\u043e\\u0440\\u044f","type":"string","options":""},{"name":"bathroom","title":"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0432\\u0430\\u043d\\u043d\\u044b\\u0445 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442","type":"string","options":""},{"name":"balkon","title":"\\u0411\\u0430\\u043b\\u043a\\u043e\\u043d","type":"boolean","options":""},{"name":"pool","title":"\\u0411\\u0430\\u0441\\u0441\\u0435\\u0439\\u043d","type":"boolean","options":"\\u0414\\u0430,\\u041d\\u0435\\u0442"},{"name":"parking","title":"\\u041f\\u0430\\u0440\\u043a\\u043e\\u0432\\u043a\\u0430","type":"boolean","options":""},{"name":"popular","title":"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u043e\\u0435","type":"boolean","options":""}]', 'long-term-rent', 3, 1, 2, 0, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_items`
--

CREATE TABLE `easyii_catalog_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `available` int(11) DEFAULT '1',
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `data` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_items`
--

INSERT INTO `easyii_catalog_items` (`id`, `category_id`, `title`, `description`, `available`, `price`, `discount`, `data`, `image_file`, `slug`, `time`, `status`) VALUES
(1, 1, 'Квартира на Carmelo Calvo', '<p>Приобрести лофт по улице Верейская, дом 29, стр. 151 в Москве в \r\nжилищном комплексе LOFT 151 в семиэтажном здании, выполненном в стиле \r\nклассицизма и модерна станет как хорошей инвестицией,так и местом для \r\nжизни. Территория оснащена круглосуточной системой видеонаблюдения, \r\nохраны. Имеется наземная парковка. Высокий качества жизни достигается \r\nблагодаря каждой мелочи, начиная от мест общественного пользования. \r\nЛобби с дизайнерской отделкой, камином и собственной библиотекой \r\nпередают атмосферу комфорта, уюта всего комплекса вцелом.</p><p><strong>Особенности:</strong></p><p>- Расположение недалеко от ст.м. Славянский бульвар и Кунцевская, рядом с Рублевским шосее и Кутузовским проспектом</p><p>- На территории находится комплекс бизнес-центров</p><p>- Различные виды: студии, двухкомнатные и двухуровневые апартаменты площадью от 32 до 161 кв.м.</p><p>- Апартаменты с внутренней отделкой, меблированной кухней и бытовой техникой, укомплектованным санузлом</p>', 1, 250000, NULL, '{"type":"","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"03004 \\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435, \\u0418\\u0441\\u043f\\u0430\\u043d\\u0438\\u044f#ChIJZ9X4uqw3Yg0RyJycawoxx5o#38.3516297#-0.48727259999998296","room":"3","square":"84","sea":"250","bathroom":"1","balkon":"1","pool":"0","parking":"1","popular":"1"}', 'catalog/mini-1-9031cbfe3e.jpg', 'kvartira-na-carmelo-calvo', 1499418037, 1),
(2, 1, 'Просторная квартира в районе Altozano', '<p>Все услуги в шаговой доступности</p>', 1, 50000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":" 3 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0438","square":"80","sea":"","bathroom":"1 \\u0432\\u0430\\u043d\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/benalua-pardo-gimeno-27-22158538-d7855ab2b7.jpg', 'prostornaa-kvartira-v-rajone-altozano', 1499622483, 1),
(3, 1, 'Уютная квартира с ремонтом и мебелью и с лифтом', '<p>Уютная квартира с ремонтом и мебелью, в доме есть лифт.</p><p>в районе Carolinas на улице Doctor Berguez .</p><p>1 этаж,  лифт</p><p>70m2 , 1 спальня, 1 ванная комната, гостиная, кухня.</p><p>все услуги в шаговой доступности.</p><p>стоимость 57200</p>', 1, 57200, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"Calle del Dr. Bergez, 03012 Alacant, Alicante, \\u0418\\u0441\\u043f\\u0430\\u043d\\u0438\\u044f#ChIJFcTf5wc3Yg0RVo2bGOfHK0w#38.359023#-0.4792068000000427","room":"1 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u044f","square":"70","sea":"10 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"1 \\u0432\\u0430\\u043d\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/carolinas-doctor-bergez-con-call-e94b231bc1.jpg', 'uutnaa-kvartira-s-remontom-i-mebelu-i-s-liftom', 1499623255, 1),
(4, 1, 'Квартира в спокойном районе El Pla', '<p>Квартира в спокойном районе El Pla (Plaza Manila) 1 этаж</p><p>недалеко от центра города и в 15 минутах ходьбы от пляжа.</p><p>70м2. 2спальни,1 ванные, кухня, зал.</p><p>Район с развитой инфраструктурой, рядом с домом магазины, кафе, бары, остановки</p><p>общественного транспорта, школа и детские дошкольные учреждения.</p><p>Очень привлекательное предложение для инвесторов и для желающих приобрести </p><p>свою недвижимость в Испании для круглогодичного проживания.</p><p>59500 т.</p>', 1, 59500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"Plaza Manila, 03013 Alacant, Alicante, \\u0418\\u0441\\u043f\\u0430\\u043d\\u0438\\u044f#ChIJmWSSGQg3Yg0RQEbSut9Qbcw#38.35845399999999#-0.47736090000000786","room":"2 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0438","square":"70","sea":"","bathroom":"1","balkon":"0","pool":"0","parking":"0","popular":"0"}', 'catalog/pla-carolinas-bejar-4-1-planta-2-892d79db1d.jpg', 'kvartira-v-spokojnom-rajone-el-pla', 1499623825, 1),
(5, 1, 'Атик в районе Benalua', '<p>Атик в районе Benalua  на улице Pardo Gimeno .</p><p>5 этаж без лифта</p><p>80M2 , 2 спальни, 1 ванная комната, гостиная, кухня, терраса 16M2.</p><p>все услуги в шаговой доступности.</p><p>стоимость 63.000</p>', 1, 63000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0438","square":"80  ","sea":"","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/benalua-pardo-gimeno-2306190543-1e18a9d590.jpg', 'atik-v-rajone-benalua', 1499799758, 1),
(6, 1, 'Квартира с ремонтом и мебелью в районе ALTOZANO', '<p>Квартира с ремонтом и мебелью в районе ALTOZANO</p><p>1 этаж .85м2 3 спальни, 1 ванная комната, зал, кухня, балкон.</p><p>все услуги в шаговой доступности</p><p>Стоимость 64.000 €</p>', 1, 64000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"85","sea":"","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/altozano-2247455664-87d4b7b897.jpg', 'kvartira-s-remontom-i-mebelu-v-rajone-altozano', 1499800003, 1),
(7, 1, 'Просторная квартира в 500 м от пляже', '<p>просторная квартира в 500 м от пляже</p><p>возле горы SANTA BARBARA </p><p>2 этаж .120м2 4 спальни 1 ваннaz комната</p><p>все услуги в шаговой доступности</p><p>Стоимость 67.000 €</p>', 1, 67000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"120","sea":"500 ","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/alicante-centro-antonio-mauro-23-574254633f.jpg', 'prostornaa-kvartira-v-500-m-ot-plaze', 1499800238, 1),
(8, 1, 'Квартира с ремонтом и лифтом в спальном районе Каролинас Алтас!', '<p>77.000 €</p><p>calle agost</p><p>Caracteristicas basicas</p><p>75 m2 construidos</p><p>71 m2 utiles</p><p>3 dormitorios</p><p>1 wc</p><p>Terraza</p><p>Segunda mano/Buen estado</p><p>Armarios empotrados</p><p>Квартира с ремонтом и лифтом в спальном районе Каролинас Алтас!</p><p>Квартира имеет большой солнечный зал , 3 спальные комнаты, новая кухня столовая, балкон, галерея, ванная комната! </p><p>4-ый этаж с лифтом! </p><p>Район с развитой инфраструктурой , магазины, бутики, парки, </p><p>отличное транспортное сообщение - МЕТРО АВТОБУС!</p><p>?</p>', 1, 77000, NULL, '{"type":"","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"75","sea":"","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/img-20170505-wa0022-5f7729e079.jpg', 'kvartira-s-remontom-i-liftom-v-spalnom-rajone-karolinas-altas', 1499800686, 1),
(9, 1, 'Светлая квартира в центре города с ремонтом и мебелью улице MAESTRO CABALLERO', '<p>Светлая квартира в центре городас ремонтом и мебелью улице MAESTRO CABALLERO, 22 .</p><p>третий этаж без лифта.100м2 2 спальни 1 ванная комната</p><p>все услуги в шаговой доступности</p><p>Стоимость 81.000 €</p>', 1, 81000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"1 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u044f","square":"100","sea":"","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/plaza-de-toros-maestro-caballero-0c4f2818db.jpg', 'svetlaa-kvartira-v-centre-goroda-s-remontom-i-mebelu-ulice-maestro-caballero', 1499803735, 1),
(10, 1, 'Атик просторной террасой и барбекю в спокойном районе Florida Alta', '<p>Атик просторной террасой и барбекю в спокойном районе Florida Alta города на улице Carrer de nit,</p><p>недалеко от центра города и в 25 минутах ходьбы от пляжа Постигет.</p><p>150м2. 2спальни,1 ванная, кухня, зал. Лифт,гараж, зеленая зона, бассейн.</p><p>Район с развитой инфраструктурой, рядом с домом магазины, кафе, бары, остановки</p><p>общественного транспорта, школа и детские дошкольные учреждения.</p><p>Очень привлекательное предложение для инвесторов и для желающих приобрести </p><p>свою недвижимость в Испании для круглогодичного проживания. </p><p>Обладает повышенной ликвидностью,отличный вариант для инвестирования.</p><p>142.000 €</p>', 1, 142000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":" 2\\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0438","square":"150","sea":"3","bathroom":"1 \\u0432\\u0430\\u043d\\u043d\\u0430\\u044f","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/atico-de-lujo-acepto-ofertas-car-1be1aecb8d.jpg', 'atik-prostornoj-terrasoj-i-barbeku-v-spokojnom-rajone-florida-alta', 1499803978, 1),
(11, 1, 'Очень светлая и просторная квартира в спокойном районе города на улице Denia', '<p>Очень светлая и просторная квартира в спокойном районе города на улице Denia,</p><p>недалеко от центра города и в 2 минутах ходьбы от пляжа Постигет.</p><p>Планировка квартиры придётся по вкусу тем, кто ищет много пространства.</p><p>100м2. 1 ванная, 4 спальни, кухня, зал. Район с развитой инфраструктурой, рядом с домом магазины, кафе, бары, остановки общественного транспорта, школа и детские дошкольные учреждения.</p><p>152.000</p>', 1, 152000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"100\\u043c2","sea":"2 \\u043c\\u0438\\u043d\\u0443\\u0442\\u0430\\u0445 \\u0445\\u043e\\u0434\\u044c\\u0431\\u044b \\u043e\\u0442 \\u043f\\u043b\\u044f\\u0436\\u0430 ","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/playa-del-postiguet-avenida-de-d-64184b5457.jpg', 'ocen-svetlaa-i-prostornaa-kvartira-v-spokojnom-rajone-goroda-na-ulice-denia', 1499804213, 1),
(12, 1, 'Квартира с просторной террасой в спокойном элитном районе Cabo de las Huertas', '<p>Квартира с просторной террасой в спокойном элитном районе Cabo de las Huertas города на улице Camino de Faro </p><p>недалеко от центра города и в 2 минутах ходьбы от пляжа.</p><p>100м2. 2спальни,2 ванные, кухня, зал. Лифт, зеленая зона, бассейн.</p><p>Район с развитой инфраструктурой, рядом с домом магазины, кафе, бары, остановки</p><p>общественного транспорта, школа и детские дошкольные учреждения.</p><p>Очень привлекательное предложение для инвесторов и для желающих приобрести </p><p>свою недвижимость в Испании для круглогодичного проживания. </p><p>.192.000</p>', 1, 192000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0438","square":"100","sea":"\\u0432 2 \\u043c\\u0438\\u043d\\u0443\\u0442\\u0430\\u0445 \\u0445\\u043e\\u0434\\u044c\\u0431\\u044b \\u043e\\u0442 \\u043f\\u043b\\u044f\\u0436\\u0430","bathroom":"2 \\u0432\\u0430\\u043d\\u043d\\u044b\\u0435","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/cabo-de-las-huertas-camino-del-f-fead0036c8.jpg', 'kvartira-s-prostornoj-terrasoj-v-spokojnom-elitnom-rajone-cabo-de-las-huertas', 1499804467, 1),
(27, 1, 'хай- тэк апартаменты', '<p>Уникальный хай-тек апартаменты на первой линии  с большой террасой, кладовой, гаражом и ИСКЛЮЧИТЕЛЬНЫМ ВИДОМ НА МОРЕ в отличном райне Аликанте Эль Кампейо.</p>', 1, 140000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"67","sea":"100","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/559540e9c88ce-9901e60f65.jpg', 'haj-tek-apartamenty', 1500744268, 1),
(25, 1, 'Уютная квартира в центре', '<p>Полезная площадь 115m2, 2 Кол-во спален, 2 Ванные комнаты, Расстояние от моря 600m., Лифт, бассейн, Все окна выходят на улицу.</p>', 1, 126000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"130","sea":"600","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"1"}', 'catalog/54d3873a52ece-ca09e9366d.jpg', 'uutnaa-kvartira-v-centre', 1500578544, 1),
(24, 1, 'Красива Вилла', '<p>Супер Шале новой постройки в дальнем элитноем районе Аликанте - Эль Кампельо с бассейном, в одном из лучших</p><p>жилых районов в нескольких минутах ходьбы от Пляжа и Центра города. В урбанизации есть корт для Тенниса и</p><p>детская игровая площадка.</p><p>Солнечные коллекторы для снабжения горячей водой жилья.</p><p>Машино места на две машины</p><p>Кондиционеры</p><p>Кухня меблирована и оснащена всей необходимой бытовой техникой.</p><p>3 СПАЛЬНИ, НО ЕСТЬ ВОЗМОЖНОСТЬ СДЕЛАТЬ МИНИМУМ 4. 3 ванных комнаты. Площадь 210 кв м.Участок 377</p><p>метров квадратных. Террасы от 11 до 20 метров!</p><p>Большой подвал с освещением.</p><p>Хорошая связь с шоссе-автострады.</p><p>1000 метров для пляжа!</p>', 1, 360000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"225","sea":"1000","bathroom":"3","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/5570755d4b914-5876a35631.jpg', 'krasiva-villa', 1500578085, 1),
(26, 1, 'квартира на первой линии', '<p>Апартаменты на 1 линии моря с полной меблировкой в закрытой. Урбанизация с бассейном в элитном пригороде Аликанте Эль Кампельо. </p><p>В цену включено парковочное место. Площадь 65 м2, 2 спальни, 1 ванная, гостиная, терраса и отдельная кухня. панорамные виды. Вся городская инфраструктура. </p><p>Эль Кампельо пригород Аликанте -элитное место проживания круглый год. всего 15 минут до центра Аликанте. Рестораны со свежими морепродуктами, кафе, 7ми километровая линия пляжа, трамвай на воздушной подушке TRAM , спа центры, магазины, аптеки и колледжи. До центра Аликанте всего 15 минут.</p>', 1, 150000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"65","sea":"50","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/57338c0d90a48-6d1a05ddba.jpg', 'kvartira-na-pervoj-linii', 1500743979, 1),
(20, 1, 'Квартира в Аликанте', '<p>Продается практически новая квартира в закрытой урбанизации с общим бассейном и спортивной зоной, в 800 метрах от пляжа Сан-Хуан (Аликанте). Жилье состоит из двух спален и двух ванных комнат. Установки полного кондиционирования воздуха, отдельная кухня и большая Терраса, которая придает жилью превосходное освещение. Также включено парковочное место и кладовая. </p>', 1, 190000, NULL, '{"type":"","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"78","sea":"800","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/559535f0992b7-c6e48eba05.jpg', 'kvartira-v-alikante', 1500494123, 1),
(21, 1, 'Квартира в  Аликанте', '<p>Новые апартаменты от застройщика в дальнем пригороде города Аликанте - Эль Кампейо </p>', 1, 115500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"60","sea":"1000","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/55952bfe97f3a-3e568bcffd.jpg', 'kvartira-v-alikante-2', 1500494492, 1),
(22, 1, 'Таунхаус', '<p>Закрытый жилой комплекс на 42 семьи, в шаговой доступности до самого лучшего поля для гольфа (Alicante Golf 18-луночное гольф-поле Сан-Хуан Плайа, Аликанте). В свою очередь, находятся всего в 800 метрах от одного из лучших пляжей в Испанском - пляж Сан-Хуан. Море, солнце, пляж и гольф - все в одном месте! Спешите!</p>', 1, 260000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"250","sea":"800","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/5571a6d1876c2-d9bf3c9123.jpg', 'taunhaus', 1500576336, 1),
(23, 1, 'Просторная квартира', '<p>Супер яркая квартира в привилегированном месте рядом с полем для гольфа и пляжем Сан Хуан. Все услуги рядом, автобус, трамвай, супермаркеты, школы, банки. Жилье с 2 спальнями, салон-столовая, оборудованная кухня, 2 ванные комнаты, застекленная терраса, гараж, кладовая, 1 этаж. Урбанизации с бассейном, площадкой для детей, теннис, детские игры. Пляжа около 900 метров.</p>', 1, 165000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"90","sea":"900","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/55719ca103f45-d7224789b1.jpg', 'prostornaa-kvartira', 1500577312, 1),
(19, 1, 'Квартира в Аликанте, Испания, 35 м2', '<p>Прекрасный жилой комплекс, состоящий из апартаментов с 1 и 2 спальнями, расположенный в Эль-Кампельо - Коста Бланка, между Аликанте и пляжем Сан-Хуан. В апартаментах есть стеклокерамическая плита, духовка, кондиционер, встроенные шкафы и просторные террасы. Помимо этого, вы можете насладиться прекрасным видом на море. Идеальные квартиры, чтобы жить здесь весь год!</p>', 1, 125000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"35","sea":"800","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/55953f0e81835-3bceaa76cb.jpg', 'kvartira-v-alikante-ispania-35-m2', 1500409275, 1),
(28, 1, 'Таунхаус', '', 1, 90000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435, \\u0418\\u0441\\u043f\\u0430\\u043d\\u0438\\u044f#ChIJS6udO9o1Yg0R44ELrHKofR0#38.3459963#-0.4906855000000405","room":"3","square":"74","sea":"1200","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/54d1f1dd13c2d-a7993db917.jpg', 'taunhaus-2', 1500744605, 1),
(29, 1, 'Таунхаус', '', 1, 95000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"57","sea":"250","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/54d1d966d75ab-0829cc18d5.jpg', 'taunhaus-3', 1500745305, 1),
(30, 1, 'Квартира в  Аликанте с лифтом', '', 1, 100000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"73","sea":"100","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/54d0dd6eb3b30-0581907b3e.jpg', 'kvartira-v-alikante-s-liftom', 1500745651, 1),
(31, 1, 'Квартира в  Аликанте', '', 1, 100000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435, \\u0418\\u0441\\u043f\\u0430\\u043d\\u0438\\u044f#ChIJS6udO9o1Yg0R44ELrHKofR0#38.3459963#-0.4906855000000405","room":"3","square":"93","sea":"100","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/54d3655486034-dd710630a4.jpg', 'kvartira-v-alikante-3', 1500746142, 1),
(32, 1, 'очень светлые аппартаменты', '', 1, 110000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"80","sea":"250","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/54d3551bddb9b-ac978a4bd8.jpg', 'ocen-svetlye-appartamenty', 1500746312, 1),
(33, 1, 'квартира в урбанизации', '', 1, 110000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"98","sea":"150","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/54d353df6f0a7-8936ce04ff.jpg', 'kvartira-v-urbanizacii', 1500746791, 1),
(34, 1, 'Продается квартира в Хуан Баллеста, Carolinas Альтас, Аликанте / Алакант', '<p>«Отличная возможность. Имущество банковского учреждения. Корпус 116 м2 с 3 спальни, ванная комната, туалет, кухня и гостиная. <br>Здание 4 этажа, хорошо расположен вдоль Авенида Periodista Родольфо Салазара. <br>Очень хорошо связан с автобусами и трамвайной остановки . обязательно договоритесь с визитом ".</p>', 1, 80000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"116","sea":"1,5 ","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/191538737-56c0407f60.jpg', 'prodaetsa-kvartira-v-huan-ballesta-carolinas-altas-alikante-alakant', 1504433168, 1),
(35, 1, 'дом в аликанте', '<p> отдельно стоящий дом в 1 - й линии от поля для гольфа, новый 500 м2 участок с застроенной площадью 300 м2 с 4 - мя спальнями (люкс) и 5 ванных комнат, которые распределяются следующим образом ;. <br><br>Подвал, гараж и хранения, а также возможность сделать комнату более ванную комнату и отдельную многофункциональную комнату в гараже, в соответствии с потребностями клиентов. <br><br>Первый этаж: гостиная с террасой с видом на бассейн и поле для гольфа, кухня и двухместный номер с ванной, Люкс . <br><br>1 - й этаж, 3 двухместный номер с ванной комнатой, люкс (одна с джакузи) , <br><br>3 - й этаж, солярий с видом на поле для гольфа и фотографа проспект FCO Cano, с возможностью , чтобы расширить дом ..<br><br>Дом построен с очень хорошими качествами. Он очень яркий , когда ориентация, имеют центральное отопление, предварительно установки A / A, видеодомофон на все этажи, возможность лифт с цокольного этажа. <br>Отличное место быть всего в нескольких метрах от пляжа Сан - Хуан и поле для гольфа с различных областях бизнеса. RE 0023. "</p>', 1, 760000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"300","sea":"","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', NULL, 'dom-v-alikante', 1504433428, 1),
(36, 1, 'Продается квартира в Научной Джейми Сантана, 7, Плайя-де-Сан-Хуан, Alicante / Alacant', '<p>Отличная возможность Дом в авеню Historiador Висенте Рамос в хорошей урбанизации. Это 4 - й этаж угловая все внешние, распределены в 3 больших спальни, 2 ванные комнаты, кухня и подсобное комната , гостиная с террасой доставляя массу от света к корпус. <br>Очень хорошее качество с мраморными полами, а / с холодной и тепловой воздуховоды естественнее газовое отопление по всему дому, гладкие стены. <br>в комплексе есть бассейн, детская площадка, весло, социальный клуб и играть область для самых маленьких . <br>имущество продается с гаражом и складское помещение в подвале.<br>Идеальное место , чтобы жить для его отличное расположение находясь рядом все удобства, магазины, C. Comercial Torre Golf, школы, банки, аптеки и т.д. ... Хорошо связан с трамвайной и автобусной остановки. <br>Не позволяйте  возможности пройти мимо вас.</p>', 1, 210000, NULL, '{"type":"","city":"","address":"","room":"3","square":"120","sea":"","bathroom":"","balkon":"1","pool":"0","parking":"0","popular":"0"}', NULL, 'prodaetsa-kvartira-v-naucnoj-dzejmi-santana-7-plaja-de-san-huan-alicante-alacant', 1504434459, 1),
(37, 1, 'Продается квартира в Мария Молинер, Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>Великолепная и просторная квартира в Av. Historiador Висенте Рамос. 142 м2 распределены в 3 - х до 4 , но дали номер на кухню, 2 ванные комнаты, гостиная с террасой, деревянные полы, гладкие стены, кондиционер воздуховоды по всему дому. гараж и хранения. <br>всеобъемлющая бассейн, детская площадка, тренажерный зал, лопатка и байк подвал. <br>Идеально , чтобы жить, находиться в главной зоне в окружении всех видов услуг вместе школы, Torre Golf торговый центр, банки, супермаркеты, остановки трамвая и автобуса</p>', 1, 238500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"","sea":"","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/201661587-178978e73a.jpg', 'prodaetsa-kvartira-v-maria-moliner-plaja-de-san-huan-alikante-alakant', 1504458191, 1),
(38, 1, 'Продается квартира в Калье Фотограф Гойо, Playa San Juan, Alicante / Alacant', '<p>Сказочные и просторный дом углу все внешние, с 2 спальни, 2 ванные комнаты, одна с ванной, гостиной с кухней, солярии с большой галереи, холодной кондиционирования воздуха и тепловых каналов по всему дому, гараж и хранения , Urb. с бассейном и играми. <br>отличное место , чтобы жить со всеми видами услуг, школы, колледжи, гр. медицинским и общественным транспортом места. лучше , чтобы посетить </p>', 1, 174000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"95","sea":"","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', NULL, 'prodaetsa-kvartira-v-kale-fotograf-gojo-playa-san-juan-alicante-alacant', 1504458714, 1),
(39, 1, 'Продается квартира в дорожном Benimagrell, Playa Мучавист, Кампеий', '<p>ПРОДВИЖЕНИЕ ЗАВЕРШЕНО новое строительство в PLAYA Мучавист здание 4 этажа в общей сложности 12 домов, менее чем в 100 метров от пляжа, уникальные в этой области для его стиля Качество высокоее.. - конец делает его уютным и в то же время современного. все дома с отличной отделкой, с выдающимся вниманием к деталям, гладким стенам, деревянным полам, оснащен большие окна , доставляя массу от света на весь дом. в комплексе есть бассейн и просторный искусственный газон. <br>дома , Они включают в себя гараж и хранения. <br>Здание очень хорошо расположен у трамвайной остановки, автобус несколько метров и зоны обслуживания. <br>Проверить цену в соответствии с высотой. 2, 3 и Penthouse</p>', 1, 191500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"86","sea":"100","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/204564153-1c8efc512e.jpg', 'prodaetsa-kvartira-v-doroznom-benimagrell-playa-mucavist-kampeij', 1504459063, 1),
(40, 1, 'Продается квартира в Маэстро Алонсо, Бугор, Аликанте / Алакант', '<p> квартира в Аликанте C / Maestro Алонсо в нескольких метрах от больницы общего Аликанте. Дом имеет 85 м2 и распределяется в трех спален, гостиной, кухни и ванной комнаты. Ферма расположена расположено на первом этаже в здании четырех высот без лифта. </p>', 1, 46200, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"85","sea":"1500","bathroom":"","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/193450728-d036960032.jpg', 'prodaetsa-kvartira-v-maestro-alonso-bugor-alikante-alakant', 1504461140, 1),
(41, 1, 'Продается квартира в Espronceda, Carolinas Bajas, Аликанте / Алакант', '<p>квартира в Carolinas области. - Pio XII, дом находится на первом этаже здания без лифта имеет площадь в 115 м2 с 4 спальни, ванная, туалет, без мебели кухни и гостиной очень хорошо связаны между собой. со всеми магазинами.</p>', 1, 68000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"115","sea":"700","bathroom":"4","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/218672506-e721e8353d.jpg', 'prodaetsa-kvartira-v-espronceda-carolinas-bajas-alikante-alakant', 1504461462, 1),
(42, 1, 'Таунхаус на продажу в La Альбуфереты, Alicante / Alacant', '<p>Террасный дом в районе Альбуфера. В настоящее время есть два дома с отдельным входом с о возможности объединения их . На <br>первом этаже в хорошем состоянии , с гостиной, кухней, ванной комнатой, 2 спальни и терраса. <br>Первый этаж с тем же распределением , чтобы обновить. <br>дом находится примерно в 300 метрах от пляжа в Албуфейра, рядом с магазинами и автобусной остановкой ".</p>', 1, 130000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"90","sea":"300","bathroom":"4","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/196297844-1-462dab1426.jpg', 'taunhaus-na-prodazu-v-la-albuferety-alicante-alacant', 1504462763, 1),
(43, 1, 'Продается квартира на улице Calle Pintor Otilio Serrano, Altozano, Alicante / Alacant', '<p>Отличное жилье рядом с больницей Аликанте в C / Pintor Otilio Serrano, состоит из 3 спален, 2 ванных комнат, кухни с галереей и гостиной с террасой, с кондиционером холода и тепла. Недвижимость <br>продается полностью меблированной и оборудованной кухней с электроприборами. <br>Здание, расположенное в коммерческом районе со всеми видами магазинов и услуг, очень хорошо связанное с общественным транспортом.</p>', 1, 84900, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"92","sea":"","bathroom":"2","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/187016496-878a5acc7b.jpg', 'prodaetsa-kvartira-na-ulice-calle-pintor-otilio-serrano-altozano-alicante-alacant', 1506372938, 1),
(44, 1, 'Квартира для продажи в Calle Maruja Pastor, Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>Квартира  расположена на третьем этаже с 2 спальнями с двуспальными кроватями, большой ванной комнатой, полностью оборудованной кухней и гостиной, половицами, двойным остеклением, рядом с автобусной и трамвайной остановкой, рядом с пляжем и центром города.В урбанизации <br>есть бассейн, площадка для игры в паддл, теннисный корт, социальный клуб, детская площадка. Рядом<br>школы, торговый центр Torre Golf, окруженный различными магазинами и коммерческими районами, такими как Меркадона и т. д. <br>Идеально подходит для проживания круглый год или инвестиций в аренду.</p>', 1, 154900, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"\\u041f\\u043b\\u0430\\u0439\\u044f \\u0434\\u0435 \\u0421\\u0430\\u043d \\u0425\\u0443\\u0430\\u043d, \\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435, \\u0418\\u0441\\u043f\\u0430\\u043d\\u0438\\u044f#ChIJbSfUwL05Yg0Rk5oUwK9_ODo#38.3737418#-0.42665839999995114","room":"3","square":"85 ","sea":"","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/229897870-ff72b8c280.jpg', 'kvartira-dla-prodazi-v-calle-maruja-pastor-plaja-de-san-huan-alikante-alakant', 1506373618, 1),
(45, 1, 'Квартира на продажу в Calle Trident, Кабо-де-лас-Уэртас, Аликанте / Алакант', '<p>Собственность распределена в 4 спальнях (3 двухместных и 1 одноместных), оборудованная кухня со стиральной машиной, посудомоечной машиной, холодильником, плитой и духовкой, ванная комната, туалет и гостиная с доступом на террасу, дом отремонтирован с деревянными полами, новые двери,  свежеокрашенная,кондиционирование. Находится в отличной урбанизации с теннисным кортом,бассейном,детской площадкой,парковка.Рядом развитая инфраструктура,школа,поликлиника.<br></p>', 1, 188000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"100","sea":"","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/229151957-e41b24cb2a.jpg', 'kvartira-na-prodazu-v-calle-trident-kabo-de-las-uertas-alikante-alakant', 1506375352, 1),
(46, 1, 'Квартира для продажи в Сан-Хуане,Аликанте.', '<p>«Отличная квартира площадью 85 м2, чтобы войти и жить. Она находится на пятом этаже(лифт),2 спальни, ванная комната, полностью оборудованная кухня, гостиная площадью 24 м2 с застекленной террасой, кондиционер и усиленная <br>дверь, с бассейном и бесплатной парковкой. <br>Финка очень хорошо общается с автобусной и трамвайной остановкой , недалеко от школ, торгового центра Fontana и различных магазинов и торговых площадок, таких как Меркадона и т. д. <br>Идеально подходит для лета, проживать весь год или как инвестировать в аренду, находясь всего в нескольких метрах от пляжа.</p>', 1, 120000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"85","sea":"\\u0431\\u043b\\u0438\\u0437\\u043a\\u043e","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/226736284-e86dfa2857.jpg', 'kvartira-dla-prodazi-v-san-huanealikante', 1506376105, 1),
(47, 1, 'Продается квартира на проспекте Голета, Кабо-де-лас-Уэртас, Аликанте / Алакант', '<section><h2>Основные функции</h2><ul><li>120 m²</li><li>3 спальни 2 ванные комнаты</li><li>2 ванные комнаты</li><li>терраса</li><li>Парковка включена в стоимость</li><li>Перепродажа / в хорошем состоянии</li><li>Встроенные шкафы</li><li>Южная ориентация</li><li>Сертификация энергии: в процессе</li></ul><h2>строительство</h2><ul><li>Внешний вид 8-го этажа</li><li>С лифтом</li></ul><h2>оборудование</h2><ul><li>Кондиционирование воздуха</li><li>Плавательный бассейн</li><li>сад</li></ul></section><a id="statsPosition" name="statsPosition"></a><h2>Город:</h2><ul><li>Авенида Голета</li><li>Соседства Кабо де лас Уэртас</li><li>Район Сан-Хуан-Эль-Кабо-Бич</li><li></li><li>Аликанте / Алакант «Элегантное жилье с приятным видом на море, идеально подходящее для проживания,рядом со всеми видами услуг, школами, здоровьем и т. д.Войти и жить.</li></ul>', 1, 245000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"120","sea":"","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/207626183-42f1c43d99.jpg', 'prodaetsa-kvartira-na-prospekte-goleta-kabo-de-las-uertas-alikante-alakant', 1506377076, 1),
(48, 1, 'Квартира для продажи в Trident, Cabo de las Huertas, Аликанте / Alacant', '<p>«Красивая квартира,полностью меблированная ,рядом со всеми услугами, школами и городским транспортом. <br>Она состоит из 2 спален, ванной комнаты, гостиной с террасой,кондиционер.Урбанизация с бассейном и парковкой. </p>', 1, 115000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"70","sea":"","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/228784048-d9d3e851e7.jpg', 'kvartira-dla-prodazi-v-trident-cabo-de-las-huertas-alikante-alacant', 1506377623, 1),
(49, 1, 'Квартира для продажи в Хуан Балеста, Каролинас Алты, Аликанте / Алакант', '<p>Недвижимость банка площадью 116 м2, построенная с 3 спальнями, ванной комнатой, туалетом, кухней и гостиной. квартира на 1 этаже с лифтом, дом расположен вдоль Авенида Журналисты Родольфо Салазар. Рядом с автобусами и трамвайной остановкой.</p>', 1, 79811, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"113","sea":"","bathroom":"2","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/191538737-ef10363404.jpg', 'kvartira-dla-prodazi-v-huan-balesta-karolinas-alty-alikante-alakant', 1506378110, 1),
(50, 1, 'Квартира для продажи в Calle Calle Fotógrafo Goyo, Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>«Просторная квартира с 2 спальнями, 2 ванными комнатами, основной с гидромассажем, гостиной с застекленной террасой, кухней с большой галереей, кондиционером холода и тепла через воздуховоды по всему дому, гаражом и кладовой , урбанизация с бассейном и <br>игровой площадкой. Отличное место, идеально подходит для  проживания,рядом все виды услуг, школа, институт, поликлиника и общественный транспорт.</p>', 1, 174000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"95","sea":"","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/186102879-fcd0b832a1.jpg', 'kvartira-dla-prodazi-v-calle-calle-fotografo-goyo-plaja-de-san-huan-alikante-alakant', 1506378557, 1),
(51, 1, 'Квартира для продажи  Playa Muchavista, El Campello', '<p>«ПРОДВИЖЕНИЕ НОВОГО СТРОИТЕЛЬСТВА на PLAYA MUCHAVISTA. Здание с 4-мя высотами, в общей сложности 12 домов, менее чем в 100 метрах от пляжа, уникально в области по своему стилю. Высокие  качества делают его уютным и современным,все дома с отличной отделкой, с выдающимся вниманием к деталям, гладким стенам, деревянным полам, оснащены большими окнами ,кондиционер, в комплексе есть бассейн и просторный искусственный газон. <br> гараж и кладовая. <br>Расположение очень хорошее, рядом трамвай, автобус  и <br>зоны обслуживания.</p>', 1, 191500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"86","sea":"100","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/204543969-c02c950ff9.jpg', 'kvartira-dla-prodazi-playa-muchavista-el-campello', 1506379601, 1),
(52, 1, 'Продается квартира пляж  Ла Альбуфереты', '<p>«Отличная недвижимость с потрясающим видом на море. Полностью отремонтирована. Это 16-й и последний этаж здания, 115 м2, распределенный в 3 спальнях, 2-х ванных комнатах, кухня со всеми бытовыми приборами, гостиная где вы можете насладиться захватывающим видом на море от Бенидорма до Аликанте. </p><p>Дом полностью отреставрирован с использованием первоклассных материалов, поворотных окон oscilobatientes с моторизованными жалюзи, белыми внутренними столярными изделиями, 4-мя кондиционерами климатизации холода и тепла в гостиной и спальнях , гладкие стены .. <br>Она продается с гаражом 14 м2, и вся мебель совершенно новая, готова к заселению.<br>квартира находится в урбанизации с 2 бассейнами, детской площадкой, спортивным кортом и теннисом. <br>Рядом с несколькими трамвайными остановками и автобусом. "</p><h2><br></h2>', 1, 270000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"115","sea":"\\u0431\\u043b\\u0438\\u0437\\u043a\\u043e","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/220034626-c1f0c77cbe.jpg', 'prodaetsa-kvartira-plaz-la-albuferety', 1506380547, 1),
(53, 1, 'Дом или вилла на продажу на пути дель Фару, Кабо-де-лас-Уэртас, Аликанте / Алакант', '<p>«Исключительная и современная независимая вилла в лучшем жилом районе Кабо-де-лас-Уэртас. <br>Недвижимость распределена на 4 этажах с внутренним лифтом. На главном этаже находится гостиная с выходом на большое крыльцо с участком,который окружает в весь дом около 175 м2, большая кухня с выходом на другую террасу и ванную комнату. на первом этаже находятся 3 спальни, основные с террасой, гардеробной комнатой и ванной , и второй ванной для двух других спален. <br>в второй этаж чердак или 4 спальни плюс две просторные террасы с прекрасным видом на бассейн и море. <br>в подвале есть вторая гостиная или зона отдыха, ванная комната, помещение для хранения и гараж. <br><br>Идеально подходит для спокойной и комфортной жизни, рядом все виды услуг, школа, трамвайная остановка и автобуса в нескольких минутах ходьбы ».</p>', 1, 669000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"6","square":"329","sea":"\\u043d\\u0435\\u0434\\u0430\\u043b\\u0435\\u043a\\u043e","bathroom":"4","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/213807325-de7c6dc5c0.jpg', 'dom-ili-villa-na-prodazu-na-puti-del-faru-kabo-de-las-uertas-alikante-alakant', 1506426744, 1),
(54, 1, 'Отдельный дом на продажу в Де-Каролина Паскуаль, Ла Альбуферета, Аликанте / Алакант', '<p>«Уютный дом на углу с участком 220 м2 и площадью 320 м2, построенный на 4 этажах с 4 спальнями (возможность 1 спальни на этаже, 3 ванные комнаты, большая кухня, гостиная с установкой домашнего кинотеатра, отопление, кондиционер холод и тепло, мансарда площадью 30 м2, терраса, крыльцо со 100 м2 частного сада, встроенные шкафы и сад, гараж на 3 машины и кладовая. Итальянские полированные мраморные полы, клималит с жалюзи с теплоизоляцией, противоугонная система, сигнализация, урбанизация с коммунальными зонами, бассейн, детская игровая площадка, в нескольких метрах от трамвайной остановки и автобуса, недалеко от всех видов услуг. <br>Это лучшее жилье в урбанизме имеет своеобразный и уникальный дизайн, имеющий прямой доступ с улицы и находится в непревзойденном районе, идеально подходит для жизни ».</p>', 1, 410000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"320","sea":"","bathroom":"3","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/197850173-d99478bff2.jpg', 'otdelnyj-dom-na-prodazu-v-de-karolina-paskual-la-albufereta-alikante-alakant', 1506427540, 1),
(55, 1, 'Квартира для продажи в Авеню де Ницца, 25, Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>210 М2 в 1-й линии Плайя-де-Сан-Хуан с прекрасным видом на море. Это два пристроенных дома с 5 спальнями, 3 ванными комнатами, большой угловой террасой, окружающей весь дом, что дает большую яркость. парковка <br>В урбанизации есть общий бассейн и детская площадка, расположенная напротив набережной Сан-Хуан- <br>Бич. Расположение непревзойденно вблизи торгового центра, супермаркетов и общественного транспорта с трамвайной остановкой и автобусом у двери. "</p>', 1, 390000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"6","square":"210","sea":"\\u0432 \\u043f\\u0435\\u0448\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0441\\u0442\\u0438","bathroom":"3","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/217402947-57cee0ce36.jpg', 'kvartira-dla-prodazi-v-avenu-de-nicca-25-plaja-de-san-huan-alikante-alakant', 1506434347, 1),
(56, 1, 'Пентхаус на продажу Benimagrell, 12, Playa Muchavista, El Campello', '<p>«Новое строительство в PLAYA Мучавист. Квартира на  4 этаже, менее чем в 100 метров от пляжа. <br>Квартира-пентхаус имеет свою террасу 64 м2 . Высококачественная  отделка делают её уютной и современной. Все дома с отличной отделкой, где внимание было уделено до мельчайших деталей, гладкие стены, половицы, оборудованные и большие окна,светлые комнаты. В урбанизации бассейн . В апартаментах есть гараж и складское помещение. <br>Расположение очень хорошее , рядом остановка трамвая, автобуса и зоны обслуживания .</p>', 1, 289500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"159","sea":"\\u0432 \\u043f\\u0435\\u0448\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0441\\u0442\\u0438","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/204558532-c4ce2806fd.jpg', 'penthaus-na-prodazu-benimagrell-12-playa-muchavista-el-campello', 1506434798, 1),
(57, 1, 'Квартира для продажи в Polop, Garbinet-Parque de las Avenidas, Аликанте / Алакант', '<p>Отличное размещение вдоль Avenida Gran Via. Имеет 3 спальни, 2 ванные комнаты, кухня, гостиная с кондиционером и просторной и удобной террасой. <br>Квартира продается с гаражом и кладовой. <br>Она находится в урбанизации с плавательным бассейном, игровой площадкой и социальным <br>клубом. Месторасположение  находится недалеко от торгового центра Gran Vía, школ и всех видов услуг и хорошо связано с общественным транспортом.</p>', 1, 155000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"110000","sea":"10 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/213799322-4d46d166c1.jpg', 'kvartira-dla-prodazi-v-polop-garbinet-parque-de-las-avenidas-alikante-alakant', 1506435676, 1),
(58, 1, 'Двухквартирный дом на продажу в дель Сарго, Кабо-де-лас-Уэртас, Аликанте / Алакант', '<p>Уютная и современная двухквартирная вилла в урбанизации в районе Cabo Huertas и в нескольких метрах от моря и пляжа. Недвижимость находится на углу, что делает ее отличной от остальных. Она распределена на 4 этажах и имеет внутренний лифт, делающий повседневную жизнь более комфортной. Первый этаж с гостиной / столовой, гостевым туалетом, большой кухней с большой галереей и большой террасой. Первый этаж с тремя спальнями, 2 ванная комната (одна ванная), четвертый этаж с 2 большими террасами и гостиной. <br>Собственность оснащена кондиционером по всему дому, газовым отоплением, мраморными полами, бронированной дверью, сигнализацией, музыкой и т. д.<br>Она расположена рядом с  общественным транспортом, вдоль автобусной и трамвайной остановки.</p>', 1, 550000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"370","sea":"\\u0432 \\u043f\\u0435\\u0448\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0441\\u0442\\u0438","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/203480057-5bfa2254cc.jpg', 'dvuhkvartirnyj-dom-na-prodazu-v-del-sargo-kabo-de-las-uertas-alikante-alakant', 1506436164, 1),
(59, 1, 'Продается квартира в Роуд Колония Романа, Ла Альбуферета, Аликанте / Алакант', '<p>Отличное жилье площадью 100 м2 построено в Junto Av. Dep. Miriam Blasco.Оно расположено на пятом этаже с 3 спальнями, 2 ванными комнатами, кухней, полностью отремонтировано и оборудовано, большая гостиная 30 м2, керамическим полом , внутренние столярные изделия из дуба, клималит, передний тент. <br>Урбанизация с бассейном, парковочное место в собственности и кладовая. <br>Собственность очень хорошо связана с автобусной и трамвайной остановкой, а также школами, различными магазинами и торговыми площадями и т. д. <br>Идеально подходит для жилья весь год или как инвестировать в аренду, находясь всего в нескольких метрах от пляжа ».</p>', 1, 175000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"100","sea":"\\u0432 \\u043f\\u0435\\u0448\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0441\\u0442\\u0438","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/229889409-674d9dbf1a.jpg', 'prodaetsa-kvartira-v-roud-kolonia-romana-la-albufereta-alikante-alakant', 1506436587, 1);
INSERT INTO `easyii_catalog_items` (`id`, `category_id`, `title`, `description`, `available`, `price`, `discount`, `data`, `image_file`, `slug`, `time`, `status`) VALUES
(60, 1, 'Квартира на продажу с. Denia, Vistahermosa, Аликанте / Alacant', '<p>Отличная квартира для продажи в Complejo Vistahermosa из Аликанте, рядом с CEU и Jesuitas(школа), дом экстерьер, угловой и очень яркий. Это второй этаж с 3 спальнями очень просторными,2мя ванными комнатами, кухня, столовая+гостиная с выходом на террасу, бесплатная парковка и урбанизация с бассейном, теннисным кортом и несколько магазинов в том же дворе. <br></p>', 1, 158500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"10","sea":"10 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/199584564-d75292de5a.jpg', 'kvartira-na-prodazu-s-denia-vistahermosa-alikante-alacant', 1506436979, 1),
(61, 1, 'Продается квартира на улице Virgen del Puig , Tómbola - Рабаса, Аликанте / Алакант', '<p>Великолепная квартира, полностью отремонтированная в Лос-Анджелесе рядом с Universidad.It имеет построенную площадь 116 м2, состоит из 3 спален, 2 ванных комнат, большой гостиной 25 м2 с террасой. Клималит корпуса,  холод и тепло в нескольких комнатах, установка осмоса, полная урбанизация с парковкой, бассейн, социальный клуб, теннисный корт и футбол. Отличное расположение и очень хорошо связано с общественным транспортом, а также всевозможные услуги для повседневной жизни.</p>', 1, 131000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"116","sea":"15 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/180747037-b520efce5f.jpg', 'prodaetsa-kvartira-na-ulice-virgen-del-puig-tombola-rabasa-alikante-alakant', 1506437570, 1),
(62, 1, 'Квартира для продажи в Кико Санчес, Кабо-де-лас-Уэртас, Аликанте / Алакант', '<p>Великолепная квартира с видом на море рядом с Авдой. Dep. Miriam Blasco.Floor расположена на пятом этаже  9этажного здания. Имеет 75 м2 с 2 спальнями , ванной комнатой, кухней с галереей и гостиной, столовая,кондиционирование горячее и холодное, солярий с прекрасным видом на море и парк Адольфо Суарес.Урбанизация с бассейном и социальным <br>клубом. Финка очень хорошо расположена в окружении всех видов услуг, магазинов, торгового центра Torre Golf, Supercor, Mercadona, трамвайной остановки, автобуса и в нескольких минутах от  пляжа, идеально подходит для проживания или сдачи в аренду посуточно в сезон.</p>', 1, 139900, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"75","sea":"\\u0431\\u043b\\u0438\\u0437\\u043a\\u043e","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/218170872-046c9a64a5.jpg', 'kvartira-dla-prodazi-v-kiko-sances-kabo-de-las-uertas-alikante-alakant', 1506438953, 1),
(63, 1, 'Продается квартира в Район Сан-Хуан-Эль-Кабо-Бич', '<p>Уютный дом в Пайя-де-Сан-Хуан-Кондомина». Квартира с  3 спальнями , 2 ванные комнаты , кухня с галереей, гостиная и солярий, подогрев полов, кондиционер- горячий и холодный воздух. </p><p>Продажа с гаражом и складским помещение в в подвале. <br>Корпус в хорошем комплексе с бассейном, детской площадкой . Идеально подходит для жизни в окружении всех видов магазинов, услуг, с. бизнес, школы, несколько метров от остановки автобуса и трамвайной остановки. "</p><h2><br></h2>', 1, 239000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"110","sea":"\\u0431\\u043b\\u0438\\u0437\\u043a\\u043e","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/205423918-d0e60e3ad5.jpg', 'prodaetsa-kvartira-v-rajon-san-huan-el-kabo-bic', 1506439473, 1),
(64, 1, 'Продается квартира , Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>Квартира расположена на окраине Сан-Хуана, в жилом районе, недалеко от пляжа , <br>урбанизация с бассейном, играми, спортивным кортом и бесплатной парковкой. <br>Расположение отличное,  в нескольких метрах от моря и очень хорошо связано с трамвайной остановкой, автобусом и торговым центром со всеми видами услуг. "</p>', 1, 133000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"110","sea":"\\u0432 \\u043f\\u0435\\u0448\\u0435\\u0439 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0441\\u0442\\u0438","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/212620794-b357d3ce09.jpg', 'prodaetsa-kvartira-plaja-de-san-huan-alikante-alakant', 1506520835, 1),
(65, 1, 'Квартира для продажи ул. Маэстро Алонсо, Альтозано, Аликанте / Алакант', '<p>Собственность банковского происхождения в Аликанте C / Maestro Alonso находится в нескольких метрах от центрального госпиталя  Аликанте. квартира на 1 этаже,площадь 85 м2,  распределены в трех спальнях, гостиной-столовой, кухне и ванной комнате. Рядом автобусная и трамвайная остановки,супермаркеты продуктовые.</p><h2><br></h2>', 1, 46200, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"85","sea":"15 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/193450724-32f7dddd60.jpg', 'kvartira-dla-prodazi-ul-maestro-alonso-altozano-alikante-alakant', 1506521191, 1),
(66, 1, 'Квартира для продажи в Марии Молинер, Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>Великолепное и просторное жилье в Ав. Historiador Vicente Ramos, 142 м2,   3 спальни, кухня, 2 ванные комнаты, гостиная с террасой, половицы, гладкие стены, кондиционер по каналам во всем доме. Паркинг и складское <br>помещение. Полная урбанизация с бассейном, детской площадкой, тренажерным залом и велосипедной <br>комнатой.  Район, окружен всеми видами услуг,  школы, торговый центр Torre Golf, банки, супермаркеты, трамвайная остановка и автобусы и т. д. ... »</p>', 1, 238500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"142","sea":"\\u0431\\u043b\\u0438\\u0437\\u043a\\u043e","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/201661587-9364dd69c7.jpg', 'kvartira-dla-prodazi-v-marii-moliner-plaja-de-san-huan-alikante-alakant', 1506521907, 1),
(67, 1, 'Квартира для продажи в Ansaldo, La Albufereta, Alicante / Alacant', '<p>«Недвижимость находится на втором этаже здания, имеет площадь 125 м2,  3 спальни , 2 ванные комнаты, кухня, подсобное помещение, гостиная 30 м2 плюс 15 м2 терраса с видом на комплекс, мраморные полы, газовое отопление и гараж. в комплексе есть бассейн, детская площадка, джакузи, тренажерный зал, сауна, теннисный корт. Идеально для жизни, окружена всеми видами услуг, школами, медицинским центром, банками и т. д., а также торговым центром со множеством магазинов.</p>', 1, 239900, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"15","sea":"","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/193888058-fd79c54b78.jpg', 'kvartira-dla-prodazi-v-ansaldo-la-albufereta-alicante-alacant', 1506523021, 1),
(68, 1, 'Квартира для продажи , Ensanche-Diputación, Аликанте / Алакант', '<p>Отличное жилье, расположенное на одной из лучших улиц Аликанте. Рядом со станцией Renfe и советом.Состоит из 3 спален, двух ванных комнат,гостиная,кухня. В каждой комнате есть кондиционирование холода и тепла. Встроенные шкафы, гладкие стены и очень хороший ремонт. <br>Цена включает в себя гараж и складское помещение в здании.</p>', 1, 300000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"120","sea":"15 \\u043c\\u0438\\u043d \\u043f\\u0435\\u0448\\u043a\\u043e\\u043c","bathroom":"2","balkon":"1","pool":"0","parking":"1","popular":"0"}', 'catalog/209083196-a56fbb9413.jpg', 'kvartira-dla-prodazi-ensanche-diputacion-alikante-alakant', 1506525975, 1),
(69, 1, 'Квартира для продажи  Calle Científico Jaime Santana, 7, Плайя-де-Сан-Хуан, Аликанте / Алакант', '<p>«Фантастическая квартира, расположенная в одном из лучших районов Плайя-Сан-Хуан. Она состоит из 3 спален, двух ванных комнат, мраморных полов, горячего / холодного кондиционирования воздуха, радиаторов, встроенных шкафов. С доступом ко всем услугам, включая трамвай и линии bus.It включает в себя парковочное место,на территории бассейн, игровая комната, социальный клуб, сад и детская площадка.</p>', 1, 230000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"10","sea":"","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/177238376-bf73a1af22.jpg', 'kvartira-dla-prodazi-calle-cientifico-jaime-santana-7-plaja-de-san-huan-alikante-alakant', 1506529718, 1),
(70, 1, 'Квартира для продажи , Carolinas Bajas, Аликанте / Alacant', '<p>Квартира банка в районе Каролинас - Пио XII,рядом с центром, собственность находится на первом этаже. Она имеет площадь 115 м2 с 4 спальнями, ванной комнатой, туалетом,  гостиной,кухня,без мебели. Рядом вся инфраструктура,автобусная остановка ».</p>', 1, 68000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"115","sea":"15 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/218672519-7f84c4c204.jpg', 'kvartira-dla-prodazi-carolinas-bajas-alikante-alacant', 1506530033, 1),
(71, 1, 'Квартира для продажи в Калле д''Альфа, Сьюдад-де-Асис, Аликанте / Алакант', '<p>Собственность распределена в трех спальнях, гостиной-столовой с балконом, просторной и удобной кухней.<br>3 этажа с лифтом. .</p>', 1, 111300, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"104","sea":"15 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/193458361-61d4cb5644.jpg', 'kvartira-dla-prodazi-v-kalle-dalfa-sudad-de-asis-alikante-alakant', 1506530829, 1),
(72, 1, 'квартира для продажи Район Сан-Блас-Пау', '<p>«Имущество банковского учреждения, жилье в pau 1 san blas, с 3 спальнями, 2 ванными комнатами, кухней и гостиной, просторным и очень светлое, жильес гаражом, хранилищем находится в урбанизации.</p>', 1, 135000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"100","sea":"15 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"0","parking":"1","popular":"0"}', 'catalog/193100546-e6f2ca0b62.jpg', 'kvartira-dla-prodazi-rajon-san-blas-pau', 1506531092, 1),
(73, 1, 'Квартира для продажи в Улица Пинтор Антонио Аморос, Хуан Xxiii, Аликанте / Alacant', '<p>«Квартира банка , расположена в Juan XXIII, имеет площадь в 73,31 м2 с 3 - мя спальнями, 1 ванная комната, кухня и гостиная, 1 этаж. <br>Здание 1980,рядом торговый центр Gran Vía, школы, институты.</p>', 1, 409000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"73","sea":"20 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/185593350-e4bc4b7ced.jpg', 'kvartira-dla-prodazi-v-ulica-pintor-antonio-amoros-huan-xxiii-alikante-alacant', 1506535334, 1),
(74, 1, 'Продается квартира на проспекте Калле Германос Мачадо, Бабель, Аликанте / Алакант', 'Апартаменты с 3 спальными комнатами,2 ванные комнаты,кухня,гостиная,есть своя парковка, кондиционер, телевизор и центральное отопление. ', 1, 93500, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"89","sea":"15 \\u043c\\u0438\\u043d \\u043d\\u0430 \\u0430\\u0432\\u0442\\u043e\\u0431\\u0443\\u0441\\u0435","bathroom":"2","balkon":"1","pool":"0","parking":"1","popular":"0"}', 'catalog/184758215-532a8d3d79.jpg', 'prodaetsa-kvartira-na-prospekte-kalle-germanos-macado-babel-alikante-alakant', 1506535697, 1),
(75, 1, 'Шале с террасой для продажи в Ла Альбуферете, Аликанте / Алакант', '<p>Террасный дом в районе Альбуфера. В настоящее время есть два дома с отдельным входом с возможностю объединения их . На <br>нижнем этаже   гостиная, кухня, ванная комната, 2 спальни и терраса. <br>второй этаж 2 спальни,ванная. <br>Жилье находится примерно в 300 метрах от пляжа Альбуфера, недалеко от магазинов и автобусной остановки ».</p>', 1, 130000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"5","square":"90","sea":"300 \\u043c","bathroom":"2","balkon":"1","pool":"0","parking":"1","popular":"0"}', 'catalog/196297845-9425ebe8a2.jpg', 'sale-s-terrasoj-dla-prodazi-v-la-albuferete-alikante-alakant', 1506536910, 1),
(76, 1, 'Апартаменты Мирамар в Кумбре дель Соль', '<p>Тишина и покой, единение природой и отдых от городской суеты Вас ждет в заповедной зоне - Кумбре дель Соль на Севере провинции Аликанте.</p><p>Величественные горные вершины, свежий морской воздух и ласковое солнышко создают уют и расслабляющую атмосферу в любом жилом комплексе класса Люкс, построенном не только в высочайшем качестве , но и с большой любовью! </p><p>В продаже два последних апартамента с 2 спальнями, большой терассой с видом на море . Площадь 93 кв м.</p><p>Урбанизация с комплексом бассейнов и баром, парковкой.</p><p>Возможна ипотека от банка до 65% от стоимости под низкие фиксированные проценты.</p>', 1, 192000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"93","sea":"200","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7394-fbca6f493e.jpg', 'apartamenty-miramar-v-kumbre-del-sol', 1507394408, 1),
(77, 1, 'Смежный дом Linda Гран Алакант', '<p>Жилой комплекс из смежных домов в Гран Алакант сданных в эксплуатацию и также есть и в процессе строительства, когда можно внести необходимые заказчику изменения в проект.</p><p>Гран Алакант - это очень красивый поселок на горе в сосновом лесу, в охраняемой привилегированной зоне, в 3 км от пляжа Лос Ареналес. Преимущественно постоянно проживают северо-европейцы.</p><p>Площадь от 99 кв м.: 2,3 или 4 спальни и 2 санузла. Участок 225 кв м, бассейн  4м*8 м входит в стоимость дома.</p><p><br></p><p>До Аликанте 15 км. Супермаркеты, магазины, школы, кафе и рестораны все в пешей доступности. Очень комфортный район для постоянного проживания или проведения отпуска.</p>', 1, 188000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"1","square":"99","sea":"3000","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7791-038adcb325.jpg', 'smeznyj-dom-linda-gran-alakant', 1507394736, 1),
(78, 1, 'Новый жилой комплекс на Ареналес дель Соль - первая линия', '<p>Жилой комплекс Сан Антонио  расположенного в привилигированном месте Лас Ареналес в 15 км на юг от Аликанте. Все квартиры имеют вид на Средиземное море.</p><p>Жилой комплекс соседствует с заповедными дюнными пляжами и является охраняемой природной зоной.</p><p>Комплекс имеет отличное местоположение относительно других городов и мест: в 15 километрах от Аликанте, Элче и Санта Пола и в 5 минутах от международного аэропорта, который соединяет с главными европейскими столицами.</p><p>Недалеко так же располагаются: больница, центры для досуга, магазины и поля для гольфа.</p><p>Комплекс состоит из квартир с 2 и 3 спальнями.</p><p>Все апартаменты очень солнечные и ориентированы на море.</p><p>Пенхаусы с 2 спальнями имеют площадь 71м2, с 3 спальнями – 123м2. Так же пенхаусы имееют террасу-солярий с навесом, зону для барбекью, душевую кабину и джакуз</p><p>Все квартиры имеют превосходную качественую отделку, встроенные шкафы, прочные двери, видео- домофон, отдельная кладовка на цокольном этаже и подземная парковка с прямым доступом к лифтам.</p><p>Идеальное место для того чтобы жить со всеми удобствами в окружении солнца, пляжа и природы.</p><p>Этот комплекс дополняет закрытую урбанизацию, которая имеет площадь 24 000 м2. На ее территории любой ребенок может провести незабываемое время на игровой площадке, которая находится в хорошо охраняемой территории.</p><p>На территории есть 4 бассейна, один из них размером 5х20м2, с двумя спортивными дорожками ; три остальных бассейна каждый из которых почти по 200м. Так же есть 2 корта для игры в падель и один теннисный корт и сад. </p><p>Возможно ипотека от банка под фиксированные 3,25 процента годовых на 15-20 лет.</p>', 1, 182000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"71","sea":"1000","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/76821001x601-024884a7fb.jpg', 'novyj-ziloj-kompleks-na-arenales-del-sol-pervaa-linia', 1507395218, 1),
(79, 1, 'Санта Пола - 150 м до пляжа', '<p>Санта Пола - великолепный испанский городок для любителей респектабельного отдыха и бесконечных песчаных пляжей.</p><p>Пляжная полоса Санта Полы в сумме занимает 11 киломеров и состоит из нескольких больших пляжей переходящих один в другой.</p><p>Квартира с 2 спальнями и 2 санузлами , 75 кв м.</p><p><br></p><p>Отделка самыми высококлассными материалами . Подогрев пола ванных. Централизованный кондиционер. Парковка в цокольном этаже дома.</p><p><strong></strong></p>', 1, 175000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"75","sea":"150","bathroom":"","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7911-2f4c84780c.jpg', 'santa-pola-150-m-do-plaza', 1507395696, 1),
(80, 1, 'Кумбре дель Соль - апартаменты с садом', '<p>Тишина и покой, единение природой и отдых от городской суеты Вас ждет в заповедной зоне - Кумбре дель Соль на Севере провинции Аликанте.</p><p>Величественные горные вершины, свежий морской воздух и ласковое солнышко создают уют и расслабляющую атмосферу в любом жилом комплексе класса Люкс, построенном не только в высочайшем качестве , но и с большой любовью! </p><p>В продаже апартаменты с 2 спальнями, большой терассой с видом на море . Площадь 90 кв м.</p><p>Урбанизация с комплексом бассейнов , баром, парковкой.</p><p>Возможна ипотека от банка до 65% от стоимости под низкие фиксированнеы проценты.</p><p><strong></strong></p>', 1, 152800, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"90","sea":"150","bathroom":"1 ","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7401-cfeceacb87.jpg', 'kumbre-del-sol-apartamenty-s-sadom', 1507396004, 1),
(81, 1, 'la Marina - смежный дом в 300 м от пляжа', '<p>Ля Марина Плайя - популярное местечко отдыха испанцев. Всего в 20 км от аэропорта Аликанте в южном направлении.</p><p>Смежный угловой дом площадью 100 кв м: 3 спальни и 2 санузла. участок - 60 кв м. На втором этаже терасса 6 кв м. На третьем этаже выход на открытую терассу типа солярия.</p><p>До пляжа 300 м.</p>', 1, 149000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"100","sea":"300","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7931-cc9536c8f1.jpg', 'la-marina-smeznyj-dom-v-300-m-ot-plaza', 1507396331, 1),
(82, 1, 'Апартаменты Гран Алакант', '<p>Двухуровневые апартаменты в Гран Алакант сданные в эксплуатацию и также есть и в процессе строительства, когда можно внести необходимые заказчику изменения в проект.</p><p>Гран Алакант - это очень красивый поселок на горе в сосновом лесу, в охраняемой привилегированной зоне, в 3 км от пляжа Лос Ареналес. Преимущественно постоянно проживают северо-европейцы.</p><p>Площадь от 86 кв м.: 2 или 3 спальни и 2 санузла. Участок 160 кв м, бассейн оплачивается отдельно по желанию заказчика, стоимость бассейна 4м*8 м около 13-14 тыс евро.</p><p>.</p><p>До Аликанте 15 км. Супермаркеты, магазины, школы, кафе и рестораны все в пешей доступности. Очень комфортный район для постоянного проживания или проведения отпуска.</p>', 1, 139000, NULL, '{"type":"\\u0414\\u043e\\u043c","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"86","sea":"3000","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/image00001-cb341f16cd.jpg', 'apartamenty-gran-alakant', 1507396628, 1),
(83, 1, 'Апартаменты Ля Марина Плайя', '<p>Апартаменты в 300 м от пляжа Ля Марина. Ля Марина  - это популярное пляжное местечко в провинции Аликанте в 20 км от аэропорта города Аликанте на юг.</p><p>В продаже новые апартаменты со своим участком или большой терассой.</p><p>В жилом комплексе есть бассейн.</p><p>В стоимость входит парковочное место.</p><p>2 спальни и 2 санузла, площади от 80 кв м. </p>', 1, 136000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"80","sea":"300","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7921-eec8191b39.jpg', 'apartamenty-la-marina-plaja', 1507397594, 1),
(84, 1, 'Таунхаус Levante Gran Alacante', '<p>Двухуровневые апартаменты в Гран Алакант сданные в эксплуатацию и также есть и в процессе строительства, когда можно внести необходимые заказчику изменения в проект.</p><p>Гран Алакант - это очень красивый поселок на горе в сосновом лесу, в охраняемой привилегированной зоне, в 3 км от пляжа Лос Ареналес. Преимущественно постоянно проживают северо-европейцы.</p><p>Площадь от 74 кв м. Участок 150 кв м, бассейн оплачивается отдельно по желанию заказчика, стоимость бассейна 4м*8 м около 13-14 тыс евро.</p><p>До Аликанте 15 км. Супермаркеты, магазины, школы, кафе и рестораны все в пешей доступности. Очень комфортный район для постоянного проживания или проведения отпуска.</p>', 1, 129000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"1","square":"74","sea":"3000","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7861-6533790ad4.jpg', 'taunhaus-levante-gran-alacante', 1507397879, 1),
(85, 1, 'Бунгало в лос Ареналес', '<p>Новые бунгало от застройщика в районе Ареналесь дель Соль в 15 км от центра Аликанте, в 500 м от пляжа Ареналес.</p><p>Состоят из двух спален, салона, кухни, 2 санузлов, открытой терассы на на третьем этаже или своего участка земли, если бунгало на первом этаже.</p><p><strong></strong></p>', 1, 99000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"85","sea":"500","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7951-6e673c6dc7.jpg', 'bungalo-v-los-arenales', 1507398299, 1),
(86, 1, 'Апартаменты Vision Gran Alacant', '<p>Двухуровневые апартаменты в Гран Алакант сданные в эксплуатацию и также есть и в процессе строительства, когда можно внести необходимые заказчику изменения в проект.</p><p>Гран Алакант - это очень красивый поселок на горе в сосновом лесу, в охраняемой привилегированной зоне, в 3 км от пляжа Лос Ареналес. Преимущественно постоянно проживают северо-европейцы.</p><p>Площадь от 75 кв м.:  Квартира с 1 спальней имеет второй жилой этаж 26 кв м, который можно использовать по вашем желанию, как спальню или кабинет.</p><p>Участок 25 кв м для парковки автомобиля или для иного использования по желанию клиента.</p><p>Бассейн общего пользования, игровая площадка для детей.</p><p><br></p><p>До Аликанте 15 км. Супермаркеты, магазины, школы, кафе и рестораны все в пешей доступности. Очень комфортный район для постоянного проживания или проведения отпуска.</p><p><strong></strong></p>', 1, 92000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"1","square":"75","sea":"3000","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/78819-88d987cc2b.jpg', 'apartamenty-vision-gran-alacant', 1507399140, 1),
(87, 1, 'квартира в районе Pla Аликанте', '<p>Продается квартира в районе Пла города Аликанте.</p><p>Квартира находится на 3 этаже в доме без лифта.</p><p>Жилая площадь квартиры 53 кв м: салон с балконом, кухня с подсобным помещением, 2 спальных комнаты, совмещенный санузел.</p>', 1, 34000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"53","sea":"1000","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/6171-d0f460ff5c.jpg', 'kvartira-v-rajone-pla-alikante', 1510492676, 1),
(88, 1, 'Квартира на улице FELIPE HERRERO ARIAS, 20', '<p>Квартира на нижнем этаже- bajo. 2 спальни.  После капитального ремонта.</p>', 1, 55000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"55","sea":"2000","bathroom":"","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/6341-271269b4bc.jpg', 'kvartira-na-ulice-felipe-herrero-arias-20', 1510492970, 1),
(89, 1, 'Квартира на Каролинас', '<p>Квартира на Каролинас в городе Аликанте. До пляжа 2 км.</p><p>Квартира находится на 4 этаже в доме без лифта.</p><p>Общая площадь 55 кв м: салон с балконом, 2 спальных комнаты, совмещенный санузел, кухня с небольшим балкончиком.</p><p>Квартира требует ремонта.</p>', 1, 42000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"55","sea":"2000","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/6342-ec2b00b460.jpg', 'kvartira-na-karolinas', 1510493768, 1),
(90, 1, 'Квартира на проспекте Padre Espla', '<p>Квартира на проспекте Падре Эспла в городе Аликанте в 1 км до городского пляжа Постигет.</p><p>Квартира находится на 3 этаже в доме без лифта.</p><p>Общая площадь 56 кв м: салон с балконом, 2 спальных комнаты, совмещенный санузел и кухня с подсобным помещением.</p><p>Очень удобная транспортная развязка соединяющая с аэропортом (15 км), центром Аликанте (2 км), шаговая доступность до магазинов, супермаркетов, Торгового центра PLaza Mar II.</p><p>Идеально для сдачи в аренду или для собственного проживания.</p>', 1, 58000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"56","sea":"1000","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/7692-d82ec3dcb2.jpg', 'kvartira-na-prospekte-padre-espla', 1510494111, 1),
(91, 1, 'Квартира в 450 м от пляжа', '<p>Квартира с 3 спальнями в 450 м от пляжа Постигет Аликанте.</p><p>Квартира находится на 3 этаже в доме без лифта.</p><p>101 кв м: 3 спальни, салон, кухня, совмещенный санузел.</p><p><br></p>', 1, 80000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"101","sea":"450","bathroom":"1","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/7972-6c2eb3e413.jpg', 'kvartira-v-450-m-ot-plaza', 1510495290, 1),
(92, 1, 'Отличная квартира с 3 спальнями и 2 санузлами в Аликанте- район Пла.', '<p>Отличная квартира с 3 спальнями и 2 санузлами в Аликанте- район Пла.</p><p>Площадь квартиры 90 кв м + 13 кв м терасса.</p><p>4 этаж с лифтом. Парковка на придомовой территории.</p><p>До пляжа Постигет Аликанте -  1 км.</p><p>Очень красивый и комфортный для проживания район.</p>', 1, 80000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"3","square":"104","sea":"1000","bathroom":"2","balkon":"1","pool":"0","parking":"0","popular":"0"}', 'catalog/7962-4fa63814ff.jpg', 'otlicnaa-kvartira-s-3-spalnami-i-2-sanuzlami-v-alikante-rajon-pla', 1510496452, 1),
(93, 1, 'calle Medico Ricardo ferre, 42 Аликанте', '<p>Великолепная квартира на 5 этаже в новом доме в районе PAU1 города Аликанте.</p><p>В районе только новые красивые дома, парк, школы, магазины, автобусное сообщение с пляжем и центром города.</p><p>Квартира находится на 5 этаже. Общая площадь 65 кв м: салон с балконом, санузел совмещенный и спальная комната.</p><p>В жилом комплексе есть бассейн, детская площадка, спортивная площадка, парковка. До моря 4 км.</p>', 1, NULL, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"65","sea":"4000","bathroom":"1","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/51111-c0cf66cbe2.jpg', '97000', 1510497057, 1),
(94, 1, 'квартира на calle damsa alonso, 5 Alicante', '<p>Квартира с 4 спальнями и 2 санузлами. До моря 2 км. Парковка включена в стоимость. </p>', 1, 106000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"4","square":"120","sea":"2000","bathroom":"4","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/7571-e9cd9b86b3.jpg', 'kvartira-na-calle-damsa-alonso-5-alicante', 1510497996, 1),
(95, 1, 'Квартира на проспекте Насьонес Аликанте', '<p>Отличная квартира по очень привлекательной цене!</p><p>До пляжа Сан хуан - 500 метров!</p><p>Общая площадь квартиры 75 кв м: салон с балконом, 2 санузла и 2 спальных комнаты.</p><p>В стоимость включено парковочное место.</p><p>В шаговой доступности супермаркет Меркадона, Консум. Остановка трамвая в 200м. А самое важное и придающее ценности этой квартире - это великолепный пляж в 5 минутах ходьбы! </p>', 1, 135000, NULL, '{"type":"\\u0410\\u043f\\u043f\\u0430\\u0440\\u0442\\u0430\\u043c\\u0435\\u043d\\u0442\\u044b","city":"\\u0410\\u043b\\u0438\\u043a\\u0430\\u043d\\u0442\\u0435","address":"","room":"2","square":"2","sea":"500","bathroom":"2","balkon":"1","pool":"1","parking":"1","popular":"0"}', 'catalog/6071-54fa24b61e.jpg', 'kvartira-na-prospekte-nasones-alikante', 1510498725, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_item_data`
--

CREATE TABLE `easyii_catalog_item_data` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_item_data`
--

INSERT INTO `easyii_catalog_item_data` (`id`, `item_id`, `name`, `value`) VALUES
(1, 1, 'type', ''),
(2, 1, 'city', 'Аликанте'),
(3, 1, 'address', '03004 Аликанте, Испания#ChIJZ9X4uqw3Yg0RyJycawoxx5o#38.3516297#-0.48727259999998296'),
(4, 1, 'room', '3'),
(5, 1, 'square', '84'),
(6, 1, 'sea', '250'),
(7, 1, 'bathroom', '1'),
(8, 1, 'balkon', '1'),
(9, 1, 'pool', '0'),
(10, 1, 'parking', '1'),
(11, 1, 'popular', '1'),
(204, 2, 'sea', ''),
(205, 2, 'bathroom', '1 ванная комната'),
(206, 2, 'balkon', '1'),
(207, 2, 'pool', '0'),
(208, 2, 'parking', '0'),
(209, 2, 'popular', '0'),
(1449, 3, 'balkon', '1'),
(199, 2, 'type', 'Аппартаменты'),
(200, 2, 'city', 'Аликанте'),
(201, 2, 'address', ''),
(202, 2, 'room', ' 3 спальни'),
(203, 2, 'square', '80'),
(1442, 3, 'type', 'Аппартаменты'),
(1443, 3, 'city', 'Аликанте'),
(1444, 3, 'address', 'Calle del Dr. Bergez, 03012 Alacant, Alicante, Испания#ChIJFcTf5wc3Yg0RVo2bGOfHK0w#38.359023#-0.4792068000000427'),
(1445, 3, 'room', '1 спальня'),
(1446, 3, 'square', '70'),
(1447, 3, 'sea', '10 мин на автобусе'),
(221, 4, 'type', 'Аппартаменты'),
(222, 4, 'city', 'Аликанте'),
(223, 4, 'address', 'Plaza Manila, 03013 Alacant, Alicante, Испания#ChIJmWSSGQg3Yg0RQEbSut9Qbcw#38.35845399999999#-0.47736090000000786'),
(224, 4, 'room', '2 спальни'),
(225, 4, 'square', '70'),
(226, 4, 'sea', ''),
(227, 4, 'bathroom', '1'),
(232, 5, 'type', 'Аппартаменты'),
(233, 5, 'city', 'Аликанте'),
(234, 5, 'address', ''),
(235, 5, 'room', '2 спальни'),
(236, 5, 'square', '80  '),
(237, 5, 'sea', ''),
(243, 6, 'type', 'Аппартаменты'),
(244, 6, 'city', 'Аликанте'),
(245, 6, 'address', ''),
(246, 6, 'room', '3'),
(247, 6, 'square', '85'),
(248, 6, 'sea', ''),
(254, 7, 'type', 'Аппартаменты'),
(255, 7, 'city', 'Аликанте'),
(256, 7, 'address', ''),
(257, 7, 'room', '4'),
(258, 7, 'square', '120'),
(259, 7, 'sea', '500 '),
(260, 7, 'bathroom', '1'),
(89, 8, 'type', ''),
(90, 8, 'city', 'Аликанте'),
(91, 8, 'address', ''),
(92, 8, 'room', '3'),
(93, 8, 'square', '75'),
(94, 8, 'sea', ''),
(95, 8, 'bathroom', '1'),
(96, 8, 'balkon', '1'),
(97, 8, 'pool', '0'),
(98, 8, 'parking', '0'),
(99, 8, 'popular', '0'),
(265, 9, 'type', 'Аппартаменты'),
(266, 9, 'city', 'Аликанте'),
(267, 9, 'address', ''),
(268, 9, 'room', '1 спальня'),
(269, 9, 'square', '100'),
(270, 9, 'sea', ''),
(276, 10, 'type', 'Аппартаменты'),
(277, 10, 'city', 'Аликанте'),
(278, 10, 'address', ''),
(279, 10, 'room', ' 2спальни'),
(280, 10, 'square', '150'),
(281, 10, 'sea', '3'),
(282, 10, 'bathroom', '1 ванная'),
(122, 11, 'type', 'Аппартаменты'),
(123, 11, 'city', 'Аликанте'),
(124, 11, 'address', ''),
(125, 11, 'room', '4'),
(126, 11, 'square', '100м2'),
(127, 11, 'sea', '2 минутах ходьбы от пляжа '),
(128, 11, 'bathroom', ''),
(129, 11, 'balkon', '1'),
(130, 11, 'pool', '1'),
(131, 11, 'parking', '1'),
(132, 11, 'popular', '0'),
(837, 12, 'type', 'Аппартаменты'),
(838, 12, 'city', 'Аликанте'),
(839, 12, 'address', ''),
(840, 12, 'room', '2 спальни'),
(841, 12, 'square', '100'),
(842, 12, 'sea', 'в 2 минутах ходьбы от пляжа'),
(843, 12, 'bathroom', '2 ванные'),
(416, 23, 'pool', '1'),
(417, 23, 'parking', '1'),
(418, 23, 'popular', '0'),
(419, 24, 'type', 'Дом'),
(420, 24, 'city', 'Аликанте'),
(421, 24, 'address', ''),
(422, 24, 'room', '5'),
(402, 22, 'sea', '800'),
(403, 22, 'bathroom', ''),
(404, 22, 'balkon', '1'),
(405, 22, 'pool', '1'),
(406, 22, 'parking', '1'),
(407, 22, 'popular', '0'),
(408, 23, 'type', 'Аппартаменты'),
(409, 23, 'city', 'Аликанте'),
(410, 23, 'address', ''),
(411, 23, 'room', '3'),
(412, 23, 'square', '90'),
(944, 21, 'pool', '1'),
(945, 21, 'parking', '1'),
(941, 21, 'sea', '1000'),
(938, 21, 'address', ''),
(939, 21, 'room', '2'),
(940, 21, 'square', '60'),
(397, 22, 'type', 'Дом'),
(398, 22, 'city', 'Аликанте'),
(934, 20, 'parking', '1'),
(935, 20, 'popular', '0'),
(932, 20, 'balkon', '1'),
(933, 20, 'pool', '1'),
(931, 20, 'bathroom', '2'),
(936, 21, 'type', 'Аппартаменты'),
(937, 21, 'city', 'Аликанте'),
(909, 19, 'bathroom', ''),
(910, 19, 'balkon', '1'),
(911, 19, 'pool', '1'),
(912, 19, 'parking', '1'),
(913, 19, 'popular', '0'),
(925, 20, 'type', ''),
(926, 20, 'city', 'Аликанте'),
(927, 20, 'address', ''),
(1448, 3, 'bathroom', '1 ванная комната'),
(228, 4, 'balkon', '0'),
(229, 4, 'pool', '0'),
(230, 4, 'parking', '0'),
(231, 4, 'popular', '0'),
(238, 5, 'bathroom', '1'),
(239, 5, 'balkon', '1'),
(240, 5, 'pool', '0'),
(241, 5, 'parking', '0'),
(242, 5, 'popular', '0'),
(249, 6, 'bathroom', '1'),
(250, 6, 'balkon', '1'),
(251, 6, 'pool', '0'),
(252, 6, 'parking', '0'),
(253, 6, 'popular', '0'),
(261, 7, 'balkon', '1'),
(262, 7, 'pool', '0'),
(263, 7, 'parking', '0'),
(264, 7, 'popular', '0'),
(271, 9, 'bathroom', '1'),
(272, 9, 'balkon', '1'),
(273, 9, 'pool', '0'),
(274, 9, 'parking', '0'),
(275, 9, 'popular', '0'),
(283, 10, 'balkon', '1'),
(284, 10, 'pool', '1'),
(285, 10, 'parking', '1'),
(286, 10, 'popular', '0'),
(425, 24, 'bathroom', '3'),
(424, 24, 'sea', '1000'),
(423, 24, 'square', '225'),
(415, 23, 'balkon', '1'),
(414, 23, 'bathroom', '2'),
(413, 23, 'sea', '900'),
(401, 22, 'square', '250'),
(400, 22, 'room', '5'),
(399, 22, 'address', ''),
(946, 21, 'popular', '0'),
(942, 21, 'bathroom', '1'),
(943, 21, 'balkon', '1'),
(930, 20, 'sea', '800'),
(928, 20, 'room', '3'),
(929, 20, 'square', '78'),
(847, 12, 'popular', '0'),
(845, 12, 'pool', '1'),
(846, 12, 'parking', '1'),
(903, 19, 'type', 'Аппартаменты'),
(904, 19, 'city', 'Аликанте'),
(905, 19, 'address', ''),
(906, 19, 'room', '2'),
(907, 19, 'square', '35'),
(908, 19, 'sea', '800'),
(426, 24, 'balkon', '1'),
(427, 24, 'pool', '1'),
(428, 24, 'parking', '1'),
(429, 24, 'popular', '0'),
(672, 25, 'type', 'Аппартаменты'),
(673, 25, 'city', 'Аликанте'),
(674, 25, 'address', ''),
(675, 25, 'room', '3'),
(676, 25, 'square', '130'),
(677, 25, 'sea', '600'),
(844, 12, 'balkon', '1'),
(680, 25, 'pool', '1'),
(679, 25, 'balkon', '1'),
(678, 25, 'bathroom', '2'),
(681, 25, 'parking', '1'),
(682, 25, 'popular', '1'),
(683, 26, 'type', 'Аппартаменты'),
(684, 26, 'city', 'Аликанте'),
(685, 26, 'address', ''),
(686, 26, 'room', '3'),
(687, 26, 'square', '65'),
(688, 26, 'sea', '50'),
(689, 26, 'bathroom', ''),
(690, 26, 'balkon', '1'),
(691, 26, 'pool', '1'),
(692, 26, 'parking', '1'),
(693, 26, 'popular', '0'),
(694, 27, 'type', 'Аппартаменты'),
(695, 27, 'city', 'Аликанте'),
(696, 27, 'address', ''),
(697, 27, 'room', '3'),
(698, 27, 'square', '67'),
(699, 27, 'sea', '100'),
(700, 27, 'bathroom', '2'),
(701, 27, 'balkon', '1'),
(702, 27, 'pool', '1'),
(703, 27, 'parking', '1'),
(704, 27, 'popular', '0'),
(705, 28, 'type', 'Аппартаменты'),
(706, 28, 'city', 'Аликанте'),
(707, 28, 'address', 'Аликанте, Испания#ChIJS6udO9o1Yg0R44ELrHKofR0#38.3459963#-0.4906855000000405'),
(708, 28, 'room', '3'),
(709, 28, 'square', '74'),
(710, 28, 'sea', '1200'),
(711, 28, 'bathroom', '2'),
(712, 28, 'balkon', '1'),
(713, 28, 'pool', '1'),
(714, 28, 'parking', '1'),
(715, 28, 'popular', '0'),
(716, 29, 'type', 'Аппартаменты'),
(717, 29, 'city', 'Аликанте'),
(718, 29, 'address', ''),
(719, 29, 'room', '3'),
(720, 29, 'square', '57'),
(721, 29, 'sea', '250'),
(722, 29, 'bathroom', '1'),
(723, 29, 'balkon', '1'),
(724, 29, 'pool', '1'),
(725, 29, 'parking', '1'),
(726, 29, 'popular', '0'),
(727, 30, 'type', 'Аппартаменты'),
(728, 30, 'city', 'Аликанте'),
(729, 30, 'address', ''),
(730, 30, 'room', '3'),
(731, 30, 'square', '73'),
(732, 30, 'sea', '100'),
(733, 30, 'bathroom', '2'),
(734, 30, 'balkon', '1'),
(735, 30, 'pool', '1'),
(736, 30, 'parking', '1'),
(737, 30, 'popular', '0'),
(738, 31, 'type', 'Аппартаменты'),
(739, 31, 'city', 'Аликанте'),
(740, 31, 'address', 'Аликанте, Испания#ChIJS6udO9o1Yg0R44ELrHKofR0#38.3459963#-0.4906855000000405'),
(741, 31, 'room', '3'),
(742, 31, 'square', '93'),
(743, 31, 'sea', '100'),
(744, 31, 'bathroom', '2'),
(745, 31, 'balkon', '1'),
(746, 31, 'pool', '1'),
(747, 31, 'parking', '1'),
(748, 31, 'popular', '0'),
(749, 32, 'type', 'Аппартаменты'),
(750, 32, 'city', 'Аликанте'),
(751, 32, 'address', ''),
(752, 32, 'room', '3'),
(753, 32, 'square', '80'),
(754, 32, 'sea', '250'),
(755, 32, 'bathroom', ''),
(756, 32, 'balkon', '1'),
(757, 32, 'pool', '1'),
(758, 32, 'parking', '1'),
(759, 32, 'popular', '0'),
(760, 33, 'type', 'Аппартаменты'),
(761, 33, 'city', 'Аликанте'),
(762, 33, 'address', ''),
(763, 33, 'room', '4'),
(764, 33, 'square', '98'),
(765, 33, 'sea', '150'),
(766, 33, 'bathroom', ''),
(767, 33, 'balkon', '1'),
(768, 33, 'pool', '1'),
(769, 33, 'parking', '1'),
(770, 33, 'popular', '0'),
(969, 34, 'type', 'Аппартаменты'),
(970, 34, 'city', 'Аликанте'),
(971, 34, 'address', ''),
(972, 34, 'room', '3'),
(973, 34, 'square', '116'),
(974, 34, 'sea', '1,5 '),
(958, 35, 'type', 'Дом'),
(959, 35, 'city', 'Аликанте'),
(960, 35, 'address', ''),
(961, 35, 'room', '4'),
(962, 35, 'square', '300'),
(963, 35, 'sea', ''),
(964, 35, 'bathroom', ''),
(965, 35, 'balkon', '1'),
(966, 35, 'pool', '1'),
(967, 35, 'parking', '1'),
(968, 35, 'popular', '0'),
(975, 34, 'bathroom', '1'),
(976, 34, 'balkon', '1'),
(977, 34, 'pool', '0'),
(978, 34, 'parking', '0'),
(979, 34, 'popular', '0'),
(980, 36, 'type', ''),
(981, 36, 'city', ''),
(982, 36, 'address', ''),
(983, 36, 'room', '3'),
(984, 36, 'square', '120'),
(985, 36, 'sea', ''),
(986, 36, 'bathroom', ''),
(987, 36, 'balkon', '1'),
(988, 36, 'pool', '0'),
(989, 36, 'parking', '0'),
(990, 36, 'popular', '0'),
(991, 37, 'type', 'Аппартаменты'),
(992, 37, 'city', 'Аликанте'),
(993, 37, 'address', ''),
(994, 37, 'room', '3'),
(995, 37, 'square', ''),
(996, 37, 'sea', ''),
(997, 37, 'bathroom', '1'),
(998, 37, 'balkon', '1'),
(999, 37, 'pool', '1'),
(1000, 37, 'parking', '1'),
(1001, 37, 'popular', '0'),
(1002, 38, 'type', 'Аппартаменты'),
(1003, 38, 'city', 'Аликанте'),
(1004, 38, 'address', ''),
(1005, 38, 'room', '2'),
(1006, 38, 'square', '95'),
(1007, 38, 'sea', ''),
(1008, 38, 'bathroom', ''),
(1009, 38, 'balkon', '1'),
(1010, 38, 'pool', '1'),
(1011, 38, 'parking', '1'),
(1012, 38, 'popular', '0'),
(1024, 39, 'type', 'Аппартаменты'),
(1025, 39, 'city', 'Аликанте'),
(1026, 39, 'address', ''),
(1027, 39, 'room', '2'),
(1028, 39, 'square', '86'),
(1029, 39, 'sea', '100'),
(1030, 39, 'bathroom', ''),
(1031, 39, 'balkon', '1'),
(1032, 39, 'pool', '1'),
(1033, 39, 'parking', '1'),
(1034, 39, 'popular', '0'),
(1035, 40, 'type', 'Аппартаменты'),
(1036, 40, 'city', 'Аликанте'),
(1037, 40, 'address', ''),
(1038, 40, 'room', '3'),
(1039, 40, 'square', '85'),
(1040, 40, 'sea', '1500'),
(1041, 40, 'bathroom', ''),
(1042, 40, 'balkon', '1'),
(1043, 40, 'pool', '0'),
(1044, 40, 'parking', '0'),
(1045, 40, 'popular', '0'),
(1046, 41, 'type', 'Аппартаменты'),
(1047, 41, 'city', 'Аликанте'),
(1048, 41, 'address', ''),
(1049, 41, 'room', '4'),
(1050, 41, 'square', '115'),
(1051, 41, 'sea', '700'),
(1052, 41, 'bathroom', '4'),
(1053, 41, 'balkon', '1'),
(1054, 41, 'pool', '0'),
(1055, 41, 'parking', '0'),
(1056, 41, 'popular', '0'),
(1057, 42, 'type', 'Аппартаменты'),
(1058, 42, 'city', 'Аликанте'),
(1059, 42, 'address', ''),
(1060, 42, 'room', '4'),
(1061, 42, 'square', '90'),
(1062, 42, 'sea', '300'),
(1063, 42, 'bathroom', '4'),
(1064, 42, 'balkon', '1'),
(1065, 42, 'pool', '0'),
(1066, 42, 'parking', '0'),
(1067, 42, 'popular', '0'),
(1068, 43, 'type', 'Аппартаменты'),
(1069, 43, 'city', 'Аликанте'),
(1070, 43, 'address', ''),
(1071, 43, 'room', '4'),
(1072, 43, 'square', '92'),
(1073, 43, 'sea', ''),
(1074, 43, 'bathroom', '2'),
(1075, 43, 'balkon', '1'),
(1076, 43, 'pool', '0'),
(1077, 43, 'parking', '0'),
(1078, 43, 'popular', '0'),
(1079, 44, 'type', 'Аппартаменты'),
(1080, 44, 'city', 'Аликанте'),
(1081, 44, 'address', 'Плайя де Сан Хуан, Аликанте, Испания#ChIJbSfUwL05Yg0Rk5oUwK9_ODo#38.3737418#-0.42665839999995114'),
(1082, 44, 'room', '3'),
(1083, 44, 'square', '85 '),
(1084, 44, 'sea', ''),
(1085, 44, 'bathroom', '1'),
(1086, 44, 'balkon', '1'),
(1087, 44, 'pool', '1'),
(1088, 44, 'parking', '1'),
(1089, 44, 'popular', '0'),
(1090, 45, 'type', 'Аппартаменты'),
(1091, 45, 'city', 'Аликанте'),
(1092, 45, 'address', ''),
(1093, 45, 'room', '5'),
(1094, 45, 'square', '100'),
(1095, 45, 'sea', ''),
(1096, 45, 'bathroom', '2'),
(1097, 45, 'balkon', '1'),
(1098, 45, 'pool', '1'),
(1099, 45, 'parking', '1'),
(1100, 45, 'popular', '0'),
(1574, 46, 'type', 'Аппартаменты'),
(1575, 46, 'city', 'Аликанте'),
(1576, 46, 'address', ''),
(1577, 46, 'room', '3'),
(1578, 46, 'square', '85'),
(1579, 46, 'sea', 'близко'),
(1580, 46, 'bathroom', '1'),
(1581, 46, 'balkon', '1'),
(1582, 46, 'pool', '1'),
(1583, 46, 'parking', '1'),
(1123, 47, 'type', 'Аппартаменты'),
(1124, 47, 'city', 'Аликанте'),
(1125, 47, 'address', ''),
(1126, 47, 'room', '4'),
(1127, 47, 'square', '120'),
(1128, 47, 'sea', ''),
(1129, 47, 'bathroom', '2'),
(1130, 47, 'balkon', '1'),
(1131, 47, 'pool', '1'),
(1132, 47, 'parking', '1'),
(1133, 47, 'popular', '0'),
(1134, 48, 'type', 'Аппартаменты'),
(1135, 48, 'city', 'Аликанте'),
(1136, 48, 'address', ''),
(1137, 48, 'room', '3'),
(1138, 48, 'square', '70'),
(1139, 48, 'sea', ''),
(1140, 48, 'bathroom', '1'),
(1141, 48, 'balkon', '1'),
(1142, 48, 'pool', '1'),
(1143, 48, 'parking', '1'),
(1144, 48, 'popular', '0'),
(1145, 49, 'type', 'Аппартаменты'),
(1146, 49, 'city', 'Аликанте'),
(1147, 49, 'address', ''),
(1148, 49, 'room', '4'),
(1149, 49, 'square', '113'),
(1150, 49, 'sea', ''),
(1151, 49, 'bathroom', '2'),
(1152, 49, 'balkon', '1'),
(1153, 49, 'pool', '0'),
(1154, 49, 'parking', '0'),
(1155, 49, 'popular', '0'),
(1156, 50, 'type', 'Аппартаменты'),
(1157, 50, 'city', 'Аликанте'),
(1158, 50, 'address', ''),
(1159, 50, 'room', '3'),
(1160, 50, 'square', '95'),
(1161, 50, 'sea', ''),
(1162, 50, 'bathroom', '2'),
(1163, 50, 'balkon', '1'),
(1164, 50, 'pool', '1'),
(1165, 50, 'parking', '1'),
(1166, 50, 'popular', '0'),
(1167, 51, 'type', 'Аппартаменты'),
(1168, 51, 'city', 'Аликанте'),
(1169, 51, 'address', ''),
(1170, 51, 'room', '3'),
(1171, 51, 'square', '86'),
(1172, 51, 'sea', '100'),
(1173, 51, 'bathroom', '2'),
(1174, 51, 'balkon', '1'),
(1175, 51, 'pool', '1'),
(1176, 51, 'parking', '1'),
(1177, 51, 'popular', '0'),
(1178, 52, 'type', 'Аппартаменты'),
(1179, 52, 'city', 'Аликанте'),
(1180, 52, 'address', ''),
(1181, 52, 'room', '4'),
(1182, 52, 'square', '115'),
(1183, 52, 'sea', 'близко'),
(1184, 52, 'bathroom', '2'),
(1185, 52, 'balkon', '1'),
(1186, 52, 'pool', '1'),
(1187, 52, 'parking', '1'),
(1188, 52, 'popular', '0'),
(1189, 53, 'type', 'Дом'),
(1190, 53, 'city', 'Аликанте'),
(1191, 53, 'address', ''),
(1192, 53, 'room', '6'),
(1193, 53, 'square', '329'),
(1194, 53, 'sea', 'недалеко'),
(1195, 53, 'bathroom', '4'),
(1196, 53, 'balkon', '1'),
(1197, 53, 'pool', '1'),
(1198, 53, 'parking', '1'),
(1199, 53, 'popular', '0'),
(1200, 54, 'type', 'Дом'),
(1201, 54, 'city', 'Аликанте'),
(1202, 54, 'address', ''),
(1203, 54, 'room', '5'),
(1204, 54, 'square', '320'),
(1205, 54, 'sea', ''),
(1206, 54, 'bathroom', '3'),
(1207, 54, 'balkon', '1'),
(1208, 54, 'pool', '1'),
(1209, 54, 'parking', '1'),
(1210, 54, 'popular', '0'),
(1211, 55, 'type', 'Аппартаменты'),
(1212, 55, 'city', 'Аликанте'),
(1213, 55, 'address', ''),
(1214, 55, 'room', '6'),
(1215, 55, 'square', '210'),
(1216, 55, 'sea', 'в пешей доступности'),
(1217, 55, 'bathroom', '3'),
(1218, 55, 'balkon', '1'),
(1219, 55, 'pool', '1'),
(1220, 55, 'parking', '1'),
(1221, 55, 'popular', '0'),
(1222, 56, 'type', 'Аппартаменты'),
(1223, 56, 'city', 'Аликанте'),
(1224, 56, 'address', ''),
(1225, 56, 'room', '4'),
(1226, 56, 'square', '159'),
(1227, 56, 'sea', 'в пешей доступности'),
(1228, 56, 'bathroom', '2'),
(1229, 56, 'balkon', '1'),
(1230, 56, 'pool', '1'),
(1231, 56, 'parking', '1'),
(1232, 56, 'popular', '0'),
(1233, 57, 'type', 'Аппартаменты'),
(1234, 57, 'city', 'Аликанте'),
(1235, 57, 'address', ''),
(1236, 57, 'room', '4'),
(1237, 57, 'square', '110000'),
(1238, 57, 'sea', '10 мин на автобусе'),
(1239, 57, 'bathroom', '2'),
(1240, 57, 'balkon', '1'),
(1241, 57, 'pool', '1'),
(1242, 57, 'parking', '1'),
(1243, 57, 'popular', '0'),
(1244, 58, 'type', 'Дом'),
(1245, 58, 'city', 'Аликанте'),
(1246, 58, 'address', ''),
(1247, 58, 'room', '5'),
(1248, 58, 'square', '370'),
(1249, 58, 'sea', 'в пешей доступности'),
(1250, 58, 'bathroom', '2'),
(1251, 58, 'balkon', '1'),
(1252, 58, 'pool', '1'),
(1253, 58, 'parking', '1'),
(1254, 58, 'popular', '0'),
(1255, 59, 'type', 'Аппартаменты'),
(1256, 59, 'city', 'Аликанте'),
(1257, 59, 'address', ''),
(1258, 59, 'room', '4'),
(1259, 59, 'square', '100'),
(1260, 59, 'sea', 'в пешей доступности'),
(1261, 59, 'bathroom', '2'),
(1262, 59, 'balkon', '1'),
(1263, 59, 'pool', '1'),
(1264, 59, 'parking', '1'),
(1265, 59, 'popular', '0'),
(1266, 60, 'type', 'Аппартаменты'),
(1267, 60, 'city', 'Аликанте'),
(1268, 60, 'address', ''),
(1269, 60, 'room', '4'),
(1270, 60, 'square', '10'),
(1271, 60, 'sea', '10 мин на автобусе'),
(1272, 60, 'bathroom', '2'),
(1273, 60, 'balkon', '1'),
(1274, 60, 'pool', '1'),
(1275, 60, 'parking', '1'),
(1276, 60, 'popular', '0'),
(1277, 61, 'type', 'Аппартаменты'),
(1278, 61, 'city', 'Аликанте'),
(1279, 61, 'address', ''),
(1280, 61, 'room', '4'),
(1281, 61, 'square', '116'),
(1282, 61, 'sea', '15 мин на автобусе'),
(1283, 61, 'bathroom', '2'),
(1284, 61, 'balkon', '1'),
(1285, 61, 'pool', '1'),
(1286, 61, 'parking', '1'),
(1287, 61, 'popular', '0'),
(1288, 62, 'type', 'Аппартаменты'),
(1289, 62, 'city', 'Аликанте'),
(1290, 62, 'address', ''),
(1291, 62, 'room', '3'),
(1292, 62, 'square', '75'),
(1293, 62, 'sea', 'близко'),
(1294, 62, 'bathroom', '1'),
(1295, 62, 'balkon', '1'),
(1296, 62, 'pool', '1'),
(1297, 62, 'parking', '1'),
(1298, 62, 'popular', '0'),
(1299, 63, 'type', 'Аппартаменты'),
(1300, 63, 'city', 'Аликанте'),
(1301, 63, 'address', ''),
(1302, 63, 'room', '4'),
(1303, 63, 'square', '110'),
(1304, 63, 'sea', 'близко'),
(1305, 63, 'bathroom', '2'),
(1306, 63, 'balkon', '1'),
(1307, 63, 'pool', '1'),
(1308, 63, 'parking', '1'),
(1309, 63, 'popular', '0'),
(1310, 64, 'type', 'Аппартаменты'),
(1311, 64, 'city', 'Аликанте'),
(1312, 64, 'address', ''),
(1313, 64, 'room', '4'),
(1314, 64, 'square', '110'),
(1315, 64, 'sea', 'в пешей доступности'),
(1316, 64, 'bathroom', '2'),
(1317, 64, 'balkon', '1'),
(1318, 64, 'pool', '1'),
(1319, 64, 'parking', '1'),
(1320, 64, 'popular', '0'),
(1321, 65, 'type', 'Аппартаменты'),
(1322, 65, 'city', 'Аликанте'),
(1323, 65, 'address', ''),
(1324, 65, 'room', '4'),
(1325, 65, 'square', '85'),
(1326, 65, 'sea', '15 мин на автобусе'),
(1327, 65, 'bathroom', '1'),
(1328, 65, 'balkon', '1'),
(1329, 65, 'pool', '0'),
(1330, 65, 'parking', '0'),
(1331, 65, 'popular', '0'),
(1332, 66, 'type', 'Аппартаменты'),
(1333, 66, 'city', 'Аликанте'),
(1334, 66, 'address', ''),
(1335, 66, 'room', '4'),
(1336, 66, 'square', '142'),
(1337, 66, 'sea', 'близко'),
(1338, 66, 'bathroom', '2'),
(1339, 66, 'balkon', '1'),
(1340, 66, 'pool', '1'),
(1341, 66, 'parking', '1'),
(1342, 66, 'popular', '0'),
(1343, 67, 'type', 'Аппартаменты'),
(1344, 67, 'city', 'Аликанте'),
(1345, 67, 'address', ''),
(1346, 67, 'room', '4'),
(1347, 67, 'square', '15'),
(1348, 67, 'sea', ''),
(1349, 67, 'bathroom', '2'),
(1350, 67, 'balkon', '1'),
(1351, 67, 'pool', '1'),
(1352, 67, 'parking', '1'),
(1353, 67, 'popular', '0'),
(1354, 68, 'type', 'Дом'),
(1355, 68, 'city', 'Аликанте'),
(1356, 68, 'address', ''),
(1357, 68, 'room', '4'),
(1358, 68, 'square', '120'),
(1359, 68, 'sea', '15 мин пешком'),
(1360, 68, 'bathroom', '2'),
(1361, 68, 'balkon', '1'),
(1362, 68, 'pool', '0'),
(1363, 68, 'parking', '1'),
(1364, 68, 'popular', '0'),
(1365, 69, 'type', 'Аппартаменты'),
(1366, 69, 'city', 'Аликанте'),
(1367, 69, 'address', ''),
(1368, 69, 'room', '4'),
(1369, 69, 'square', '10'),
(1370, 69, 'sea', ''),
(1371, 69, 'bathroom', '2'),
(1372, 69, 'balkon', '1'),
(1373, 69, 'pool', '1'),
(1374, 69, 'parking', '1'),
(1375, 69, 'popular', '0'),
(1376, 70, 'type', 'Аппартаменты'),
(1377, 70, 'city', 'Аликанте'),
(1378, 70, 'address', ''),
(1379, 70, 'room', '5'),
(1380, 70, 'square', '115'),
(1381, 70, 'sea', '15 мин на автобусе'),
(1382, 70, 'bathroom', '2'),
(1383, 70, 'balkon', '1'),
(1384, 70, 'pool', '0'),
(1385, 70, 'parking', '0'),
(1386, 70, 'popular', '0'),
(1387, 71, 'type', 'Аппартаменты'),
(1388, 71, 'city', 'Аликанте'),
(1389, 71, 'address', ''),
(1390, 71, 'room', '4'),
(1391, 71, 'square', '104'),
(1392, 71, 'sea', '15 мин на автобусе'),
(1393, 71, 'bathroom', '2'),
(1394, 71, 'balkon', '1'),
(1395, 71, 'pool', '0'),
(1396, 71, 'parking', '0'),
(1397, 71, 'popular', '0'),
(1398, 72, 'type', 'Аппартаменты'),
(1399, 72, 'city', 'Аликанте'),
(1400, 72, 'address', ''),
(1401, 72, 'room', '4'),
(1402, 72, 'square', '100'),
(1403, 72, 'sea', '15 мин на автобусе'),
(1404, 72, 'bathroom', '2'),
(1405, 72, 'balkon', '1'),
(1406, 72, 'pool', '0'),
(1407, 72, 'parking', '1'),
(1408, 72, 'popular', '0'),
(1409, 73, 'type', 'Аппартаменты'),
(1410, 73, 'city', 'Аликанте'),
(1411, 73, 'address', ''),
(1412, 73, 'room', '4'),
(1413, 73, 'square', '73'),
(1414, 73, 'sea', '20 мин на автобусе'),
(1415, 73, 'bathroom', '1'),
(1416, 73, 'balkon', '1'),
(1417, 73, 'pool', '0'),
(1418, 73, 'parking', '0'),
(1419, 73, 'popular', '0'),
(1420, 74, 'type', 'Аппартаменты'),
(1421, 74, 'city', 'Аликанте'),
(1422, 74, 'address', ''),
(1423, 74, 'room', '4'),
(1424, 74, 'square', '89'),
(1425, 74, 'sea', '15 мин на автобусе'),
(1426, 74, 'bathroom', '2'),
(1427, 74, 'balkon', '1'),
(1428, 74, 'pool', '0'),
(1429, 74, 'parking', '1'),
(1430, 74, 'popular', '0'),
(1431, 75, 'type', 'Дом'),
(1432, 75, 'city', 'Аликанте'),
(1433, 75, 'address', ''),
(1434, 75, 'room', '5'),
(1435, 75, 'square', '90'),
(1436, 75, 'sea', '300 м'),
(1437, 75, 'bathroom', '2'),
(1438, 75, 'balkon', '1'),
(1439, 75, 'pool', '0'),
(1440, 75, 'parking', '1'),
(1441, 75, 'popular', '0'),
(1450, 3, 'pool', '0'),
(1451, 3, 'parking', '0'),
(1452, 3, 'popular', '0'),
(1453, 76, 'type', 'Аппартаменты'),
(1454, 76, 'city', 'Аликанте'),
(1455, 76, 'address', ''),
(1456, 76, 'room', '2'),
(1457, 76, 'square', '93'),
(1458, 76, 'sea', '200'),
(1459, 76, 'bathroom', '2'),
(1460, 76, 'balkon', '1'),
(1461, 76, 'pool', '1'),
(1462, 76, 'parking', '1'),
(1463, 76, 'popular', '0'),
(1464, 77, 'type', 'Аппартаменты'),
(1465, 77, 'city', 'Аликанте'),
(1466, 77, 'address', ''),
(1467, 77, 'room', '1'),
(1468, 77, 'square', '99'),
(1469, 77, 'sea', '3000'),
(1470, 77, 'bathroom', '2'),
(1471, 77, 'balkon', '1'),
(1472, 77, 'pool', '1'),
(1473, 77, 'parking', '1'),
(1474, 77, 'popular', '0'),
(1475, 78, 'type', 'Аппартаменты'),
(1476, 78, 'city', 'Аликанте'),
(1477, 78, 'address', ''),
(1478, 78, 'room', '2'),
(1479, 78, 'square', '71'),
(1480, 78, 'sea', '1000'),
(1481, 78, 'bathroom', '2'),
(1482, 78, 'balkon', '1'),
(1483, 78, 'pool', '1'),
(1484, 78, 'parking', '1'),
(1485, 78, 'popular', '0'),
(1486, 79, 'type', 'Аппартаменты'),
(1487, 79, 'city', 'Аликанте'),
(1488, 79, 'address', ''),
(1489, 79, 'room', '2'),
(1490, 79, 'square', '75'),
(1491, 79, 'sea', '150'),
(1492, 79, 'bathroom', ''),
(1493, 79, 'balkon', '1'),
(1494, 79, 'pool', '1'),
(1495, 79, 'parking', '1'),
(1496, 79, 'popular', '0'),
(1497, 80, 'type', 'Аппартаменты'),
(1498, 80, 'city', 'Аликанте'),
(1499, 80, 'address', ''),
(1500, 80, 'room', '2'),
(1501, 80, 'square', '90'),
(1502, 80, 'sea', '150'),
(1503, 80, 'bathroom', '1 '),
(1504, 80, 'balkon', '1'),
(1505, 80, 'pool', '1'),
(1506, 80, 'parking', '1'),
(1507, 80, 'popular', '0'),
(1508, 81, 'type', 'Аппартаменты'),
(1509, 81, 'city', 'Аликанте'),
(1510, 81, 'address', ''),
(1511, 81, 'room', '3'),
(1512, 81, 'square', '100'),
(1513, 81, 'sea', '300'),
(1514, 81, 'bathroom', '2'),
(1515, 81, 'balkon', '1'),
(1516, 81, 'pool', '1'),
(1517, 81, 'parking', '1'),
(1518, 81, 'popular', '0'),
(1519, 82, 'type', 'Дом'),
(1520, 82, 'city', 'Аликанте'),
(1521, 82, 'address', ''),
(1522, 82, 'room', '2'),
(1523, 82, 'square', '86'),
(1524, 82, 'sea', '3000'),
(1525, 82, 'bathroom', '2'),
(1526, 82, 'balkon', '1'),
(1527, 82, 'pool', '1'),
(1528, 82, 'parking', '1'),
(1529, 82, 'popular', '0'),
(1530, 83, 'type', 'Аппартаменты'),
(1531, 83, 'city', 'Аликанте'),
(1532, 83, 'address', ''),
(1533, 83, 'room', '2'),
(1534, 83, 'square', '80'),
(1535, 83, 'sea', '300'),
(1536, 83, 'bathroom', '2'),
(1537, 83, 'balkon', '1'),
(1538, 83, 'pool', '1'),
(1539, 83, 'parking', '1'),
(1540, 83, 'popular', '0'),
(1541, 84, 'type', 'Аппартаменты'),
(1542, 84, 'city', 'Аликанте'),
(1543, 84, 'address', ''),
(1544, 84, 'room', '1'),
(1545, 84, 'square', '74'),
(1546, 84, 'sea', '3000'),
(1547, 84, 'bathroom', '2'),
(1548, 84, 'balkon', '1'),
(1549, 84, 'pool', '1'),
(1550, 84, 'parking', '1'),
(1551, 84, 'popular', '0'),
(1552, 85, 'type', 'Аппартаменты'),
(1553, 85, 'city', 'Аликанте'),
(1554, 85, 'address', ''),
(1555, 85, 'room', '2'),
(1556, 85, 'square', '85'),
(1557, 85, 'sea', '500'),
(1558, 85, 'bathroom', '2'),
(1559, 85, 'balkon', '1'),
(1560, 85, 'pool', '1'),
(1561, 85, 'parking', '1'),
(1562, 85, 'popular', '0'),
(1563, 86, 'type', 'Аппартаменты'),
(1564, 86, 'city', 'Аликанте'),
(1565, 86, 'address', ''),
(1566, 86, 'room', '1'),
(1567, 86, 'square', '75'),
(1568, 86, 'sea', '3000'),
(1569, 86, 'bathroom', '1'),
(1570, 86, 'balkon', '1'),
(1571, 86, 'pool', '1'),
(1572, 86, 'parking', '1'),
(1573, 86, 'popular', '0'),
(1584, 46, 'popular', '0'),
(1585, 87, 'type', 'Аппартаменты'),
(1586, 87, 'city', 'Аликанте'),
(1587, 87, 'address', ''),
(1588, 87, 'room', '2'),
(1589, 87, 'square', '53'),
(1590, 87, 'sea', '1000'),
(1591, 87, 'bathroom', '1'),
(1592, 87, 'balkon', '1'),
(1593, 87, 'pool', '0'),
(1594, 87, 'parking', '0'),
(1595, 87, 'popular', '0'),
(1607, 88, 'type', 'Аппартаменты'),
(1608, 88, 'city', 'Аликанте'),
(1609, 88, 'address', ''),
(1610, 88, 'room', '2'),
(1611, 88, 'square', '55'),
(1612, 88, 'sea', '2000'),
(1613, 88, 'bathroom', ''),
(1614, 88, 'balkon', '1'),
(1615, 88, 'pool', '0'),
(1616, 88, 'parking', '0'),
(1617, 88, 'popular', '0'),
(1618, 89, 'type', 'Аппартаменты'),
(1619, 89, 'city', 'Аликанте'),
(1620, 89, 'address', ''),
(1621, 89, 'room', '2'),
(1622, 89, 'square', '55'),
(1623, 89, 'sea', '2000'),
(1624, 89, 'bathroom', '1'),
(1625, 89, 'balkon', '1'),
(1626, 89, 'pool', '0'),
(1627, 89, 'parking', '0'),
(1628, 89, 'popular', '0'),
(1629, 90, 'type', 'Аппартаменты'),
(1630, 90, 'city', 'Аликанте'),
(1631, 90, 'address', ''),
(1632, 90, 'room', '2'),
(1633, 90, 'square', '56'),
(1634, 90, 'sea', '1000'),
(1635, 90, 'bathroom', '1'),
(1636, 90, 'balkon', '1'),
(1637, 90, 'pool', '0'),
(1638, 90, 'parking', '0'),
(1639, 90, 'popular', '0'),
(1640, 91, 'type', 'Аппартаменты'),
(1641, 91, 'city', 'Аликанте'),
(1642, 91, 'address', ''),
(1643, 91, 'room', '3'),
(1644, 91, 'square', '101'),
(1645, 91, 'sea', '450'),
(1646, 91, 'bathroom', '1'),
(1647, 91, 'balkon', '1'),
(1648, 91, 'pool', '0'),
(1649, 91, 'parking', '0'),
(1650, 91, 'popular', '0'),
(1651, 92, 'type', 'Аппартаменты'),
(1652, 92, 'city', 'Аликанте'),
(1653, 92, 'address', ''),
(1654, 92, 'room', '3'),
(1655, 92, 'square', '104'),
(1656, 92, 'sea', '1000'),
(1657, 92, 'bathroom', '2'),
(1658, 92, 'balkon', '1'),
(1659, 92, 'pool', '0'),
(1660, 92, 'parking', '0'),
(1661, 92, 'popular', '0'),
(1662, 93, 'type', 'Аппартаменты'),
(1663, 93, 'city', 'Аликанте'),
(1664, 93, 'address', ''),
(1665, 93, 'room', '2'),
(1666, 93, 'square', '65'),
(1667, 93, 'sea', '4000'),
(1668, 93, 'bathroom', '1'),
(1669, 93, 'balkon', '1'),
(1670, 93, 'pool', '1'),
(1671, 93, 'parking', '1'),
(1672, 93, 'popular', '0'),
(1684, 94, 'type', 'Аппартаменты'),
(1685, 94, 'city', 'Аликанте'),
(1686, 94, 'address', ''),
(1687, 94, 'room', '4'),
(1688, 94, 'square', '120'),
(1689, 94, 'sea', '2000'),
(1690, 94, 'bathroom', '4'),
(1691, 94, 'balkon', '1'),
(1692, 94, 'pool', '1'),
(1693, 94, 'parking', '1'),
(1694, 94, 'popular', '0'),
(1695, 95, 'type', 'Аппартаменты'),
(1696, 95, 'city', 'Аликанте'),
(1697, 95, 'address', ''),
(1698, 95, 'room', '2'),
(1699, 95, 'square', '2'),
(1700, 95, 'sea', '500'),
(1701, 95, 'bathroom', '2'),
(1702, 95, 'balkon', '1'),
(1703, 95, 'pool', '1'),
(1704, 95, 'parking', '1'),
(1705, 95, 'popular', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_entity_categories`
--

CREATE TABLE `easyii_entity_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '1',
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_entity_items`
--

CREATE TABLE `easyii_entity_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `data` text NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_faq`
--

CREATE TABLE `easyii_faq` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_feedback`
--

CREATE TABLE `easyii_feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer_subject` varchar(128) DEFAULT NULL,
  `answer_text` text,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_files`
--

CREATE TABLE `easyii_files` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(11) DEFAULT '0',
  `slug` varchar(128) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_gallery_categories`
--

CREATE TABLE `easyii_gallery_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_guestbook`
--

CREATE TABLE `easyii_guestbook` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer` text,
  `email` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_loginform`
--

CREATE TABLE `easyii_loginform` (
  `id` int(11) NOT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `user_agent` varchar(1024) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_loginform`
--

INSERT INTO `easyii_loginform` (`id`, `username`, `password`, `ip`, `user_agent`, `time`, `success`) VALUES
(1, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 1494598691, 1),
(2, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499410358, 1),
(3, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499427975, 1),
(4, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.0 Mobile/14F89 Safari/602.1', 1499458294, 1),
(5, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1499622385, 1),
(6, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1499799715, 1),
(7, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1499979670, 1),
(8, 'root', '******', '109.165.96.47', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36 OPR/46.0.2597.46', 1500053208, 1),
(9, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1500232873, 1),
(10, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1500405825, 1),
(11, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36', 1500642344, 1),
(12, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1500705169, 1),
(13, 'root', '******', '109.165.112.74', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36 OPR/46.0.2597.57', 1500818110, 1),
(14, 'root', '******', '87.117.13.68', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55', 1503144280, 1),
(15, 'root', '******', '87.117.14.240', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36 OPR/47.0.2631.71', 1504371706, 1),
(16, 'root', '******', '84.120.72.95', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1504432092, 1),
(17, 'root', '******', '84.120.38.193', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 1506254465, 1),
(18, 'root', '******', '84.120.38.193', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 1506371388, 1),
(19, 'root', '******', '47.60.42.31', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1', 1506938274, 1),
(20, 'root', '******', '84.120.38.193', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1506971539, 1),
(21, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1507393815, 1),
(22, 'root', '******', '87.117.12.133', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.39', 1508083970, 1),
(23, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1508176256, 1),
(24, 'root', '******', '87.117.13.75', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.39', 1508260813, 1),
(25, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1508306586, 1),
(26, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36', 1508392719, 1),
(27, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1508438309, 1),
(28, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1508680161, 1),
(29, 'root', '******', '87.117.14.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.39', 1508705435, 1),
(30, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1508784088, 1),
(31, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1508937449, 1),
(32, 'root', '******', '87.117.38.175', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.50', 1509032839, 1),
(33, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1509188151, 1),
(34, 'root', '******', '87.117.14.243', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52', 1509292835, 1),
(35, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1509473265, 1),
(36, 'root', '******', '31.23.171.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52', 1509911052, 1),
(37, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1510351683, 1),
(38, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1510489354, 1),
(39, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1510605486, 1),
(40, 'root', '******', '87.117.14.238', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52', 1510674096, 1),
(41, 'root', '******', '188.168.215.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', 1510677275, 1),
(42, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1510696053, 1),
(43, 'root', '******', '84.127.21.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36', 1510951123, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_menu`
--

CREATE TABLE `easyii_menu` (
  `menu_id` int(11) NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `items` text,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_menu`
--

INSERT INTO `easyii_menu` (`menu_id`, `slug`, `title`, `items`, `status`) VALUES
(1, 'main', 'Главное меню', '[\n    {\n        "label": "Продажа",\n        "url": "/sale"\n    },\n    {\n        "label": "Новости",\n        "url": "/news"\n    }\n]', 1),
(2, 'services-owner', 'Услуги. Владельцам', '[\n    {\n        "label": "Юрист",\n        "url": "/services/urist"\n    },\n    {\n        "label": "Экскурсия",\n        "url": "/services/ekskursia"\n    },\n    {\n        "label": "Консьерж-услуга",\n        "url": "/services/konserz-usluga"\n    },\n    {\n        "label": "Ремонт",\n        "url": "/services/remont"\n    },\n    {\n        "label": "Дизайн-проект",\n        "url": "/services/dizajn-proekt"\n    }\n]', 1),
(3, 'services-find', 'Услуги. Поиск недвижимости', '[\n    {\n        "label": "Процедура покупки и оформления недвижимости в Испании",\n        "url": "/services/procedura-pokupki-i-oformlenia-nedvizimosti-v-ispanii"\n    },\n    {\n        "label": "Недвижимость в кредит",\n        "url": "/services/nedvizimost-v-kredit-2"\n    }\n]', 1),
(4, 'footer', 'Меню в футере', '[\n    {\n        "label": "Продажа",\n        "url": "/sale"\n    },\n    {\n        "label": "Новости",\n        "url": "/news"\n    },\n    {\n        "label": "Услуги",\n        "url": "/services"\n    }\n]', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_migration`
--

CREATE TABLE `easyii_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `easyii_migration`
--

INSERT INTO `easyii_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1494598688),
('m000000_000000_install', 1494598690),
('m000009_100000_update', 1494598690),
('m000009_200000_update', 1494598691),
('m000009_200003_module_menu', 1494598691),
('m000009_200004_update', 1494598691),
('m170816_153554_source_article', 1510685100),
('m170816_154828_source_news', 1510685101);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_modules`
--

CREATE TABLE `easyii_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `class` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `settings` text,
  `notice` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_modules`
--

INSERT INTO `easyii_modules` (`id`, `name`, `class`, `title`, `icon`, `settings`, `notice`, `order_num`, `status`) VALUES
(1, 'entity', 'yii\\easyii\\modules\\entity\\EntityModule', 'Объекты', 'asterisk', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"itemsInFolder":false}', 0, 95, 1),
(2, 'article', 'yii\\easyii\\modules\\article\\ArticleModule', 'Статьи', 'pencil', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"articleThumb":true,"enablePhotos":true,"enableTags":true,"enableShort":true,"enableSource":true,"shortMaxLength":"255","itemsInFolder":false,"itemSlugImmutable":false}', 0, 65, 1),
(3, 'carousel', 'yii\\easyii\\modules\\carousel\\CarouselModule', 'Карусель', 'picture', '{"enableTitle":true,"enableText":true}', 0, 40, 0),
(4, 'catalog', 'yii\\easyii\\modules\\catalog\\CatalogModule', 'Каталог', 'list-alt', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"itemsInFolder":false,"itemThumb":true,"itemPhotos":true,"itemDescription":true,"itemSlugImmutable":false}', 0, 100, 1),
(5, 'faq', 'yii\\easyii\\modules\\faq\\FaqModule', 'Вопросы и ответы', 'question-sign', '{"questionHtmlEditor":true,"answerHtmlEditor":true,"enableTags":true}', 0, 45, 0),
(6, 'feedback', 'yii\\easyii\\modules\\feedback\\FeedbackModule', 'Обратная связь', 'earphone', '{"mailAdminOnNewFeedback":true,"subjectOnNewFeedback":"New feedback","templateOnNewFeedback":"@easyii\\/modules\\/feedback\\/mail\\/en\\/new_feedback","answerTemplate":"@easyii\\/modules\\/feedback\\/mail\\/en\\/answer","answerSubject":"Answer on your feedback message","answerHeader":"Hello,","answerFooter":"Best regards.","telegramAdminOnNewFeedback":false,"telegramTemplateOnNewFeedback":"@easyii\\/modules\\/feedback\\/telegram\\/en\\/new_feedback","enableTitle":false,"enableEmail":true,"enablePhone":true,"enableText":true,"enableCaptcha":false}', 0, 60, 1),
(7, 'file', 'yii\\easyii\\modules\\file\\FileModule', 'Файлы', 'floppy-disk', '{"slugImmutable":false}', 0, 30, 0),
(8, 'gallery', 'yii\\easyii\\modules\\gallery\\GalleryModule', 'Фотогалерея', 'camera', '{"categoryThumb":true,"itemsInFolder":false,"categoryTags":true,"categorySlugImmutable":false,"categoryDescription":true}', 0, 90, 1),
(9, 'guestbook', 'yii\\easyii\\modules\\guestbook\\GuestbookModule', 'Гостевая книга', 'book', '{"enableTitle":false,"enableEmail":true,"preModerate":false,"enableCaptcha":false,"mailAdminOnNewPost":true,"subjectOnNewPost":"New message in the guestbook.","templateOnNewPost":"@easyii\\/modules\\/guestbook\\/mail\\/en\\/new_post","frontendGuestbookRoute":"\\/guestbook","subjectNotifyUser":"Your post in the guestbook answered","templateNotifyUser":"@easyii\\/modules\\/guestbook\\/mail\\/en\\/notify_user"}', 0, 80, 0),
(10, 'menu', 'yii\\easyii\\modules\\menu\\MenuModule', 'Меню', 'menu-hamburger', '{"slugImmutable":false}', 0, 51, 1),
(11, 'news', 'yii\\easyii\\modules\\news\\NewsModule', 'Новости', 'bullhorn', '{"enableThumb":true,"enablePhotos":true,"enableShort":true,"shortMaxLength":256,"enableTags":true,"slugImmutable":false}', 0, 70, 1),
(12, 'page', 'yii\\easyii\\modules\\page\\PageModule', 'Страницы', 'file', '{"slugImmutable":true,"defaultFields":"[]"}', 0, 50, 1),
(13, 'shopcart', 'yii\\easyii\\modules\\shopcart\\ShopcartModule', 'Заказы', 'shopping-cart', '{"mailAdminOnNewOrder":true,"subjectOnNewOrder":"New order","templateOnNewOrder":"@easyii\\/modules\\/shopcart\\/mail\\/en\\/new_order","subjectNotifyUser":"Your order status changed","templateNotifyUser":"@easyii\\/modules\\/shopcart\\/mail\\/en\\/notify_user","frontendShopcartRoute":"\\/shopcart\\/order","enablePhone":true,"enableEmail":true}', 0, 120, 0),
(14, 'subscribe', 'yii\\easyii\\modules\\subscribe\\SubscribeModule', 'E-mail рассылка', 'envelope', '[]', 0, 10, 1),
(15, 'text', 'yii\\easyii\\modules\\text\\TextModule', 'Текстовые блоки', 'font', '[]', 0, 20, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_news`
--

CREATE TABLE `easyii_news` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `source` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_pages`
--

CREATE TABLE `easyii_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT '0',
  `fields` text,
  `data` text,
  `tree` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  `depth` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_photos`
--

CREATE TABLE `easyii_photos` (
  `id` int(11) NOT NULL,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_photos`
--

INSERT INTO `easyii_photos` (`id`, `class`, `item_id`, `image_file`, `description`, `order_num`) VALUES
(1, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/alicanteapartments5photo-58b2c8af53.jpg', '', 1),
(2, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/5963358-e17eea5f6d.jpg', '', 2),
(3, 'yii\\easyii\\modules\\article\\models\\Item', 1, 'article/bc88aee2fe-5-0bb77dc0fd.jpg', 'После произведенной клиентом оплаты, мы отправим вам деньги в течение 3 рабочих дней после окончания срока аренды!', 3),
(4, 'yii\\easyii\\modules\\article\\models\\Item', 1, 'article/cd66064631-6-1b06efc535.jpg', 'Мы не любим бюрократию и все, что с ней связано, поэтому для организации работы понадобится лишь самый необходимый набор документов!', 4),
(5, 'yii\\easyii\\modules\\article\\models\\Item', 1, 'article/9e2cdb864d-2-6ade2c51d6.jpg', 'Мы бесплатно разместим информацию о вас в каталоге, а размер комиссии составит всего лишь 10% с любого типа помещения!', 5),
(6, 'yii\\easyii\\modules\\catalog\\models\\Item', 2, 'catalog/benalua-pardo-gimeno-27-22158538-a26ab1f06c.jpg', '', 6),
(7, 'yii\\easyii\\modules\\catalog\\models\\Item', 2, 'catalog/benalua-pardo-gimeno-27-22158538-0b5f09618d.jpg', '', 7),
(8, 'yii\\easyii\\modules\\catalog\\models\\Item', 2, 'catalog/benalua-pardo-gimeno-27-22158538-726b19f469.jpg', '', 8),
(9, 'yii\\easyii\\modules\\catalog\\models\\Item', 2, 'catalog/benalua-pardo-gimeno-27-22158538-0e37ffe098.jpg', '', 9),
(10, 'yii\\easyii\\modules\\catalog\\models\\Item', 2, 'catalog/benalua-pardo-gimeno-27-22158538-bf5035335c.jpg', '', 10),
(11, 'yii\\easyii\\modules\\catalog\\models\\Item', 2, 'catalog/benalua-pardo-gimeno-27-22158538-cf4cc36e43.jpg', '', 11),
(12, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-e3ee4cda37.jpg', '', 12),
(13, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-55d5f14ee2.jpg', '', 13),
(14, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-bd42c602ff.jpg', '', 14),
(15, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-213528d4dc.jpg', '', 15),
(16, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-e1717a52bc.jpg', '', 16),
(17, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-e7c05f8362.jpg', '', 17),
(18, 'yii\\easyii\\modules\\catalog\\models\\Item', 3, 'catalog/carolinas-doctor-bergez-con-call-ed3e7870c3.jpg', '', 18),
(19, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-f538b600a3.jpg', '', 19),
(20, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-a3e65a7276.jpg', '', 20),
(21, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-be17857e28.jpg', '', 21),
(22, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-e60c364a06.jpg', '', 22),
(23, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-13a46757d8.jpg', '', 23),
(24, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-0ce2177251.jpg', '', 24),
(25, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-e4f9af2670.jpg', '', 25),
(26, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-14feb66556.jpg', '', 26),
(27, 'yii\\easyii\\modules\\catalog\\models\\Item', 4, 'catalog/pla-carolinas-bejar-4-1-planta-2-02816fb60a.jpg', '', 27),
(28, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186702-00f70b992d.jpg', '', 28),
(29, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186709-6ff4f7d256.jpg', '', 29),
(30, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186701-d5034b2194.jpg', '', 30),
(31, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186703-a9898f0f33.jpg', '', 31),
(32, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186704-161de2611e.jpg', '', 32),
(33, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306190543-a22ef73289.jpg', '', 33),
(34, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186706-042a37717a.jpg', '', 34),
(35, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306190542-64b797d8da.jpg', '', 35),
(36, 'yii\\easyii\\modules\\catalog\\models\\Item', 5, 'catalog/benalua-pardo-gimeno-2306186705-1bdba27ce9.jpg', '', 36),
(37, 'yii\\easyii\\modules\\catalog\\models\\Item', 6, 'catalog/altozano-2247455661-f40671e594.jpg', '', 37),
(38, 'yii\\easyii\\modules\\catalog\\models\\Item', 6, 'catalog/altozano-2247455662-6ba2c60205.jpg', '', 38),
(39, 'yii\\easyii\\modules\\catalog\\models\\Item', 6, 'catalog/altozano-2247455663-1e1524e8d1.jpg', '', 39),
(40, 'yii\\easyii\\modules\\catalog\\models\\Item', 6, 'catalog/altozano-2247455665-e1de6e7908.jpg', '', 40),
(41, 'yii\\easyii\\modules\\catalog\\models\\Item', 6, 'catalog/altozano-2247455666-96cf913143.jpg', '', 41),
(42, 'yii\\easyii\\modules\\catalog\\models\\Item', 6, 'catalog/altozano-2247455664-e85e7a583a.jpg', '', 42),
(43, 'yii\\easyii\\modules\\catalog\\models\\Item', 7, 'catalog/alicante-centro-antonio-mauro-23-9a47b299ca.jpg', '', 43),
(44, 'yii\\easyii\\modules\\catalog\\models\\Item', 7, 'catalog/alicante-centro-antonio-mauro-23-708129cf0d.jpg', '', 44),
(45, 'yii\\easyii\\modules\\catalog\\models\\Item', 7, 'catalog/alicante-centro-antonio-mauro-23-40fe3cf6ac.jpg', '', 45),
(46, 'yii\\easyii\\modules\\catalog\\models\\Item', 7, 'catalog/alicante-centro-antonio-mauro-23-b5c924723d.jpg', '', 46),
(47, 'yii\\easyii\\modules\\catalog\\models\\Item', 7, 'catalog/alicante-centro-antonio-mauro-23-fe35baa865.jpg', '', 47),
(48, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110833-b20f4a7bea.jpg', '', 48),
(49, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110659-646f7a41f8.jpg', '', 49),
(50, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110747-3f60b0c0cd.jpg', '', 50),
(51, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110758-8e3d0237c2.jpg', '', 51),
(52, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110849-2f3091fe24.jpg', '', 52),
(53, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110721-f0100a465d.jpg', '', 53),
(54, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110902-9e2d870836.jpg', '', 54),
(55, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515110917-153f0e005f.jpg', '', 55),
(56, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0006-579007215d.jpg', '', 56),
(57, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0007-21c1d3935a.jpg', '', 57),
(58, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0008-c9907a2855.jpg', '', 58),
(59, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0009-40cde041cb.jpg', '', 59),
(60, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515111012-069d5fddad.jpg', '', 60),
(61, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0010-6708cb5424.jpg', '', 61),
(62, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0012-f4162d62a7.jpg', '', 62),
(63, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0011-fd1624be9e.jpg', '', 63),
(64, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0013-cfca9d1d3a.jpg', '', 64),
(65, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0014-8b761f6607.jpg', '', 65),
(66, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0015-2b4eb002ff.jpg', '', 66),
(67, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0016-ecb4c7b94e.jpg', '', 67),
(68, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0017-a98f923888.jpg', '', 68),
(69, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0019-3e49bb26eb.jpg', '', 69),
(70, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/img-20170505-wa0024-88c475b9d1.jpg', '', 70),
(71, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515111144-2be5199142.jpg', '', 71),
(72, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515111130-a660df1867.jpg', '', 72),
(73, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515111025-a781f0bdb1.jpg', '', 73),
(74, 'yii\\easyii\\modules\\catalog\\models\\Item', 8, 'catalog/20170515111103-de01fee806.jpg', '', 74),
(75, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-d734a4092a.jpg', '', 75),
(76, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-df1d58fcc4.jpg', '', 76),
(77, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-7f0d0ec661.jpg', '', 77),
(78, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-9ba04eb952.jpg', '', 78),
(79, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-be2737b4d8.jpg', '', 79),
(80, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-7251fac2ea.jpg', '', 80),
(81, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-024ee53e0d.jpg', '', 81),
(82, 'yii\\easyii\\modules\\catalog\\models\\Item', 9, 'catalog/plaza-de-toros-maestro-caballero-de157a24bf.jpg', '', 82),
(83, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-bcd5d3d1a5.jpg', '', 83),
(84, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-7f1d91efa8.jpg', '', 84),
(85, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-171dda1f08.jpg', '', 85),
(86, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-da6b9950a9.jpg', '', 86),
(87, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-38820a9be2.jpg', '', 87),
(88, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-f6e750ffcb.jpg', '', 88),
(89, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-755ab1c0e2.jpg', '', 89),
(90, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-725ee83dd2.jpg', '', 90),
(91, 'yii\\easyii\\modules\\catalog\\models\\Item', 10, 'catalog/atico-de-lujo-acepto-ofertas-car-999851f7b9.jpg', '', 91),
(92, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-f4bf6a3fcc.jpg', '', 92),
(93, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-07c62d9137.jpg', '', 93),
(94, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-9681ff0a85.jpg', '', 94),
(95, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-d7d3256fb9.jpg', '', 95),
(96, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-aa262519c4.jpg', '', 96),
(97, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-52ad325dac.jpg', '', 97),
(98, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-b7dffefe56.jpg', '', 98),
(99, 'yii\\easyii\\modules\\catalog\\models\\Item', 11, 'catalog/playa-del-postiguet-avenida-de-d-e45af30c92.jpg', '', 99),
(100, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-8de9fdecf4.jpg', '', 100),
(101, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-9eb6805274.jpg', '', 101),
(102, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-530e66b441.jpg', '', 102),
(103, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-23966f9058.jpg', '', 103),
(104, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-ec1420100f.jpg', '', 104),
(105, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-57754fc414.jpg', '', 105),
(106, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-9c4528ad64.jpg', '', 106),
(107, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-75e4f94f76.jpg', '', 107),
(108, 'yii\\easyii\\modules\\catalog\\models\\Item', 12, 'catalog/cabo-de-las-huertas-camino-del-f-d3518d588c.jpg', '', 108),
(258, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755331e81-3ae97cab3f.jpg', '', 167),
(259, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/557075524460e-101e048176.jpg', '', 168),
(260, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/55707552bcda4-dab3b87d37.jpg', '', 169),
(261, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755de792e-1400a0cdca.jpg', '', 170),
(262, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755dd697e-12747b0029.jpg', '', 171),
(263, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/55707552f20e1-d549ac0da4.jpg', '', 172),
(264, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3873a52ece-cbbfc22ec9.jpg', '', 173),
(265, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d38725b4799-44fa98224f.jpg', '', 174),
(266, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d38725e8124-b4d3b1da77.jpg', '', 175),
(267, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3873b55be7-a1628e4806.jpg', '', 176),
(268, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d38739c8576-83161c6973.jpg', '', 177),
(269, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3873acf6d6-550432fd7e.jpg', '', 178),
(244, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca0e1ba7-c2e858af3d.jpg', '', 158),
(245, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca08db7f-39d0a6c92c.jpg', '', 159),
(251, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755daa344-210d4f02a3.jpg', '', 160),
(252, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755d4b914-c4f82878c4.jpg', '', 161),
(253, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755d19729-14f680fbff.jpg', '', 162),
(254, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755d78150-3f7bd16915.jpg', '', 163),
(255, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/55707551c006a-bdc95e415b.jpg', '', 164),
(256, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/5570755e23d49-5a18704e05.jpg', '', 165),
(257, 'yii\\easyii\\modules\\catalog\\models\\Item', 24, 'catalog/557075520a57c-d6b946a1d2.jpg', '', 166),
(232, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d174cf6-a262f718fb.jpg', '', 146),
(233, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/55952bff29bde-c26223d669.jpg', '', 147),
(234, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d265a18-b270b18824.jpg', '', 148),
(235, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d1610d2-fba87f3034.jpg', '', 149),
(236, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d237736-e404ab5ee5.jpg', '', 150),
(237, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca0a3346-919e84932f.jpg', '', 151),
(238, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca103f45-ea007060c2.jpg', '', 152),
(239, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca0b8476-46825b656e.jpg', '', 153),
(240, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca0351a2-9315a1adbb.jpg', '', 154),
(241, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca078256-eb66e0a9ad.jpg', '', 155),
(242, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca04d2b7-a966b25050.jpg', '', 156),
(243, 'yii\\easyii\\modules\\catalog\\models\\Item', 23, 'catalog/55719ca0ccbfa-6d29bf877c.jpg', '', 157),
(220, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d1c5280-ab9272d869.jpg', '', 134),
(221, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d1b1f01-d1bf562579.jpg', '', 135),
(222, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d1ec58d-18dec2e85b.jpg', '', 136),
(223, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d19c5e7-2a2e5b75aa.jpg', '', 137),
(224, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d28edd9-1-7021bef991.jpg', '', 138),
(225, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d28edd9-cf17f41a36.jpg', '', 139),
(226, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d222f11-a577eb6e3c.jpg', '', 140),
(227, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d24f349-1d288b147b.jpg', '', 141),
(228, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d2a3b13-9d5c1005b2.jpg', '', 142),
(229, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d27acaf-67d9350805.jpg', '', 143),
(230, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d2b9e70-a18e4e304b.jpg', '', 144),
(231, 'yii\\easyii\\modules\\catalog\\models\\Item', 22, 'catalog/5571a6d1876c2-1f9474c789.jpg', '', 145),
(208, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535efd24d0-52999ac3a1.jpg', '', 122),
(209, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535efbbfc6-bfd85de7b4.jpg', '', 123),
(210, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f001724-6dd0b3a1cf.jpg', '', 124),
(211, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535efe7009-fdf60e8de5.jpg', '', 125),
(212, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535efac9a0-774d9ca3c7.jpg', '', 126),
(213, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535ef95f05-6c5659b393.jpg', '', 127),
(214, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f072422-3d92d65a29.jpg', '', 128),
(215, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f0992b7-3e166e4d64.jpg', '', 129),
(216, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/55952bfe97f3a-e8b0328f2d.jpg', '', 130),
(217, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/55952bfede70e-3e418a8127.jpg', '', 131),
(218, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/55952bff29bde-283d8b2b4f.jpg', '', 132),
(219, 'yii\\easyii\\modules\\catalog\\models\\Item', 21, 'catalog/55952bff15f4a-218741f26e.jpg', '', 133),
(195, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0e5999c-903fb0e205.jpg', '', 109),
(196, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0e81835-32aef3cca7.jpg', '', 110),
(197, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0f060bc-137ba10a5a.jpg', '', 111),
(198, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0f17f34-3bc3ebde69.jpg', '', 112),
(199, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0ee553e-1a303ea698.jpg', '', 113),
(200, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0e44596-2df51574d6.jpg', '', 114),
(201, 'yii\\easyii\\modules\\catalog\\models\\Item', 19, 'catalog/55953f0f2c56c-4868dbcc95.jpg', '', 115),
(202, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535ef53b6d-39ec9653ba.jpg', '', 116),
(203, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f01eee1-6804a28160.jpg', '', 117),
(204, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f05d73c-8b5ad17789.jpg', '', 118),
(205, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f047ee4-1e4691cbb1.jpg', '', 119),
(206, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f081a85-ccef5a5c0e.jpg', '', 120),
(207, 'yii\\easyii\\modules\\catalog\\models\\Item', 20, 'catalog/559535f0333bb-1fa9e81b64.jpg', '', 121),
(270, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3872536239-d7a335f147.jpg', '', 179),
(271, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3873b1bbd5-7000ee28a1.jpg', '', 180),
(272, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3873b88bfe-e731d7adb8.jpg', '', 181),
(273, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3873a1307c-32d424873b.jpg', '', 182),
(274, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d38725753ee-1cd4be3f25.jpg', '', 183),
(275, 'yii\\easyii\\modules\\catalog\\models\\Item', 25, 'catalog/54d3872658031-0e78adbff0.jpg', '', 184),
(276, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be7c7967-f38cfb3c93.jpg', '', 185),
(277, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be928196-61ad117899.jpg', '', 186),
(278, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be8e8a76-0b2001d467.jpg', '', 187),
(279, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338c0dd0018-66c955c19b.jpg', '', 188),
(280, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338c0e1aed8-5a78392bf3.jpg', '', 189),
(281, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be8b8df9-213a2e15d5.jpg', '', 190),
(282, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338c0e5ac39-04d96d0f6b.jpg', '', 191),
(283, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338c0d90a48-00d8f8fe5d.jpg', '', 192),
(284, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be84cfcd-c78a0071fb.jpg', '', 193),
(285, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338c0ec188c-bef9955abc.jpg', '', 194),
(286, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be889b02-74c3d67027.jpg', '', 195),
(287, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338be958e92-bed55524ba.jpg', '', 196),
(288, 'yii\\easyii\\modules\\catalog\\models\\Item', 26, 'catalog/57338c0e8ff19-e8310db125.jpg', '', 197),
(289, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/559540e9b3a67-b1ee6ac266.jpg', '', 198),
(290, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/559540e987539-dd7fb0b08e.jpg', '', 199),
(291, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/559540e9c88ce-3ed0a7cad2.jpg', '', 200),
(292, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/559540e95b192-cf0748c116.jpg', '', 201),
(293, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/559540e9167b7-6f1af56beb.jpg', '', 202),
(294, 'yii\\easyii\\modules\\catalog\\models\\Item', 27, 'catalog/559540e99cf9c-9ea81501be.jpg', '', 203),
(295, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1acc6826-65741d8e22.jpg', '', 204),
(296, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1c5495b3-36f26db893.jpg', '', 205),
(297, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1ad11255-037b69888d.jpg', '', 206),
(298, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1c518724-58ef046287.jpg', '', 207),
(299, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1dc8606d-677e0768f3.jpg', '', 208),
(300, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1dcc638e-0b8463a5f7.jpg', '', 209),
(301, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1dd13c2d-724ea41384.jpg', '', 210),
(302, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1dd928da-4081b95fb8.jpg', '', 211),
(303, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1ddd096a-3e768c1195.jpg', '', 212),
(304, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1de7e7a1-4d4b801043.jpg', '', 213),
(305, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1de49ae1-a18968d33f.jpg', '', 214),
(306, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1ad4ecc3-f60442e1f5.jpg', '', 215),
(307, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1deb3bc3-2ef7b319a6.jpg', '', 216),
(308, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1c3dd76d-6d216e29c9.jpg', '', 217),
(309, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1c39e78b-969ec181c0.jpg', '', 218),
(310, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1c4285fc-262c157dbb.jpg', '', 219),
(311, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1dee8663-549a1c0cf4.jpg', '', 220),
(312, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1df27c3e-67bf0aa086.jpg', '', 221),
(313, 'yii\\easyii\\modules\\catalog\\models\\Item', 28, 'catalog/54d1f1de17346-014294eae2.jpg', '', 222),
(314, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d908eb2c6-f87509f98b.jpg', '', 223),
(315, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d966d75ab-59dd80ddfb.jpg', '', 224),
(316, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d909b31ec-dbc7cefb9b.jpg', '', 225),
(317, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9638de94-81f6086a86.jpg', '', 226),
(318, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9649a0f5-912c8c0a62.jpg', '', 227),
(319, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9655ff05-f2df7f5f43.jpg', '', 228),
(320, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9660ab3f-ff2abe6a19.jpg', '', 229),
(321, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d965c9d97-09b2f00eed.jpg', '', 230),
(322, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d938bcf81-dd1dfa6488.jpg', '', 231),
(323, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d934bf502-95ed7a6a8a.jpg', '', 232),
(324, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d964d81ee-0e4165e196.jpg', '', 233),
(325, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d965220ab-a44f3e447e.jpg', '', 234),
(326, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d966718ac-25d69287a5.jpg', '', 235),
(327, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d96594d15-0cb0c91855.jpg', '', 236),
(328, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d90974992-4d3ed75fab.jpg', '', 237),
(329, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9630ee17-b6ba5fb92b.jpg', '', 238),
(330, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d96419397-9844207da1.jpg', '', 239),
(331, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9663f6da-03deca4672.jpg', '', 240),
(332, 'yii\\easyii\\modules\\catalog\\models\\Item', 29, 'catalog/54d1d9671681d-6bfc240b04.jpg', '', 241),
(333, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d364ec4c031-361840f55d.jpg', '', 242),
(334, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d36555c481f-d99afcd6dd.jpg', '', 243),
(335, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d364ece3ef1-d91b74b48f.jpg', '', 244),
(336, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d36554b844c-ba6f6cf1e2.jpg', '', 245),
(337, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d36537d8437-ee5b4da3db.jpg', '', 246),
(338, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d3655590100-527861a381.jpg', '', 247),
(339, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d364ed224f2-0e968c3a1c.jpg', '', 248),
(340, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d3653822bca-3884ca75a7.jpg', '', 249),
(341, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d3655486034-5027f4e12d.jpg', '', 250),
(342, 'yii\\easyii\\modules\\catalog\\models\\Item', 31, 'catalog/54d3653a9d619-10fb9c893c.jpg', '', 251),
(343, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353ab5811c-0f04654cc1.jpg', '', 252),
(344, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353d0d39ab-a4160133d3.jpg', '', 253),
(345, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353ac82c37-e7f47746aa.jpg', '', 254),
(346, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353cec361d-b1a7c2856a.jpg', '', 255),
(347, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353d1771e5-64ccb9f2e0.jpg', '', 256),
(348, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353d112258-8f1a16713e.jpg', '', 257),
(349, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353df6f0a7-a43e49fbe5.jpg', '', 258),
(350, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353e078ffe-0802cb38b2.jpg', '', 259),
(351, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353d145a73-1ecc597afd.jpg', '', 260),
(352, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353cf01b26-7c9e2b5683.jpg', '', 261),
(353, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353dfafb19-efd90be661.jpg', '', 262),
(354, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353cf99cdb-cad6f22594.jpg', '', 263),
(355, 'yii\\easyii\\modules\\catalog\\models\\Item', 33, 'catalog/54d353ce8f5d5-92d2c43e7f.jpg', '', 264),
(356, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538735-4ef439be50.jpg', '', 265),
(357, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538734-7d68e448c5.jpg', '', 266),
(358, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538736-e33ea19158.jpg', '', 267),
(359, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538767-0822cba9d5.jpg', '', 268),
(360, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538768-b50c909dd3.jpg', '', 269),
(361, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538766-1a9901e92b.jpg', '', 270),
(362, 'yii\\easyii\\modules\\catalog\\models\\Item', 34, 'catalog/191538737-3fc5972d5e.jpg', '', 271),
(363, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842847-35ec4b6424.jpg', '', 272),
(364, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842871-d3282b2dc4.jpg', '', 273),
(365, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842875-f54eed9d44.jpg', '', 274),
(366, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842876-566ec8d73a.jpg', '', 275),
(367, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842878-16fd6d7ede.jpg', '', 276),
(368, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842335-db5faa3a65.jpg', '', 277),
(369, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842880-37efc497b0.jpg', '', 278),
(370, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842859-a28b915d04.jpg', '', 279),
(371, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842881-3775a2e383.jpg', '', 280),
(372, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842864-bac019bc30.jpg', '', 281),
(373, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842882-48f3c9c0de.jpg', '', 282),
(374, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842863-c7c120be6e.jpg', '', 283),
(375, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842851-97b903dccd.jpg', '', 284),
(376, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842886-03cf94c203.jpg', '', 285),
(377, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192842884-e1d3c9b82b.jpg', '', 286),
(378, 'yii\\easyii\\modules\\catalog\\models\\Item', 35, 'catalog/192844969-bc62c09bb9.jpg', '', 287),
(379, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354624-7c5fbbbe29.jpg', '', 288),
(380, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354631-fc4cf0d0e3.jpg', '', 289),
(381, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354632-fad879236e.jpg', '', 290),
(382, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354634-fe9781f120.jpg', '', 291),
(383, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354630-e22141b2a3.jpg', '', 292),
(384, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354628-2b329c622b.jpg', '', 293),
(385, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354625-ce550ce394.jpg', '', 294),
(386, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354638-56c05418a7.jpg', '', 295),
(387, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354629-06983d94ec.jpg', '', 296),
(388, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354645-a76f70e57b.jpg', '', 297),
(389, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354646-1-2b0e08e3f5.jpg', '', 298),
(390, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354646-8711e31af1.jpg', '', 299),
(391, 'yii\\easyii\\modules\\catalog\\models\\Item', 36, 'catalog/197354648-9f4277814d.jpg', '', 300),
(392, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661498-83c7d80ef1.jpg', '', 301),
(393, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661770-cb77d53d1f.jpg', '', 302),
(394, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661747-ad6f245b09.jpg', '', 303),
(395, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661787-32c2002d0e.jpg', '', 304),
(396, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661823-23d1b7eba1.jpg', '', 305),
(397, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661868-35456f1226.jpg', '', 306),
(398, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661869-b9ebef0449.jpg', '', 307),
(399, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661886-570744b7c4.jpg', '', 308),
(400, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661939-1-624721db28.jpg', '', 309),
(401, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661939-306b406c50.jpg', '', 310),
(402, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201662012-00de4b4f55.jpg', '', 311),
(403, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201662027-f96cb84f24.jpg', '', 312),
(404, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661587-cb68ef8167.jpg', '', 313),
(405, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661767-04fdb571cb.jpg', '', 314),
(406, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661752-07273e31e7.jpg', '', 315),
(407, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201661739-6b4b08395e.jpg', '', 316),
(408, 'yii\\easyii\\modules\\catalog\\models\\Item', 37, 'catalog/201662228-ac67b14809.jpg', '', 317),
(409, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102851-1aa0fb2a26.jpg', '', 318),
(410, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102869-a16fc94abe.jpg', '', 319),
(411, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102856-7d6ab3cc95.jpg', '', 320),
(412, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102853-5a31e8c1e2.jpg', '', 321),
(413, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102871-3eb1d6d399.jpg', '', 322),
(414, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102872-5c73e32ece.jpg', '', 323),
(415, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102874-9991d337aa.jpg', '', 324),
(416, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102854-15aab0ea9c.jpg', '', 325),
(417, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102855-8b0dca2edb.jpg', '', 326),
(418, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102852-b0c3ee05ae.jpg', '', 327),
(419, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102876-3d71a893ba.jpg', '', 328),
(420, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102870-b6e91ad041.jpg', '', 329),
(421, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102878-0689f062a6.jpg', '', 330),
(422, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102879-79755a2389.jpg', '', 331),
(423, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102873-8bdb1b7380.jpg', '', 332),
(424, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102875-e7f9acf573.jpg', '', 333),
(425, 'yii\\easyii\\modules\\catalog\\models\\Item', 38, 'catalog/186102877-97a8249ba5.jpg', '', 334),
(426, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204543969-4468be7b68.jpg', '', 335),
(427, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544420-cef1492ff4.jpg', '', 336),
(428, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544383-f0536274ef.jpg', '', 337),
(429, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544019-f56e139e33.jpg', '', 338),
(430, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544451-3cbab96c02.jpg', '', 339),
(431, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544550-81a19e30f0.jpg', '', 340),
(432, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544554-ca86454aa6.jpg', '', 341),
(433, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544579-73087b0a30.jpg', '', 342),
(434, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544580-fdc51613c3.jpg', '', 343),
(435, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544594-7c93adf8f6.jpg', '', 344),
(436, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544598-1f680f74a4.jpg', '', 345),
(437, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544711-73a25d92f0.jpg', '', 346),
(438, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544425-f7844c91d8.jpg', '', 347),
(439, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544712-ee68c15016.jpg', '', 348),
(440, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544390-3edbc5a89d.jpg', '', 349),
(441, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544043-f5a0f8a313.jpg', '', 350),
(442, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564095-d20ffe59fc.jpg', '', 351),
(443, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564104-2062edfd80.jpg', '', 352),
(444, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544360-19c803496d.jpg', '', 353),
(445, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564154-2e3683fac0.jpg', '', 354),
(446, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564158-011a5b99d6.jpg', '', 355),
(447, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564153-959fcc3a17.jpg', '', 356),
(448, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564271-7df94c5f36.jpg', '', 357),
(449, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204544718-09acbc114d.jpg', '', 358),
(450, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564159-7c55707056.jpg', '', 359),
(451, 'yii\\easyii\\modules\\catalog\\models\\Item', 39, 'catalog/204564262-6d6bf9821b.jpg', '', 360),
(452, 'yii\\easyii\\modules\\catalog\\models\\Item', 40, 'catalog/193450724-764b9abd26.jpg', '', 361),
(453, 'yii\\easyii\\modules\\catalog\\models\\Item', 40, 'catalog/193450729-f9778b6f55.jpg', '', 362),
(454, 'yii\\easyii\\modules\\catalog\\models\\Item', 40, 'catalog/193450728-6917e7cf89.jpg', '', 363),
(455, 'yii\\easyii\\modules\\catalog\\models\\Item', 40, 'catalog/193450730-76a59b61b0.jpg', '', 364),
(456, 'yii\\easyii\\modules\\catalog\\models\\Item', 40, 'catalog/193450731-0d7e2b2846.jpg', '', 365),
(457, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672505-3a251c1f12.jpg', '', 366),
(458, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672519-c7bda8aef0.jpg', '', 367),
(459, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672510-67d95b3e76.jpg', '', 368),
(460, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672520-b88ba4a2f8.jpg', '', 369),
(461, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672521-512c9994a1.jpg', '', 370),
(462, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672523-503606aadf.jpg', '', 371),
(463, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672507-a82a939752.jpg', '', 372),
(464, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672518-96b7b2306b.jpg', '', 373),
(465, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672509-f8e50c36d2.jpg', '', 374),
(466, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672506-6de3e851de.jpg', '', 375),
(467, 'yii\\easyii\\modules\\catalog\\models\\Item', 41, 'catalog/218672522-758ec40b30.jpg', '', 376),
(468, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297843-4f61160772.jpg', '', 377),
(469, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297851-cb092e7d74.jpg', '', 378),
(470, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297846-02d9cc9a9f.jpg', '', 379),
(471, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297847-a8b99135bf.jpg', '', 380),
(472, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297852-dd3df79872.jpg', '', 381),
(473, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297845-42a432441d.jpg', '', 382),
(474, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297844-7f2620ae78.jpg', '', 383),
(475, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297853-3d217818f4.jpg', '', 384),
(476, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297858-7c03aeecd3.jpg', '', 385),
(477, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297844-1-fa92e0c9a6.jpg', '', 386),
(478, 'yii\\easyii\\modules\\catalog\\models\\Item', 42, 'catalog/196297854-34625d43f6.jpg', '', 387),
(479, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016497-c730796a8c.jpg', '', 388),
(480, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016656-78522ff1bc.jpg', '', 389),
(481, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016655-36fa1496e1.jpg', '', 390),
(482, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016659-d30546ad26.jpg', '', 391),
(483, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016660-c48f60ac8d.jpg', '', 392),
(484, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016663-371ecae469.jpg', '', 393),
(485, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016665-d2d9f14b94.jpg', '', 394),
(486, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016654-232aecde12.jpg', '', 395),
(487, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016496-1af1cdeef9.jpg', '', 396),
(488, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016653-cd3a0b1653.jpg', '', 397),
(489, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016652-df73486928.jpg', '', 398),
(490, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016669-da0318bd68.jpg', '', 399),
(491, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016670-9d236e0061.jpg', '', 400),
(492, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016661-ba843c87a8.jpg', '', 401),
(493, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016666-3bb441aded.jpg', '', 402),
(494, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016673-8db791a25c.jpg', '', 403),
(495, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016674-377f901028.jpg', '', 404),
(496, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016671-22b7810f9a.jpg', '', 405),
(497, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/187016668-2f79f7238e.jpg', '', 406),
(498, 'yii\\easyii\\modules\\catalog\\models\\Item', 43, 'catalog/staticmap-1-31dfa38c3f.png', '', 407),
(499, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897868-94f2d0211a.jpg', '', 408),
(500, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897874-ae08618552.jpg', '', 409),
(501, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897870-c7b3a6fa65.jpg', '', 410),
(502, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897875-5e7f07b543.jpg', '', 411),
(503, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897881-965a41c171.jpg', '', 412),
(504, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897883-bf58ba3681.jpg', '', 413),
(505, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897885-d2ea6c80f8.jpg', '', 414),
(506, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897872-ada86d718c.jpg', '', 415),
(507, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897873-b222afb461.jpg', '', 416),
(508, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897871-cb34756d91.jpg', '', 417),
(509, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897869-e07d5e104d.jpg', '', 418),
(510, 'yii\\easyii\\modules\\catalog\\models\\Item', 44, 'catalog/229897884-15f8798f94.jpg', '', 419),
(511, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151966-a2c53c3e83.jpg', '', 420),
(512, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151964-30f23045c3.jpg', '', 421),
(513, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151967-5c12f52846.jpg', '', 422),
(514, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151969-4e805226c4.jpg', '', 423),
(515, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151970-b5454f5016.jpg', '', 424),
(516, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151957-56cef5952a.jpg', '', 425),
(517, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151978-2148c969df.jpg', '', 426),
(518, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151956-df40ca5e76.jpg', '', 427),
(519, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151968-eda2b542b3.jpg', '', 428),
(520, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151965-89b77cf4b2.jpg', '', 429),
(521, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151955-208a72df33.jpg', '', 430),
(522, 'yii\\easyii\\modules\\catalog\\models\\Item', 45, 'catalog/229151979-87df69e3b2.jpg', '', 431),
(523, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736271-a1cf8267fb.jpg', '', 432),
(524, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736281-ecdf0f169e.jpg', '', 433),
(525, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736282-5181ad5fb6.jpg', '', 434),
(526, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736283-cae5b76eec.jpg', '', 435),
(527, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736274-0daebb25c4.jpg', '', 436),
(528, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736272-0da32c4f61.jpg', '', 437),
(529, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736269-eadaa6414a.jpg', '', 438),
(530, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736273-4b25f98ccc.jpg', '', 439),
(531, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736286-c8dbd647a0.jpg', '', 440),
(532, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736288-b4ce41c1bc.jpg', '', 441),
(533, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736289-343c4c9f35.jpg', '', 442),
(534, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736280-2bbf3e6cec.jpg', '', 443),
(535, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736287-47abe04fc3.jpg', '', 444),
(536, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736291-0f73d6550c.jpg', '', 445),
(537, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736284-f71d2c411c.jpg', '', 446),
(538, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736293-13a6e2c3bb.jpg', '', 447),
(539, 'yii\\easyii\\modules\\catalog\\models\\Item', 46, 'catalog/226736292-432ccae01c.jpg', '', 448),
(540, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626202-8a4b7eec9c.jpg', '', 449),
(541, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626183-7d34fbab03.jpg', '', 450),
(542, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626315-f1a262f041.jpg', '', 451),
(543, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626265-0cc78e5b2d.jpg', '', 452),
(544, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626357-b86b33d8ea.jpg', '', 453),
(545, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626393-f07a36692f.jpg', '', 454),
(546, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626392-af4d7e7032.jpg', '', 455),
(547, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626457-f20af6d960.jpg', '', 456),
(548, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626256-937ea4030e.jpg', '', 457),
(549, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626196-820fd85c37.jpg', '', 458),
(550, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626540-9ec2d3c6fe.jpg', '', 459),
(551, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626321-a73e63a87d.jpg', '', 460),
(552, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626573-de392b1258.jpg', '', 461),
(553, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626367-27bbee1046.jpg', '', 462),
(554, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626581-c842ef4c8a.jpg', '', 463),
(555, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626606-dbad74d08e.jpg', '', 464),
(556, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626541-7a20c31849.jpg', '', 465),
(557, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207626572-ba9d68a427.jpg', '', 466),
(558, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207628096-7f3af62046.jpg', '', 467),
(559, 'yii\\easyii\\modules\\catalog\\models\\Item', 47, 'catalog/207628101-678bcd800a.jpg', '', 468);
INSERT INTO `easyii_photos` (`id`, `class`, `item_id`, `image_file`, `description`, `order_num`) VALUES
(560, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/226304910-d2484d1a34.jpg', '', 469),
(561, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784037-69a82b62cc.jpg', '', 470),
(562, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784042-40aedef58f.jpg', '', 471),
(563, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784043-79e4ce7515.jpg', '', 472),
(564, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784035-11f8c994b9.jpg', '', 473),
(565, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/226304908-962684ec8f.jpg', '', 474),
(566, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784034-83f6cca229.jpg', '', 475),
(567, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784045-cefd389205.jpg', '', 476),
(568, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784033-252c93fde4.jpg', '', 477),
(569, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784046-f5cb458a5b.jpg', '', 478),
(570, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784047-33ca15d8d3.jpg', '', 479),
(571, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784036-8d0588ef29.jpg', '', 480),
(572, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784044-ee71abaa2f.jpg', '', 481),
(573, 'yii\\easyii\\modules\\catalog\\models\\Item', 48, 'catalog/228784048-4898050fc3.jpg', '', 482),
(574, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538734-ad30aaefc6.jpg', '', 483),
(575, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538768-5876b4005f.jpg', '', 484),
(576, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538736-8571dda647.jpg', '', 485),
(577, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538737-89a8fd6de6.jpg', '', 486),
(578, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538766-75a0e60b53.jpg', '', 487),
(579, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538767-1fe65d0ee0.jpg', '', 488),
(580, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/191538735-2ec08faacf.jpg', '', 489),
(581, 'yii\\easyii\\modules\\catalog\\models\\Item', 49, 'catalog/staticmap-78a13e6a10.png', '', 490),
(582, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102854-1653d0aa24.jpg', '', 491),
(583, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102855-1ee80eaab4.jpg', '', 492),
(584, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102869-7ed4f447fa.jpg', '', 493),
(585, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102871-87dad4f9a1.jpg', '', 494),
(586, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102851-8c07a77cdd.jpg', '', 495),
(587, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102852-7885120449.jpg', '', 496),
(588, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102853-84de6c2513.jpg', '', 497),
(589, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102856-232d06f8a3.jpg', '', 498),
(590, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102874-36a04c46fd.jpg', '', 499),
(591, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102875-20bc0f5b65.jpg', '', 500),
(592, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102872-a0d744b0f7.jpg', '', 501),
(593, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102876-33e7903b9d.jpg', '', 502),
(594, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102870-6b64d71f8c.jpg', '', 503),
(595, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102878-ea26570e77.jpg', '', 504),
(596, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102877-5e705f63b9.jpg', '', 505),
(597, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102873-58838e224f.jpg', '', 506),
(598, 'yii\\easyii\\modules\\catalog\\models\\Item', 50, 'catalog/186102879-d140d209b2.jpg', '', 507),
(599, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204543969-fafcecdfb1.jpg', '', 508),
(600, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544420-5581b77fa2.jpg', '', 509),
(601, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544360-cd28548821.jpg', '', 510),
(602, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544451-e7f95033e3.jpg', '', 511),
(603, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544019-00f2a91017.jpg', '', 512),
(604, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544390-c94f62b406.jpg', '', 513),
(605, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544554-af8f4cfc32.jpg', '', 514),
(606, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544579-39435144b6.jpg', '', 515),
(607, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544383-10aafea4c3.jpg', '', 516),
(608, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544425-a24d681e3d.jpg', '', 517),
(609, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544580-53f2fa6873.jpg', '', 518),
(610, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544043-de29d1017c.jpg', '', 519),
(611, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544711-1ffba05359.jpg', '', 520),
(612, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544594-fc113c91f2.jpg', '', 521),
(613, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544712-53e7e76ae9.jpg', '', 522),
(614, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564095-1b865f4df8.jpg', '', 523),
(615, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544718-a119164440.jpg', '', 524),
(616, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544550-f850f0dc40.jpg', '', 525),
(617, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564154-375ed57e2a.jpg', '', 526),
(618, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564153-6b5eca0cbc.jpg', '', 527),
(619, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564104-846efaec0d.jpg', '', 528),
(620, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564159-1d9d08aa65.jpg', '', 529),
(621, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564158-78aaf3dac4.jpg', '', 530),
(622, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204544598-1f4d3aa2eb.jpg', '', 531),
(623, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564262-3259917b25.jpg', '', 532),
(624, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/204564271-6e2f75b6a1.jpg', '', 533),
(625, 'yii\\easyii\\modules\\catalog\\models\\Item', 51, 'catalog/staticmap-a0c6c36a00.png', '', 534),
(626, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034626-0a7b75fe8c.jpg', '', 535),
(627, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034740-0ef0fc8ea3.jpg', '', 536),
(628, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034725-3b850a07fb.jpg', '', 537),
(629, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034700-02b0fb5ed4.jpg', '', 538),
(630, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034799-b3b7c2867b.jpg', '', 539),
(631, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034897-db1d5b91ad.jpg', '', 540),
(632, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034901-196b84b17e.jpg', '', 541),
(633, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034902-9b8e5c585d.jpg', '', 542),
(634, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035003-60ecfe0b39.jpg', '', 543),
(635, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034701-ddb94123ef.jpg', '', 544),
(636, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034798-fc0e3d7015.jpg', '', 545),
(637, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035007-ce61e38b7e.jpg', '', 546),
(638, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034702-da42ba55b9.jpg', '', 547),
(639, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035043-bd8243724e.jpg', '', 548),
(640, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035006-462f317e2f.jpg', '', 549),
(641, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035144-a6538579bd.jpg', '', 550),
(642, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034884-d80fc9241d.jpg', '', 551),
(643, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035146-e7b2d7db87.jpg', '', 552),
(644, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035147-dbf50b3b60.jpg', '', 553),
(645, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035044-1bfe034b6d.jpg', '', 554),
(646, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035145-369f764293.jpg', '', 555),
(647, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035351-2de61c63c0.jpg', '', 556),
(648, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035352-cecb65ffb7.jpg', '', 557),
(649, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035353-47c12871cb.jpg', '', 558),
(650, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035354-6c87ab35b4.jpg', '', 559),
(651, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035250-5d2224e49f.jpg', '', 560),
(652, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220034731-1a5cbc79c0.jpg', '', 561),
(653, 'yii\\easyii\\modules\\catalog\\models\\Item', 52, 'catalog/220035355-a5b2de5462.jpg', '', 562),
(654, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807143-fc46c6082c.jpg', '', 563),
(655, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807326-82675d9fab.jpg', '', 564),
(656, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807314-f35e934694.jpg', '', 565),
(657, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807324-ba445f9d54.jpg', '', 566),
(658, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807315-a33c5d6ef7.jpg', '', 567),
(659, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807479-0bbce8f881.jpg', '', 568),
(660, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807325-c54e503b7c.jpg', '', 569),
(661, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808165-3afdc3c2b9.jpg', '', 570),
(662, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808172-d5ac4d4b8d.jpg', '', 571),
(663, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808254-bad5282325.jpg', '', 572),
(664, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807457-0ead278784.jpg', '', 573),
(665, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807323-51d5a37d00.jpg', '', 574),
(666, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808337-bfa37ca2fd.jpg', '', 575),
(667, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808338-1290246ae0.jpg', '', 576),
(668, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807328-6c2539c3e7.jpg', '', 577),
(669, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808339-d8ba41b905.jpg', '', 578),
(670, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808327-19421e137f.jpg', '', 579),
(671, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213807925-d383e9364b.jpg', '', 580),
(672, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808341-4b417fc833.jpg', '', 581),
(673, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808340-d1384097dd.jpg', '', 582),
(674, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808350-b76300e2fc.jpg', '', 583),
(675, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808727-d22830273d.jpg', '', 584),
(676, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808349-090fec28b6.jpg', '', 585),
(677, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808351-9c62246fb5.jpg', '', 586),
(678, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808348-6c2a7668f5.jpg', '', 587),
(679, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808738-baf84b92b4.jpg', '', 588),
(680, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808736-8088b45423.jpg', '', 589),
(681, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808737-a4c7063487.jpg', '', 590),
(682, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808748-0d872be028.jpg', '', 591),
(683, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808750-96febd239a.jpg', '', 592),
(684, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808739-a8723bf07e.jpg', '', 593),
(685, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808749-89915a74fe.jpg', '', 594),
(686, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808820-6ce6a25e24.jpg', '', 595),
(687, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808867-c705516bca.jpg', '', 596),
(688, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213808975-b8d257aec8.jpg', '', 597),
(689, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213809205-5897f24ee9.jpg', '', 598),
(690, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213809087-359b69f250.jpg', '', 599),
(691, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213809204-e2a4863f84.jpg', '', 600),
(692, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213809208-8e3fdb48d6.jpg', '', 601),
(693, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/213809209-b95f462418.jpg', '', 602),
(694, 'yii\\easyii\\modules\\catalog\\models\\Item', 53, 'catalog/staticmap-349f72d3c7.png', '', 603),
(695, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/196719786-affa362471.jpg', '', 604),
(696, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850136-dd8bb7008b.jpg', '', 605),
(697, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850137-9d66b8c772.jpg', '', 606),
(698, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850135-9a8326c049.jpg', '', 607),
(699, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850155-cad0f3ebfd.jpg', '', 608),
(700, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850158-9f66dd454d.jpg', '', 609),
(701, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850159-eb842e0be3.jpg', '', 610),
(702, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850152-fe01224078.jpg', '', 611),
(703, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850154-ab85c2a835.jpg', '', 612),
(704, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850156-dabafecb78.jpg', '', 613),
(705, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850157-4634d1a3e1.jpg', '', 614),
(706, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850162-73db8ba95f.jpg', '', 615),
(707, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850164-8227aba8a1.jpg', '', 616),
(708, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850160-26347e0985.jpg', '', 617),
(709, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850165-2be315d25d.jpg', '', 618),
(710, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850166-dcee6242e7.jpg', '', 619),
(711, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850161-17c5f62420.jpg', '', 620),
(712, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850169-edbdc57dd6.jpg', '', 621),
(713, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850171-60b3d02396.jpg', '', 622),
(714, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850163-6e85320339.jpg', '', 623),
(715, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850172-403e53e9b2.jpg', '', 624),
(716, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850173-ce2e0325af.jpg', '', 625),
(717, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850168-3c7a0fd797.jpg', '', 626),
(718, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850167-a218581965.jpg', '', 627),
(719, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850177-51ecbf4485.jpg', '', 628),
(720, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850181-9847fcb665.jpg', '', 629),
(721, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850180-a9ef9cb74b.jpg', '', 630),
(722, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850182-922f54180c.jpg', '', 631),
(723, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850183-6fa873c27c.jpg', '', 632),
(724, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850175-924202e1a6.jpg', '', 633),
(725, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850186-1efddc751e.jpg', '', 634),
(726, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850188-f57587eab6.jpg', '', 635),
(727, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850176-200569bfad.jpg', '', 636),
(728, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850184-000ce6f487.jpg', '', 637),
(729, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/197850187-f6e05e5ea5.jpg', '', 638),
(730, 'yii\\easyii\\modules\\catalog\\models\\Item', 54, 'catalog/staticmap-a98bc734af.png', '', 639),
(731, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399165-2a079f496c.jpg', '', 640),
(732, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399183-cade063b05.jpg', '', 641),
(733, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399179-3cde38f4da.jpg', '', 642),
(734, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399184-ab3e3e8008.jpg', '', 643),
(735, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399175-dded7bb82e.jpg', '', 644),
(736, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399180-a759be77db.jpg', '', 645),
(737, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399174-469e71fd2f.jpg', '', 646),
(738, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399188-9614dc8755.jpg', '', 647),
(739, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399185-d58ed19de3.jpg', '', 648),
(740, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399186-e19a7e9b90.jpg', '', 649),
(741, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399177-97e1bd262f.jpg', '', 650),
(742, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399187-e45792c00b.jpg', '', 651),
(743, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399191-714ae86b82.jpg', '', 652),
(744, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399189-4804896b1c.jpg', '', 653),
(745, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399192-66f92ed8c2.jpg', '', 654),
(746, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399193-6a8a4a2757.jpg', '', 655),
(747, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402945-8ef378c7dc.jpg', '', 656),
(748, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217399194-ec56a4be76.jpg', '', 657),
(749, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402981-bfb0d4dd62.jpg', '', 658),
(750, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402982-11d4022a65.jpg', '', 659),
(751, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402944-5f6a7ec5d5.jpg', '', 660),
(752, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402947-bd563d527d.jpg', '', 661),
(753, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402943-ab6877f8e1.jpg', '', 662),
(754, 'yii\\easyii\\modules\\catalog\\models\\Item', 55, 'catalog/217402983-07a024acc3.jpg', '', 663),
(755, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204557970-0311208090.jpg', '', 664),
(756, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204558788-7881166bdc.jpg', '', 665),
(757, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204558532-214b821615.jpg', '', 666),
(758, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204558545-6f46957123.jpg', '', 667),
(759, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559120-967daac946.jpg', '', 668),
(760, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559322-9cf714804d.jpg', '', 669),
(761, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559230-6e19926a3e.jpg', '', 670),
(762, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204558544-66cbbb4562.jpg', '', 671),
(763, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559312-f7db1024b3.jpg', '', 672),
(764, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559625-116698b87a.jpg', '', 673),
(765, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559225-1630587e37.jpg', '', 674),
(766, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559754-005020a910.jpg', '', 675),
(767, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559335-1037b6436c.jpg', '', 676),
(768, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204561823-4d5388142e.jpg', '', 677),
(769, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204561837-a925dc3a8c.jpg', '', 678),
(770, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204559347-c2809e0997.jpg', '', 679),
(771, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562168-9c405bc2b1.jpg', '', 680),
(772, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562656-0340fa911f.jpg', '', 681),
(773, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562667-f16dfc5ba9.jpg', '', 682),
(774, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562725-01edd0f520.jpg', '', 683),
(775, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204561266-993de99061.jpg', '', 684),
(776, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562793-5ab7b59968.jpg', '', 685),
(777, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562792-5ba19adea9.jpg', '', 686),
(778, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562462-699fd05e32.jpg', '', 687),
(779, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562810-6e2c0dfd9f.jpg', '', 688),
(780, 'yii\\easyii\\modules\\catalog\\models\\Item', 56, 'catalog/204562811-2871daf25e.jpg', '', 689),
(781, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799306-2716814c3d.jpg', '', 690),
(782, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799322-0a82e2d9e8.jpg', '', 691),
(783, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799316-f77979f3b1.jpg', '', 692),
(784, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799323-427b285a54.jpg', '', 693),
(785, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799324-cb29f9e7cd.jpg', '', 694),
(786, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799326-59dd61ed02.jpg', '', 695),
(787, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799319-e79d46bd65.jpg', '', 696),
(788, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799318-cf57605e50.jpg', '', 697),
(789, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799321-085b6351c7.jpg', '', 698),
(790, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799329-d7d5b19aeb.jpg', '', 699),
(791, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799325-1f99026101.jpg', '', 700),
(792, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799330-b46e3a1229.jpg', '', 701),
(793, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799317-89e68ba6e7.jpg', '', 702),
(794, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799327-5a82c0fead.jpg', '', 703),
(795, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799332-4e302c89aa.jpg', '', 704),
(796, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799328-4c50e1472b.jpg', '', 705),
(797, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799335-a360f75067.jpg', '', 706),
(798, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799334-ea9c31d8a8.jpg', '', 707),
(799, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799338-ee78b84dff.jpg', '', 708),
(800, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799331-06216c8d80.jpg', '', 709),
(801, 'yii\\easyii\\modules\\catalog\\models\\Item', 57, 'catalog/213799333-fcb6543662.jpg', '', 710),
(802, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480054-c343846e9d.jpg', '', 711),
(803, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480097-0f9005179d.jpg', '', 712),
(804, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480057-c4dea8b67c.jpg', '', 713),
(805, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480068-e0b9c1adc1.jpg', '', 714),
(806, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480069-3163885c30.jpg', '', 715),
(807, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480099-11a6acd166.jpg', '', 716),
(808, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480118-4aa0203eea.jpg', '', 717),
(809, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480388-0766da2ac0.jpg', '', 718),
(810, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480390-22f8fe562f.jpg', '', 719),
(811, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480107-ec85982276.jpg', '', 720),
(812, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480066-78f059b4e0.jpg', '', 721),
(813, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480067-576cdee55f.jpg', '', 722),
(814, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480399-f971854500.jpg', '', 723),
(815, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480111-9d82fdbf40.jpg', '', 724),
(816, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481936-d27bfc06c7.jpg', '', 725),
(817, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481939-c633db90f6.jpg', '', 726),
(818, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481937-fad96b544a.jpg', '', 727),
(819, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480389-6a45ac2664.jpg', '', 728),
(820, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481940-a648005270.jpg', '', 729),
(821, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481947-9baf72d971.jpg', '', 730),
(822, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203480398-eef1af7be2.jpg', '', 731),
(823, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481941-82cd2c054d.jpg', '', 732),
(824, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481949-1f288a9ebd.jpg', '', 733),
(825, 'yii\\easyii\\modules\\catalog\\models\\Item', 58, 'catalog/203481948-9d9fecbe00.jpg', '', 734),
(826, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889375-aa7730941a.jpg', '', 735),
(827, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889411-817dec2a0e.jpg', '', 736),
(828, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889377-79b12aa4d4.jpg', '', 737),
(829, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889409-e45d6d8c48.jpg', '', 738),
(830, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889413-e5d71a503a.jpg', '', 739),
(831, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889415-ea68ff8e0b.jpg', '', 740),
(832, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889419-230eccad6a.jpg', '', 741),
(833, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889412-264a732efc.jpg', '', 742),
(834, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889407-ad8696bb02.jpg', '', 743),
(835, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889410-9b9d300b74.jpg', '', 744),
(836, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889414-e0c482d66c.jpg', '', 745),
(837, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889406-b3dd3e9ea8.jpg', '', 746),
(838, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889423-c12994e06a.jpg', '', 747),
(839, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889422-037ae935ae.jpg', '', 748),
(840, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889420-b6be4f0d09.jpg', '', 749),
(841, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/229889421-9893fc1be9.jpg', '', 750),
(842, 'yii\\easyii\\modules\\catalog\\models\\Item', 59, 'catalog/230373776-17b4cffc1b.jpg', '', 751),
(843, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/190797958-98ae34d697.jpg', '', 752),
(844, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584401-4978f5af82.jpg', '', 753),
(845, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/190797960-668b88cf21.jpg', '', 754),
(846, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584400-f25efcc5e8.jpg', '', 755),
(847, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584421-24c2ef9273.jpg', '', 756),
(848, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584422-70f90557a8.jpg', '', 757),
(849, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584515-64b2cc0e75.jpg', '', 758),
(850, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584516-d0f887a833.jpg', '', 759),
(851, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584368-da22b19196.jpg', '', 760),
(852, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584369-0cb8d4bec0.jpg', '', 761),
(853, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584558-f1dd6e2344.jpg', '', 762),
(854, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584559-d7195c4194.jpg', '', 763),
(855, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584399-a665ed8e11.jpg', '', 764),
(856, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584420-12ee634300.jpg', '', 765),
(857, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584513-f7db425f24.jpg', '', 766),
(858, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584561-6352c54e7c.jpg', '', 767),
(859, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584557-106d834cea.jpg', '', 768),
(860, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584563-597cd4a6eb.jpg', '', 769),
(861, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584562-72f055f788.jpg', '', 770),
(862, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584565-3f6b1c6876.jpg', '', 771),
(863, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584564-a766799301.jpg', '', 772),
(864, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584560-8dd2290e52.jpg', '', 773),
(865, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584566-f0c5ea2ced.jpg', '', 774),
(866, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/199584567-133ed186bc.jpg', '', 775),
(867, 'yii\\easyii\\modules\\catalog\\models\\Item', 60, 'catalog/staticmap-8309b01e9f.png', '', 776),
(868, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747036-8aa4298baa.jpg', '', 777),
(869, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747060-2a24e6f111.jpg', '', 778),
(870, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747037-5f21e0a468.jpg', '', 779),
(871, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747068-d368901967.jpg', '', 780),
(872, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747069-d0705ae534.jpg', '', 781),
(873, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747057-634142bfd0.jpg', '', 782),
(874, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747059-434b71b8ad.jpg', '', 783),
(875, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747056-30d845561f.jpg', '', 784),
(876, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747058-f301215566.jpg', '', 785),
(877, 'yii\\easyii\\modules\\catalog\\models\\Item', 61, 'catalog/180747061-7e22b54585.jpg', '', 786),
(878, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170847-ec6a479ea3.jpg', '', 787),
(879, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170870-5ceda82596.jpg', '', 788),
(880, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170869-66ef58c75c.jpg', '', 789),
(881, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170867-116267190e.jpg', '', 790),
(882, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170872-ccca3c7254.jpg', '', 791),
(883, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170873-7a3c4d583f.jpg', '', 792),
(884, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170879-6d1b0213cd.jpg', '', 793),
(885, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170880-aebf74f836.jpg', '', 794),
(886, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170868-e1f94afece.jpg', '', 795),
(887, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170865-5df35e652e.jpg', '', 796),
(888, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170864-6efd445132.jpg', '', 797),
(889, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170882-495923ab3e.jpg', '', 798),
(890, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170878-818cf11fca.jpg', '', 799),
(891, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218171845-a7bed9f616.jpg', '', 800),
(892, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170871-e1f152d185.jpg', '', 801),
(893, 'yii\\easyii\\modules\\catalog\\models\\Item', 62, 'catalog/218170881-2725afddbf.jpg', '', 802),
(894, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423973-7fbb2c4e9f.jpg', '', 803),
(895, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423967-20b938a012.jpg', '', 804),
(896, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423990-bf9bffeb1c.jpg', '', 805),
(897, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423998-4167826042.jpg', '', 806),
(898, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205424015-5adc975fb6.jpg', '', 807),
(899, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423869-966ed9579e.jpg', '', 808),
(900, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423925-227a601711.jpg', '', 809),
(901, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423918-04e2bb77b2.jpg', '', 810),
(902, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423996-9df641c144.jpg', '', 811),
(903, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205423921-9022f9f8b7.jpg', '', 812),
(904, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205424050-9a5a1418d5.jpg', '', 813),
(905, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205424078-d1adc9ecbb.jpg', '', 814),
(906, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205424026-c694320773.jpg', '', 815),
(907, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205424044-4221091436.jpg', '', 816),
(908, 'yii\\easyii\\modules\\catalog\\models\\Item', 63, 'catalog/205424047-f834207d04.jpg', '', 817),
(909, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620766-9aaba61bc6.jpg', '', 818),
(910, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620767-4892962a8e.jpg', '', 819),
(911, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620798-9b6945dd30.jpg', '', 820),
(912, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620799-91660645e3.jpg', '', 821),
(913, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620765-f96fa10b74.jpg', '', 822),
(914, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620797-de7d1cd6c0.jpg', '', 823),
(915, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620795-cd06d27e24.jpg', '', 824),
(916, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620794-7e23fa1b32.jpg', '', 825),
(917, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620796-ea7f00b205.jpg', '', 826),
(918, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620803-832d466941.jpg', '', 827),
(919, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620800-9041c5edd8.jpg', '', 828),
(920, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620801-12f351c6df.jpg', '', 829),
(921, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620806-a82affb1ba.jpg', '', 830),
(922, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620807-6feaa2baaf.jpg', '', 831),
(923, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620802-961f1df798.jpg', '', 832),
(924, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620808-9a916cac26.jpg', '', 833),
(925, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620805-0a0765a60d.jpg', '', 834),
(926, 'yii\\easyii\\modules\\catalog\\models\\Item', 64, 'catalog/212620804-0122dd30c7.jpg', '', 835),
(927, 'yii\\easyii\\modules\\catalog\\models\\Item', 65, 'catalog/193450724-11bb98993b.jpg', '', 836),
(928, 'yii\\easyii\\modules\\catalog\\models\\Item', 65, 'catalog/193450729-1-e21931d89d.jpg', '', 837),
(929, 'yii\\easyii\\modules\\catalog\\models\\Item', 65, 'catalog/193450731-1-bf220bab51.jpg', '', 838),
(930, 'yii\\easyii\\modules\\catalog\\models\\Item', 65, 'catalog/193450730-572b3a0ebb.jpg', '', 839),
(931, 'yii\\easyii\\modules\\catalog\\models\\Item', 65, 'catalog/193450728-9b12d77a5b.jpg', '', 840),
(932, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661498-cc3e9c1eda.jpg', '', 841),
(933, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661770-c8a170f6f0.jpg', '', 842),
(934, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661787-955f835e5b.jpg', '', 843),
(935, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661823-6c72d6aa32.jpg', '', 844),
(936, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661587-acea128b8d.jpg', '', 845),
(937, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661747-e7bde73d5f.jpg', '', 846),
(938, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661752-0a8dfe0a9b.jpg', '', 847),
(939, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661869-1c94cbd3f0.jpg', '', 848),
(940, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661767-66547b170a.jpg', '', 849),
(941, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661886-1b1c436f87.jpg', '', 850),
(942, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661939-2858c9ace6.jpg', '', 851),
(943, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661739-d88485eb89.jpg', '', 852),
(944, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201662011-0e36246ebb.jpg', '', 853),
(945, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661868-fdfdfa9547.jpg', '', 854),
(946, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201662012-d340a52de3.jpg', '', 855),
(947, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201661886-1-c315cf3a9e.jpg', '', 856),
(948, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201662228-db80375ce7.jpg', '', 857),
(949, 'yii\\easyii\\modules\\catalog\\models\\Item', 66, 'catalog/201662027-e62c06cf30.jpg', '', 858),
(950, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888213-f49909ffe6.jpg', '', 859),
(951, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888320-5098647050.jpg', '', 860),
(952, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888325-96f83afc91.jpg', '', 861),
(953, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888176-f38a775816.jpg', '', 862),
(954, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888215-eb89ef2fc0.jpg', '', 863),
(955, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888091-cd67c38c57.jpg', '', 864),
(956, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888058-51da254c2f.jpg', '', 865),
(957, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888451-44592f6f6a.jpg', '', 866),
(958, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888477-f4cf083c50.jpg', '', 867),
(959, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888478-cf7f12be82.jpg', '', 868),
(960, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888279-94450472ba.jpg', '', 869),
(961, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888435-2e5e9fa823.jpg', '', 870),
(962, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888401-56e873e9f9.jpg', '', 871),
(963, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888599-fbc591a830.jpg', '', 872),
(964, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888499-c93ca6c447.jpg', '', 873),
(965, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888500-3a6bb5fc17.jpg', '', 874),
(966, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888601-4560a08c30.jpg', '', 875),
(967, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888600-35b8816c42.jpg', '', 876),
(968, 'yii\\easyii\\modules\\catalog\\models\\Item', 67, 'catalog/193888546-2ce502679e.jpg', '', 877),
(969, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083175-b9922c870a.jpg', '', 878),
(970, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083196-1-85d65578c9.jpg', '', 879),
(971, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083196-c46c0d3420.jpg', '', 880),
(972, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083194-68db9dbf7e.jpg', '', 881),
(973, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083195-3595a5d89a.jpg', '', 882),
(974, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083198-3d8a8150ea.jpg', '', 883),
(975, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083199-0fbda31ba0.jpg', '', 884),
(976, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083193-ed463413b2.jpg', '', 885),
(977, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083176-0578275961.jpg', '', 886),
(978, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083197-3a5042b43b.jpg', '', 887),
(979, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083200-bc3ee33ca3.jpg', '', 888),
(980, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083177-afbc0411b3.jpg', '', 889),
(981, 'yii\\easyii\\modules\\catalog\\models\\Item', 68, 'catalog/209083201-4e0b0cf056.jpg', '', 890),
(982, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238376-e601edf613.jpg', '', 891),
(983, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238724-f58657c96f.jpg', '', 892),
(984, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238693-8d784ae66c.jpg', '', 893),
(985, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238820-19ed5789a0.jpg', '', 894),
(986, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238406-c2aca51d27.jpg', '', 895),
(987, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238874-ba9b5fb4a2.jpg', '', 896),
(988, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238905-705bfbd571.jpg', '', 897),
(989, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238985-4458b79df9.jpg', '', 898),
(990, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177239099-80f7360fcc.jpg', '', 899),
(991, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177239265-e3ba9d2960.jpg', '', 900),
(992, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177239408-041884c53f.jpg', '', 901),
(993, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177239486-a813927b64.jpg', '', 902),
(994, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238498-7681cd8a35.jpg', '', 903),
(995, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238572-9eac80ea11.jpg', '', 904),
(996, 'yii\\easyii\\modules\\catalog\\models\\Item', 69, 'catalog/177238614-87ed99c97e.jpg', '', 905),
(997, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672505-f2af9de05e.jpg', '', 906),
(998, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672519-87b0e4020f.jpg', '', 907),
(999, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672506-3b74f054d1.jpg', '', 908),
(1000, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672518-24960b18d6.jpg', '', 909),
(1001, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672522-b161581ce9.jpg', '', 910),
(1002, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672523-8b674b4e47.jpg', '', 911),
(1003, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672510-05936002a2.jpg', '', 912),
(1004, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672507-dd6a60a16c.jpg', '', 913),
(1005, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672521-942f4f668e.jpg', '', 914),
(1006, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672520-b8a609350f.jpg', '', 915),
(1007, 'yii\\easyii\\modules\\catalog\\models\\Item', 70, 'catalog/218672509-fbea05d088.jpg', '', 916),
(1008, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458361-10b10156f0.jpg', '', 917),
(1009, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458378-420499efb0.jpg', '', 918),
(1010, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458363-2a8b6972cb.jpg', '', 919),
(1011, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458377-0b43d020b9.jpg', '', 920),
(1012, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458364-0e24b52da7.jpg', '', 921),
(1013, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458362-203b80f23c.jpg', '', 922),
(1014, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458376-e105bf7c65.jpg', '', 923),
(1015, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458546-ff2fb886b6.jpg', '', 924),
(1016, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458380-226cb9c125.jpg', '', 925),
(1017, 'yii\\easyii\\modules\\catalog\\models\\Item', 71, 'catalog/193458379-01dca8e25a.jpg', '', 926),
(1018, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100522-1209fa44c1.jpg', '', 927),
(1019, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100523-31d64ef16d.jpg', '', 928),
(1020, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100544-fd0026a12c.jpg', '', 929),
(1021, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100546-1-6d522f4d81.jpg', '', 930),
(1022, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100547-122c7c4ef7.jpg', '', 931),
(1023, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100548-f40204319b.jpg', '', 932),
(1024, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100525-cd8f659e6d.jpg', '', 933),
(1025, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100546-83dc026d90.jpg', '', 934),
(1026, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100545-54773d34b2.jpg', '', 935),
(1027, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100524-f8071cdb50.jpg', '', 936),
(1028, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100543-af38e1a03a.jpg', '', 937),
(1029, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100551-f85cd8bb21.jpg', '', 938),
(1030, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100549-c2b24855d7.jpg', '', 939),
(1031, 'yii\\easyii\\modules\\catalog\\models\\Item', 72, 'catalog/193100550-a3e16fd9a4.jpg', '', 940),
(1032, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593337-40d3e95414.jpg', '', 941),
(1033, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593349-ac7640198e.jpg', '', 942),
(1034, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593350-e946bce999.jpg', '', 943),
(1035, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593351-f686f84fc8.jpg', '', 944),
(1036, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593352-076feb9703.jpg', '', 945),
(1037, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593353-c1614d2df7.jpg', '', 946),
(1038, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593341-49f22b0490.jpg', '', 947),
(1039, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593340-d85e3d3058.jpg', '', 948),
(1040, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593343-065dd93be8.jpg', '', 949),
(1041, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593342-6b6447962d.jpg', '', 950),
(1042, 'yii\\easyii\\modules\\catalog\\models\\Item', 73, 'catalog/185593348-8ca9d11c93.jpg', '', 951),
(1043, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184757952-50c0501e94.jpg', '', 952),
(1044, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758450-4a84aed246.jpg', '', 953),
(1045, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758385-3591cc4a21.jpg', '', 954),
(1046, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758597-24ad15e31a.jpg', '', 955),
(1047, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758619-20886aaabb.jpg', '', 956),
(1048, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758196-de8815aaf5.jpg', '', 957),
(1049, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758620-003a783c81.jpg', '', 958),
(1050, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758278-bfe56758fb.jpg', '', 959),
(1051, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758621-855195fa92.jpg', '', 960),
(1052, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758308-a170bfb50b.jpg', '', 961);
INSERT INTO `easyii_photos` (`id`, `class`, `item_id`, `image_file`, `description`, `order_num`) VALUES
(1053, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758215-28a4b048fc.jpg', '', 962),
(1054, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758623-108535bacd.jpg', '', 963),
(1055, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758629-ad0e4eb1fa.jpg', '', 964),
(1056, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758628-e5eee40d26.jpg', '', 965),
(1057, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758454-ae1537332d.jpg', '', 966),
(1058, 'yii\\easyii\\modules\\catalog\\models\\Item', 74, 'catalog/184758622-0a0b8d036f.jpg', '', 967),
(1059, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297843-dbabaefadd.jpg', '', 968),
(1060, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297852-c32a489821.jpg', '', 969),
(1061, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297851-10648a7059.jpg', '', 970),
(1062, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297844-2bd2240c81.jpg', '', 971),
(1063, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297858-3f4ca3c361.jpg', '', 972),
(1064, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297853-039f1d2170.jpg', '', 973),
(1065, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297846-e9ef32771e.jpg', '', 974),
(1066, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297845-60d2d58d1c.jpg', '', 975),
(1067, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297847-2a7aec5fbd.jpg', '', 976),
(1068, 'yii\\easyii\\modules\\catalog\\models\\Item', 75, 'catalog/196297854-ddfb4e7317.jpg', '', 977),
(1069, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7392-6407c9d8f7.jpg', '', 978),
(1070, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7396-a1cc8ea754.jpg', '', 979),
(1071, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7393-6d3f6fbd74.jpg', '', 980),
(1072, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7397-44c56b8e52.jpg', '', 981),
(1073, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7391-bb23f94080.jpg', '', 982),
(1074, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7394-6bf576bce4.jpg', '', 983),
(1075, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7395-b6eb193aea.jpg', '', 984),
(1076, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7399-724d31dbbd.jpg', '', 985),
(1077, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/7398-5fb584336d.jpg', '', 986),
(1078, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73910-0da4240bac.jpg', '', 987),
(1079, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73914-1eb51a0feb.jpg', '', 988),
(1080, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73911-825afd5e9b.jpg', '', 989),
(1081, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73912-194134888c.jpg', '', 990),
(1082, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73917-541f371021.jpg', '', 991),
(1083, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73916-bb5fb2ce23.jpg', '', 992),
(1084, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73924-1d0b52e5e7.jpg', '', 993),
(1085, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73919-3510b2328f.jpg', '', 994),
(1086, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73925-5387e191ef.jpg', '', 995),
(1087, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73918-e79a7d755a.jpg', '', 996),
(1088, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73923-ea74027fa3.jpg', '', 997),
(1089, 'yii\\easyii\\modules\\catalog\\models\\Item', 76, 'catalog/73922-e68abd0ba1.jpg', '', 998),
(1090, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7791-be72074525.jpg', '', 999),
(1091, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7793-c14e653125.jpg', '', 1000),
(1092, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7795-bf95dbcbc0.jpg', '', 1001),
(1093, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7798-8d35b67487.jpg', '', 1002),
(1094, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7792-e3090c410c.jpg', '', 1003),
(1095, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7796-83643f3e65.jpg', '', 1004),
(1096, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7799-cc7420b8eb.jpg', '', 1005),
(1097, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7794-1fba5020cd.jpg', '', 1006),
(1098, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/77911-b5fab8990e.jpg', '', 1007),
(1099, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/77914-5630a2494b.jpg', '', 1008),
(1100, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/77910-eec39f5ae9.jpg', '', 1009),
(1101, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/7797-d7aee1aee8.jpg', '', 1010),
(1102, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/77913-08bafb8c80.jpg', '', 1011),
(1103, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/77915-da30f56680.jpg', '', 1012),
(1104, 'yii\\easyii\\modules\\catalog\\models\\Item', 77, 'catalog/77912-c99fc82a9e.jpg', '', 1013),
(1105, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76811001x671-3b5633ba1b.jpg', '', 1014),
(1106, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76871001x667-be05ec6721.jpg', '', 1015),
(1107, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76881001x667-3cf0f6bc22.jpg', '', 1016),
(1108, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76831001x501-03a9e75726.jpg', '', 1017),
(1109, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/768101001x667-058085f621.jpg', '', 1018),
(1110, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76841001x501-b6781bb7d5.jpg', '', 1019),
(1111, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76861001x667-dbb11fedf2.jpg', '', 1020),
(1112, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76851001x501-af1fcaa423.jpg', '', 1021),
(1113, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76821001x601-2c7c2fe40a.jpg', '', 1022),
(1114, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76811-9084c693c5.jpg', '', 1023),
(1115, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76891001x667-ce660691f5.jpg', '', 1024),
(1116, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76815-d606c521f0.jpg', '', 1025),
(1117, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76813-252fca7a25.jpg', '', 1026),
(1118, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76816-20a2eda910.jpg', '', 1027),
(1119, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76812-735ab90307.jpg', '', 1028),
(1120, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76817-708ef12906.jpg', '', 1029),
(1121, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76818-381f8ebaff.jpg', '', 1030),
(1122, 'yii\\easyii\\modules\\catalog\\models\\Item', 78, 'catalog/76814-ec83396dea.jpg', '', 1031),
(1123, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7911-e28719f06f.jpg', '', 1032),
(1124, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7912-e31515cef8.jpg', '', 1033),
(1125, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7916-24ea3848a7.jpg', '', 1034),
(1126, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7915-710f6a65dd.jpg', '', 1035),
(1127, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7917-fd9a96a095.jpg', '', 1036),
(1128, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7913-5222e84fa4.jpg', '', 1037),
(1129, 'yii\\easyii\\modules\\catalog\\models\\Item', 79, 'catalog/7914-9267d1b79c.jpg', '', 1038),
(1130, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7401-37ad5621be.jpg', '', 1039),
(1131, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7407-0501dd4aeb.jpg', '', 1040),
(1132, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7405-0d38dc8142.jpg', '', 1041),
(1133, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7402-220b16da94.jpg', '', 1042),
(1134, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74010-765bb158c2.jpg', '', 1043),
(1135, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7409-7837ab7bef.jpg', '', 1044),
(1136, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74011-456922e958.jpg', '', 1045),
(1137, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7406-af85cc406c.jpg', '', 1046),
(1138, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7403-5c7950bfb2.jpg', '', 1047),
(1139, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74012-3a5ee081f9.jpg', '', 1048),
(1140, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/7404-f39b97c786.jpg', '', 1049),
(1141, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74015-39fe6ed49b.jpg', '', 1050),
(1142, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74014-e2d52aea09.jpg', '', 1051),
(1143, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74013-8957fb2646.jpg', '', 1052),
(1144, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74017-e94f5e09df.jpg', '', 1053),
(1145, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74021-021673a021.jpg', '', 1054),
(1146, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74020-f02679f25f.jpg', '', 1055),
(1147, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74018-17fd398446.jpg', '', 1056),
(1148, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74019-255636b456.jpg', '', 1057),
(1149, 'yii\\easyii\\modules\\catalog\\models\\Item', 80, 'catalog/74016-b2e6a6dad8.jpg', '', 1058),
(1150, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7931-0671d61deb.jpg', '', 1059),
(1151, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7932-f01328928d.jpg', '', 1060),
(1152, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7937-338a08d590.jpg', '', 1061),
(1153, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7936-f35ed40a81.jpg', '', 1062),
(1154, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7934-4317d76507.jpg', '', 1063),
(1155, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7935-51cb14f98b.jpg', '', 1064),
(1156, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7933-ee7e2271a0.jpg', '', 1065),
(1157, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7938-faeedaae50.jpg', '', 1066),
(1158, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/7939-ca46bfb514.jpg', '', 1067),
(1159, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/79310-af52cc402d.jpg', '', 1068),
(1160, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/79321-4d69babcc0.jpg', '', 1069),
(1161, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/79311-f466f3eb31.jpg', '', 1070),
(1162, 'yii\\easyii\\modules\\catalog\\models\\Item', 81, 'catalog/793112-d398f3879e.jpg', '', 1071),
(1163, 'yii\\easyii\\modules\\catalog\\models\\Item', 82, 'catalog/image00001-f00e405fbc.jpg', '', 1072),
(1164, 'yii\\easyii\\modules\\catalog\\models\\Item', 82, 'catalog/image00003-1086ec3330.jpg', '', 1073),
(1165, 'yii\\easyii\\modules\\catalog\\models\\Item', 82, 'catalog/image00005-9965ac78b8.jpg', '', 1074),
(1166, 'yii\\easyii\\modules\\catalog\\models\\Item', 82, 'catalog/image00002-65de1718ea.jpg', '', 1075),
(1167, 'yii\\easyii\\modules\\catalog\\models\\Item', 82, 'catalog/image00004-00e7e3b45a.jpg', '', 1076),
(1168, 'yii\\easyii\\modules\\catalog\\models\\Item', 82, 'catalog/image00006-d6abf2f590.jpg', '', 1077),
(1169, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7921-f85a1a2d93.jpg', '', 1078),
(1170, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7927-f8ef049e94.jpg', '', 1079),
(1171, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7923-15c52a0ac5.jpg', '', 1080),
(1172, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7928-d15acf4ad2.jpg', '', 1081),
(1173, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7929-1fcc36a2d9.jpg', '', 1082),
(1174, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7922-db2cb230aa.jpg', '', 1083),
(1175, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7926-ceb1cb5acf.jpg', '', 1084),
(1176, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7925-7b4ba9742d.jpg', '', 1085),
(1177, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/79211-6d07d61e0a.jpg', '', 1086),
(1178, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/7924-3145113041.jpg', '', 1087),
(1179, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/79210-e3e95da12c.jpg', '', 1088),
(1180, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/79212-a8a258528b.jpg', '', 1089),
(1181, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/79216-9f30b7255f.jpg', '', 1090),
(1182, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/79213-b84624bf31.jpg', '', 1091),
(1183, 'yii\\easyii\\modules\\catalog\\models\\Item', 83, 'catalog/79214-246496de16.jpg', '', 1092),
(1184, 'yii\\easyii\\modules\\catalog\\models\\Item', 84, 'catalog/7861-633003b741.jpg', '', 1093),
(1185, 'yii\\easyii\\modules\\catalog\\models\\Item', 84, 'catalog/7863-95bb935ddc.jpg', '', 1094),
(1186, 'yii\\easyii\\modules\\catalog\\models\\Item', 84, 'catalog/7862-148d14167e.jpg', '', 1095),
(1187, 'yii\\easyii\\modules\\catalog\\models\\Item', 84, 'catalog/7864-e144737618.jpg', '', 1096),
(1188, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7951-b8123d3c7c.jpg', '', 1097),
(1189, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7957-396d6ee5bb.jpg', '', 1098),
(1190, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7952-2560cbcbb9.jpg', '', 1099),
(1191, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7954-52391512c8.jpg', '', 1100),
(1192, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7955-9f0b8f53c2.jpg', '', 1101),
(1193, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7953-9ffb2885b2.jpg', '', 1102),
(1194, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7958-88b8a06f66.jpg', '', 1103),
(1195, 'yii\\easyii\\modules\\catalog\\models\\Item', 85, 'catalog/7956-318daa8445.jpg', '', 1104),
(1196, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/788-0d1a822c30.jpg', '', 1105),
(1197, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7885-b92d599aaa.jpg', '', 1106),
(1198, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7880-8014e7f700.jpg', '', 1107),
(1199, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7881-8cb1833e1d.jpg', '', 1108),
(1200, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7889-28984d70b7.jpg', '', 1109),
(1201, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7886-804e58a560.jpg', '', 1110),
(1202, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7882-952f28922d.jpg', '', 1111),
(1203, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7883-5169d55b22.jpg', '', 1112),
(1204, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78814-0c5a340886.jpg', '', 1113),
(1205, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78811-3f6a15fb1c.jpg', '', 1114),
(1206, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/7884-d4b66dac24.jpg', '', 1115),
(1207, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78818-287509af2d.jpg', '', 1116),
(1208, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78810-92277e3e11.jpg', '', 1117),
(1209, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78819-4d736a6507.jpg', '', 1118),
(1210, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78816-f8e0eac886.jpg', '', 1119),
(1211, 'yii\\easyii\\modules\\catalog\\models\\Item', 86, 'catalog/78815-ca784c9f22.jpg', '', 1120),
(1246, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/6341-17820d9214.jpg', '', 1154),
(1231, 'yii\\easyii\\modules\\article\\models\\Item', 5, 'article/img0767-e96fe22cf1.jpg', '', 1139),
(1232, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/6171-485d941830.jpg', '', 1140),
(1233, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/6175-40f4d9f1e7.jpg', '', 1141),
(1234, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/61712-a95520a86e.jpg', '', 1142),
(1235, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/6173-869db81305.jpg', '', 1143),
(1236, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/6177-5429d23cb6.jpg', '', 1144),
(1237, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/6179-dd01fb1b4e.jpg', '', 1145),
(1238, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/6174-0f8132d6c8.jpg', '', 1146),
(1239, 'yii\\easyii\\modules\\catalog\\models\\Item', 87, 'catalog/61711-58fd697d16.jpg', '', 1147),
(1240, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7714-71811b118f.jpg', '', 1148),
(1241, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7712-a8ae0902a5.jpg', '', 1149),
(1242, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7716-71d03c820a.jpg', '', 1150),
(1243, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7717-db1b8985d0.jpg', '', 1151),
(1244, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7718-92b51458a3.jpg', '', 1152),
(1229, 'yii\\easyii\\modules\\article\\models\\Item', 5, 'article/img0556-a7c70eb022.jpg', '', 1138),
(1245, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/6342-cf31f05a27.jpg', '', 1153),
(1247, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7715-ed9f479fde.jpg', '', 1155),
(1248, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7711-3b987b3ab4.jpg', '', 1156),
(1249, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7713-89fd1901c8.jpg', '', 1157),
(1250, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/7719-b37c51fa10.jpg', '', 1158),
(1251, 'yii\\easyii\\modules\\catalog\\models\\Item', 88, 'catalog/77110-8c9ffff745.jpg', '', 1159),
(1252, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6342-7de388943e.jpg', '', 1160),
(1253, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6341-80d014efa8.jpg', '', 1161),
(1254, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6348-9d51c77c84.jpg', '', 1162),
(1255, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6343-7a60fd55f8.jpg', '', 1163),
(1256, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/63410-82f0d53a53.jpg', '', 1164),
(1257, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6345-506208692b.jpg', '', 1165),
(1258, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6346-103d710993.jpg', '', 1166),
(1259, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6344-d2d64fff3f.jpg', '', 1167),
(1260, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6347-099481e924.jpg', '', 1168),
(1261, 'yii\\easyii\\modules\\catalog\\models\\Item', 89, 'catalog/6349-fff9067e9d.jpg', '', 1169),
(1262, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7693-5aca58bc69.jpg', '', 1170),
(1263, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7694-36681c7227.jpg', '', 1171),
(1264, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7697-9c99e86995.jpg', '', 1172),
(1265, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7699-afd6d28bce.jpg', '', 1173),
(1266, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/76910-f8cca808a8.jpg', '', 1174),
(1267, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7691-b76d0aa30b.jpg', '', 1175),
(1268, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7695-8d62cf6269.jpg', '', 1176),
(1269, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7692-3bb09b99d8.jpg', '', 1177),
(1270, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/76912-181a2d42e4.jpg', '', 1178),
(1271, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7696-005b0e94f7.jpg', '', 1179),
(1272, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/76913-8261ad611b.jpg', '', 1180),
(1273, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/7698-e8da83c812.jpg', '', 1181),
(1274, 'yii\\easyii\\modules\\catalog\\models\\Item', 90, 'catalog/76911-d48ee7b59c.jpg', '', 1182),
(1275, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7971-5e06f11dd3.jpg', '', 1183),
(1276, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7973-fd79e3df94.jpg', '', 1184),
(1277, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7976-93d6c12624.jpg', '', 1185),
(1278, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7979-0adcc48cb1.jpg', '', 1186),
(1279, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/79710-49fdd4f4bd.jpg', '', 1187),
(1280, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/79711-d928da32df.jpg', '', 1188),
(1281, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7977-fd91307d82.jpg', '', 1189),
(1282, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7974-c831645d6b.jpg', '', 1190),
(1283, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7972-60824d252e.jpg', '', 1191),
(1284, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/79714-48292949aa.jpg', '', 1192),
(1285, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7975-8129760296.jpg', '', 1193),
(1286, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/7978-c4ebb11a30.jpg', '', 1194),
(1287, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/79713-b7b5b4fbe8.jpg', '', 1195),
(1288, 'yii\\easyii\\modules\\catalog\\models\\Item', 91, 'catalog/79712-56c36a9ee0.jpg', '', 1196),
(1289, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7966-7319c1f0b1.jpg', '', 1197),
(1290, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7963-c73a0c85a0.jpg', '', 1198),
(1291, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7968-0ccf592692.jpg', '', 1199),
(1292, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7969-5db1227f6e.jpg', '', 1200),
(1293, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/79610-28d1a99627.jpg', '', 1201),
(1294, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7961-6cc5ffad16.jpg', '', 1202),
(1295, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7967-d1ec5eb0e9.jpg', '', 1203),
(1296, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7964-3f62c41ffa.jpg', '', 1204),
(1297, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/79613-61c1fc9c62.jpg', '', 1205),
(1298, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7965-4d05943335.jpg', '', 1206),
(1299, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/79614-6a94df9a70.jpg', '', 1207),
(1300, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/7962-4cef2da30a.jpg', '', 1208),
(1301, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/79611-f651add1d8.jpg', '', 1209),
(1302, 'yii\\easyii\\modules\\catalog\\models\\Item', 92, 'catalog/79612-bfa056f902.jpg', '', 1210),
(1303, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/51111-2d632bd917.jpg', '', 1211),
(1304, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/51113-8a7991e898.jpg', '', 1212),
(1305, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5122-749554a624.jpg', '', 1213),
(1306, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5123-86b7d6ae8d.jpg', '', 1214),
(1307, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/51115-b4c917733f.jpg', '', 1215),
(1308, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/51112-48759cc3d7.jpg', '', 1216),
(1309, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5121-06173099f2.jpg', '', 1217),
(1310, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5124-f046dbe9e9.jpg', '', 1218),
(1311, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5127-18e2935fbc.jpg', '', 1219),
(1312, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5128-6387188f77.jpg', '', 1220),
(1313, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5125-37a701c92e.jpg', '', 1221),
(1314, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5129-0b69d168b8.jpg', '', 1222),
(1315, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/51114-c8f4974ac7.jpg', '', 1223),
(1316, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/5126-2bf250e0c7.jpg', '', 1224),
(1317, 'yii\\easyii\\modules\\catalog\\models\\Item', 93, 'catalog/51210-58f4d1c892.jpg', '', 1225),
(1318, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7576-c448a0301b.jpg', '', 1226),
(1319, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7577-263ff2d711.jpg', '', 1227),
(1320, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7578-1f53b999bd.jpg', '', 1228),
(1321, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7579-fe767025ec.jpg', '', 1229),
(1322, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75710-6dc52f3203.jpg', '', 1230),
(1323, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7571-181d41c1e6.jpg', '', 1231),
(1324, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75713-1acde3a431.jpg', '', 1232),
(1325, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7572-d342f70093.jpg', '', 1233),
(1326, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7573-345bbfd0b5.jpg', '', 1234),
(1327, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75714-3627e7da07.jpg', '', 1235),
(1328, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75715-1f79855c13.jpg', '', 1236),
(1329, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75716-038a9a2da6.jpg', '', 1237),
(1330, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7574-8a79960d08.jpg', '', 1238),
(1331, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75718-0372d5c8a6.jpg', '', 1239),
(1332, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/7575-74ba7d17d4.jpg', '', 1240),
(1333, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75717-1e1f004e9e.jpg', '', 1241),
(1334, 'yii\\easyii\\modules\\catalog\\models\\Item', 94, 'catalog/75711-cc03c47b78.jpg', '', 1242),
(1335, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6073-0da5091c13.jpg', '', 1243),
(1336, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6075-1d988550a1.jpg', '', 1244),
(1337, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6072-840e5224eb.jpg', '', 1245),
(1338, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6074-40db6423f8.jpg', '', 1246),
(1339, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6076-c8d562fc8a.jpg', '', 1247),
(1340, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6071-a48881c9f8.jpg', '', 1248),
(1341, 'yii\\easyii\\modules\\catalog\\models\\Item', 95, 'catalog/6077-c73191752f.jpg', '', 1249);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_seotext`
--

CREATE TABLE `easyii_seotext` (
  `id` int(11) NOT NULL,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_settings`
--

CREATE TABLE `easyii_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_settings`
--

INSERT INTO `easyii_settings` (`id`, `name`, `title`, `value`, `visibility`) VALUES
(1, 'easyii_version', 'EasyiiCMS version', '0.91', 0),
(2, 'image_max_width', 'Максимальная ширина загружаемых изображений, которые автоматически не сжимаются', '1900', 2),
(3, 'redactor_plugins', 'Список плагинов редактора Redactor через запятую', 'imagemanager, filemanager, table, fullscreen', 1),
(4, 'ga_service_email', 'E-mail сервис аккаунта Google Analytics', '', 1),
(5, 'ga_profile_id', 'Номер профиля Google Analytics', '', 1),
(6, 'ga_p12_file', 'Путь к файлу ключей p12 сервис аккаунта Google Analytics', '', 1),
(7, 'gm_api_key', 'Google Maps API ключ', 'AIzaSyD3TNxUzBswDgaODRYXWsHdWgfwe-i3Vm0', 1),
(8, 'recaptcha_key', 'ReCaptcha key', '', 1),
(9, 'password_salt', 'Password salt', 'YChImDaIIgdjQqld9hTXNgBhDO-pMK1n', 0),
(10, 'root_auth_key', 'Root authorization key', '6z5pdzZnVNvnphkpF8gpq2ulwXhEy6M4', 0),
(11, 'root_password', 'Пароль разработчика', 'd4b4c0614805cfd1183f08c753dd1080a7ce3692', 1),
(12, 'auth_time', 'Время авторизации', '86400', 1),
(13, 'robot_email', 'E-mail рассыльщика', 'noreply@loft.local', 1),
(14, 'admin_email', 'E-mail администратора', 'b059ae@gmail.com', 2),
(15, 'telegram_chat_id', 'Telegram chat ID', '', 2),
(16, 'telegram_bot_token', 'Telegram bot token', '', 1),
(17, 'recaptcha_secret', 'ReCaptcha secret', '', 1),
(18, 'toolbar_position', 'Позиция панели на сайте ("top" or "bottom" or "hide")', 'hide', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_shopcart_goods`
--

CREATE TABLE `easyii_shopcart_goods` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_shopcart_orders`
--

CREATE TABLE `easyii_shopcart_orders` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `access_token` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_subscribe_history`
--

CREATE TABLE `easyii_subscribe_history` (
  `id` int(11) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `body` text,
  `sent` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_subscribe_subscribers`
--

CREATE TABLE `easyii_subscribe_subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_tags`
--

CREATE TABLE `easyii_tags` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_tags_assign`
--

CREATE TABLE `easyii_tags_assign` (
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_texts`
--

CREATE TABLE `easyii_texts` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_texts`
--

INSERT INTO `easyii_texts` (`id`, `text`, `slug`) VALUES
(1, 'г. Ростов-на-Дону, ул. Ленина', 'address'),
(2, '+7 (800) 900 20 23', 'phone'),
(3, 'info@spain.ru', 'mail'),
(4, 'https://www.facebook.com/profile.php?id=spain', 'facebook'),
(5, 'https://vk.com/spain', 'vkontakte'),
(6, 'https://www.instagram.com/spain/', 'instagram'),
(8, 'ALICANTE DESCANSO', 'sitename');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `easyii_admins`
--
ALTER TABLE `easyii_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token` (`access_token`);

--
-- Индексы таблицы `easyii_article_categories`
--
ALTER TABLE `easyii_article_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_article_items`
--
ALTER TABLE `easyii_article_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_carousel`
--
ALTER TABLE `easyii_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_catalog_categories`
--
ALTER TABLE `easyii_catalog_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_catalog_items`
--
ALTER TABLE `easyii_catalog_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_catalog_item_data`
--
ALTER TABLE `easyii_catalog_item_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id_name` (`item_id`,`name`),
  ADD KEY `value` (`value`(300));

--
-- Индексы таблицы `easyii_entity_categories`
--
ALTER TABLE `easyii_entity_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_entity_items`
--
ALTER TABLE `easyii_entity_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_faq`
--
ALTER TABLE `easyii_faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_feedback`
--
ALTER TABLE `easyii_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_files`
--
ALTER TABLE `easyii_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_gallery_categories`
--
ALTER TABLE `easyii_gallery_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_guestbook`
--
ALTER TABLE `easyii_guestbook`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_loginform`
--
ALTER TABLE `easyii_loginform`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_menu`
--
ALTER TABLE `easyii_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Индексы таблицы `easyii_migration`
--
ALTER TABLE `easyii_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `easyii_modules`
--
ALTER TABLE `easyii_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_news`
--
ALTER TABLE `easyii_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_pages`
--
ALTER TABLE `easyii_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_photos`
--
ALTER TABLE `easyii_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_item` (`class`,`item_id`);

--
-- Индексы таблицы `easyii_seotext`
--
ALTER TABLE `easyii_seotext`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `model_item` (`class`,`item_id`);

--
-- Индексы таблицы `easyii_settings`
--
ALTER TABLE `easyii_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_shopcart_goods`
--
ALTER TABLE `easyii_shopcart_goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_shopcart_orders`
--
ALTER TABLE `easyii_shopcart_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_subscribe_history`
--
ALTER TABLE `easyii_subscribe_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_subscribe_subscribers`
--
ALTER TABLE `easyii_subscribe_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `easyii_tags`
--
ALTER TABLE `easyii_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_tags_assign`
--
ALTER TABLE `easyii_tags_assign`
  ADD KEY `class` (`class`),
  ADD KEY `item_tag` (`item_id`,`tag_id`);

--
-- Индексы таблицы `easyii_texts`
--
ALTER TABLE `easyii_texts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `easyii_admins`
--
ALTER TABLE `easyii_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_article_categories`
--
ALTER TABLE `easyii_article_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `easyii_article_items`
--
ALTER TABLE `easyii_article_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `easyii_carousel`
--
ALTER TABLE `easyii_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_categories`
--
ALTER TABLE `easyii_catalog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_items`
--
ALTER TABLE `easyii_catalog_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_item_data`
--
ALTER TABLE `easyii_catalog_item_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1706;
--
-- AUTO_INCREMENT для таблицы `easyii_entity_categories`
--
ALTER TABLE `easyii_entity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_entity_items`
--
ALTER TABLE `easyii_entity_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_faq`
--
ALTER TABLE `easyii_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_feedback`
--
ALTER TABLE `easyii_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_files`
--
ALTER TABLE `easyii_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_gallery_categories`
--
ALTER TABLE `easyii_gallery_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_guestbook`
--
ALTER TABLE `easyii_guestbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_loginform`
--
ALTER TABLE `easyii_loginform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT для таблицы `easyii_menu`
--
ALTER TABLE `easyii_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `easyii_modules`
--
ALTER TABLE `easyii_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `easyii_news`
--
ALTER TABLE `easyii_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `easyii_pages`
--
ALTER TABLE `easyii_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_photos`
--
ALTER TABLE `easyii_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1342;
--
-- AUTO_INCREMENT для таблицы `easyii_seotext`
--
ALTER TABLE `easyii_seotext`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_settings`
--
ALTER TABLE `easyii_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `easyii_shopcart_goods`
--
ALTER TABLE `easyii_shopcart_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_shopcart_orders`
--
ALTER TABLE `easyii_shopcart_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_subscribe_history`
--
ALTER TABLE `easyii_subscribe_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_subscribe_subscribers`
--
ALTER TABLE `easyii_subscribe_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_tags`
--
ALTER TABLE `easyii_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_texts`
--
ALTER TABLE `easyii_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;